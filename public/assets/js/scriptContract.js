$(function () {

    switchIsHere();
    deleteContractAdherent();

});
/* fin ready */

/* switch isHere */
function switchIsHere() {
    $('.switchHere').unbind().on('change', function () {
        let params = {};
        params.id = $(this).closest("tr").attr('data-id');
        switchIsHereActionBdd($(this), params, (!!this.checked));
    });
}

function switchIsHereActionBdd(position, params, active = true) {
    params.action = active;

    fetch(Routing.generate('app_adherentevent_switch', params), {
        method: "GET",
        headers: {"Authorization": "No Auth"},
        // body: params
    }).then(function (response) {
        return response.json();
    }).then(function (data) {

        if (data.message === 'ok') {
            toastr.options.progressBar = true;
            toastr.options = {
                positionClass: 'toast-top-right',
                hideDuration: 300,
                timeOut: 3000
            };

            toastr.success('Participant '
                + (params.action ? 'activé' : 'désactivé')).css({
                "width": "auto",
                "background-color": "#B22222"
            });
        }
    }).catch((error) => {
        console.log(error);
    });
}

function deleteContractAdherent() {
    $('.deleteAdherent').unbind().on('click', function (e) {
        e.preventDefault();

        let idContract = $(this).closest('table').data('contract');
        let idAdherent = $(this).closest('tr').data('id');
        fetch(Routing.generate('app_contract_adherent_delete', {id: idContract,adherent:idAdherent}), {
            method: "GET",
            headers: {"Authorization": "No Auth"},
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            if (data.message === 'ok') {
                toastr.options.progressBar = true;
                toastr.options = {
                    positionClass: 'toast-top-right',
                    hideDuration: 300,
                    timeOut: 3000
                };

                toastr.success('Participant supprimé').css({
                    "width": "auto",
                    "background-color": "#B22222"
                });

                //TODO:récupérer l'url du back via crudUrlGenerator !!!
                window.location.href = 'http://127.0.0.1:8000/fr/admin?crudAction=detail&crudId=ec35513&entityId=32&menuIndex=17&submenuIndex=-1';
            }
        }).catch((error) => {
            console.log(error);
        });
    });
}
