$(function(){
    /* copier/coller email */
    jQuery('.btn-copy').on('click', function () {
        let element = jQuery(this).closest('td')[0];

        let selection = window.getSelection();
        let range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);

        try {
            let successful = document.execCommand('copy');
            if (successful) {
                console.log("Copied:" + selection);
            }
            else {
                console.log("Unable to copy!");
            }
        } catch (err) {
            console.log(err);
        }
    });

    /* switch isPdf */
    function switchIsHere() {
        $('.switchPdf').unbind().on('change', function () {
            let params = {};
            params.id = $(this).closest("tr").attr('data-id');
            switchParameterIsHere($(this), params, (!!this.checked));
        });
    }

    function switchParameterIsHere(position, params, active = true) {
        params.action = active;

        console.log(params);
        // console.log(params.action);

        fetch(Routing.generate('app_adherentevent_switch', params), {
            method: "POST",
            headers: {"Authorization": "No Auth"}
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            if (data.message === 'ok') {
                toastr.options.progressBar = true;
                toastr.options = {
                    positionClass: 'toast-top-right',
                    hideDuration: 300,
                    timeOut: 3000
                };

                toastr.success('Export PDF du paramètre '
                    + (params.action ? 'activé' : 'désactivé')).css({
                    "width": "auto",
                    "background-color": "#B22222"
                });
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    // sample graph 1
    var ctx = document.getElementById('myChartBar').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    // sample graph 2
    var ctxPie = document.getElementById('myChartPie').getContext('2d');
    var myPieChart = new Chart(ctxPie, {
        type: 'pie',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    // sample graph Line
    var ctxLine = document.getElementById('myChartLine').getContext('2d');
    var stackedLine = new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });

});