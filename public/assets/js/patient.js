$(function () {

    addPatientEvent();
    selectCheckboxes();
    selectAllCheckboxes();
    editPatientEvent();
    deletePatientEvent();
    sendPatientInvoice();
});

/* checkboxes */
let ids = [];

function selectCheckboxes() {
    $('table#patientEvents tbody input[type="checkbox"]').click(function () {
        let countcheck = $('table#patientEvents input[type="checkbox"]:checked').length;
        let idpatient = $(this).parent().parent().parent().data('patient');

        let id = $(this).parent().parent().attr('data-id');

        if (this.checked) {
            ids.push(id);
        } else {
            const index = ids.indexOf(id);
            if (index > -1) {
                ids.splice(index, 1);
            }
        }

        let strIds = '';
        ids.forEach(function (id) {
            strIds += id + '_';
        });
        console.log(strIds);

        let btnAddInvoice = $('#addInvoice');
        if (countcheck > 0) {
            btnAddInvoice.show();
            if (countcheck === 1) {
                $('#countItems').html(countcheck + ' séance');
            } else {
                $('#countItems').html(countcheck + ' séances');
            }

            strIds = strIds.substring(0, strIds.length - 1);
            btnAddInvoice.attr('data-ids', strIds);
            addPatientInvoice(idpatient);

        } else {
            btnAddInvoice.hide();
        }
        return strIds;
    });
}

/* select all checkboxes */
function selectAllCheckboxes() {
    $('#allSelected').click(function () {
        if ($(this).is(":checked")) {
            $('#btnGenerateInvoicePdf').show();
            $('#countItems').html(' tous les éléments');
        } else {
            $('#btnGenerateInvoicePdf').hide();
        }
    });
}

/* création invoice in bdd */
function addPatientInvoice(idpatient) {
    let btnAddInvoice = $('#addInvoice');
    btnAddInvoice.unbind('click');
    btnAddInvoice.on('click', function (e) {
        e.preventDefault();

        let eventIds = btnAddInvoice.data('ids');
        fetch(Routing.generate('patient_events_invoice_add', {id: idpatient, eventIds: eventIds}), {
            method: "GET",
            headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
        }).then((resp) => resp.json()
        ).then(function (data) {
            $url = Routing.generate('patient_events_invoice_generatepdf', {id: data.invoiceId});

            //ajout LI à la liste des factures
            $('ul#invoicesList').append('<li><a href="' + $url + '" >' + data.filename + ' créée le jj/mm/aaaax</a></li>')

        }).catch((error) => {
            console.log(error);
        });
    });
}

//TABS EVENTS
//Récupération du formulaire en modal
function addPatientEvent() {
    $('.addPatientEvent').on('click', function () {
        let idPatient = $(this).data('idpatient');

        fetch(Routing.generate('app_patient_add_event', {patient: idPatient}), {
            method: "GET",
            headers: {"Authorization": "No Auth"}
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            if (data.formHtml) {
                let modalTitle = 'Nouveau rendez-vous';
                //rendu modal
                $('#adminModal .modal-title').html(modalTitle);
                $('#adminModal .modal-body').html(data.formHtml);
                $('#adminModal').modal('show');
            }

            submitFormNewPatient();
        }).catch((error) => {
            console.log(error);
        });
    });
}

//Soumission du formulaire de création d'un rdv patient
function submitFormNewPatient() {
    $('.submitForm').on('click', function (e) {
        e.preventDefault();

        let idPatient = $('.addPatientEvent').data('idpatient');
        let data = new FormData(document.getElementById('formEventPatient'));

        $('.loaderMiniBtnSave').show();

        fetch(Routing.generate('app_patient_add_event', {patient: idPatient}), {
            method: "POST",
            headers: {"Authorization": "No Auth"},
            body: data
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            // //retour erreur enregistrement manager php
            if (data.errorSys !== undefined) {
                console.log(data.errorSys);

                toastr.options = {positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.warning(data.errorSys).css({"width": "auto"});
            }

            //si aucun intervenant/perso créé
            if (data.errorPerson !== undefined) {
                toastr.options = {positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.warning(data.errorPerson).css({"width": "auto"});
            }

            //retour erreurs de validation formulaire
            if (data.formHtml) {
                $('.formZone').html(data.formHtml);
                cancelForm('.formZone');
                submitForm();
            }

            //ok save
            if (data.errorSys === undefined && data.formHtml === undefined) {
                $('.loaderMiniBtnSave').hide();
                $('#adminModal').modal('hide');

                //MAJ table des séances
                $('.patientBodyEvents').html(data.tableHtml);

                editPatientEvent();
                deletePatientEvent();
            }
        }).catch((error) => {
            console.log(error);
        });
    });
}

/* edit patient event */
function editPatientEvent() {
    $('.editPatientEvent').on('click', function (e) {
        e.preventDefault();

        let id = $(this).closest('tr').data('id');

        alert(id);
        fetch(Routing.generate('app_event_edit', {id: id}), {
            method: "GET",
            headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
        }).then((resp) => resp.json()
        ).then(function (data) {
            console.log(data);

        }).catch((error) => {
            console.log(error);
        });
    });
}

/* delete patient event */
function deletePatientEvent() {
    $('.deletePatientEvent').on('click', function (e) {
        e.preventDefault();

        if (confirm('Confirmez-vous la suppression de la séance  ?')) {
            let id = $(this).closest('tr').data('id');
            fetch(Routing.generate('app_event_delete', {id: id}), {
                method: "DELETE",
                headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
            }).then((resp) => resp.json()
            ).then(function (data) {

                toastr.options.progressBar = true;
                toastr.options = {
                    positionClass: 'toast-top-right',
                    hideDuration: 300,
                    timeOut: 3000
                };

                toastr.success(data.message).css({
                    "width": "auto",
                    "background-color": "#B22222"
                });

                //MAJ table
                $('.patientBodyEvents').html(data.tableHtml);

                editPatientEvent();
                deletePatientEvent();
            }).catch((error) => {
                console.log(error);
            });
        }

    });
}

/* send patient invoice */
function sendPatientInvoice() {
    $('.sendPatientInvoice').on('click', function (e) {
        e.preventDefault();

        // Swal.fire({
        //     title: '<strong>HTML <u>example</u></strong>',
        //     icon: 'info',
        //     html:
        //     'You can use <b>bold text</b>, ' +
        //     '<a href="//sweetalert2.github.io">links</a> ' +
        //     'and other HTML tags',
        //     showCloseButton: true,
        //     showCancelButton: true,
        //     focusConfirm: false,
        //     confirmButtonText:
        //         '<i class="fa fa-thumbs-up"></i> Great!',
        //     confirmButtonAriaLabel: 'Thumbs up, great!',
        //     cancelButtonText:
        //         '<i class="fa fa-thumbs-down"></i>',
        //     cancelButtonAriaLabel: 'Thumbs down'
        // })
        let that = $(this);
        Swal.fire({
            title: 'Confirmez-vous l\'envoi de la facture au patient ?',
            html:
            '<input type="checkbox" name="bcc" id="bcc" title="Copie de contrôle d\'envoi"> Copie administrateur',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Confirmer`,
            denyButtonText: `Annuler`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                let bcc = false;
                if ($('input[name=bcc]').is(':checked')) {
                    bcc = true;
                }

                let invoiceId = $(that).closest('tr').data('id');
                console.log(invoiceId);
                $('.loaderMiniBtnSave_' + invoiceId).show();
                fetch(Routing.generate('patient_events_invoice_send', {id: invoiceId, bcc: bcc}), {
                    method: "GET",
                    headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
                }).then((resp) => resp.json()
                ).then(function (data) {

                    toastr.options.progressBar = true;
                    toastr.options = {
                        positionClass: 'toast-top-right',
                        hideDuration: 300,
                        timeOut: 3000
                    };

                    toastr.success(data.message).css({
                        "width": "auto",
                        "background-color": "#B22222"
                    });

                    $('.loaderMiniBtnSave_' + invoiceId).hide();
                }).catch((error) => {
                    console.log(error);
                });


                // Swal.fire('Email envoyé', '', 'success')
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })










    });
}
