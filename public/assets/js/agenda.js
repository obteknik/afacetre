$(function () {

    fetchEvents(document.getElementById("calendar"));
    $("#divCalendar").fadeIn(3000);

    /* SELECT => get personal calendar*/
    $('#select-person-agenda').on('change', function () {
        let personToken = $('#select-person-agenda option:selected').val();
        let calendar = $('#calendar');
        let divCalendar = $('#divCalendar');
        calendar.data('id', personToken);

        divCalendar.fadeOut();
        calendar.empty();
        fetchEvents(calendar);
        divCalendar.fadeIn(3000);

    });

    /* SELECT => get timeslot calendar*/
    $('#select-type-agenda').on('change', function () {
        let optionValue = $('#select-type-agenda option:selected').val();

        let str = optionValue.split('__');
        let personToken = str[1];
        let type = str[0];

        let calendar = $('#calendar');
        let divCalendar = $('#divCalendar');
        calendar.data('id', personToken);

        divCalendar.fadeOut();
        calendar.empty();

        switch (type) {
            case 'agenda':
                fetchEvents(calendar);
                break;

            case 'timeslot':
                calendar.data('type', type);
                fetchTimeslots(calendar);
                break;
        }
        divCalendar.fadeIn(2000);
    });



}); /* fin ready */

/* get show CALENDAR */
function fetchEvents(elementCalendar) {

    let userToken = $('#calendar').data('id');

    fetch(Routing.generate('app_event_all', {'token': userToken}), {
        method: "GET",
        headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
    }).then((resp) => resp.json()
    ).then(function (data) {

        let events = data.listing;
        let settings = data.settings;

        // $('#addEvent').on('click', function () {
        //     $(this).prop('disabled', true); //éviter multi appels ajax
        //     addCreateForm(null, false);
        // });

        let Calendar = FullCalendar.Calendar;
        let Draggable = FullCalendarInteraction.Draggable;

        // let containerEl = document.getElementById('external-events');
        let calendarEl = document.getElementById('calendar');

        // initialize the external events
        // new Draggable(containerEl, {
        //     itemSelector: '.fc-event',
        //     eventData: function (eventEl) {
        //         console.log(eventEl);
        //         let dataEvent = $(eventEl).data('event');
        //         let dataEventParsed = JSON.parse(dataEvent);
        //
        //         return {
        //             title: eventEl.innerText,
        //             duration: dataEventParsed.duration,
        //             // duration: "04:00:00",
        //             backgroundColor: dataEventParsed.backgroundColor,
        //             // stick: true, // maintain when user navigates (see docs on the renderEvent method)
        //             htmlTitleList: '<p style="color: yellow"><i class="icon-study"></i> ' + eventEl.innerText + '</p>',
        //             htmlTitleList2: '<p style="color: yellow"><i class="icon-study"></i> ' + eventEl.innerText + '</p>'
        //         };
        //     }
        // });

        //instanciation du calendrier
        let calendar = new Calendar(calendarEl, {
            // themeSystem: 'bootstrap',
            plugins: ['dayGrid', 'timeGrid', 'list', 'interaction'],
            defaultView: 'timeGridWeek',
            header: {
                left: 'title',
                center: '',
                right: 'dayGridDay, timeGridWeek, dayGridMonth modeList prev,today,next'
            },
            customButtons: {
                modeList: {
                    text: 'Mode liste',
                    click: function () {
                        if (calendar.view.type === 'dayGridDay') {
                            calendar.changeView('listDay');
                        } else if (calendar.view.type === 'timeGridWeek') {
                            calendar.changeView('listWeek');
                        } else if (calendar.view.type === 'dayGridMonth') {
                            calendar.changeView('listMonth');
                        }
                    }
                }
            },
            views: {
                listDay: {buttonText: 'list day'},
                listWeek: {buttonText: 'list week'},
                listMonth: {buttonText: 'list month'},
                // week: {
                //     titleFormat: '[Week from] D MMMM YYYY',
                //     titleRangeSeparator: ' to ',
                // }
            },
            buttonText: {
                today: 'Aujourd\'hui',
                month: 'Mois',
                week: 'Semaine',
                day: 'Jour',
                listDay: 'Jour',
                listWeek: 'Semaine',
                listMonth: 'Mois'
            },
            columnHeaderFormat: {
                month: 'long',    // Mon
                week: 'long', // Mon 7
                day: 'numeric',  // Monday 9/7
                agendaDay: 'dddd d',
                weekday: 'long'
            },
            titleFormat: {
                //https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Intl/DateTimeFormat
                year: 'numeric', // September 2009
                month: 'long', // September 2009
                week: "long", // September 2009
                day: 'numeric', // Tuesday, Sep 8, 2009
                weekday: 'long'
            },
            events: events,
            nowIndicator: true,
            // defaultTimedEventDuration: '00:30:00',
            droppable: true,
            drop: function (infos) { //EXTERNAL DRAGGING EVENT
                // newPredefinedEvent(infos);
            },
            navLinks: true,
            contentHeight: "auto", //resizes table to fit any screen
            slotEventOverlap: true,
            selectable: true,
            eventClick: (infos) => {
                showEvent(infos);
            },
            select: function (infos) {
                addCreateForm(infos);
            },
            selectHelper: true,
            eventLimit: true, // allow "more" link when too many events
            businessHours: [ // array possible
                {
                    daysOfWeek: [1, 2, 3, 4, 5],
                    startTime: '09:00',
                    endTime: '17:00'
                },
                // {
                //     daysOfWeek: [ 4, 5 ], // Thursday, Friday
                //     startTime: '10:00', // 10am
                //     endTime: '16:00' // 4pm
                // }
            ],
            editable: true,
            eventDrop: (infos) => {
                toastr.info("<br /><br /><button type='button' class='button btn-mini btn-default' id='confirmationRevertYes'>Oui</button>",
                    "Confirmez-vous le déplacement de ce rendez-vous ?", {
                        closeButton: false, allowHtml: true, positionClass: "toast-top-center",
                        onShown: function () {
                            $("#confirmationRevertYes").click(function () {
                                updateEvent(infos.event);
                            });
                        }
                    });
            },
            eventResize: (infos) => {
                toastr.info("<br /><br /><button type='button' class='button btn-mini btn-default' id='confirmationRevertYes'>Oui</button>",
                    "Confirmez-vous le redimensionnement de ce rendez-vous ?", {
                        closeButton: false, allowHtml: true, positionClass: "toast-top-center",
                        onShown: function () {
                            $("#confirmationRevertYes").click(function () {
                                updateEvent(infos.event);
                            });
                        }
                    });
            },
            eventRender: function (info) {
                renderEvent(info)
            },
            eventReceive: function (event, view) {
                // alert(event.re-sourceId);

            },
            // eventPositioned: function( info ) { console.log(info.event) }
        });

        //options
        calendar.setOption('locale', 'fr');
        calendar.setOption('firstDay', parseInt(settings.firstDayWeek));
        calendar.setOption('minTime', settings.minTimeDay);
        calendar.setOption('maxTime', settings.maxTimeDay);
        calendar.setOption('weekNumbers', (settings.weekNumbers === '1' ? true : false));
        calendar.setOption('validRange', {start: settings.agendaMinDate, end: settings.agendaMaxDate});

        //rendering
        calendar.render();

        // myDatepicker(calendar);

        $('.loader').hide();
    }).catch((error) => {
        console.log(error);
    });
}

/*** ACTIONS ***/

/* render event
* V5:eventContent: function(eventInfo) { return { html: eventInfo.event.extendedProps.customHtml }}
* */
function renderEvent(info) {
    if (info.el.getElementsByClassName('fc-title').length > 0) {
        // info.el.getElementsByClassName('fc-title')[0].innerHTML = info.event.extendedProps.htmlTitleList2;
        info.el.getElementsByClassName('fc-title')[0].innerHTML = info.event.extendedProps.htmlTitle;
    }
    if (info.el.getElementsByClassName('fc-list-item-title').length > 0) {
        // info.el.getElementsByClassName('fc-list-item-title')[0].innerHTML = info.event.extendedProps.htmlTitleList;
        info.el.getElementsByClassName('fc-list-item-title')[0].innerHTML = info.event.extendedProps.htmlTitle;
    }
}

/* show event */
function showEvent(infos) {

    let evt = infos.event;
    let evtStart = evt.start;
    let evtEnd = evt.end;

    let year_start = evtStart.getFullYear();
    let month_start = evtStart.getMonth();
    let day_start = evtStart.getDate();
    let hours_start = evtStart.getHours();
    let minute_start = evtStart.getMinutes();

    let year_end = evtEnd.getFullYear();
    let month_end = evtEnd.getMonth();
    let day_end = evtEnd.getDate();
    let hours_end = evtEnd.getHours();
    let minute_end = evtEnd.getMinutes();

    let title = evt.title;
    let evtProps = infos.event.extendedProps;
    let eventBddId = evtProps.resourceId;
    let eventType = evtProps.type;
    let eventObject = evtProps.object;
    let eventLocation = evtProps.location;
    let eventDescription = evtProps.description;
    let eventIsAllDay = evtProps.isAllDay;
    let eventIsPeriodic = evtProps.isPeriodic;
    let eventIsConfirmedByEmail = evtProps.isConfirmedByEmail;

    let startDateFormated = sprintfX(day_start, 2) + '/' + sprintfX((month_start + 1), 2) + '/' + year_start;
    let startDateTimeFormated = sprintfX(day_start, 2) + '/' + sprintfX((month_start + 1), 2) + '/' + year_start
        + ' ' + sprintfX(hours_start, 2) + ':' + sprintfX(minute_start, 2);

    let endDateFormated = sprintfX(day_end, 2) + '/' + sprintfX((month_end + 1), 2) + '/' + year_end;
    let endDateTimeFormated = sprintfX(day_end, 2) + '/' + sprintfX((month_end + 1), 2) + '/' + year_end
        + ' ' + sprintfX(hours_end, 2) + ':' + sprintfX(minute_end, 2);


    let eventIsAllDayCheckbox = eventIsAllDay ? '<i class="icon-checkbox-checked"></i>' : '<i class="icon-check-empty"></i>';
    let eventIsPeriodicCheckbox = eventIsPeriodic ? '<i class="icon-checkbox-checked"></i>' : '<i class="icon-check-empty"></i>';
    let eventIsConfirmedByEmailCheckbox = eventIsConfirmedByEmail ? '<i class="icon-checkbox-checked"></i>' : '<i class="icon-check-empty"></i>';

    let period = eventIsAllDay ? '<div class="col">Date heure de début: <b>' + startDateTimeFormated + '</b></div>' +
        '<div class="col">Date heure de fin: <b>' + endDateTimeFormated + '</b></div>' :
        '<div class="col">Toute la journée (de xh à yh)</div>';

    let modalTitle = '<i class="fa fa-calendar-alt"></i> Atelier: ' + '<b>' + title + '</b>';

    fetch(Routing.generate('app_event_show', {id: eventBddId}), {
        method: "GET",
        headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
    }).then((resp) => resp.json()
    ).then(function (data) {

        //rendu modal
        $('#adminModal .modal-title').html(modalTitle);
        $('#adminModal .modal-body').html(data.contentHtml);
        $('#adminModal').modal('show');

    }).catch((error) => {
        console.log(error);
    });
}

/* get creation form */
function addCreateForm(infos, modeSelect = true) {

    $('.loader').show();

    fetch(Routing.generate('app_event_new', {}), {
        method: "GET",
        headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
    }).then((resp) => resp.json()
    ).then(function (data) {

        console.log(data);

        let title = '<i class="icon-calendar-1"></i> Nouveau rendez-vous sur la période';

        $('#adminModal .modal-title').html(title);
        $('#adminModal .modal-body').html(data.formHtml);
        $('#adminModal').modal('show');

        //autofocus
        setTimeout(function() {
            //clic-esc desactivated
            // $('#adminModal').modal({backdrop: 'static', keyboard: false});
            $('#event_title').trigger('focus');
        }, 1000);

        //summernote
        // $('#event_description').summernote({height: 300, summernote: {mode: 'text/html'}});
        //
        // // cancelModal();

        //alimentation des champs de formulaires avec calendar data
        if (modeSelect) {
            let evt = infos;
            let evtStart = evt.start;
            let evtEnd = evt.end;

            let year_start = evtStart.getFullYear();
            let month_start = evtStart.getMonth();
            let day_start = evtStart.getDate();
            let hours_start = evtStart.getHours();
            let minute_start = evtStart.getMinutes();

            let year_end = evtEnd.getFullYear();
            let month_end = evtEnd.getMonth();
            let day_end = evtEnd.getDate();
            let hours_end = evtEnd.getHours();
            let minute_end = evtEnd.getMinutes();

            $('#event_startAt').val(year_start + '-' + sprintfX((month_start + 1), 2) + '-' + sprintfX(day_start, 2));
            $('#event_startAt_time_hour').val(hours_start);
            $('#event_startAt_time_minute').val(minute_start);

            $('#event_endAt').val(year_end + '-' + sprintfX((month_end + 1), 2) + '-' + sprintfX(day_end, 2));
            $('#event_endAt_time_hour').val(hours_end);
            $('#event_endAt_time_minute').val(minute_end);
        }

        // //CollectionType Participants
        // $('.persons').collection({
        //     min: 1,
        //     add: '<a href="#" class="collection-add btn btn-default" title="Ajouter un partiipant"><span class="fa fa-plus-circle"></span> Ajouter</a>',
        //     remove: '<a href="#" class="collection-remove btn btn-default" title="Supprimer un participant"><span class="fa fa-trash"></span> Supprimer</a>',
        //     fade_in: true,
        //     fade_out: true
        // });

        //check persons
        // $('#checkPersons').unbind('click').on('click', function () {
        //     // $('#tableCheck tbody').html('');
        //     $('#checkZone').show();
        //
        //     //récupération des informations NOM PRENOM saisies
        //     let nbPersons = 0;
        //     let firstname; let lastname; let birthdate;
        //
        //     // console.log(document.getElementById('tableCheck'));
        //     // if(document.getElementById('tableCheck') !== null){
        //     $('input[type=text][id^=event_persons_][id$=firstName]').each(function () {
        //         nbPersons++;
        //     });
        //
        //     let tableHeader = '<table id="tableCheck" class="table">' +
        //         '<caption>Contrôle de l\'existence des participants</caption>' +
        //         '<thead>' +
        //         '<tr><th></th><th>Prénom</th><th>Nom</th><th>Date de naissance</th><th>Contrôle effectué</th>' +
        //         '<th>Résultat</th>' +
        //         '</tr>' +
        //         '</thead><tbody>';
        //
        //     let tableFooter = '</tbody></table>';
        //     let tableBody = '';
        //
        //     //poiur chaque personne saisie dans le collectionType
        //     for (let j = 0; j <= nbPersons; j++) {
        //         firstname = $('#event_persons_' + j + '_firstName').val();
        //         lastname = $('#event_persons_' + j + '_lastName').val();
        //         birthdate = $('#event_persons_' + j + '_birthDate').val();
        //
        //         tableBody += '<tr data-id="' + j + '">' +
        //             '<td>' +
        //             '<div class="loaderEventPerson-' + j + '" style="display: none">' +
        //             '    <img src="/images/loader/Bars-1s-27px-transparent.gif" alt="loader"><input type="checkbox">' +
        //             '</div>' +
        //             '</td>' +
        //             '<td>' + firstname + '</td>' +
        //             '<td>' + lastname + '</td>' +
        //             '<td>' + formatStringToDate(birthdate) + '</td>' +
        //             '<td><i class="icon-check-empty"></i>' +
        //             '</td>' +
        //             '<td></td>' +
        //             '</tr>';
        //     }
        //
        //     // $('#checkZone').empty().html(tableHeader + tableBody + tableFooter);
        //     $('#checkZone').html(tableHeader + tableBody + tableFooter);
        //     $('#tableCheck tr:last').remove();
        //     // }
        //
        //
        //     //parcours table pour check ajax avec affichage loader sur la ligne
        //     // (uniquement ligne sans résultat pour éviter multi appel)
        //     //qd check effectué, mettre icone 'checked' dans colonne 'contrôle effectué'
        //     //+ 'Récupération du contact pour l'associer à cette prise de rendez-vous'
        //     // OU 'Le contact va être créé lors de cette prise de rendez-vous.'
        //
        //     $('table#tableCheck > tbody  > tr').each(function () {
        //         let checkId = $(this).attr('data-id');
        //         let checkFirstname = $(this).find('td').eq(1).html();
        //         let checkLastname = $(this).find('td').eq(2).html();
        //         let checkBirthdate = formatDateToString($(this).find('td').eq(3).html());
        //         let checkResult = $(this).find('td').eq(5).html();
        //
        //         // console.log(checkId);
        //         // console.log(checkFirstname);
        //         // console.log(checkLastname);
        //         // console.log(checkBirthdate);
        //
        //         console.log('champ = ' + $('#event_persons_' + checkId + '_firstName').val());
        //         console.log('table = ' + checkFirstname);
        //         console.log('champ = ' + $('#event_persons_' + checkId + '_lastName').val());
        //         console.log('table = ' + checkLastname);
        //         console.log('champ = ' + $('#event_persons_' + checkId + '_birthDate').val());
        //         console.log('table = ' + checkBirthdate);
        //         console.log('result = ' + $(this).find('td').eq(4).html());
        //         console.log('result = ' + $(this).find('td').eq(5).html());
        //         console.log('result = ' + checkResult);
        //
        //         //Mise à jour que si chagement sur les champs
        //         if ((checkResult ==="") && ($('#event_persons_' + checkId + '_firstName').val() === checkFirstname) ||
        //             ($('#event_persons_' + checkId + '_lastName').val() === checkLastname) ||
        //             ($('#event_persons_' + checkId + '_birthDate').val() === checkBirthdate)
        //         ) {
        //             console.log('Modification');
        //
        //             $('.loaderEventPerson-' + checkId).show();
        //
        //             fetch(Routing.generate('app_event_check_person', {
        //                 'idTableCheck': checkId,
        //                 'firstname': checkFirstname,
        //                 'lastname': checkLastname,
        //                 'birthdate': checkBirthdate
        //             }), {
        //                 method: "GET",
        //                 headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
        //             }).then((resp) => resp.json()
        //             ).then(function (data) {
        //                 console.log(data);
        //
        //                 let tr = 'table#tableCheck > tbody  > tr[data-id=' + checkId + ']';
        //                 $(tr).find('td').eq(4).html('<i class="icon-checkbox-checked"></i>');
        //                 $(tr).find('td').eq(5).html(data.result);
        //
        //                 $('.loaderEventPerson-' + checkId).hide();
        //             }).catch((error) => {
        //                 console.log(error);
        //             });
        //         } else {
        //             console.log('Pas de modification');
        //         }
        //     });
        // });

        //SOUMISSION DU FORMULAIRE DE CREATION
        submitForm('eventForm');

        //Champ de recherche contacts existants
        // $('#event_existing_contact_search_text').on('keyup', function () {
        //
        //     //Récupération form html EventExistingContact
        //     if (xhr) {
        //         xhr.abort();
        //     }
        //
        //     xhr = $.ajax({
        //         url: Routing.generate('app_event_searchExistingContacts'),
        //         type: 'POST',
        //         data: {"input": $(this).val()}
        //     }).done(function (response) {
        //
        //         $("#result-existingcontacts").html(response.formView);
        //
        //     }).fail(function () {
        //         console.log('error');
        //     });
        // });

        // $('#addEvent').prop('disabled', false);
        // $('.loader').hide();

    }).catch((error) => {
        console.log(error);
    });
}

/* update event*/
function updateEvent(event) {

    $('.loader').show();

    console.log(event.extendedProps);

    let startDate = event.start.getFullYear() + '-' + sprintfX((event.start.getMonth() + 1), 2) + '-' + sprintfX(event.start.getDate(), 2);
    let endDate = event.end.getFullYear() + '-' + sprintfX((event.end.getMonth() + 1), 2) + '-' + sprintfX(event.end.getDate(), 2);

    let startTime = sprintfX(event.start.getHours(), 2) + ':' + sprintfX(event.start.getMinutes(), 2);
    let endTime = sprintfX(event.end.getHours(), 2) + ':' + sprintfX(event.end.getMinutes(), 2);

    if (event.extendedProps.resourceId !== undefined) {
        let id = event.extendedProps.resourceId;

        fetch(Routing.generate('app_event_edit', {
            id: id, mode: 'direct',
            startDate: startDate, startTime: startTime, endDate: endDate, endTime: endTime
        }), {
            method: "POST",
            headers: {"Authorization": "No Auth"}
            // body: data
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            //retour erreur enregistrement manager php
            if (data.errorSys !== undefined) {
                toastr.options = {positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.warning(data.errorSys).css({"width": "auto"});
            }

            //retour erreurs de validation formulaire
            if (data.formHtml) {
                $('#adminModal .modal-body').html(data.formHtml);
            }

            //ok save
            if (data.errorSys === undefined && data.formHtml === undefined) {
                let msg = '';

                toastr.options = {progressBar: true, positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.success(data.message).css({"width": "auto"});

                $('.loader').hide();
            }
        }).catch((error) => {
            console.log(error);
        });
    }
}

/* submit form */
function submitForm(formId) {
    $(document).on('submit', '#' + formId, function (e) {
        e.preventDefault();

        let params = {};

        //id selon new/edit
        if($(this).attr('data-action') === 'edit'){
            params.id = $(this).attr('data-id');
        }

        let data = new FormData(document.getElementById(formId));

        $('.loader').show();

        let route = 'app_event_' + (params.id === undefined ? 'new' : 'edit');

        fetch(Routing.generate(route, params), {
            method: "POST",
            headers: {"Authorization": "No Auth"},
            body: data
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            //retour erreur enregistrement manager php
            if (data.errorSys !== undefined) {
                toastr.options = {positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.warning(data.errorSys).css({"width": "auto"});
            }

            //retour erreurs de validation formulaire
            if (data.formHtml) {
                $('#adminModal .modal-body').html(data.formHtml);
            }

            //ok save
            if (data.errorSys === undefined && data.formHtml === undefined) {
                toastr.options = {
                    progressBar: true,
                    positionClass: 'toast-top-right',
                    hideDuration: 300,
                    timeOut: 2000
                };
                toastr.clear();
                toastr.success(data.message).css({"width": "auto"});

                //hide modal/loader
                $('#adminModal').modal('hide');
                $('.loader').hide();

                window.location.href = Routing.generate('events',{token:data.token});
            }
        }).catch((error) => {
            console.log(error);
        });
    });
}

/* get show TIMESLOTS */
function fetchTimeslots(elementCalendar) {

    let userToken = $('#calendar').data('id');
    console.log(userToken);

    fetch(Routing.generate('app_timeslot_all', {'token': userToken}), {
        method: "GET",
        headers: {'Content-Type': 'application/json', "Authorization": "No Auth"}
    }).then((resp) => resp.json()
    ).then(function (data) {

        let timeslots = data.listing;
        let settings = data.settings;

        console.log(settings);
        console.log(timeslots);

        let Calendar = FullCalendar.Calendar;
        // let Draggable = FullCalendarInteraction.Draggable;

        let calendarEl = document.getElementById('calendar');

        //instanciation du calendrier
        let calendar = new Calendar(calendarEl, {
            // themeSystem: 'bootstrap',
            plugins: ['dayGrid', 'timeGrid', 'list', 'interaction'],
            defaultView: 'timeGridWeek',
            header: {
                left: 'title',
                center: '',
                right: 'dayGridDay, timeGridWeek, dayGridMonth modeList prev,today,next'
            },
            customButtons: {
                modeList: {
                    text: 'Mode liste',
                    click: function () {
                        if (calendar.view.type === 'dayGridDay') {
                            calendar.changeView('listDay');
                        } else if (calendar.view.type === 'timeGridWeek') {
                            calendar.changeView('listWeek');
                        } else if (calendar.view.type === 'dayGridMonth') {
                            calendar.changeView('listMonth');
                        }
                    }
                }
            },
            views: {
                listDay: {buttonText: 'list day'},
                listWeek: {buttonText: 'list week'},
                listMonth: {buttonText: 'list month'},
                // week: {
                //     titleFormat: '[Week from] D MMMM YYYY',
                //     titleRangeSeparator: ' to ',
                // }
            },
            buttonText: {
                today: 'Aujourd\'hui',
                month: 'Mois',
                week: 'Semaine',
                day: 'Jour',
                listDay: 'Jour',
                listWeek: 'Semaine',
                listMonth: 'Mois'
            },
            columnHeaderFormat: {
                month: 'long',    // Mon
                week: 'long', // Mon 7
                day: 'numeric',  // Monday 9/7
                agendaDay: 'dddd d',
                weekday: 'long'
            },
            titleFormat: {
                //https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Intl/DateTimeFormat
                year: 'numeric', // September 2009
                month: 'long', // September 2009
                week: "long", // September 2009
                day: 'numeric', // Tuesday, Sep 8, 2009
                weekday: 'long'
            },
            events: timeslots,
            nowIndicator: true,
            // defaultTimedEventDuration: '00:30:00',
            droppable: true,
            drop: function (infos) { //EXTERNAL DRAGGING EVENT
                // newPredefinedEvent(infos);
            },
            navLinks: true,
            contentHeight: "auto", //resizes table to fit any screen
            slotEventOverlap: true,
            selectable: true,
            eventClick: (infos) => {
                showEvent(infos);
            },
            select: function (infos) {
                addCreateForm(infos);
            },
            selectHelper: true,
            eventLimit: true, // allow "more" link when too many events
            businessHours: [ // array possible
                {
                    daysOfWeek: [1, 2, 3, 4, 5],
                    startTime: '09:00',
                    endTime: '17:00'
                },
                // {
                //     daysOfWeek: [ 4, 5 ], // Thursday, Friday
                //     startTime: '10:00', // 10am
                //     endTime: '16:00' // 4pm
                // }
            ],
            editable: true,
            eventDrop: (infos) => {
                toastr.info("<br /><br /><button type='button' class='button btn-mini btn-default' id='confirmationRevertYes'>Oui</button>",
                    "Confirmez-vous le déplacement de cette page d'activité ?", {
                        closeButton: false, allowHtml: true, positionClass: "toast-top-center",
                        onShown: function () {
                            $("#confirmationRevertYes").click(function () {
                                updateEvent(infos.event);
                            });
                        }
                    });
            },
            eventResize: (infos) => {
                toastr.info("<br /><br /><button type='button' class='button btn-mini btn-default' id='confirmationRevertYes'>Oui</button>",
                    "Confirmez-vous le redimensionnement de cette plage d'activité ?", {
                        closeButton: false, allowHtml: true, positionClass: "toast-top-center",
                        onShown: function () {
                            $("#confirmationRevertYes").click(function () {
                                updateTimeslot(infos.event);
                                // console.log(infos.event);
                            });
                        }
                    });
            },
            eventRender: function (info) {
                renderEvent(info)
            },
            eventReceive: function (event, view) {
                // alert(event.re-sourceId);

            },
            // eventPositioned: function( info ) { console.log(info.event) }
        });

        //options
        calendar.setOption('locale', 'fr');
        calendar.setOption('firstDay', parseInt(settings.firstDayWeek));
        calendar.setOption('minTime', settings.minTimeDay);
        calendar.setOption('maxTime', settings.maxTimeDay);
        calendar.setOption('weekNumbers', (settings.weekNumbers === '1' ? true : false));
        calendar.setOption('validRange', {start: settings.agendaMinDate, end: settings.agendaMaxDate});

        //rendering
        calendar.render();

        // myDatepicker(calendar);

        $('.loader').hide();
    }).catch((error) => {
        console.log(error);
    });
}

/* update event*/
function updateTimeslot(timeslot) {

    $('.loader').show();

    console.log(timeslot.extendedProps);

    let startDate = timeslot.start.getFullYear() + '-' + sprintfX((timeslot.start.getMonth() + 1), 2) + '-' + sprintfX(timeslot.start.getDate(), 2);
    let endDate = timeslot.end.getFullYear() + '-' + sprintfX((timeslot.end.getMonth() + 1), 2) + '-' + sprintfX(timeslot.end.getDate(), 2);

    let startTime = sprintfX(timeslot.start.getHours(), 2) + ':' + sprintfX(timeslot.start.getMinutes(), 2);
    let endTime = sprintfX(timeslot.end.getHours(), 2) + ':' + sprintfX(timeslot.end.getMinutes(), 2);

    if (timeslot.extendedProps.resourceId !== undefined) {
        let id = timeslot.extendedProps.resourceId;

        fetch(Routing.generate('app_timeslot_edit', {
            id: id, mode: 'direct',
            startDate: startDate, startTime: startTime, endDate: endDate, endTime: endTime
        }), {
            method: "POST",
            headers: {"Authorization": "No Auth"}
            // body: data
        }).then(function (response) {
            return response.json();
        }).then(function (data) {

            //retour erreur enregistrement manager php
            if (data.errorSys !== undefined) {
                toastr.options = {positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.warning(data.errorSys).css({"width": "auto"});
            }

            //retour erreurs de validation formulaire
            if (data.formHtml) {
                $('#adminModal .modal-body').html(data.formHtml);
            }

            //ok save
            if (data.errorSys === undefined && data.formHtml === undefined) {
                let msg = '';

                toastr.options = {progressBar: true, positionClass: 'toast-top-right', hideDuration: 300, timeOut: 2000};
                toastr.clear();
                toastr.success(data.message).css({"width": "auto"});

                $('.loader').hide();
            }
        }).catch((error) => {
            console.log(error);
        });
    }
}

/*-----------------------------------*/
function sprintfX(str, nb) {
    return sprintf("%'00" + nb + "s", str);
}

// dateString = '2020-04-03';
function formatStringToDate(dateString) {
    if (dateString !== undefined) {
        let date = dateString.split('-');
        return [date[2], date[1], date[0]].join("/");
    }
    return 'La chaîne à formatter n\'est pas définie !';
}

// date = '03/04/2020';
function formatDateToString(date) {
    if (date !== undefined) {
        let dateString = date.split('/');
        return [dateString[2], dateString[1], dateString[0]].join("-");
    }
    return 'La chaîne à formatter n\'est pas définie !';

}

// dateString = '2020-04-03 20:30:55';
function formatStringToDateTime(dateString) {
    if (dateString !== undefined) {
        let allDate = dateString.split(' ');
        let thisDate = allDate[0].split('-');
        let thisTime = allDate[1].split(':');
        let newDate = [thisDate[2], thisDate[1], thisDate[0]].join("-");

        let suffix = thisTime[0] >= 12 ? "PM" : "AM";
        let hour = thisTime[0] > 12 ? thisTime[0] - 12 : thisTime[0];
        hour = hour < 10 ? "0" + hour : hour;
        let min = thisTime[1];
        let sec = thisTime[2];
        let newTime = hour + ':' + min + suffix;

        return newDate + ' ' + newTime;
    }
    return 'La chaîne à formatter n\'est pas définie !';
}

// dateString = d-m-Y => Y-m-d
function formatdmYToYmd(dateString) {
    let date = dateString.split('-');
    return [date[2], date[1], date[0]].join("-");
}


