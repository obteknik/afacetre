Feature:
  I want to connect a the application

  Scenario: Bad connexion
    Given I am on "/login"
    When I fill in "_username" with "adminXXX@afac.fr"
    And I fill in "_password" with "test"
    And I press "Se connecter"
    #And I save a screenshot in "shot.png"
    Then I should see "identifiant non valide"

  @javascript
  Scenario:Success Admin Connexion
    Given I am on "/"
    When  I fill in the following:
      | _username | admin@afac.fr |
      | _password | test |
    And I press "Se connecter"
    Then I should see "Bienvenue"