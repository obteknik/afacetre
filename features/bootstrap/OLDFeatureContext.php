<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Mink\Driver\Selenium2Driver;
use WebDriver\Exception\UnexpectedAlertOpen;


/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class OLDFeatureContext extends DefaultContext implements  Context, SnippetAcceptingContext

{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When I click on the link :arg1
     */
    public function iClickOnTheLink($link)
    {
        $this->clickLink($link);
    }

    /**
     * @When I fill in the following :arg1 in field :arg2
     */
    public function iFillInTheFollowing($value,$key)
    {
        $this->fillField($key,$value);
    }

    /**
     * Resize Browser Width And Height
     * @Given (I )set browser window size to :width(px) x :height(px)
     */
    public function iSetBrowserWindowSizeToX($width, $height)
    {
        $this->getSession()->resizeWindow((int)$width, (int)$height, 'current');
//        dump($this->getSession()); die;
    }

    /**
     * Click on the element with the provided CSS Selector
     *
     * @When /^I click on the element with css selector "([^"]*)"$/
     *
     *
     */
    public function iClickOnTheElementWithCSSSelector($cssSelector)
    {
        $page = $this->getSession()->getPage();
        $element = $page->findAll('css', $cssSelector);

        foreach ($element as $elements) {
            if ($elements->hasClass($cssSelector)) {
               return $elements->click();
                // dump($elements);die;
            }
        }

    }

    /**
     * Select on element CSS with index name
     *
     * @When /^(?:|I )select "(?P<id>(?:[^"]|\\")*)"$ element from "" $/
     */
    public function iSelectElement($element, $option)
    {
        $session = $this->getSession();
       $select = $session->getPage()->find("xpath","//select[@name='".$element."']");

        $this->getSession()->getPage()->selectFieldOption($select, $option);
        ;
    }


    /**
     * Logged In as Administrator
     * @Given /^I am logged in as "(?P<username>[^"]*)" with password "(?P<password>[^"]*)"$/
     * @param $username
     * @param $password
     */
    public function iAmLoggedInAs($username, $password)
    {
        $session = $this->getSession();
        $session->visit($this->locatePath("/"));
        $page = $this->getSession()->getPage();
        $page->fillField('username', $username);
        $page->fillField('password', $password);
        $page->pressButton('Se connecter');

    }

    /**
     * check radio button
     * @Then /^I check the "([^"]*)" element$/
     *
     * @param $radioLabel
     * @throws Behat\Behat\Tester\Exception\PendingException;
     */
    public function iCheckElement($radioLabel)
    {
        $session = $this->getSession();
        $radioButton = $session->getPage()->findField($radioLabel);

        if (null === $radioButton) {
            throw new PendingException();
        }

        $session->getDriver()->click($radioButton->getXPath());
    }

    /**
     * @Given /^I click to "([^"]*)" element$/
     *
     * @param $radioLabel
     * @throws Behat\Behat\Tester\Exception\PendingException;
     */
    public function iClickToxElement($element){
        $session= $this->getSession();
        $page= $session->getPage()->findAll('xpath', '//*[text()="'.$element.'"]');
        //$session->getDriver()->click($page);
        foreach ($page as $item) {
            if(null === $page){
                throw new PendingException();
            }else{
            $item->click();
            }
        }

    }


    /**
     * @When I select the :arg1 link from line :arg2 with parent :arg3
     */
    public function iSelectTheLinkFromLineWithParent($arg1, $arg2, $arg3)
    {
        $session= $this->getSession();
        $page= $session->getPage()->findAll('css', 'tbody>tr:nth-child('.$arg3.')>td:nth-child('.$arg2.')>a:nth-child('.$arg1.')');
        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }
    }



    /**
     * @When I get link :arg1 from col :arg2 with id :arg3
     */
    public function iGetLinkFromColWithId($arg1, $arg2, $arg3)
    {
        $session= $this->getSession();
//        $page= $session->getPage()->findAll('css', ''.$arg3.' td:nth-child('.$arg2.')>a:nth-child('.$arg1.')');
        $page= $session->getPage()->findAll('xpath', '//*[@id="'.$arg3.'"]/td['.$arg2.']/a['.$arg1.']');
        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }
    }

    /**
     * @When I get option :arg1 from index :arg2 with id :arg3
     */
    public function iGetOptionWithXFromX($arg1, $arg2, $arg3)
    {
        $session= $this->getSession();
        $page= $session->getPage()->findAll('xpath', '//*[@id="'.$arg3.'"]/optgroup['.$arg2.']/option['.$arg1.']');
        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }
    }

    /**
     * @When I select index :arg1 from id :arg2
     */
    public function iselectValue($arg1, $arg2)
    {
        $session= $this->getSession();
        $page= $session->getPage()->findAll('xpath', '//*[@id="'.$arg2.'"]/option['.$arg1.']');
        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }
    }

    /**
     * @When I click the :arg1 element from col :arg2 with index :arg3
     */
    public function iGetIndexWithColBy($arg1, $arg2, $arg3)
    {
        $session= $this->getSession();
        $page= $session->getPage()->findAll('xpath', '//*[@class="'.$arg3.'"]['.$arg2.']/div['.$arg1.']');
        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }
    }

    /**Chevk alert
     * @When I disable alert :arg1
     */
    public function idisableAlert($arg1)
    {
        $session= $this->getSession();
        $page= $session->getPage()->find('xpath', '//*[@id="'.$arg1.'"]');
        if($page === true){
            $this->pressButton('Fermer');
        }else{
            throw new PendingException();
        }

        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }
    }

    /**
    * @When I wait for the modal to load
    */
    public function iWaitForTheModalToLoad()
    {
        $page= $this->getSession()->getPage()->find('xpath', '//*[@id="reminderModalAlerts"]');
        if($page == true){
        $this->getSession()->wait(
            3000,
            $this->getSession()->evaluateScript("$('#reminderModalAlerts').modal('hide');")
        );
        }else{

        }
    }

    /**
     * @When I click line :arg1 from col :arg2
     */
    public function iClickLinexFromColx($arg1, $arg2){
        $session= $this->getSession();
        $page= $session->getPage()->findAll('xpath', '//*[@class="rbc-day-slot"]['.$arg2.']/div['.$arg1.']/div[1]');

        foreach ($page as $pages){
            if ($pages === null){
                throw new PendingException();
            }else{
                $pages->click();
            }
        }

    }

//    /**
//     * @When I accept Popup
//     */
//    public function iAcceptPopUp(){
//
//            $this->getSession()->evaluateScript("window.alert = function(){
//return true;
//}");
    //}









}
