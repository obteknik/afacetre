<?php

namespace App\Controller;

use App\Entity\Adherent;
use App\Entity\Contract;
use App\Entity\Event;
use App\Repository\AdherentEventsRepository;
use App\Repository\EventRepository;
use App\Services\DompdfService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ContractController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var DompdfService
     */
    private $dompdfService;

    /**
     * ContractController constructor.
     * @param LoggerInterface $logger
     * @param DompdfService $dompdfService
     */
    public function __construct(LoggerInterface $logger, DompdfService $dompdfService)
    {
        $this->logger = $logger;
        $this->dompdfService = $dompdfService;
    }

    /**
     * GENERATION DE LA CONVENTION DE FORMATION
     * @Route("/contract/{id}/convention", name="app_contract_convention", methods={"GET"})
     * @param Contract $contract
     * @return Response
     */
    public function getConventionPdf(Contract $contract)
    {

        /*
         * La CONVENTION DE FORMATION comprend les informations:
         * -
         */

        try {
            $this->dompdfService->getConventionPdf($contract);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION DES CONVOCATIONS DE FORMATION
     * @Route("/contract/{id}/convocations", name="app_contract_convocations", methods={"GET"})
     * @param Contract $contract
     * @return Response
     */
    public function getConvocationPdf(Contract $contract)
    {

        /*
         * La CONVOCATION DE FORMATION comprend les informations:
         * -
         */

        try {
            $this->dompdfService->getConvocationPdf($contract);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION DES FEUILLES D'EMARGEMENT
     * @Route("/contract/{id}/attendance-sheets", name="app_contract_attendance_sheets", methods={"GET"})
     * @param Contract $contract
     * @return Response
     */
    public function getAttendanceSheetPdf(Contract $contract)
    {

        /*
         * La FEUILLE DE PRESENCE contient pour 1 séance/1 event :
         * - les informations d'organisation [Contract//Organization]
         * - la raison sociale du client [Contract//Client]
         * - le nom du formateur [Contract//Person]
         * - le nom complet du stagiaire [Contract//Adherents]
         * - les dates hebdomadaires de la session de formation [Contract]
         */

        try {
            $this->dompdfService->getAttendanceSheetPdf($contract);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION DES ATTESTATIONS DE FIN DE FORMATION
     * @Route("/contract/{id}/certificates", name="app_contract_certificates", methods={"GET"})
     * @param Contract $contract
     * @return Response
     */
    public function getCertificatePdf(Contract $contract)
    {

        /*
         * L'ATTESTATION DE FIN DE FORMATION comprend les informations:
         * -
         */

        try {
            $this->dompdfService->getCertificatePdf($contract);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION DU SUPPORT DE FORMATION
     * @Route("/contract/{id}/trainingSupport", name="app_contract_training_support", methods={"GET"})
     * @param Contract $contract
     * @return Response
     */
    public function getTrainingSupportPdf(Contract $contract)
    {

        /*
         * Le SUPPORT DE FORMATION comprend les informations:
         * -
         */

        try {
            $this->dompdfService->getEventContentPdf($contract);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION DU SUPPORT DE SEANCE DE FORMATION
     * @Route("/contract/{id}/eventContent", name="app_contract_event_support", methods={"GET"})
     * @param Contract $contract
     * @return Response
     */
    public function getEventSupportPdf(Contract $contract)
    {

        /*
         * Le SUPPORT DE SEANCE DE FORMATION comprend les informations:
         * -
         */

        try {
            $this->dompdfService->getEventContentPdf($contract);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * SUPPRESSION D'UN PARTICIPANT AU CONTRAT
     * @Route("/contract/{id}/delete/{adherent}", name="app_contract_adherent_delete", options={"expose":true}, methods={"GET"})
     * @param Contract $contract
     * @param Adherent $adherent
     * @param EventRepository $eventRepository
     * @param AdherentEventsRepository $adherentEventsRepository
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     */
    public function deleteAdherent(Contract $contract, Adherent $adherent,
                                   EventRepository $eventRepository,
                                   AdherentEventsRepository $adherentEventsRepository,
                                   EntityManagerInterface $manager)
    {

        //Récupérer tous les rdvs/séances du contrat
        //Pour chaque séance, retirer le participant
        $events = $eventRepository->findByContract($contract);
        /** @var Event $e */
        $adhEvents = [];
        foreach ($events as $e) {
            $adherentEvents = $adherentEventsRepository->findOneBy(['event' => $e,'adherent' => $adherent]);
            array_push($adhEvents, $adherentEvents);
        }

        foreach ($adhEvents as $ae) {
            $manager->remove($ae);
            $manager->flush();
        }

        //suppression du participant du contrat
        $contract->removeAdherent($adherent);
        $adherent->removeContract($contract);
        $manager->persist($contract);
        $manager->persist($adherent);
        $manager->flush();

        return new JsonResponse(['message' => 'ok', Response::HTTP_OK]);
    }
}
