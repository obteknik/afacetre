<?php

namespace App\Controller;

use App\Classe\MailJet;
use App\Form\ContactType;
use App\Repository\OrganizationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ContactController extends AbstractController
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route({ "en": "/contact-us", "fr": "/nous-contacter"}, name="contact")
     * @param Request $request
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function index(Request $request, OrganizationRepository $organizationRepository): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->addFlash('notice', "Merci de nous avoir contacté. Notre équipe va vous répondre dans les meilleurs délais.");

            $content = "Bonjour <br>Nouvelle demande de contact<br><br>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores atque dignissimos distinctio 
                eos est expedita fuga laboriosam laudantium minus mollitia 
                obcaecati pariatur quia, ratione repellat rerum, velit voluptatem voluptates!";

            $email = new MailJet();
            $toEmail = $this->params->get('app.admin_email');
            $toName = $this->params->get('app.admin_fullname');
            $email->send($toEmail, $toName, "Vous avez reçu une nouvelle demande de contact", $content);
        }

        return $this->render('contact/index.html.twig', [
            'organization' => $organizationRepository->find(1),
            'form' => $form->createView(),
            'page' => true,
        ]);
    }
}
