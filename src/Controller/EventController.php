<?php

namespace App\Controller;

use App\Entity\Adherent;
use App\Entity\CalendarParameter;
use App\Entity\Contract;
use App\Entity\Event;
use App\Entity\Search\EventSearch;
use App\Entity\User;
use App\Form\EventType;
use App\Form\Search\EventSearchType;
use App\Manager\EventManager;
use App\Repository\EventRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PersonRepository;
use App\Repository\UserRepository;
use App\Services\DompdfService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class EventController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var DompdfService
     */
    private $dompdfService;

    /**
     * EventController constructor.
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $manager
     * @param DompdfService $dompdfService
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $manager, DompdfService $dompdfService)
    {
        $this->logger = $logger;
        $this->manager = $manager;
        $this->dompdfService = $dompdfService;
    }

    /**
     * @Route("/events/{token}", name="events", methods={"GET"}, options={"expose":true})
     * @param $token
     * @param Request $request
     * @param EventRepository $eventRepository
     * @param PersonRepository $personRepository
     * @param UserRepository $userRepository
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function index($token, Request $request, EventRepository $eventRepository,
                          PersonRepository $personRepository, UserRepository $userRepository, OrganizationRepository $organizationRepository): Response
    {
        $search = new EventSearch();

        /*
         * Si token récupéré en GET correspond à un user ROLE_ADMIN, alors tous events
         * Si token récupéré en GET correspond à un user ROLE_PERSON, alors perso events
         */

        $userAskedToken = $userRepository->findOneByAgendaToken($token); //user concerné
        $roleUserAskedToken = $userAskedToken->getRoles()[0];

        if ($roleUserAskedToken == 'ROLE_ADMIN') {
            //tous les rendez-vous
            $events = $eventRepository->findAll();
        } else {
            //Rendez-vous de l'intervenant
            $search->setConnectedUser($userAskedToken);
            $events = $eventRepository->findAllQuery($search);
        }

        $persons = null;
        $persons = $personRepository->findAll();

        $formSearch = $this->createForm(EventSearchType::class, $search);
        $formSearch->handleRequest($request);

        $dataEvents = [];
        //Parcours des rendez-vous
        foreach ($events as $e) {
            try {
                /** @var Event $e */
                $dataEvent['resourceId'] = $e->getId();
                $dataEvent['id'] = $e->getId();
                $dataEvent['title'] = $e->getTitle();
                $dataEvent['start'] = $e->getstartAt()->format('Y-m-d H:i:s');
                $dataEvent['end'] = $e->getEndAt()->format('Y-m-d H:i:s');

//                $settings['agendaMinDate'] = $agendaMinDate->getOptionValue();
//                $settings['agendaMaxDate'] = $agendaMaxDate->getOptionValue();
//                $settings['firstDayWeek'] = $firstDayWeek->getOptionValue();
//                $settings['minTimeDay'] = $minTimeDay->getOptionValue();
//                $settings['maxTimeDay'] = $maxTimeDay->getOptionValue();
//                $settings['weekNumbers'] = $weekNumbers->getOptionValue();

                //objectParameter
//                if ($e->getEventObjectParameter() !== null) {
//                    $dataEvent['object'] = $e->getEventObjectParameter()->getValue();
//                    $dataEvent['backgroundColor'] = $e->getEventObjectParameter()->getOptionBackgroundColor();
//                    $dataEvent['borderColor'] = $e->getEventObjectParameter()->getOptionBorderColor();
//                    $dataEvent['textColor'] = $e->getEventObjectParameter()->getOptionTextColor();
//
//                } else {
//                    $dataEvent['object'] = null;
//
//                    $dataEvent['type'] = $e->getEventTypeParameter()->getValue();
//                    $dataEvent['backgroundColor'] = $e->getEventTypeParameter()->getOptionBackgroundColor();
//                    $dataEvent['borderColor'] = $e->getEventTypeParameter()->getOptionBorderColor();
//                    $dataEvent['textColor'] = $e->getEventTypeParameter()->getOptionTextColor();
//                }


                //typeParameter & locationParameter
////                $dataEvent['type'] = $e->getEventTypeParameter()->getValue();
//                $dataEvent['location'] = $e->getEventLocationParameter()->getValue();
//
//                $dataEvent['status'] = $e->getStatus();
//                $dataEvent['description'] = $e->getComment() !== null ? $e->getComment()->getNote():'';
//                $dataEvent['isAllDay'] = $e->getIsAllDay();
//                $dataEvent['isPeriodic'] = $e->getIsPeriodic();
//                $dataEvent['isConfirmedByEmail'] = $e->getIsConfirmedByEmail();

                //participants (persons)
//                $dataEvent['persons'] = $e->getPersons();

                $dataEvent['htmlTitle'] = "
                    <span style='color:#FFF; font-weight: bold;'>" . $e->getTitle() . "</span>
                    ";
                $dataEvent['htmlTitleList'] = "<img style='width: 30px; border-radius: 4px' src='' alt=''> <!-- /images/about/1.jpg -->
                    <span style='color:#000;margin-left: 2px;'>Patient/Client: " . $e->getTitle() . "</span>" .
                    " - Type de rendez-vous: <b>" . "</b>" .
                    " - <a target='_blank' href='/fr/secure/charge' style='text-decoration: underline'>Lien vers la fiche du participant</a>";

                $dataEvent['htmlTitleList2'] = "<img style='width: 30px; border-radius: 4px' src='/images/about/1.jpg' alt=''>
                    <span style='color:#000'>" . $e->getTitle() . "</span>" .
                    " - <a href='/fr/secure/charge'><i class='icon-link' href=''></i></a>";

                //Ajout de $data au tableau consolidé final passé à la vue
                array_push($dataEvents, $dataEvent);

            } catch (\Exception $e) {
                $this->logger->critical('[EVENTS] : ' . $e->getMessage());
            }
        }

        return $this->render('admin/event/agenda.html.twig', [
            'organization' =>$organizationRepository->find(1),
            'formSearch' => $formSearch->createView(),
            'listing' => $dataEvents,
            'persons' => $persons
        ]);
    }

    /**
     * RECUPERATION AJAX
     * Liste des rendez-vous par organisation
     *
     * @Route("/all/{token}", name="app_event_all", options={"expose":true}, methods={"GET"})
     * @param $token
     * @param Request $request
     * @param EventRepository $eventRepository
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function getAll($token, Request $request, EventRepository $eventRepository,
                           UserRepository $userRepository
    ): JsonResponse
    {

        //Tableau de données & calendar settings
        $dataEvents = [];
        $settings = [];

        // *** Paramétrage d'affichage de l'agenda ***
//        $agendaMinDate = $calendarParameterRepository->findOneBy(['organization' => $organization,
//            'value' => CalendarParameter::CAL_AGENDA_MINDATE]);
//        $agendaMaxDate = $calendarParameterRepository->findOneBy(['organization' => $organization,
//            'value' => CalendarParameter::CAL_AGENDA_MAXDATE]);
//        $firstDayWeek = $calendarParameterRepository->findOneBy(['organization' => $organization,
//            'value' => CalendarParameter::CAL_FIRSTDAY_WEEK]);
//        $minTimeDay = $calendarParameterRepository->findOneBy(['organization' => $organization,
//            'value' => CalendarParameter::CAL_MINTIME_DAY]);
//        $maxTimeDay = $calendarParameterRepository->findOneBy(['organization' => $organization,
//            'value' => CalendarParameter::CAL_MAXTIME_DAY]);
//        $weekNumbers = $calendarParameterRepository->findOneBy(['organization' => $organization,
//            'value' => CalendarParameter::CAL_WEEK_NUMBERS]);

        //TODO: business_days_hours
//                $dataEvent['business_days_hours'] = $business_days_hours->getOptionValue();

        // ***  des rendez-vous PAR DEFAUT SUR L APERIODE EN COURS ***
        $search = new EventSearch();

        $userAskedToken = $userRepository->findOneByAgendaToken($token); //user concerné
        $roleUserAskedToken = $userAskedToken->getRoles()[0];

        if ($roleUserAskedToken == 'ROLE_ADMIN') {
            //tous les rendez-vous
            $events = $eventRepository->findAll();
        } else {
            //Rendez-vous de l'intervenant
            $search->setConnectedUser($userAskedToken);
            $events = $eventRepository->findAllQuery($search);
        }

        /** @var CalendarParameter $calendarParameter */
        $calendarParameter = $userAskedToken->getCalendarParameter();

        if (!is_null($calendarParameter)) {
            $settings['agendaMinDate'] = $calendarParameter->getMinDate()->format('Y-m-d');
            $settings['agendaMaxDate'] = $calendarParameter->getMaxDate()->format('Y-m-d');
            $settings['firstDayWeek'] = $calendarParameter->getFirstDayWeek();
            $settings['minTimeDay'] = $calendarParameter->getMinTimeDay()->format('H:i');
            $settings['maxTimeDay'] = $calendarParameter->getMaxTimeDay()->format('H:i');
            $settings['weekNumbers'] = $calendarParameter->getWeekNumbers();
        }


//        $settings['validRangeStartDate'] = $accountingYear->getStartAt()->format('Y-m-d');
//        $settings['validRangeEndDate'] = $accountingYear->getEndAt()->format('Y-m-d');

        //Parcours des rendez-vous
        foreach ($events as $e) {
            try {
                /** @var Event $e */
                $dataEvent['resourceId'] = $e->getId();
                $dataEvent['id'] = $e->getId();
                $dataEvent['title'] = $e->getTitle();
                $dataEvent['start'] = $e->getstartAt()->format('Y-m-d H:i:s');
                $dataEvent['end'] = $e->getEndAt()->format('Y-m-d H:i:s');

                //design
                //couleur de fond associée au type du rdv
                $dataEvent['backgroundColor'] = $e->getType() != null ? $e->getType()->getEventBgColor():'#800080';


                $dataEvent['borderColor'] = '#FF0000';
                //couleur de fond associée au type du rdv
                $dataEvent['textColor'] = $e->getType() != null ? $e->getType()->getEventFtColor() :'#FFF';

                //objectParameter
//                if ($e->getEventObjectParameter() !== null) {
//                    $dataEvent['object'] = $e->getEventObjectParameter()->getValue();
//                    $dataEvent['backgroundColor'] = $e->getEventObjectParameter()->getOptionBackgroundColor();
//                    $dataEvent['borderColor'] = $e->getEventObjectParameter()->getOptionBorderColor();
//                    $dataEvent['textColor'] = $e->getEventObjectParameter()->getOptionTextColor();
//
//                } else {
//                    $dataEvent['object'] = null;
//
//                    $dataEvent['type'] = $e->getEventTypeParameter()->getValue();
//                    $dataEvent['backgroundColor'] = $e->getEventTypeParameter()->getOptionBackgroundColor();
//                    $dataEvent['borderColor'] = $e->getEventTypeParameter()->getOptionBorderColor();
//                    $dataEvent['textColor'] = $e->getEventTypeParameter()->getOptionTextColor();
//                }


                //typeParameter & locationParameter
                $dataEvent['type'] = $e->getType();
                $dataEvent['location'] = $e->getLocation();

                $dataEvent['status'] = $e->getStatus();
                $dataEvent['isAllDay'] = $e->getIsAllDay();
                $dataEvent['isPeriodic'] = $e->getIsPeriodic();
                $dataEvent['isConfirmedByEmail'] = $e->getIsConfirmedByEmail();

                //participants (persons)
//                $urlPerson = $this->generateUrl('person_show',['slug' => $e->getPerson()->getSlug()]);
                $object = $e->getObject() ? '<br>'.$e->getObject()->getName():'';

                //TITRE DU RDV
                $dataEvent['htmlTitle'] = "";
                if($e->getPatient()){  /* RDV DE TYPE PATIENT CABINET */
                    //TODO: vers crud easy admin fiche patient
                    $urlPatient = $this->generateUrl('person_show',['slug' => $e->getPerson()->getSlug()]);

                    $dataEvent['htmlTitle'] = $e->getPatient()->getFirstname().' '.$e->getPatient()->getLastname().
                    " - <span style='color:yellow;font-weight:bold'>" . $e->getType()->getShortType() ."</span>";

                } else if($e->getContract()) { /* RDV DE TYPE CONTRACT FORMATION */
                    $dataEvent['htmlTitle'] = 'To do';
//                        "<a href='".$urlPerson."'><span style='color:#FFF'>" .
//                        $e->getPerson()->getFirstname() . " " . $e->getPerson()->getLastname() . "</span></a>
//                    <span style='color:yellow;font-weight:bold'>" . $object ."</span>";

                } else { /* AUTRES TYPES DE RDV sans contract ni patient */
                    $urlPerson = $this->generateUrl('person_show',['slug' => $e->getPerson()->getSlug()]);
                    $dataEvent['htmlTitle'] = "<a href='".$urlPerson."'><span style='color:#FFF'>" .
                        $e->getPerson()->getFirstname() . " " . $e->getPerson()->getLastname() . "</span></a>
                    <span style='color:yellow;font-weight:bold'>" . $object ."</span>";
                }

//                $dataEvent['htmlTitleList'] = "<img style='width: 30px; border-radius: 4px' src='/images/about/1.jpg' alt=''>
//                    <span style='color:#000;margin-left: 2px;'>Patient/Client: " . $e->getTitle() . "</span>" .
//                    " - Type de rendez-vous: <b>" . $e->getEventTypeParameter()->getValue() . "</b>" .
//                    " - <a target='_blank' href='/fr/secure/charge' style='text-decoration: underline'>Lien vers la fiche du participant</a>";
//
//                $dataEvent['htmlTitleList2'] = "<img style='width: 30px; border-radius: 4px' src='/images/about/1.jpg' alt=''>
//                    <span style='color:#000'>" . $e->getTitle() . "</span>" .
//                    " - <a href='/fr/secure/charge'><i class='icon-link' href=''></i></a>";

                //Ajout de $data au tableau consolidé final passé à la vue
                array_push($dataEvents, $dataEvent);

            } catch (\Exception $e) {
                $this->logger->critical('[EVENTS] : ' . $e->getMessage());
            }
        }

        //objets de RDV prédéfinis
//        $predefinedObjets = $eventObjectParameterRepository->findBy(['organization' => $this->getUser()->getOrganization()]);
//        $predefinedEventTypes = $this->render('admin/event/_predefinedEventTypes.html.twig', ['predefinedObjets' => $predefinedObjets]);

        //prise en compte du mécanisme de recherche pour l'affichage de la liste initiale
        $search = new EventSearch();
        $formSearch = $this->createForm(EventSearchType::class, $search);
        $formSearch->handleRequest($request);
        $formSearchView = $this->render('admin/event/_searchForm.html.twig',
            ['formSearch' => $formSearch->createView()]);

        return new JsonResponse([
            'settings' => $settings,
            'listing' => $dataEvents,
//            'predefinedObjets' => $predefinedEventTypes->getContent(),
            'formSearchView' => $formSearchView->getContent()], Response::HTTP_OK);
    }

    /**
     * @Route("/event/edit/{id}/", name="app_event_edit", requirements={"id"="\d+"}, options={"expose":true}, methods={"GET", "POST"})
     * @param Request $request
     * @param Event $event
     * @return Response
     */
    public function edit(Request $request, Event $event): Response
    {

        $form = $this->createForm(EventType::class, $event, [
            'organization' => $this->getUser()->getOrganization(),
            'action' => 'edit',
            'startAt' => $event->getStartAt(),
            'endAt' => $event->getEndAt(),
        ]);

        $form->handleRequest($request);

        //traitement POST => redimensionnement & déplacement du rendez-vous
        if ($request->getMethod() === 'POST' && $request->query->get('mode') === 'direct') {

            $startDate = $request->query->get('startDate');
            $endDate = $request->query->get('endDate');
            $startTime = $request->query->get('startTime');
            $endTime = $request->query->get('endTime');

            $startDateTime = \DateTime::createFromFormat('Y-m-d H:i', $startDate . ' ' . $startTime);
            $endDateTime = \DateTime::createFromFormat('Y-m-d H:i', $endDate . ' ' . $endTime);

            $event->setStartAt($startDateTime);
            $event->setEndAt($endDateTime);

            //TODO: appeler le manager !
            $this->manager->persist($event);
            $this->manager->flush();

            //historical
//            try {
//                $historicalService->generate([$event], 'edit');
//            } catch (\Exception $e) {
//                $resultManager["errorHistoricalMessage"] = "Anomalie rencontrée lors de l'historisation de l'édition (redimensionnement/déplacement) du rendez-vous : "
//                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
//                $this->logger->error("[HistoricalService/generate] - " . $resultManager["errorHistoricalMessage"]);
//            }

            return new JsonResponse(['message' => "Rendez-vous modifié"], Response::HTTP_OK);
        }

        //traitement POST du formulaire de modification
        if ($form->isSubmitted() && $form->isValid()) {

            //TODO: appeler le manager !
            $this->manager->persist($event);
            $this->manager->flush();

            //historical
//            try {
//                $historicalService->generate([$event], 'edit');
//            } catch (\Exception $e) {
//                $resultManager["errorHistoricalMessage"] = "Anomalie rencontrée lors de l'historisation de l'édition du rendez-vous : "
//                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
//                $this->logger->error("[HistoricalService/generate] - " . $resultManager["errorHistoricalMessage"]);
//            }

            return new JsonResponse(['message' => "Rendez-vous modifié"], Response::HTTP_OK);
        }

        $formHtml = $this->render('admin/event/_form.html.twig', [
            'action' => 'edit',
            'event' => $event,
            'form' => $form->createView(),
        ]);

        return new JsonResponse(['formHtml' => $formHtml->getContent()], Response::HTTP_OK);
    }

    /**
     * @Route("/show/{id}/", name="app_event_show", requirements={"id"="\d+"}, options={"expose":true}, methods={"GET", "POST"})
     * @param Event $event
     * @return Response
     */
    public function show(Event $event): Response
    {

        $contentHtml = $this->render('admin/event/show.html.twig', [
            'event' => $event
        ]);

        return new JsonResponse(['contentHtml' => $contentHtml->getContent()], Response::HTTP_OK);
    }

    /**
     * @Route("/event/new", name="app_event_new", options={"expose":true}, methods={"GET", "POST"})
     * @param Request $request
     * @param EventManager $eventManager
     * @return Response
     */
    public function new(Request $request, EventManager $eventManager): Response
    {
        $event = new Event();

        $form = $this->createForm(EventType::class, $event, [ 'action' => 'create']);
        $form->handleRequest($request);

        //traitement POST => Rendez-vous créé par D&D selon type prédéfni
        if ($request->getMethod() === 'POST' && $request->query->get('mode') === 'direct') {

            $title = $request->query->get('title');
            $startDate = $request->query->get('startDate');
            $endDate = $request->query->get('endDate');
            $startTime = $request->query->get('startTime');
            $endTime = $request->query->get('endTime');

            $startDateTime = \DateTime::createFromFormat('Y-m-d H:i', $startDate . ' ' . $startTime);
            $endDateTime = \DateTime::createFromFormat('Y-m-d H:i', $endDate . ' ' . $endTime);

            $event->setTitle($title);
            $event->setStartAt($startDateTime);
            $event->setEndAt($endDateTime);

            //TODO: appeler le manager !
            $this->manager->persist($event);
            $this->manager->flush();

//            //historical
//            try {
//                $historicalService->generate([$event], 'edit');
//            } catch (\Exception $e) {
//                $resultManager["errorHistoricalMessage"] = "Anomalie rencontrée lors de l'historisation de l'édition (redimensionnement/déplacement) du rendez-vous : "
//                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
//                $this->logger->error("[HistoricalService/generate] - " . $resultManager["errorHistoricalMessage"]);
//            }

            return new JsonResponse(['message' => "Rendez-vous modifié"], Response::HTTP_OK);
        }

        // *** soumission & validation du formulaire ***
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();

//            dd($data);
//            $isAllDay = isset($data["event"]["isAllDay"]) ? $data["event"]["isAllDay"] : false;
//            $isPeriodic = isset($data["event"]["isPeriodic"]) ? $data["event"]["isPeriodic"] : false;
//            $isConfirmedByEmail = isset($data["event"]["isConfirmedByEmail"]) ? $data["event"]["isConfirmedByEmail"] : false;

            $isAllDay = $data["event"]["isAllDay"] ?? false;
            $isPeriodic = $data["event"]["isPeriodic"] ?? false;
            $isConfirmedByEmail = $data["event"]["isConfirmedByEmail"] ?? false;

            $strDateStart = $data["event"]["startAt"].' '.$data["event"]["startAt_time"]['hour'].':'.$data["event"]["startAt_time"]['minute'];
            $strDateEnd = $data["event"]["endAt"].' '.$data["event"]["endAt_time"]['hour'].':'.$data["event"]["endAt_time"]['minute'];

            $resultManager = $eventManager->create(
                new \DateTime($strDateStart),
                new \DateTime($strDateEnd),
                $data["event"]["title"], $data["event"]["comment"],
//                $data["event"]["persons"],
                $isAllDay, $isPeriodic, $isConfirmedByEmail);

            if (isset($resultManager["errorMessage"]) && $resultManager["errorMessage"] instanceof \Exception) {
                return new JsonResponse([
                    'errorSys' => $resultManager["errorMessage"]],
                    Response::HTTP_OK);

            } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof Event) {
                return new JsonResponse([
                    'message' => "Rendez-vous enregistré.",
                    'token' => $this->getUser()->getAgendaToken()
                ], Response::HTTP_OK);
            }
        }

        $formHtml = $this->render('admin/event/_form2.html.twig', [
            'action' => 'create',
            'event' => $event,
            'form' => $form->createView(),
        ]);

        return new JsonResponse(['formHtml' => $formHtml->getContent()], Response::HTTP_OK);
    }

    /**
     * @Route("/event/pdf", name="app_event_pdf", methods={"GET"})
     * @param EventRepository $eventRepository
     * @return Response
     */
    public function getPdf(EventRepository $eventRepository){

        $agendaEvents = $eventRepository->findAll();

        try {
            $this->dompdfService->getAgendaEventPdf($agendaEvents);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * @Route("/event/delete/{id}", name="app_event_delete", options={"expose":true}, methods={"GET"})
     * @param Event $event
     * @param EventRepository $eventRepository
     * @return JsonResponse
     */
    public function delete(Event $event, EventRepository $eventRepository): JsonResponse
    {
        if(!$event){
            $message = "Rendez-vous non identifié";
            return new JsonResponse(['message' => $message, Response::HTTP_BAD_REQUEST]);
        }

        $info = $event->getStartAt()->format('d/m/Y');
        $this->manager->remove($event);
        $this->manager->flush();
        $message =  'Rendez-vous du '.$info.' supprimé.';

        $tableHtml = $this->render('admin/patient/detail/tabs/event/_table.html.twig', [
            'patient' => $event->getPatient(),
            'events' => $eventRepository->findByPatient($event->getPatient())
        ]);

        return new JsonResponse(['message' => $message, 'tableHtml' => $tableHtml->getContent()], Response::HTTP_OK);
    }
}
