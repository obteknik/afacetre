<?php

namespace App\Controller;

use App\Classe\MailJet;
use App\Entity\User;
use App\Form\RegisterType;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegisterController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class RegisterController extends AbstractController
{
    private $entityManager;

    /**
     * RegisterController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route({ "en": "/signup", "fr": "/inscription"}, name="register")
     * @param Request $request
     * @param UserManager $userManager
     * @return Response
     */
    public function index(Request $request, UserManager $userManager): Response
    {

        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            $search_email = $this->entityManager->getRepository(User::class)->findOneByEmail($user->getEmail());
            if (!$search_email) {
//                $password = $encoder->encodePassword($user, $user->getPassword());
//
//                $user->setPassword($password);
//                $this->entityManager->persist($user);
//                $this->entityManager->flush();

                $resultManager = $userManager->create($user,$user->getEmail(), $user->getPassword());

                if (isset($resultManager["errorMessage"]) && $resultManager["errorMessage"] instanceof \Exception) {
                    $this->addFlash('warning',  $resultManager["errorMessage"]);
                    return $this->redirectToRoute('register');

                } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof User) {

                    //envoi email de confirmation d'inscription
                    $mail = new MailJet();

                    $contentTwig = $this->render('registerSucess.html.twig', ['user' => $resultManager["item"]])->getContent();

//                    $content = "Bonjour " . $user->getFirstname() . "<br>Bienvenue sur le site de l'A.F.A.C de l'Être<br><br>
//                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores atque dignissimos distinctio
//                eos est expedita fuga laboriosam laudantium minus mollitia
//                obcaecati pariatur quia, ratione repellat rerum, velit voluptatem voluptates!";

                    $subject = '';
                    $mail->send($user->getEmail(), $user->getFirstname(), "Bienvenue sur le site de l'A.F.A.C de l'Être",
                        $contentTwig);

                    //message flash
                    $this->addFlash('info', 'Votre inscription s\'est correctement déroulée. Vous pouvez dès à présent 
                vous connecter ci-dessous à votre compte.');

                    return $this->redirectToRoute('app_login', ['email' => $user->getEmail()]);
                }

            } else {
                $this->addFlash('warning', "L'email que vous avez renseigné existe déjà.");
            }
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
