<?php

namespace App\Controller;

use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PatientRepository;
use App\Repository\PatientTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AccountController extends AbstractController
{
    /**
     * @Route({ "en": "/account", "fr": "/compte"}, name="account")
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function index(OrganizationRepository $organizationRepository): Response
    {
        return $this->render('account/index.html.twig', [
            'organization' => $organizationRepository->find(1)
        ]);
    }

    /**
     * @Route({ "en": "/my-invoices", "fr": "/mes-factures-cabinet"}, name="account_invoices")
     * @param InvoiceRepository $invoiceRepository
     * @param PatientRepository $patientRepository
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function getInvoices(InvoiceRepository $invoiceRepository, PatientRepository $patientRepository,
                                OrganizationRepository $organizationRepository): Response
    {
        $patient = $patientRepository->findOneByUser($this->getUser());

        return $this->render('account/invoices.html.twig', [
            'organization' => $organizationRepository->find(1),
            'patient' => $patient
        ]);
    }
}
