<?php

namespace App\Controller;

use App\Entity\Adherent;
use App\Entity\Contract;
use App\Services\DompdfService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class AdherentController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AdherentController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var DompdfService
     */
    private $dompdfService;

    /**
     * ContractController constructor.
     * @param LoggerInterface $logger
     * @param DompdfService $dompdfService
     */
    public function __construct(LoggerInterface $logger, DompdfService $dompdfService)
    {
        $this->logger = $logger;
        $this->dompdfService = $dompdfService;
    }

    /**
     * GENERATION D'UNE CONVOCATION DE FORMATION
     * @Route("/adherent/{id}/convocation/{contract}", name="app_adherent_convocation", methods={"GET"})
     * @param Adherent $adherent
     * @param Contract $contract
     * @return Response
     */
    public function getIndividualConvocationPdf(Adherent $adherent, Contract $contract){

        try {
            $this->dompdfService->getIndividualConvocationPdf($contract, $adherent);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION DES FEUILLES DE PRESENCE D'UN ADHERENT
     * @Route("/adherent/{id}/attendanceSheet/{contract}", name="app_adherent_attendance_sheet", methods={"GET"})
     * @param Contract $contract
     * @param Adherent $adherent
     * @return Response
     */
    public function getIndividualAdherentAttendanceSheetPdf(Contract $contract, Adherent $adherent){

        try {
            $this->dompdfService->getIndividualAdherentAttendanceSheetPdf($contract, $adherent);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    /**
     * GENERATION D'UNE ATTESTATION DE FIN DE FORMATION
     * @Route("/adherent/{id}/certificate/{contract}", name="app_adherent_certificate", methods={"GET"})
     * @param Adherent $adherent
     * @param Contract $contract
     * @return Response
     */
    public function getIndividualCertificatePdf(Adherent $adherent, Contract $contract){

        try {
            $this->dompdfService->getIndividualCertificatePdf($contract, $adherent);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }


}
