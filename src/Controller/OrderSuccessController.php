<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\MailJet;
use App\Entity\Order;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderSuccessController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class OrderSuccessController extends AbstractController
{
    /**
     * @Route({ "en": "/order/thank/{stripeSessionId}", "fr": "/commande/merci/{stripeSessionId}"}, name="order_success")
     * @param Cart $cart
     * @param $stripeSessionId
     * @param OrderRepository $orderRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function index(Cart $cart, $stripeSessionId, OrderRepository $orderRepository,
                          EntityManagerInterface $entityManager): Response
    {
        /** @var Order $order */
        $order = $orderRepository->findOneByStripeSessionId($stripeSessionId);

        if (!$order || $order->getUser() != $this->getUser()) {
            $this->redirectToRoute('home');
        }

        //Mise à jour du statut IsPaid()
        if ($order->getState() == Order::STATE_UNPAID) {
            //vider la session $cart
            $cart->remove();

            $order->setState(Order::STATE_PAID);
            $entityManager->flush();

            //Envoi d'un email au client pour confirmation de commande
            $mail = new MailJet();

            $content = "Bonjour " . $order->getUser()->getFirstname() . "<br>Merci pour votre commande.<br><br>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores atque dignissimos distinctio 
                eos est expedita fuga laboriosam laudantium minus mollitia 
                obcaecati pariatur quia, ratione repellat rerum, velit voluptatem voluptates!";
            $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstname(),
                "Votre commande sur le site de l'A.F.A.C de l'Être est bien validée.",
                $content);
        }

        //Afficher quelques informations de commande de l'utilisateur
        return $this->render('order_success/index.html.twig', [
            'order' => $order
        ]);
    }
}
