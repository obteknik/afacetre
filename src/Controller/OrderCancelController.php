<?php

namespace App\Controller;

use App\Classe\MailJet;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderCancelController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class OrderCancelController extends AbstractController
{
    /**
     * @Route({ "en": "/order/cancel/{stripeSessionId}", "fr": "/commande/erreur/{stripeSessionId}"}, name="order_cancel")
     * @param $stripeSessionId
     * @param OrderRepository $orderRepository
     * @return Response
     */
    public function index($stripeSessionId, OrderRepository $orderRepository): Response
    {
        /** @var Order $order */
        $order = $orderRepository->findOneByStripeSessionId($stripeSessionId);

        if (!$order || $order->getUser() != $this->getUser()) {
            $this->redirectToRoute('home');
        }

        //Envoi d'un email d'échec de paiement à l'utilisateur
        $mail = new MailJet();
        $url = $this->generateUrl('order_recap');

        $content = "Bonjour " . $order->getUser()->getFirstname() . "<br><br>
                Nous vous informons que le paiement de votre commande n°".$order->getReference()." n'a pas pu aboutir
                <br>Veuillez recommencer votre opération en cliquant <a href=".$url.">ici<a/>";
        $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstname(),
            "Anomalie de paiement pour votre commande sur le site de l'A.F.A.C de l'Être",
            $content);

        return $this->render('order_cancel/index.html.twig', [
            'order' => $order
        ]);
    }
}
