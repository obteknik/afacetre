<?php

namespace App\Controller;

use App\Classe\MailJet;
use App\Entity\ResetPassword;
use App\Entity\User;
use App\Form\ResetPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ResetPasswordController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ResetPasswordController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ResetPasswordController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route({ "en": "/forgotten-password", "fr": "/mot-de-passe-oublie"}, name="reset_password")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        if ($request->get('email')) {

            $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->get('email'));

            if ($user) {
                //1 - enregistrer en base la demande de reset_password avec user/token/createdAt
                $resetPassword = new ResetPassword();
                $resetPassword->setUser($user);
                $resetPassword->setToken(uniqid());
                $resetPassword->setCreatedAt(new \DateTime());

                $this->entityManager->persist($resetPassword);
                $this->entityManager->flush();

                //2 - envoyer un email à l'utilisateur avec un lien permettant de mettre à jour son mot de passe
                $url = $this->generateUrl('update_password', ['token' => $resetPassword->getToken()]);

                $content = "Bonjour " . $user->getFirstname() . ",<br>Vous avez demandé à réinitialiser votre mot de passe sur le site
                    de l'A.F.A.C de l'Être<br><br>";
                $content .= "Merci de bien vouloir cliquer sur le lien suivant pour <a href='" . $url . "'>mettre à jour votre mot de passe</a>.";

                $email = new MailJet();
                $email->send($user->getEmail(), $user->getFirstname() . ' ' . $user->getLastname(),
                    "Réinitialiser votre mot de passe sur l'A.F.A.C de l'Être", $content);

                $this->addFlash('notice', "Vous allez recevoir dans quelques secondes un email avec la procédure 
                pour réinitialiser votre mot de passe.");
            } else {
                $this->addFlash('notice', "Cette adresse email est inconnue.");
            }
        }

        return $this->render('reset_password/index.html.twig');
    }

    /**
     * @Route("/modifier-mon-mot-de-passe/{token}", name="update_password")
     * @param Request $request
     * @param $token
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function update(Request $request, $token, UserPasswordEncoderInterface $encoder): Response
    {
        $reset_password = $this->entityManager->getRepository(ResetPassword::class)->findOneByToken($token);
        if (!$reset_password) {
            return $this->redirectToRoute('reset_password');
        }

        //vérifier que le createdAt = now - 3 heures
        $now = new \DateTime();
        if ($now > $reset_password->getCreatedAt()->modify('+ 3 hour')) {
            $this->addFlash('notice', "Votre demande de mot de passe a expiré. Merci de la renouveler.");
            return $this->redirectToRoute('reset_password');
        }

        //Rendre une vue avec mot de passe et confirmez votre mot de passe
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $new_password = $form->get('new_password')->getData();

            //Encodage des mots de passe & flush
            $password = $encoder->encodePassword($reset_password->getUser(), $new_password);
            $reset_password->getUser()->setPassword($password);
            $this->entityManager->flush();

            //message & redirection vers la page de connexion
            $this->addFlash('notice', "Votre mot de passe a bien été mis à jour.");
            return $this->redirectToRoute('app_login');
        }

        return $this->render('reset_password/update.html.twig', ['form' => $form->createView()]);
    }
}
