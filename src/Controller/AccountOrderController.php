<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountOrderController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AccountOrderController extends AbstractController
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * AccountOrderController constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route({ "en": "/my-orders", "fr": "/mes-commandes"}, name="account_order")
     * @return Response
     */
    public function index(): Response
    {

        $orders = $this->orderRepository->findBy(['user' => $this->getUser(),'state' => Order::STATE_PAID], ['id' => 'DESC']);
        return $this->render('account/order.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route({ "en": "/my-orders/{reference}", "fr": "/mes-commandes/{reference}"}, name="account_order_show")
     * @param $reference
     * @return Response
     */
    public function show($reference): Response
    {

        $order = $this->orderRepository->findOneByReference($reference);

        if(!$order || $order->getUser() != $this->getUser()){
            return $this->redirectToRoute('account_order');
        }

        return $this->render('account/order_show.html.twig', [
            'order' => $order
        ]);
    }
}
