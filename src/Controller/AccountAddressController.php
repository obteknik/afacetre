<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Address;
use App\Form\AddressType;
use App\Repository\AddressRepository;
use App\Repository\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountAddressController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AccountAddressController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * AccountAddressController constructor.
     * @param EntityManagerInterface $entityManager
     * @param AddressRepository $addressRepository
     */
    public function __construct(EntityManagerInterface $entityManager, AddressRepository $addressRepository)
    {
        $this->entityManager = $entityManager;
        $this->addressRepository = $addressRepository;
    }


    /**
     * @Route({ "en": "/account/addresses", "fr": "/compte/adresses"}, name="account_address")
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function index(OrganizationRepository $organizationRepository): Response
    {
        return $this->render('account/address.html.twig', [
            'organization' => $organizationRepository->find(1)
        ]);
    }

    /**
     * @Route({ "en": "/account/add-an-address", "fr": "/compte/ajouter-une-adresse"}, name="account_address_add")
     * @param Cart $cart
     * @param Request $request
     * @return Response
     */
    public function add(Cart $cart, Request $request): Response
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $address->setUser($this->getUser());
            $this->entityManager->persist($address);
            $this->entityManager->flush();

            //si panier non vide, redirection vers le tunnel d'achat, sinon Mes adresses
            if ($cart->get()) {
                return $this->redirectToRoute('order');
            } else {
                return $this->redirectToRoute('account_address');
            }
        }

        return $this->render('account/address_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route({ "en": "/account/edit-an-address/{id}", "fr": "/compte/modifer-une-adresse/{id}"},
     *     name="account_address_edit", requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function edit(Request $request, $id): Response
    {
        $address = $this->addressRepository->findOneById($id);

        if (!$address || $address->getUser() != $this->getUser()) {
            return $this->redirectToRoute('account_address');
        }

        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('account_address');
        }

        return $this->render('account/address_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route({ "en": "/compte/delete-an-address/{id}", "fr": "/compte/supprimer-une-adresse/{id}"},
     *     name="account_address_delete", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        $address = $this->addressRepository->findOneById($id);

        if ($address || $address->getUser() == $this->getUser()) {
            $this->entityManager->remove($address);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('account_address');
    }
}
