<?php

namespace App\Controller\Admin;

use App\Classe\MailJet;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class OrderCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class OrderCrudController extends AbstractCrudController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var TranslatorInterface
     */
    private $translator;


    /**
     * OrderCrudController constructor.
     * @param EntityManagerInterface $entityManager
     * @param CrudUrlGenerator $crudUrlGenerator
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManagerInterface $entityManager, CrudUrlGenerator $crudUrlGenerator,
                                TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', $this->translator->trans('order.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail', $this->translator->trans('order.detail.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        $updatePreparation = Action::new('updatePreparation', 'Préparation en cours', 'fas fa-truck')
            ->linkToCrudAction('updatePreparation');

        $updateDelivery = Action::new('updateDelivery', 'Livraison en cours', 'fas fa-box-open')
            ->linkToCrudAction('updateDelivery');

        return $actions
            ->add('detail', $updatePreparation)
            ->add('detail', $updateDelivery)
            ->add('index', 'detail')
            ->disable('new')
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function updatePreparation(AdminContext $context)
    {
        /** @var Order $order */
        $order = $context->getEntity()->getInstance();

        if ($order->getState() == Order::STATE_ONGOING_PREPARATION) {
            $this->addFlash('notice', "<span style='color:green;'><strong>La commande n°"
                . $order->getReference() . " est déjà <u>en cours de préparation</u>.</strong></span>");

            $url = $this->crudUrlGenerator->build()
                ->setController(OrderCrudController::class)
                ->setAction(ACTION::DETAIL)//ou 'detail'
                ->generateUrl();

            return $this->redirect($url);
        }

        //MISE A JOUR DU STATUT
        $order->setState(Order::STATE_ONGOING_PREPARATION);
        $this->entityManager->flush();

        //message flash
        $this->addFlash('notice', "<span style='color:green;'><strong>La commande n°"
            . $order->getReference() . " est bien passée en statut <u>en cours de préparation</u>.</strong></span>");
        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction(ACTION::INDEX)//ou 'index'
            ->generateUrl();

        return $this->redirect($url);
    }

    public function updateDelivery(AdminContext $context)
    {
        /** @var Order $order */
        $order = $context->getEntity()->getInstance();

        if ($order->getState() == Order::STATE_ONGOING_DELIVERY) {
            $this->addFlash('notice', "<span style='color:green;'><strong>La commande n°"
                . $order->getReference() . " est déjà <u>en cours de livraison</u>.</strong></span>");


            //Envoi d'un email au client pour confirmation de commande
            $mail = new MailJet();

            $content = "Bonjour " . $order->getUser()->getFirstname() . "<br><br>
                Nous vous informons que votre commande n°" . $order->getReference() . " est en cours de préparation.";
            $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstname(),
                "A.F.A.C de l'Être - Votre commande n° " . $order->getReference() . " en cours de préparation.",
                $content);

            //redirection
            $url = $this->crudUrlGenerator->build()
                ->setController(OrderCrudController::class)
                ->setAction(ACTION::DETAIL)//ou 'detail'
                ->generateUrl();

            return $this->redirect($url);
        }

        //MISE A JOUR DU STATUT
        $order->setState(Order::STATE_ONGOING_DELIVERY);
        $this->entityManager->flush();

        //message flash
        $this->addFlash('notice', "<span style='color:orange;'><strong>La commande n°"
            . $order->getReference() . " est bien passée en statut <u>en cours de livraison</u>.</strong></span>");

        //Envoi d'un email au client pour confirmation de commande
        $mail = new MailJet();

        $content = "Bonjour " . $order->getUser()->getFirstname() . "<br><br>
                Nous vous informons que votre commande n°" . $order->getReference() . " est en cours de livraison.";
        $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstname(),
            "A.F.A.C de l'Être - Votre commande n° " . $order->getReference() . " en cours de livraison.",
            $content);

        //redirection
        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction(ACTION::INDEX)//ou 'index'
            ->generateUrl();

        return $this->redirect($url);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            DateTimeField::new('createdAt', $this->translator->trans('order.table.th.order_date',
                [], 'admin'))->hideOnForm(),
            TextField::new('user.getFullName', $this->translator->trans('order.table.th.order_user',
                [], 'admin')),
            TextEditorField::new('delivery', $this->translator->trans('order.table.th.order_carrier_address',
                [], 'admin'))->onlyOnDetail(),
            MoneyField::new('total', $this->translator->trans('order.table.th.order_product_total',
                [], 'admin'))->setCurrency('EUR'),
            TextField::new('carrierName', $this->translator->trans('order.table.th.order_carrier',
                [], 'admin')),
            MoneyField::new('carrierPrice', $this->translator->trans('order.table.th.order_carrier_price',
                [], 'admin'))->setCurrency('EUR'),
            ChoiceField::new('state', $this->translator->trans('order.table.th.order_state',
                [], 'admin'))->setChoices([
                "Payée" => Order::STATE_PAID,
                "Non payée" => Order::STATE_UNPAID,
                "En cours de préparation" => Order::STATE_ONGOING_PREPARATION,
                "En cours de livraison" => Order::STATE_ONGOING_DELIVERY,
            ]),
            ArrayField::new('orderDetails', 'Produits achetés')->hideOnIndex()
        ];
    }

}
