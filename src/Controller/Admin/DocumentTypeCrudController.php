<?php

namespace App\Controller\Admin;

use App\Entity\DocumentType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocumentTypeCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DocumentTypeCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return DocumentType::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.documentType.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.documentType.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.documentType.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.documentType.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/documentType/index.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/documentType/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/documentType/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.documentType.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.documentType.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.documentType.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id')->hideOnForm();
        $type = TextField::new('type', $this->translator->trans('admin.documentType.form.type.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['autofocus' => true, 'placeholder' => $this->translator->trans('admin.documentType.form.type.placeholder',
                [], 'admin'),],
        ]);
        $isActive = BooleanField::new('isActive', "Actif");

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $type, $isActive];
        }

        return [  $type, $isActive];
    }

}