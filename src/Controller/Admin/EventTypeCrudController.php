<?php

namespace App\Controller\Admin;

use App\Entity\EventType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventTypeCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * eventCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return EventType::class;
    }

    public function configureAssets(Assets $assets): Assets
    {
        return Assets::new()
            ->addJsFile('/assets/libs/spectrum-master/spectrum-master/spectrum.js')
            ->addCssFile('/assets/libs/spectrum-master/spectrum-master/spectrum.css');
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventType.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventType.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventType.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventType.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['startAt', 'endAt']);
        $crud->overrideTemplate('crud/index', 'admin/eventType/index.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/eventType/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/eventType/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.eventType.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.eventType.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.eventType.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            yield IdField::new('id'),

            Yield AssociationField::new('workflow', $this->translator->trans('admin.eventType.form.workflow.label',
                [], 'admin'))->onlyOnForms(),

            yield ArrayField::new('getProcessusWithEventColors', $this->translator->trans('admin.eventType.form.getProcessusWithEventColors.label',
                [], 'admin'))->hideOnForm()->setTemplatePath('admin/eventType/_workflowWithEventColors.html.twig'),

            yield TextField::new('type', $this->translator->trans('admin.eventType.form.type.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.eventType.form.type.placeholder',
                    [], 'admin')
                ]]),

            yield TextField::new('shortType', $this->translator->trans('admin.eventType.form.shortType.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.eventType.form.shortType.placeholder',
                    [], 'admin')
                ]]),

            yield ArrayField::new('getEventColors', $this->translator->trans('admin.eventType.form.getEventColors.label',
                [], 'admin'))->hideOnForm()->setTemplatePath('admin/eventType/_bgColor.html.twig'),

        ];
    }
}
