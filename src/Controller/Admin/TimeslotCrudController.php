<?php

namespace App\Controller\Admin;

use App\Entity\Timeslot;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class TimeslotCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Timeslot::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.timeslot.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail','<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.timeslot.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new','<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.timeslot.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit','<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.timeslot.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name']);
        $crud->overrideTemplate('crud/index', 'admin/timeslot/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/timeslot/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/timeslot/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/timeslot/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.timeslot.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.timeslot.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.timeslot.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }


    public function configureFields(string $pageName): iterable
    {

        $id = IdField::new('id')->hideOnForm();

        $startDate = DateTimeField::new('startAt', $this->translator->trans('admin.timeslot.form.startAt.label',
            [], 'admin'));

        $endDate = DateTimeField::new('endAt', $this->translator->trans('admin.timeslot.form.endAt.label',
            [], 'admin'));

        $title = TextField::new('title', $this->translator->trans('admin.timeslot.form.title.label',
            [], 'admin'))->setFormTypeOptions(
            [
                'attr' => ['placeholder' => $this->translator->trans('admin.timeslot.form.title.placeholder',
                    [], 'admin'),
                    'autofocus' => true]
            ]
        );

        $object = AssociationField::new('object', $this->translator->trans('admin.timeslot.form.object.label',
            [], 'admin'));

        $isAllDay = BooleanField::new('isAllDay', $this->translator->trans('admin.timeslot.form.isAllDay.label',
            [], 'admin'));

        $isPeriodic = BooleanField::new('isPeriodic', $this->translator->trans('admin.timeslot.form.isPeriodic.label',
            [], 'admin'));

        $isConfirmedByEmail = BooleanField::new('isConfirmedByEmail', $this->translator->trans('admin.timeslot.form.isConfirmedByEmail.label',
            [], 'admin'));

        $person = AssociationField::new('person', $this->translator->trans('admin.timeslot.form.person.label',
            [], 'admin'));

        $illustration = ImageField::new('person.illustration', $this->translator->trans('admin.post.form.illustration.label',
            [], 'admin'))
            ->setBasePath('uploads/')
            ->setUploadDir('public/uploads')
            ->setUploadedFileNamePattern('[randomhash].[extension]')
            ->setRequired(false);

        $status = TextField::new('status', $this->translator->trans('admin.timeslot.form.status.label',
            [], 'admin'));

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $illustration, $person, $title, $object, $startDate, $endDate, $isAllDay, $isPeriodic, $isConfirmedByEmail,
                $status];
        }

        return [
            $title, $object, $startDate, $endDate, $person, $isAllDay, $isPeriodic, $isConfirmedByEmail, $status
        ];
    }
}
