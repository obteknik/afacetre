<?php

namespace App\Controller\Admin;

use App\Entity\Discipline;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DisciplineCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class DisciplineCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Discipline::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', $this->translator->trans('admin.discipline.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail', $this->translator->trans('admin.discipline.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new', $this->translator->trans('admin.discipline.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit', $this->translator->trans('admin.discipline.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name']);
        $crud->overrideTemplate('crud/index', 'admin/discipline/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/discipline/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/discipline/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/discipline/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.discipline.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.discipline.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.discipline.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {

        $id = IdField::new('id')->hideOnForm();
        $createdAt = DateTimeField::new('createdAt', $this->translator->trans('admin.invoice.form.createdAt.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['disabled' => true,],
        ])->hideOnForm();
        $isActive = BooleanField::new('isActive', "Actif");
        $name = TextField::new('name', $this->translator->trans('admin.discipline.form.name.label',
            [], 'admin'))->setFormTypeOptions(
            ['attr' => ['autofocus' => true,
                'placeholder' => $this->translator->trans('admin.discipline.form.name.placeholder',
                    [], 'admin')]]);
        $slug = TextField::new('slug')->hideOnForm();
        $description = TextareaField::new('description', $this->translator->trans('admin.discipline.form.description.label',
            [], 'admin'))->setFormTypeOptions(
            ['attr' => ['placeholder' => $this->translator->trans('admin.discipline.form.description.placeholder',
                    [], 'admin')]]);

        $illustration = ImageField::new('illustration', $this->translator->trans('admin.organization.form.logo.label',
            [], 'admin'))
            ->setBasePath('uploads/')
            ->setUploadDir('public/uploads')
            ->setUploadedFileNamePattern('[randomhash].[extension]')
            ->setRequired(false)->setFormTypeOptions(['required' => false]);

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $isActive, $createdAt, $illustration, $name, $slug,$description,];
        }

        return [  $createdAt, $name,$isActive, $description, $illustration ];
    }

}
