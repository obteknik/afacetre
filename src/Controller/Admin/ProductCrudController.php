<?php

namespace App\Controller\Admin;

use App\Classe\AdminCkEditorField;
use App\Entity\Product;
use App\Form\DocumentType;
use App\Form\ProductDayType;
use App\Form\ProductDetailType;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use App\Services\AppService;
use App\Services\DompdfService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ProductCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ProductCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var DompdfService
     */
    private $dompdfService;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var AdminContextProvider
     */
    private $context;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var KernelInterface
     */
    private $kernel;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;

    /**
     * ProductCrudController constructor.
     * @param TranslatorInterface $translator
     * @param ProductRepository $productRepository
     * @param DompdfService $dompdfService
     * @param LoggerInterface $logger
     * @param AppService $appService
     * @param AdminContextProvider $context
     * @param OrganizationRepository $organizationRepository
     * @param KernelInterface $kernel
     * @param CrudUrlGenerator $crudUrlGenerator
     */
    public function __construct(TranslatorInterface $translator, ProductRepository $productRepository,
                                DompdfService $dompdfService, LoggerInterface $logger, AppService $appService,
                                AdminContextProvider $context, OrganizationRepository $organizationRepository,
                                KernelInterface $kernel, CrudUrlGenerator $crudUrlGenerator)
    {
        $this->translator = $translator;
        $this->productRepository = $productRepository;
        $this->dompdfService = $dompdfService;
        $this->logger = $logger;
        $this->appService = $appService;
        $this->context = $context;
        $this->organizationRepository = $organizationRepository;
        $this->kernel = $kernel;
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
        $crud->setPageTitle('index', '<i class="fa fa-graduation-cap"></i> ' . $this->translator->trans('admin.product.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function (Product $product) {
            return (string)'<i class="fa fa-graduation-cap"></i> '
                . $this->translator->trans('admin.product.detail.headTitle', [], 'admin') . ': '
                . $product->getReference() . ' - ' . $product->getName();
        });
        $crud->setPageTitle('new', '<i class="fa fa-graduation-cap"></i> ' . $this->translator->trans('admin.product.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-graduation-cap"></i> ' . $this->translator->trans('admin.product.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['reference', 'category', 'name']);
        $crud->overrideTemplate('crud/index', 'admin/product/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/product/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/product/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/product/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        $getPedagogicSheetPdf = Action::new('getPedagogicSheetPdf', 'Fiche pédagogique', 'fas fa-download')
            ->linkToCrudAction('getPedagogicSheetPdf');

        $getEducationalProcessPdf = Action::new('getEducationalProcessPdf', 'Déroulé pédagogique', 'fas fa-download')
            ->linkToCrudAction('getEducationalProcessPdf');

        $getBooklet = Action::new('getBooklet', 'Livret de formation du stagiaire', 'fas fa-download')
            ->linkToCrudAction('getBooklet');

        return $actions
            ->add('index', 'detail')
            ->add('detail', $getEducationalProcessPdf)
            ->add('detail', $getPedagogicSheetPdf)
            ->add(Crud::PAGE_DETAIL, $getBooklet)
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.product.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.product.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.product.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function getPedagogicSheetPdf(AdminContext $context)
    {
        /** @var Product $product */
        $product = $context->getEntity()->getInstance();
        if ($product->getGoal() && $product->getMeans()) {
            try {
                $this->dompdfService->getPedagogicSheetPdf($product);
            } catch (LoaderError $e) {
                $this->logger->error($e->getMessage());
            } catch (RuntimeError $e) {
                $this->logger->error($e->getMessage());
            } catch (SyntaxError $e) {
                $this->logger->error($e->getMessage());
            }
            return new Response();
        } else {
            //message flash
            $this->addFlash('notice', "<span style='color:green;'>
            <strong>
            <i class='fa fa-exclamation-circle'></i> Veuillez compléter les informations dans l'onglet <span style='color: dodgerblue;'>Fiche pédagogique</span> afin de pouvoir générer la fiche pédagogique.</strong></span>");
            $url = $this->crudUrlGenerator->build()
                ->setController(ProductCrudController::class)
                ->setAction(ACTION::DETAIL)
                ->generateUrl();

            return $this->redirect($url);
        }
    }

    public function getEducationalProcessPdf(AdminContext $context)
    {
        /** @var Product $product */
        $product = $context->getEntity()->getInstance();

        if (count($product->getProductDays()) != 0) {
            try {
                $this->dompdfService->getEducationalProcessPdf($product);
            } catch (LoaderError $e) {
                $this->logger->error($e->getMessage());
            } catch (RuntimeError $e) {
                $this->logger->error($e->getMessage());
            } catch (SyntaxError $e) {
                $this->logger->error($e->getMessage());
            }
            return new Response();
        } else {
            //message flash
            $this->addFlash('notice', "<span style='color:green;'>
            <strong>
            <i class='fa fa-exclamation-circle'></i> Veuillez créer un ou plusieurs jours de formation dans l'onglet <span style='color: dodgerblue;'>Jours de formation</span> afin de pouvoir générer la fiche de déroulé pédagogique.
            </strong></span>");
            $url = $this->crudUrlGenerator->build()
                ->setController(ProductCrudController::class)
                ->setAction(ACTION::DETAIL)
                ->generateUrl();
            return $this->redirect($url);
        }
    }

    /**
     * @param AdminContext $adminContext
     * @return BinaryFileResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getBooklet(AdminContext $adminContext)
    {
        /** @var Product $product */
        $product = $adminContext->getEntity()->getInstance();
        $publicResourcesFolderPath = $this->kernel->getProjectDir() . '/public/uploads/documents/';
        $filename = $product->getDocumentUrlByType('Livret du stagiaire');

        if ("" !== $filename) {
            /* https://ourcodeworld.com/articles/read/329/how-to-send-a-file-as-response-from-a-controller-in-symfony-3 */
            // This should return the file to the browser as response
            $response = new BinaryFileResponse($publicResourcesFolderPath . $filename);

            // To generate a file download, you need the mimetype of the file
            $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

            // Set the mimetype with the guesser or manually
            if ($mimeTypeGuesser->isGuesserSupported()) {
                // Guess the mimetype of the file according to the extension of the file
                $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($publicResourcesFolderPath . $filename));
            } else {
                // Set the mimetype of the file manually, in this case for a text file is text/plain
                $response->headers->set('Content-Type', 'text/plain');
            }

            // Set content disposition inline of the file
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $filename
            );

            return $response;
        } else {
            //message flash
            $this->addFlash('notice', "<span style='color:green;'>
                <strong>
                <i class='fa fa-exclamation-circle'></i> Le <span style='color: dodgerblue;'>livret de formation du stagiaire</span> n'a pas encore été ajouté à cette formation. Veuillez le charger par le formulaire de modification de la formation.
                </strong></span>");
            $url = $this->crudUrlGenerator->build()
                ->setController(ProductCrudController::class)
                ->setAction(ACTION::DETAIL)
                ->generateUrl();

            return $this->redirect($url);
        }
    }

    public function getEntity()
    {
        $entity = $this->context->getContext()->getEntity();
        return $entity->getInstance();
    }

    public function configureFields(string $pageName): iterable
    {

        //Référence produit
        $lastProduct = $this->productRepository->findOneBy([], ['id' => 'DESC']);
        if (!$lastProduct) {
            $nextProductReference = 'F000001';
        } else {
            $lastChrono = substr($lastProduct->getReference(), -6);
            $nextProductReference = 'F' . sprintf("%06d", $lastChrono + 1);
        }

        return [
            yield IdField::new('id')->hideOnForm(),
            yield DateTimeField::new('createdAt', $this->translator->trans('admin.product.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
//            'attr' => ['disabled' => true,]
            ])->hideOnForm(),
            yield TextField::new('reference', $this->translator->trans('admin.product.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextProductReference
            ])->hideOnForm(),
            yield TextField::new('slug', $this->translator->trans('admin.product.form.slug.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.reference.placeholder',
                    [], 'admin')],
            ])->hideOnIndex(),
            yield AssociationField::new('category', $this->translator->trans('admin.product.form.category.label',
                [], 'admin'))->setFormTypeOptions([
                'placeholder' => $this->translator->trans('admin.product.form.category.placeholder',
                    [], 'admin'),
            ]),
            yield TextField::new('name', $this->translator->trans('admin.product.form.name.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.name.placeholder',
                    [], 'admin'), 'autofocus' => true]
            ]),

            yield TextField::new('subtitle', $this->translator->trans('admin.product.form.subtitle.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.subtitle.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextareaField::new('description', $this->translator->trans('admin.product.form.description.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.description.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),

            yield IntegerField::new('hourDuration', $this->translator->trans('admin.product.form.hourDuration.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.hourDuration.placeholder',
                    [], 'admin')]
            ])->setTextAlign('right')->hideOnIndex(),

            yield IntegerField::new('maxAdherents', $this->translator->trans('admin.product.form.maxAdherents.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.maxAdherents.placeholder',
                    [], 'admin')]
            ])->setTextAlign('right')->hideOnIndex(),

            yield BooleanField::new('isBest', $this->translator->trans('admin.product.form.isBest.label',
                [], 'admin')),

            yield BooleanField::new('isAvailable', $this->translator->trans('admin.product.form.isAvailable.label',
                [], 'admin')),

            yield MoneyField::new('price', $this->translator->trans('admin.product.form.price.label',
                [], 'admin'))->setCurrency('EUR')
                ->setFormTypeOptions([
                    'attr' => ['placeholder' => $this->translator->trans('admin.product.form.price.placeholder',
                        [], 'admin')]
                ]),

            yield BooleanField::new('isPriceShow', $this->translator->trans('admin.product.form.isPriceShow.label',
                [], 'admin')),

            yield BooleanField::new('isEnabled', $this->translator->trans('admin.product.form.isEnabled.label',
                [], 'admin')),

            yield CollectionField::new('productDetails', 'Détail de formation')
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(ProductDetailType::class)
                ->setFormTypeOptions([
                    'by_reference' => false
                ])->onlyOnForms(),

            yield ArrayField::new('getProductDetails', $this->translator->trans('admin.product.form.getProductDetails.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/product/detailProductDetails.html.twig'),

            //FICHE PEDAGOGIQUE
            yield TextField::new('audience', $this->translator->trans('admin.product.form.audience.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.audience.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextField::new('effective', $this->translator->trans('admin.product.form.effective.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.effective.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            Yield AdminCkEditorField::new('goal', $this->translator->trans('admin.product.form.goal.label',
                [], 'admin'))
                ->setCustomOptions(
                    ['config' => ['placeholder' => $this->translator->trans('admin.product.form.goal.placeholder',
                        [], 'admin')]]
                )->hideOnIndex(),
            Yield AdminCkEditorField::new('program', $this->translator->trans('admin.product.form.program.label',
                [], 'admin'))
                ->setCustomOptions(
                    ['config' => ['placeholder' => $this->translator->trans('admin.product.form.program.placeholder',
                        [], 'admin')]]
                )->hideOnIndex(),
            Yield AdminCkEditorField::new('method', $this->translator->trans('admin.product.form.method.label',
                [], 'admin'))
                ->setCustomOptions(
                    ['config' => ['placeholder' => $this->translator->trans('admin.product.form.method.placeholder',
                        [], 'admin')]]
                )->hideOnIndex(),
            yield TextField::new('means', $this->translator->trans('admin.product.form.means.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.means.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextField::new('evaluation', $this->translator->trans('admin.product.form.evaluation.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.evaluation.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextField::new('validation', $this->translator->trans('admin.product.form.validation.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.validation.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextField::new('duration', $this->translator->trans('admin.product.form.duration.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.duration.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextField::new('planification', $this->translator->trans('admin.product.form.planification.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.planification.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            yield TextField::new('location', $this->translator->trans('admin.product.form.location.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.product.form.location.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),

            yield CollectionField::new('productDays', 'Jours de formation')
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(ProductDayType::class)
                ->setFormTypeOptions([
                    'by_reference' => false,
                ])->onlyOnForms(),

            yield ArrayField::new('getProductDays', $this->translator->trans('admin.product.form.getProductDays.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/product/detailProductDays.html.twig'),

            //Collection DOCUMENTS
            yield CollectionField::new('documents', $this->translator->trans('admin.product.form.documents.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(DocumentType::class)
                ->setFormTypeOptions(['by_reference' => false]),
        ];

    }
}
