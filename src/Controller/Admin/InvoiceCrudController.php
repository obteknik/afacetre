<?php

namespace App\Controller\Admin;

use App\Entity\Invoice;
use App\Entity\PaymentCondition;
use App\Form\PaymentType;
use App\Form\ProductType;
use App\Repository\ClientRepository;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PaymentConditionRepository;
use App\Services\AppService;
use App\Services\DompdfService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class InvoiceCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var PaymentConditionRepository
     */
    private $paymentConditionRepository;
    /**
     * @var DompdfService
     */
    private $dompdfService;
    /**
     * @var AppService
     */
    private $appService;

    /**
     * CarrierCrudController constructor.
     * @param TranslatorInterface $translator
     * @param TokenStorageInterface $tokenStorage
     * @param DompdfService $dompdfService
     * @param LoggerInterface $logger
     * @param OrganizationRepository $organizationRepository
     * @param InvoiceRepository $invoiceRepository
     * @param ClientRepository $clientRepository
     * @param PaymentConditionRepository $paymentConditionRepository
     * @param AppService $appService
     */
    public function __construct(TranslatorInterface $translator, TokenStorageInterface $tokenStorage,
                                DompdfService $dompdfService, LoggerInterface $logger,
                                OrganizationRepository $organizationRepository,
                                InvoiceRepository $invoiceRepository,
                                ClientRepository $clientRepository,
                                PaymentConditionRepository $paymentConditionRepository, AppService $appService)
    {
        $this->translator = $translator;
        $this->tokenStorage = $tokenStorage;
        $this->logger = $logger;
        $this->organizationRepository = $organizationRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->clientRepository = $clientRepository;
        $this->paymentConditionRepository = $paymentConditionRepository;
        $this->dompdfService = $dompdfService;
        $this->appService = $appService;
    }

    public static function getEntityFqcn(): string
    {
        return Invoice::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-file-pdf"></i> ' . $this->translator->trans('admin.invoice.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function(Invoice $invoice){
            return (string) '<i class="fa fa-file-pdf"></i> '
                . $this->translator->trans('admin.invoice.detail.headTitle',[], 'admin').': '
                .$invoice->getReference(). ' du '.$invoice->getCreatedAt()->format('d/m/Y').' de '.
                number_format(($invoice->getAmountHT()/100), 2,',','.').' €';
        });
        $crud->setPageTitle('new', '<i class="fa fa-file-pdf"></i> ' . $this->translator->trans('admin.invoice.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-file-pdf"></i> ' . $this->translator->trans('admin.invoice.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);

        $crud->setSearchFields(['reference', 'client', 'amountHT']);
        $crud->overrideTemplate('crud/index', 'admin/invoice/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/invoice/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/invoice/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/invoice/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        // in PHP 7.4 and newer you can use arrow functions
        // ->update(Crud::PAGE_INDEX, Action::NEW,
        //     fn (Action $action) => $action->setIcon('fa fa-file-alt')->setLabel(false))

        $generatePDF = Action::new('generatePDF', '', 'fas fa-file-pdf')
            ->linkToCrudAction('generatePDF');
//            ->displayIf(static function ($entity) {
//                return $entity->isPublished();
//            }
//            )
        ;

        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->add(Crud::PAGE_INDEX, $generatePDF)
            ->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE, 'generatePDF'])
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.invoice.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.invoice.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.invoice.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, $generatePDF, function (Action $action) {
                return $action
                    ->setIcon('fa fa-file-pdf')
                    ->setLabel($this->translator->trans('admin.action.generatePdf.label', [], 'admin'));
            });
    }

    /**
     * Génération de la facture en PDF
     * @param AdminContext $context
     * @return string
     */
    public function generatePDF(AdminContext $context)
    {
        /** @var Invoice $invoice */
        $invoice = $context->getEntity()->getInstance();

        try {
            $this->dompdfService->getInvoicePdf($invoice);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    public function configureFields(string $pageName): iterable
    {
        $organization = $this->organizationRepository->find(1);
        $nextInvoiceReference = $this->appService->getNextReference($organization, 'invoice');

        return [
            Yield IdField::new('id')->hideOnForm(),
            Yield AssociationField::new('organization', $this->translator->trans('admin.invoice.form.organization.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
                'data' => $organization,
            ]),

            Yield AssociationField::new('fiscalYear', $this->translator->trans('admin.invoice.form.fiscalYear.label',
                [], 'admin')),

            Yield DateTimeField::new('createdAt', $this->translator->trans('admin.invoice.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,]
            ])->hideOnForm(),
            Yield TextField::new('reference', $this->translator->trans('admin.invoice.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true, 'placeholder' => $this->translator->trans('admin.invoice.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextInvoiceReference,
            ])->hideOnForm(),

            Yield AssociationField::new('client', $this->translator->trans('admin.invoice.form.client.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.invoice.form.client.label',
                    [], 'admin'),],
            ]),

            Yield AssociationField::new('patient', $this->translator->trans('admin.invoice.form.patient.label',
                [], 'admin'))->setFormTypeOptions([
            ]),

            yield AssociationField::new('products', $this->translator->trans('admin.invoice.form.products.label',
                [], 'admin')),
            yield AssociationField::new('quote', $this->translator->trans('admin.invoice.form.quote.label',
                [], 'admin')),

            Yield AssociationField::new('contract', $this->translator->trans('admin.invoice.form.contract.label',
                [], 'admin'))->setFormTypeOptions([
            ])->hideOnIndex(),

            Yield AssociationField::new('paymentCondition', $this->translator->trans('admin.invoice.form.paymentCondition.label',
                [], 'admin'))->setFormTypeOptions([
            ])->hideOnIndex(),
            yield CollectionField::new('payments', $this->translator->trans('admin.contract.form.payments.label',
                [], 'admin'))
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(PaymentType::class)
                ->setFormTypeOptions([
                    'by_reference' => false
                ])->onlyWhenUpdating(),
            Yield MoneyField::new('amountHT', $this->translator->trans('admin.invoice.form.amountHT.label',
                [], 'admin'))->setCurrency('EUR'),

            yield ArrayField::new('getInvoiceDetails', $this->translator->trans('admin.invoice.form.getInvoiceDetails.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/invoice/detailInvoiceDetails.html.twig'),

        ];
    }
}
