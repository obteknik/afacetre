<?php

namespace App\Controller\Admin;

use App\Classe\AdminCkEditorField;
use App\Entity\Adherent;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class adherentCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AdherentCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $manager
     * @param CrudUrlGenerator $crudUrlGenerator
     */
    public function __construct(TranslatorInterface $translator, EntityManagerInterface $manager,
                                CrudUrlGenerator $crudUrlGenerator)
    {
        $this->translator = $translator;
        $this->manager = $manager;
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Adherent::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');

        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.adherent.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.adherent.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.adherent.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.adherent.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->overrideTemplate('crud/index', 'admin/adherent/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/adherent/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/adherent/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/adherent/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        $activeUserAccount = Action::new('activeUserAccount', 'Activation du compte utilisateur', 'fas fa-user-lock')
            ->linkToCrudAction('activeUserAccount');

        return $actions
            ->add('detail', $activeUserAccount)
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.adherent.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.adherent.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.adherent.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function activeUserAccount(AdminContext $context)
    {
        /** @var adherent $adherent */
        $adherent = $context->getEntity()->getInstance();

//        dump($adherent);

        if ($adherent->getIsActive()) {
            //si intervenant ACTIF alors si je clique sur cette action, je souhaite activer son compte utiisateur
            //donc je passe isActive à true dans User
            $adherent->getUser()->setIsActive(true);
            $adherent->getUser()->setActivatedAt(new \DateTime());
            $this->manager->persist($adherent);
            $this->manager->flush();

            $this->addFlash('notice', "<span style='color:green;'><strong>
            Activation du compte utilisateur affectuée pour <u>" . $adherent->getFirstname() . ' ' .
                $adherent->getLastname() . "</u></strong></span>");
        }

        $url = $this->crudUrlGenerator->build()
            ->setController(adherentCrudController::class)
            ->setAction(ACTION::DETAIL)
            ->generateUrl();

        return $this->redirect($url);

    }

    public function configureFields(string $pageName): iterable
    {

        return [

            yield IdField::new('id')->hideOnForm(),
            yield DateTimeField::new('createdAt', $this->translator->trans('admin.invoice.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
            ])->hideOnForm(),
            yield BooleanField::new('isActive', "Actif"),

            yield ChoiceField::new('civility', $this->translator->trans('admin.adherent.form.civility.label',
                [], 'admin'))->setChoices([
                Adherent::CIVILITY_MISTER => Adherent::CIVILITY_MISTER,
                Adherent::CIVILITY_MISS => Adherent::CIVILITY_MISS,
            ])->onlyOnForms(),

            yield TextField::new('getFullname', $this->translator->trans('admin.patient.form.getFullname.label',
                [], 'admin'))->hideOnForm(),

            yield TextField::new('firstname', $this->translator->trans('admin.adherent.form.firstname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.adherent.form.firstname.placeholder',
                    [], 'admin'), 'autofocus' => true]
            ])->onlyOnForms(),
            yield TextField::new('lastname', $this->translator->trans('admin.adherent.form.lastname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.adherent.form.lastname.placeholder',
                    [], 'admin')]
            ])->onlyOnForms(),
            yield TextField::new('address', $this->translator->trans('admin.adherent.form.address.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.adherent.form.address.placeholder',
                    [], 'admin')]
            ]),
            yield TextField::new('phoneNumber', $this->translator->trans('admin.adherent.form.phoneNumber.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.adherent.form.phoneNumber.placeholder',
                    [], 'admin')]
            ]),
            yield EmailField::new('email', $this->translator->trans('admin.adherent.form.email.label',
                [], 'admin')),

//            yield ImageField::new('illustration', $this->translator->trans('admin.adherent.form.illustration.label',
//                [], 'admin'))
//                ->setBasePath('uploads/')
//                ->setUploadDir('public/uploads')
//                ->setUploadedFileNamePattern('[randomhash].[extension]')
//                ->setRequired(false),

            yield AdminCkEditorField::new('description', $this->translator->trans('admin.adherent.form.description.label',
                [], 'admin'))
                ->setCustomOptions(['config' => ['placeholder' => 'Contenu de la page...']])->hideOnIndex(),

            yield AssociationField::new('disciplines', $this->translator->trans('admin.adherent.form.disciplines.label',
                [], 'admin'))->hideOnForm()->hideOnIndex(),

        ];

    }
}
