<?php

namespace App\Controller\Admin;

use App\Entity\Workflow;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class WorkflowCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Workflow::class;
    }

    public function configureAssets(Assets $assets): Assets
    {
        return Assets::new()
            ->addJsFile('/assets/libs/spectrum-master/spectrum-master/spectrum.js')
            ->addCssFile('/assets/libs/spectrum-master/spectrum-master/spectrum.css');
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-list"></i> ' . $this->translator->trans('admin.workflow.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function (Workflow $workflow) {
            return (string)'<i class="fa fa-list"></i> '
                . $this->translator->trans('admin.workflow.detail.headTitle', [], 'admin') . ': '
                . $workflow->getName();
        });
        $crud->setPageTitle('new', '<i class="fa fa-list"></i> ' . $this->translator->trans('admin.workflow.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-list"></i> ' . $this->translator->trans('admin.workflow.edit.headTitle',
                [], 'admin'));
        $crud->overrideTemplate('crud/index', 'admin/workflow/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/workflow/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/workflow/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/workflow/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.workflow.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.workflow.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.workflow.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            yield IdField::new('id')->onlyOnIndex(),
        yield TextField::new('name', $this->translator->trans('admin.workflow.form.name.label',
            [], 'admin')),

        yield TextField::new('eventBgColor', $this->translator->trans('admin.workflow.form.eventBgColor.label',
            [], 'admin'))->setTemplatePath('admin/workflow/_bgColor.html.twig'),

        yield TextField::new('eventFtColor', $this->translator->trans('admin.workflow.form.eventFtColor.label',
            [], 'admin'))->setTemplatePath('admin/workflow/_bgColor.html.twig'),

        yield TextField::new('pdfFont', $this->translator->trans('admin.workflow.form.fontPdf.label',
            [], 'admin')),
        ];
    }

}
