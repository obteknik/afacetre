<?php

namespace App\Controller\Admin;

use App\Entity\Adherent;
use App\Entity\Category;
use App\Entity\Charge;
use App\Entity\ChargeType;
use App\Entity\Client;
use App\Entity\ClientContact;
use App\Entity\ClientContactFunction;
use App\Entity\Connexion;
use App\Entity\Contract;
use App\Entity\Discipline;
use App\Entity\Document;
use App\Entity\DocumentType;
use App\Entity\Event;
use App\Entity\EventLocation;
use App\Entity\EventObject;
use App\Entity\EventScale;
use App\Entity\EventType;
use App\Entity\FamilySituation;
use App\Entity\FiscalYear;
use App\Entity\Invoice;
use App\Entity\Patient;
use App\Entity\PatientSheet;
use App\Entity\PatientType;
use App\Entity\Payment;
use App\Entity\PaymentCondition;
use App\Entity\PaymentType;
use App\Entity\Person;
use App\Entity\Quotation;
use App\Entity\Quote;
use App\Entity\Order;
use App\Entity\Organization;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Timeslot;
use App\Entity\User;
use App\Entity\Workflow;
use App\Repository\ClientRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PatientRepository;
use App\Repository\PaymentConditionRepository;
use App\Repository\PersonRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DashboardAminController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class DashboardAminController extends AbstractDashboardController
{
    private $params;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var PaymentConditionRepository
     */
    private $paymentConditionRepository;
    /**
     * @var PersonRepository
     */
    private $personRepository;
    /**
     * @var PatientRepository
     */
    private $patientRepository;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    /**
     * DashboardController constructor.
     * @param ParameterBagInterface $params
     * @param TranslatorInterface $translator
     * @param ClientRepository $clientRepository
     * @param PaymentConditionRepository $paymentConditionRepository
     * @param PersonRepository $personRepository
     * @param PatientRepository $patientRepository
     * @param OrganizationRepository $organizationRepository
     */
    public function __construct(ParameterBagInterface $params, TranslatorInterface $translator,
                                ClientRepository $clientRepository,
                                PaymentConditionRepository $paymentConditionRepository,
                                PersonRepository $personRepository, PatientRepository $patientRepository,
                                OrganizationRepository $organizationRepository)
    {
        $this->params = $params;
        $this->translator = $translator;
        $this->clientRepository = $clientRepository;
        $this->paymentConditionRepository = $paymentConditionRepository;
        $this->personRepository = $personRepository;
        $this->patientRepository = $patientRepository;
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * Dashboard SUPER ADMIN
     * @Route("/my-admin", name="my_admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img width="80px" src="/assets/img/logo/signature.jpg"> <span class="text-small">' . $this->params->get('app.name') . '</span>')
            ->setTranslationDomain('superadmin');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('assets/css/admin.css')
            ->addCssFile('assets/css/bs-switches.css')
            ->addJsFile('assets/js/form-type-collection.js')
            ->addJsFile('assets/js/scripts.js');
    }

    public function configureMenuItems(): iterable
    {

        //TABLEAU DE BORD
        yield MenuItem::linktoDashboard($this->translator->trans('admin.sidebar.menu.dashboard.label',
            [], 'admin'), 'fas fa-chart-bar');

        //ALL ORGANIZATIONS + ADD AN ORGANIZATION
        yield MenuItem::section('Organisations');
        yield MenuItem::linkToCrud('Liste des organisations', 'far fa-clock', Organization::class)
            ->setAction('index');
        yield MenuItem::linkToCrud('Créer une organisation', 'fa fa-plus-circle', Organization::class)
            ->setAction('new');

        //SETTINGS
        yield MenuItem::section('Processsus');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.workflow.label',
            [], 'admin'), 'fa fa-list', Workflow::class)
            ->setAction('index');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.addWorkflow.label',
            [], 'admin'), 'fa fa-plus-circle', Workflow::class)
            ->setAction('new');

    }

}
