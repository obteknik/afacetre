<?php

namespace App\Controller\Admin;

use App\Entity\Adherent;
use App\Entity\Category;
use App\Entity\Charge;
use App\Entity\ChargeType;
use App\Entity\Client;
use App\Entity\ClientContact;
use App\Entity\ClientContactFunction;
use App\Entity\Connexion;
use App\Entity\Contract;
use App\Entity\Discipline;
use App\Entity\Document;
use App\Entity\DocumentType;
use App\Entity\Event;
use App\Entity\EventLocation;
use App\Entity\EventObject;
use App\Entity\EventScale;
use App\Entity\EventType;
use App\Entity\FamilySituation;
use App\Entity\FiscalYear;
use App\Entity\Invoice;
use App\Entity\Patient;
use App\Entity\PatientSheet;
use App\Entity\PatientType;
use App\Entity\Payment;
use App\Entity\PaymentCondition;
use App\Entity\PaymentType;
use App\Entity\Person;
use App\Entity\Quotation;
use App\Entity\Quote;
use App\Entity\Order;
use App\Entity\Organization;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Timeslot;
use App\Entity\User;
use App\Repository\ClientRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PatientRepository;
use App\Repository\PaymentConditionRepository;
use App\Repository\PersonRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class DashboardController extends AbstractDashboardController
{
    private $params;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var PaymentConditionRepository
     */
    private $paymentConditionRepository;
    /**
     * @var PersonRepository
     */
    private $personRepository;
    /**
     * @var PatientRepository
     */
    private $patientRepository;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    /**
     * DashboardController constructor.
     * @param ParameterBagInterface $params
     * @param TranslatorInterface $translator
     * @param ClientRepository $clientRepository
     * @param PaymentConditionRepository $paymentConditionRepository
     * @param PersonRepository $personRepository
     * @param PatientRepository $patientRepository
     * @param OrganizationRepository $organizationRepository
     */
    public function __construct(ParameterBagInterface $params, TranslatorInterface $translator,
                                ClientRepository $clientRepository,
                                PaymentConditionRepository $paymentConditionRepository,
                                PersonRepository $personRepository, PatientRepository $patientRepository,
                                OrganizationRepository $organizationRepository)
    {
        $this->params = $params;
        $this->translator = $translator;
        $this->clientRepository = $clientRepository;
        $this->paymentConditionRepository = $paymentConditionRepository;
        $this->personRepository = $personRepository;
        $this->patientRepository = $patientRepository;
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * @Route("/my-access", name="my_access")
     */
    public function index(): Response
    {

        // *** Homepage Dashboard ***
        return parent::index();
        // redirect to some CRUD controller
//        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
//        return $this->redirect($routeBuilder->setController(OrderCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img width="80px" src="/assets/img/logo/signature.jpg"> <span class="text-small">' . $this->params->get('app.name') . '</span>')
            ->setTranslationDomain('admin');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('assets/css/admin.css')
            ->addCssFile('assets/css/bs-switches.css')
            ->addJsFile('assets/js/form-type-collection.js')
            ->addJsFile('assets/js/scripts.js');
    }

    public function configureMenuItems(): iterable
    {

        $organization = $this->organizationRepository->find(1);
        $workflows = $organization->getWorkflows();
        $names = [];
        foreach ($workflows as $w){
            $names[] = $w->getName();
        }

//        yield MenuItem::linktoRoute('Site vitrine', 'fas fa-home', 'home');

        //TABLEAU DE BORD
        yield MenuItem::linktoDashboard($this->translator->trans('admin.sidebar.menu.admindashboard.label',
            [], 'admin'), 'fas fa-chart-bar');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            yield MenuItem::linktoRoute($this->translator->trans('admin.sidebar.menu.superadmindashboard.label',
                [], 'admin'), 'fas fa-chart-bar', 'my_admin');
        }

        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.information.label',
            [], 'admin'), 'fa fa-home', Organization::class)
            ->setAction('detail')->setEntityId(1);

        //SECURITE
        $securitySubItems = [
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.user.label',
                [], 'admin'), 'fa fa-user-lock', User::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.connexion.label',
                [], 'admin'), 'fa fa-link', Connexion::class),
        ];
        yield MenuItem::subMenu('Sécurité', 'fa fa-lock')->setSubItems($securitySubItems);

        //COMPTABILITE
        $comptabilitySubItems = [
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.fiscalYear.label',
                [], 'admin'), 'fa fa-home', FiscalYear::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.charge.label',
                [], 'admin'), 'fa fa-euro', Charge::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.payment.label',
                [], 'admin'), 'fa fa-euro', Payment::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.quote.label',
                [], 'admin'), 'fa fa-file-pdf', Quote::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.invoice.label',
                [], 'admin'), 'fa fa-file-pdf', Invoice::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.chargeType.label',
                [], 'admin'), 'fa fa-cogs', ChargeType::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.paymentType.label',
                [], 'admin'), 'fa fa-cogs', PaymentType::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.paymentCondition.label',
                [], 'admin'), 'fa fa-cogs', PaymentCondition::class),
        ];
        yield MenuItem::subMenu('Comptabilité', 'fa fa-euro')->setSubItems($comptabilitySubItems);

        // ***** AGENDA *****
        yield MenuItem::section('Agenda');
//        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.event.label',
//            [], 'admin'), 'far fa-clock', Event::class);
//        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.addEvent.label',
//            [], 'admin'), 'fa fa-plus-circle', Event::class)
//            ->setAction('new');
        yield MenuItem::linktoRoute($this->translator->trans('admin.sidebar.menu.agenda.label',
            [], 'admin'), 'fas fa-calendar-alt', 'events', ['token' => $this->getUser()->getAgendaToken()])
            ->setLinkTarget('_blank');

        // Paramétrage AGENDA
        yield MenuItem::subMenu('Paramétrage des rendez-vous', 'fa fa-cogs')->setSubItems([
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.eventType.label',
                [], 'admin'), 'fa fa-clock', EventType::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.eventObject.label',
                [], 'admin'), 'fa fa-edit', EventObject::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.eventLocation.label',
                [], 'admin'), 'fa fa-map-marker', EventLocation::class),
        ]);

        // ***** ACTIVITE CABINET *****
        if(in_array("Parcours Cabinet", $names)){
            yield MenuItem::section('Activité cabinet');
            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.patient.label',
                [], 'admin'), 'fa fa-users', Patient::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.addPatient.label',
                [], 'admin'), 'fa fa-plus-circle', Patient::class)
                ->setAction('new');
//        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.addPatientSheet.label',
//            [], 'admin'), 'fas fa-plus-circle', PatientSheet::class)->setAction('new');

            // Paramétrage Activité CABINET
            yield MenuItem::subMenu('Paramétrage Cabinet', 'fa fa-cogs')->setSubItems([
                MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.patientType.label',
                    [], 'admin'), 'fa fa-cogs', PatientType::class),
                MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.eventScale.label',
                    [], 'admin'), 'fa fa-euro', EventScale::class),
                MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.familySituation.label',
                    [], 'admin'), 'fa fa-cogs', FamilySituation::class),
            ]);
        }


        // ***** ACTIVITE FORMATION *****
        if(in_array("Parcours Contrat", $names)) {
            yield MenuItem::section('Activité formation');
            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.client.label',
                [], 'admin'), 'fa fa-users', Client::class);
            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.clientContact.label',
                [], 'admin'), 'fa fa-users', ClientContact::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.product.label',
                [], 'admin'), 'fa fa-graduation-cap', Product::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.contract.label',
                [], 'admin'), 'fa fa-pencil', Contract::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.document.label',
                [], 'admin'), 'fa fa-file', Document::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.order.label',
                [], 'admin'), 'fa fa-shopping-cart', Order::class);

            // Paramétrage Activité FORMATION
            yield MenuItem::subMenu('Paramétrage Formation', 'fa fa-cogs')->setSubItems([
                MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.clientContactFunction.label',
                    [], 'admin'), 'fa fa-edit', ClientContactFunction::class),
                MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.category.label',
                    [], 'admin'), 'fa fa-list', Category::class),
                MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.documentType.label',
                    [], 'admin'), 'fa fa-file', DocumentType::class),
            ]);
        }

        // ***** ACTIVITE ATELIERS *****
        if(in_array("Parcours Atelier", $names)){
            yield MenuItem::section('Activité ateliers');
            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.person.label',
                [], 'admin'), 'fa fa-users', Person::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.discipline.label',
                [], 'admin'), 'fa fa-edit', Discipline::class);

            yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.adherent.label',
                [], 'admin'), 'fa fa-users', Adherent::class);

            //Lien  CRENEAUX/RENDEZ-VOUS ssi au-moint 1 intervenant créé
            if (count($this->personRepository->findAll()) > 0) {
                yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.timeslot.label',
                    [], 'admin'), 'far fa-clock', Timeslot::class);
            }
        }

        // ***** BLOG & CITATIONS *****
        yield MenuItem::section('Blog & citations');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.post.label',
            [], 'admin'), 'fas fa-edit', Post::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.addPost.label',
            [], 'admin'), 'fas fa-plus-circle', Post::class)->setAction('new');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.quotation.label',
            [], 'admin'), 'fa fa-pen-fancy', Quotation::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.addQuotation.label',
            [], 'admin'), 'fas fa-plus-circle', Quotation::class)->setAction('new');

        //conception
        /*
        yield MenuItem::section('Conception');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.slider.label',
            [], 'admin'), 'fa fa-desktop', Slider::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.page.label',
            [], 'admin'), 'fas fa-file', Page::class);
        */
    }

}
