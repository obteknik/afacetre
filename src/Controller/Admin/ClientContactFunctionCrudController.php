<?php

namespace App\Controller\Admin;

use App\Entity\ClientContactFunction;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ClientContactFunctionCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ClientContactFunctionCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return ClientContactFunction::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-edit"></i> ' . $this->translator->trans('admin.clientContactFunction.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-edit"></i> ' . $this->translator->trans('admin.clientContactFunction.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-edit"></i> ' . $this->translator->trans('admin.clientContactFunction.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-edit"></i> ' . $this->translator->trans('admin.clientContactFunction.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/clientContactFunction/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/clientContactFunction/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/clientContactFunction/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/clientContactFunction/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.charge.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.charge.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.charge.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id')->hideOnForm();
        $createdAt = DateTimeField::new('createdAt', $this->translator->trans('admin.clientContactFunction.form.createdAt.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['disabled' => true,],
        ])->hideOnForm();
        $name = TextField::new('name', $this->translator->trans('admin.clientContactFunction.form.name.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['autofocus' => true, 'placeholder' => $this->translator->trans('admin.clientContactFunction.form.name.placeholder',
                [], 'admin'),'autofocus' =>true, 'minlength' => 15, 'maxlength' => 30],
        ])->setHelp('Entre 15 et 30 caractères');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $createdAt, $name];
        }

        return [  $createdAt, $name ];
    }
}
