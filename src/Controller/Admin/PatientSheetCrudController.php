<?php

namespace App\Controller\Admin;

use App\Entity\PatientSheet;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use Symfony\Contracts\Translation\TranslatorInterface;

class PatientSheetCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * patientSheetCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return PatientSheet::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.patientSheet.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.patientSheet.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.patientSheet.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.patientSheet.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/patientSheet/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/patientSheet/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/patientSheet/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/patientSheet/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.patientSheet.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.patientSheet.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.patientSheet.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            yield IdField::new('id')->hideOnForm(),
            yield DateTimeField::new('createdAt', $this->translator->trans('admin.patientSheet.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
            ])->hideOnForm(),
            yield AssociationField::new('patient', $this->translator->trans('admin.patientSheet.form.patient.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patientSheet.form.patient.label',
                    [], 'admin'),],
            ]),

        ];
    }

}
