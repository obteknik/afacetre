<?php

namespace App\Controller\Admin;

use App\Entity\Slider;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class SliderCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * SliderCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Slider::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-desktop"></i> ' . $this->translator->trans('admin.slider.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-desktop"></i> ' . $this->translator->trans('admin.slider.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-desktop"></i> ' . $this->translator->trans('admin.slider.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-desktop"></i> ' . $this->translator->trans('admin.slider.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->overrideTemplate('crud/index', 'admin/slider/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/slider/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/slider/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/slider/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.slider.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.slider.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.slider.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title', "Titre du slider")->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => 'Titre du slider...', 'autofocus' => true]
                ]
            ),
            TextareaField::new('content', "Contenu du slider")->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => 'Contenu du slider...']
                ]
            ),
            TextField::new('btnTitle', "Titre du bouton")->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => 'Titre du bouton...']
                ]
            ),
            ChoiceField::new('alignTitle', "Position du titre du bouton")
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => 'Position du titre du bouton...']
                    ]
                )->setChoices([
                    "Left" => "left",
                    "Center" => "center",
                    "Right" => "right",
                ]),
            TextField::new('btnUrl', "Url de destination du bouton")
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => 'Url de destination du bouton...']
                    ]),
            ImageField::new('illustration')
                ->setBasePath('uploads/')->setFormTypeOptions(['mapped' => false, 'required' => false]),
            BooleanField::new('isEnabled', 'Activer'),
        ];
    }
}
