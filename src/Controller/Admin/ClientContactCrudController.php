<?php

namespace App\Controller\Admin;

use App\Entity\ClientContact;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ClientContactCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ClientContactCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    public static function getEntityFqcn(): string
    {
        return ClientContact::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.clientContact.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.clientContact.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.clientContact.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.clientContact.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/clientContact/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/clientContact/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/clientContact/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/clientContact/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.clientContact.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.clientContact.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.clientContact.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id')->hideOnForm();
        $createdAt = DateTimeField::new('createdAt', $this->translator->trans('admin.clientContact.form.createdAt.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['disabled' => true,],
        ])->hideOnForm();
        $client = AssociationField::new('client', $this->translator->trans('admin.clientContact.form.client.label',
            [], 'admin'));
        $firstname = TextField::new('firstname', $this->translator->trans('admin.clientContact.form.firstname.label',
            [], 'admin'))->setFormTypeOptions(
            ['attr' => ['autofocus' => true,
                'placeholder' => $this->translator->trans('admin.clientContact.form.firstname.placeholder',
                    [], 'admin')]]);
        $lastname = TextField::new('lastname', $this->translator->trans('admin.clientContact.form.lastname.label',
            [], 'admin'))->setFormTypeOptions(
            ['attr' => [
                'placeholder' => $this->translator->trans('admin.clientContact.form.lastname.placeholder',
                    [], 'admin')]]);
        $phoneNumber = TextField::new('phoneNumber', $this->translator->trans('admin.clientContact.form.phoneNumber.label',
            [], 'admin'))->setFormTypeOptions(
            ['attr' => [
                'placeholder' => $this->translator->trans('admin.clientContact.form.phoneNumber.placeholder',
                    [], 'admin')]]);
        $function = AssociationField::new('function', $this->translator->trans('admin.clientContact.form.function.label',
            [], 'admin'));


        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $createdAt, $client, $firstname, $lastname, $phoneNumber, $function];
        }

        return [  $createdAt, $client, $firstname, $lastname, $phoneNumber, $function];
    }
}
