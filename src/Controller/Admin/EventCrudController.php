<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Form\CommentType;
use App\Form\DocumentType;
use App\Form\ExistingAdherentType;
use App\Form\PersonAdherentType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * eventCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Event::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.event.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function (Event $event) {
            return (string)'<i class="fa fa-calendar-alt"></i> '
                . $this->translator->trans('admin.event.detail.headTitle', [], 'admin') . ': '
                . $event->getStartAt()->format('d/m/Y').' au '.$event->getEndAt()->format('d/m/Y')
                .' de '.$event->getStartAt()->format('H:i'). ' à '.$event->getEndAt()->format('H:i');
        });
        $crud->setPageTitle('new', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.event.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.event.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['startAt', 'endAt']);
        $crud->overrideTemplate('crud/index', 'admin/event/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/event/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/event/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/event/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.event.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.event.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.event.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {

        return [

            yield IdField::new('id')->hideOnForm(),

            yield AssociationField::new('organization', $this->translator->trans('admin.event.form.organization.label',
                [], 'admin'))->onlyOnForms(),

            yield AssociationField::new('type', $this->translator->trans('admin.event.form.type.label',
                [], 'admin')),

            yield TextField::new('title', $this->translator->trans('admin.event.form.title.label',
                [], 'admin'))->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => $this->translator->trans('admin.event.form.title.placeholder',
                        [], 'admin'),
                        'autofocus' => true]
                ])->hideOnIndex(),

            yield AssociationField::new('object', $this->translator->trans('admin.event.form.object.label',
                [], 'admin')),

            yield AssociationField::new('location', $this->translator->trans('admin.event.form.location.label',
                [], 'admin'))->hideOnIndex(),

            yield DateTimeField::new('startAt', $this->translator->trans('admin.event.form.startAt.label',
                [], 'admin'))->hideOnIndex(),

            yield DateTimeField::new('endAt', $this->translator->trans('admin.event.form.endAt.label',
                [], 'admin')),

            yield BooleanField::new('isAllDay', $this->translator->trans('admin.event.form.isAllDay.label',
                [], 'admin')),

            yield BooleanField::new('isPeriodic', $this->translator->trans('admin.event.form.isPeriodic.label',
                [], 'admin')),

            yield AssociationField::new('person', $this->translator->trans('admin.event.form.person.label',
                [], 'admin')),

            yield AssociationField::new('patient', $this->translator->trans('admin.event.form.patient.label',
                [], 'admin')),

            yield AssociationField::new('contract', $this->translator->trans('admin.event.form.contract.label',
                [], 'admin')),

            yield CollectionField::new('adherents', $this->translator->trans('admin.event.form.adherents.label',
                [], 'admin'))
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(PersonAdherentType::class)
                ->setFormTypeOptions([
                    'mapped' => false,
                    'by_reference' => false
                ])->onlyOnForms(),

            yield CollectionField::new('existingAdherents', 'Adhérents existants')
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(ExistingAdherentType::class)
                ->setFormTypeOptions([
                    'mapped' => false,
                    'by_reference' => false
                ])->onlyOnForms(),

            yield CollectionField::new('comments', $this->translator->trans('admin.event.form.comments.label',
                [], 'admin'))
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(CommentType::class)
                ->setFormTypeOptions([
                    'by_reference' => false
                ])->onlyOnForms(),

            yield CollectionField::new('documents', $this->translator->trans('admin.event.form.documents.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(DocumentType::class)
                ->setFormTypeOptions(['by_reference' => false]),

            yield ArrayField::new('getAdherents', $this->translator->trans('admin.event.form.getAdherents.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/event/detailAdherents.html.twig'),

            yield ArrayField::new('getComments', $this->translator->trans('admin.event.form.getComments.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/event/detailComments.html.twig'),

        ];

    }
}