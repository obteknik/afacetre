<?php

namespace App\Controller\Admin;

use App\Entity\EventScale;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EventScaleCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventScaleCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * eventCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return EventScale::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.eventScale.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.eventScale.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.eventScale.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.eventScale.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['startAt', 'endAt']);
        $crud->overrideTemplate('crud/index', 'admin/eventScale/index.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/eventScale/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/eventScale/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.eventScale.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.eventScale.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.eventScale.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            yield IdField::new('id')->hideOnForm(),
            Yield DateTimeField::new('createdAt', $this->translator->trans('admin.eventScale.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,]
            ])->hideOnForm(),
            yield AssociationField::new('patientType', $this->translator->trans('admin.eventScale.form.patientType.label',
                [], 'admin')),
            yield MoneyField::new('firstEventPrice', $this->translator->trans('admin.eventScale.form.firstEventPrice.label',
                [], 'admin'))->setCurrency('EUR')
                ->setFormTypeOptions([
                    'attr' => ['placeholder' => $this->translator->trans('admin.eventScale.form.firstEventPrice.placeholder',
                        [], 'admin')]
                ]),
            yield MoneyField::new('otherEventPrice', $this->translator->trans('admin.eventScale.form.otherEventPrice.label',
                [], 'admin'))->setCurrency('EUR')
                ->setFormTypeOptions([
                    'attr' => ['placeholder' => $this->translator->trans('admin.eventScale.form.otherEventPrice.placeholder',
                        [], 'admin')]
                ]),
        ];
    }
}
