<?php

namespace App\Controller\Admin;

use App\Entity\EventLocation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventLocationCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * eventCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return EventLocation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventLocation.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventLocation.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventLocation.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-calendar-alt"></i> ' . $this->translator->trans('admin.eventLocation.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['startAt', 'endAt']);
        $crud->overrideTemplate('crud/index', 'admin/eventLocation/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/eventLocation/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/eventLocation/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/eventLocation/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.eventLocation.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.eventLocation.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.eventLocation.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id');
        $label = TextField::new('label', $this->translator->trans('admin.eventLocation.form.label.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['placeholder' => $this->translator->trans('admin.eventLocation.form.label.placeholder',
                [], 'admin')
            ]]);

        $address = TextField::new('address', $this->translator->trans('admin.eventLocation.form.address.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['placeholder' => $this->translator->trans('admin.eventLocation.form.address.placeholder',
                [], 'admin')
            ]]);

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $label, $address];
        }

        return [
            $label, $address,
        ];
    }
}
