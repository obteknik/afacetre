<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Form\ClientContactType;
use App\Form\EmployeeType;
use App\Form\PaymentType;
use App\Repository\ClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClientCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     * @param ClientRepository $clientRepository
     */
    public function __construct(TranslatorInterface $translator, ClientRepository $clientRepository)
    {
        $this->translator = $translator;
        $this->clientRepository = $clientRepository;
    }


    public static function getEntityFqcn(): string
    {
        return Client::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-users"></i> ' .
            $this->translator->trans('admin.client.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function (Client $client) {
            return (string)'<i class="fa fa-user"></i> '
                . $this->translator->trans('admin.client.detail.headTitle', [], 'admin') . ': '
                . $client->getReference() . ' - ' . $client->getCompanyName() . ' - ' . $client->getZipcode()
                . ' ' . $client->getCity();
        });
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.client.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.client.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['reference', 'companyName', 'firstname', 'lastname', 'city']);
        $crud->overrideTemplate('crud/index', 'admin/client/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/client/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/client/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/client/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.client.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.client.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.client.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        //référence client
        $lastClient = $this->clientRepository->findOneBy([], ['id' => 'DESC']);

        if (!$lastClient) {
            $nextClientReference = 'C000001';
        } else {
            $lastReference = $lastClient->getReference();
            $lastChrono = substr($lastReference, -6);

            $nextClientReference = 'C' . sprintf("%06d", (int)$lastChrono + 1);
        }

        return [
            yield IdField::new('id')->hideOnForm(),
            yield DateTimeField::new('createdAt', $this->translator->trans('admin.client.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
            ])->hideOnForm(),
            yield AssociationField::new('organization', 'Organisation')->onlyOnIndex(),
            yield ImageField::new('illustration', $this->translator->trans('admin.client.form.illustration.label',
                [], 'admin'))
                ->setBasePath('uploads/client/')
                ->setUploadDir('public/uploads/client')
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),
            yield TextField::new('companyName', $this->translator->trans('admin.client.form.companyName.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['autofocus' => true, 'placeholder' => $this->translator->trans('admin.client.form.companyName.placeholder',
                    [], 'admin')
                ]]),
            yield TextField::new('reference', $this->translator->trans('admin.client.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextClientReference,
            ])->hideOnForm(),
            yield TextField::new('street', $this->translator->trans('admin.client.form.street.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.street.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield TextField::new('zipcode', $this->translator->trans('admin.client.form.zipcode.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.zipcode.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield TextField::new('city', $this->translator->trans('admin.client.form.city.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.city.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield CountryField::new('country', $this->translator->trans('admin.client.form.country.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.country.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield TextField::new('fullAddress', $this->translator->trans('admin.client.form.fullAddress.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.fullAddress.placeholder',
                    [], 'admin')
                ]])->onlyOnIndex(),
            yield TelephoneField::new('phoneNumber', $this->translator->trans('admin.client.form.phoneNumber.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.phoneNumber.placeholder',
                    [], 'admin')
                ]]),
            yield EmailField::new('email', $this->translator->trans('admin.client.form.email.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.email.placeholder',
                    [], 'admin')
                ]]),
            yield CollectionField::new('clientContacts', 'Contacts client')
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(ClientContactType::class)
                ->setFormTypeOptions([
                    'by_reference' => false
                ])->onlyOnForms(),

            yield ArrayField::new('getClientContacts', $this->translator->trans('admin.client.form.getClientContacts.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/client/detailClientContacts.html.twig'),
            yield ArrayField::new('getContracts', $this->translator->trans('admin.client.form.getContracts.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/client/detailContracts.html.twig'),

        ];

    }
}
