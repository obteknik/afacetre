<?php

namespace App\Controller\Admin;

use App\Entity\Organization;
use App\Form\DocumentType;
use App\Form\OrganizationActivityType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrganizationCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     * @param CrudUrlGenerator $crudUrlGenerator
     * @param KernelInterface $kernel
     */
    public function __construct(TranslatorInterface $translator, CrudUrlGenerator $crudUrlGenerator,
                                KernelInterface $kernel)
    {
        $this->translator = $translator;
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->kernel = $kernel;
    }

//    public function index(AdminContext $context)
//    {
//        // redirect to edit organization
//        /*
//        {% set url = ea_url()
//            .setDashboard('App\\Controller\\Admin\\DashboardController')
//            .setController('App\\Controller\\Admin\\ClientCrudController')
//            .setAction('detail') %}
//        */
//        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
//
//        $crudId = '0d20ca2';
//        $enityId = 1;
//        $routeBuilder
////            ->setDashboard('App\\Controller\\Admin\\DashboardController')
//            ->setController('App\\Controller\\Admin\OrganizationCrudController')
//            ->setAction('detail')
//            ->setCrudId($crudId)
//            ->setEntityId($enityId);
//
//        return $this->redirect($routeBuilder->generateUrl());
//    }

    public static function getEntityFqcn(): string
    {
        return Organization::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setPageTitle('index', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.organization.index.headTitle',
                    [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.organization.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.organization.edit.headTitle',
                [], 'admin'));
        $crud->overrideTemplate('crud/index', 'admin/organization/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/organization/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/organization/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/organization/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
//            ->disable('new', 'delete')
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [

            Yield IdField::new('id')->hideOnForm(),

            yield AssociationField::new('businessType', $this->translator->trans('admin.organization.form.businessType.label',
                [], 'admin'))->hideOnIndex(),
            yield AssociationField::new('legalForm', $this->translator->trans('admin.organization.form.legalForm.label',
                [], 'admin'))->hideOnIndex(),

            Yield ImageField::new('illustration', $this->translator->trans('admin.organization.form.logo.label',
                [], 'admin'))
                ->setBasePath('uploads/organization')
                ->setUploadDir('public/uploads/organization')
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false)->setFormTypeOptions(['required' => false])
                ->addCssClass('org_logo'),

            Yield TextField::new('name', $this->translator->trans('admin.organization.form.name.label',
                [], 'admin')),
            Yield TextField::new('phoneNumber', $this->translator->trans('admin.organization.form.phoneNumber.label',
                [], 'admin')),
            Yield EmailField::new('email', $this->translator->trans('admin.organization.form.email.label',
                [], 'admin')),
            Yield TextField::new('street', $this->translator->trans('admin.organization.form.street.label',
                [], 'admin')),
            Yield TextField::new('zipcode', $this->translator->trans('admin.organization.form.zipcode.label',
                [], 'admin')),
            Yield TextField::new('city', $this->translator->trans('admin.organization.form.city.label',
                [], 'admin')),
            Yield TextField::new('siretNumber', $this->translator->trans('admin.organization.form.siretNumber.label',
                [], 'admin'))->hideOnIndex(),
            Yield TextField::new('rcs', $this->translator->trans('admin.organization.form.rcs.label',
                [], 'admin'))->hideOnIndex(),
            Yield TextField::new('bankName', $this->translator->trans('admin.organization.form.bankName.label',
                [], 'admin'))->hideOnIndex(),
            Yield TextField::new('iban', $this->translator->trans('admin.organization.form.iban.label',
                [], 'admin'))->hideOnIndex(),
            Yield NumberField::new('taxRate', $this->translator->trans('admin.organization.form.taxRate.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isTaxSubject', $this->translator->trans('admin.organization.form.isTaxSubject.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isHomeTeam', $this->translator->trans('admin.organization.form.isHomeTeam.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isHomeBest', $this->translator->trans('admin.organization.form.isHomeBest.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isHomePost', $this->translator->trans('admin.organization.form.isHomePost.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isActiveEcommerce', $this->translator->trans('admin.organization.form.isActiveEcommerce.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isActiveProductShowBest', $this->translator->trans('admin.organization.form.isActiveProductShowBest.label',
                [], 'admin'))->hideOnIndex(),
            Yield BooleanField::new('isMultilingual', $this->translator->trans('admin.organization.form.isMultilingual.label',
                [], 'admin'))->hideOnIndex(),
            yield BooleanField::new('isPriceShow', $this->translator->trans('admin.organization.form.isPriceShow.label',
                [], 'admin'))->hideOnIndex(),

            //citation
            Yield BooleanField::new('isHomeCitation', $this->translator->trans('admin.organization.form.isHomeCitation.label',
                [], 'admin')),

            //event colors
            Yield BooleanField::new('isWorkflowEventColor', $this->translator->trans('admin.organization.form.isWorkflowEventColor.label',
                [], 'admin')),

            Yield IntegerField::new('homeCitationTimer', $this->translator->trans('admin.organization.form.homeCitationTimer.label',
                [], 'admin'))->hideOnIndex(),

            yield ArrayField::new('getOrganizationActivities', $this->translator->trans('admin.organization.form.getOrganizationActivities.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/organization/detailActivities.html.twig'),

            Yield ImageField::new('signature', $this->translator->trans('admin.organization.form.signature.label',
                [], 'admin'))
                ->setBasePath('uploads/organization')
                ->setUploadDir('public/uploads/organization')
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false)->setFormTypeOptions(['required' => false])->hideOnIndex(),

            //Collection DOCUMENTS
            yield CollectionField::new('documents', $this->translator->trans('admin.organization.form.documents.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(DocumentType::class)
                ->setFormTypeOptions(['by_reference' => false]),

            //Collection ACTIVITIES
            yield CollectionField::new('organizationActivities', $this->translator->trans('admin.organization.form.organizationActivities.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(OrganizationActivityType::class)
                ->setFormTypeOptions(['by_reference' => false]),

            //WORKFLOWS
            yield AssociationField::new('workflows', $this->translator->trans('admin.organization.form.workflows.label',
                [], 'admin'))->hideOnIndex(),

            //PDF font
            Yield TextField::new('pdfFont', $this->translator->trans('admin.organization.form.pdfFont.label',
                [], 'admin'))->hideOnIndex(),

            //emails transactionnels
            Yield ChoiceField::new('emailTransportMode', $this->translator->trans('admin.organization.form.emailTransportMode.label',
                [], 'admin'))->setChoices([
                Organization::EMAIL_TRANSPORT_MODE_CLASSIC => Organization::EMAIL_TRANSPORT_MODE_CLASSIC,
                Organization::EMAIL_TRANSPORT_MODE_MAILJET => Organization::EMAIL_TRANSPORT_MODE_MAILJET,
            ]),
            Yield EmailField::new('emailCc', $this->translator->trans('admin.organization.form.emailCc.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.organization.form.emailCc.placeholder',
                    [], 'admin')],
            ]),
            Yield EmailField::new('emailBcc', $this->translator->trans('admin.organization.form.emailBcc.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.organization.form.emailBcc.placeholder',
                    [], 'admin')],
            ]),

        ];
    }
}
