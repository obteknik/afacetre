<?php

namespace App\Controller\Admin;

use App\Entity\FamilySituation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class FamilySituationCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class FamilySituationCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * familySituationCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return FamilySituation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.familySituation.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.familySituation.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.familySituation.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-file-o"></i> ' . $this->translator->trans('admin.familySituation.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/familySituation/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/familySituation/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/familySituation/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/familySituation/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.familySituation.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.familySituation.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.familySituation.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id')->hideOnForm();
        $situation = TextField::new('situation', $this->translator->trans('admin.familySituation.form.situation.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['autofocus' => true, 'placeholder' => $this->translator->trans('admin.familySituation.form.situation.label',
                [], 'admin'),],
        ]);

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $situation ];
        }

        return [  $situation,  ];
    }

}
