<?php

namespace App\Controller\Admin;

use App\Entity\PaymentCondition;
use App\Entity\Quote;
use App\Form\ProductType;
use App\Repository\OrganizationRepository;
use App\Repository\PaymentConditionRepository;
use App\Services\AppService;
use App\Services\DompdfService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class QuoteCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var DompdfService
     */
    private $dompdfService;
    /**
     * @var PaymentConditionRepository
     */
    private $paymentConditionRepository;

    /**
     * CarrierCrudController constructor.
     * @param TranslatorInterface $translator
     * @param DompdfService $dompdfService
     * @param LoggerInterface $logger
     * @param AppService $appService
     * @param OrganizationRepository $organizationRepository
     * @param PaymentConditionRepository $paymentConditionRepository
     */
    public function __construct(TranslatorInterface $translator, DompdfService $dompdfService, LoggerInterface $logger,
                                AppService $appService, OrganizationRepository $organizationRepository,
                                PaymentConditionRepository $paymentConditionRepository)
    {
        $this->translator = $translator;
        $this->logger = $logger;
        $this->appService = $appService;
        $this->organizationRepository = $organizationRepository;
        $this->dompdfService = $dompdfService;
        $this->paymentConditionRepository = $paymentConditionRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Quote::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-file-pdf"></i> ' . $this->translator->trans('admin.quote.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function(Quote $quote){
            return (string) '<i class="fa fa-file-pdf"></i> '
                . $this->translator->trans('admin.quote.detail.headTitle',[], 'admin').': '
                .$quote->getReference(). ' du '.$quote->getCreatedAt()->format('d/m/Y').' de '.
                number_format(($quote->getAmountHT()/100), 2,',','.').' €';
        });
        $crud->setPageTitle('new', '<i class="fa fa-file-pdf"></i> ' . $this->translator->trans('admin.quote.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-file-pdf"></i> ' . $this->translator->trans('admin.quote.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['reference', 'client', 'amountHT']);
        $crud->overrideTemplate('crud/index', 'admin/quote/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/quote/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/quote/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/quote/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        $generatePDF = Action::new('generatePDF', '', 'fas fa-file-pdf')
            ->linkToCrudAction('generatePDF');

        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->add(Crud::PAGE_INDEX, $generatePDF)
            ->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE, 'generatePDF'])
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.quote.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.quote.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.quote.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, $generatePDF, function (Action $action) {
                return $action
                    ->setIcon('fa fa-file-pdf')
                    ->setLabel($this->translator->trans('admin.action.generatePdf.label', [], 'admin'));
            });
    }

    /**
     * Génération du devis en PDF
     * @param AdminContext $context
     * @return string
     */
    public function generatePDF(AdminContext $context)
    {
        /** @var Quote $quote */
        $quote = $context->getEntity()->getInstance();

        try {
            $this->dompdfService->getQuotePdf($quote);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }

    public function configureFields(string $pageName): iterable
    {
        $organization = $this->organizationRepository->find(1);
        $nextQuoteReference = $this->appService->getNextReference($organization, 'quote');

        return [
            yield IdField::new('id')->hideOnForm(),
            yield AssociationField::new('organization', $this->translator->trans('admin.quote.form.organization.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
                'data' => $organization,
            ]),
            Yield AssociationField::new('fiscalYear', $this->translator->trans('admin.invoice.form.fiscalYear.label',
                [], 'admin')),
            yield DateTimeField::new('createdAt', $this->translator->trans('admin.quote.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
            ])->hideOnForm(),
            yield TextField::new('reference', $this->translator->trans('admin.quote.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true, 'placeholder' => $this->translator->trans('admin.quote.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextQuoteReference,
            ])->hideOnForm(),
            yield AssociationField::new('client', $this->translator->trans('admin.quote.form.client.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.quote.form.client.label',
                    [], 'admin'),],
            ]),
            yield AssociationField::new('products', $this->translator->trans('admin.invoice.form.products.label',
                [], 'admin')),
            yield AssociationField::new('paymentCondition', $this->translator->trans('admin.quote.form.paymentCondition.label',
                [], 'admin'))->setFormTypeOptions([
            ]),

            yield ArrayField::new('getQuoteDetails', $this->translator->trans('admin.quote.form.getQuoteDetails.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/quote/detailquoteDetails.html.twig'),

        ];

    }

}
