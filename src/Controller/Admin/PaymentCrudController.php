<?php

namespace App\Controller\Admin;

use App\Entity\Payment;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class PaymentCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PaymentCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var AppService
     */
    private $appService;

    /**
     * PaymentCrudController constructor.
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $manager
     * @param OrganizationRepository $organizationRepository
     * @param AppService $appService
     */
    public function __construct(TranslatorInterface $translator, EntityManagerInterface $manager,
                                OrganizationRepository $organizationRepository, AppService $appService)
    {
        $this->translator = $translator;
        $this->manager = $manager;
        $this->organizationRepository = $organizationRepository;
        $this->appService = $appService;
    }

    public static function getEntityFqcn(): string
    {
        return Payment::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.payment.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.payment.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.payment.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.payment.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/payment/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/payment/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/payment/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/payment/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.post.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.post.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.post.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $organization = $this->organizationRepository->find(1);
        $nextChargeReference = $this->appService->getNextReference($organization, 'payment');

        return [
            yield IdField::new('id')->hideOnForm(),
            yield AssociationField::new('organization', $this->translator->trans('admin.payment.form.organization.label',
                [], 'admin'))->setFormTypeOptions([
                'data' => $this->organizationRepository->find(1),
            ])->onlyOnDetail(),

            Yield AssociationField::new('fiscalYear', $this->translator->trans('admin.payment.form.fiscalYear.label',
                [], 'admin')),

            yield DateTimeField::new('createdAt', $this->translator->trans('admin.payment.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
            ])->hideOnForm(),

            Yield TextField::new('reference', $this->translator->trans('admin.payment.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true, 'placeholder' => $this->translator->trans('admin.charge.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextChargeReference,
            ])->hideOnForm(),

            yield AssociationField::new('type', $this->translator->trans('admin.payment.form.type.label',
                [], 'admin')),

            yield AssociationField::new('patient', $this->translator->trans('admin.payment.form.patient.label',
                [], 'admin')),

            Yield MoneyField::new('amount', $this->translator->trans('admin.payment.form.amount.label',
                [], 'admin'))->setCurrency('EUR')->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.charge.form.amount.placeholder',
                    [], 'admin')],
            ]),

            yield AssociationField::new('contract', $this->translator->trans('admin.payment.form.contract.label',
                [], 'admin')),

            yield AssociationField::new('invoice', $this->translator->trans('admin.payment.form.invoice.label',
                [], 'admin')),

        ];

    }

}
