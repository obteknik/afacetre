<?php

namespace App\Controller\Admin;

use App\Entity\Quotation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class QuotationCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * QuotationCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Quotation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setPageTitle('index', '<i class="fa fa-pen-fancy"></i> ' . $this->translator->trans('admin.quotation.index.headTitle',
                    [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-pen-fancy"></i> ' . $this->translator->trans('admin.quotation.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-pen-fancy"></i> ' . $this->translator->trans('admin.quotation.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-pen-fancy"></i> ' . $this->translator->trans('admin.quotation.edit.headTitle',
                [], 'admin'));
        $crud->overrideTemplate('crud/index', 'admin/quotation/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/quotation/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/quotation/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/quotation/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.quotation.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            Yield IdField::new('id')->onlyOnDetail(),
            Yield TextField::new('author', $this->translator->trans('admin.quotation.form.author.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.quotation.form.author.placeholder',
                    [], 'admin'), 'autofocus' => true],
            ]),
            Yield TextareaField::new('content', $this->translator->trans('admin.quotation.form.content.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.quotation.form.content.placeholder',
                    [], 'admin')],
            ]),
            Yield BooleanField::new('isActive', $this->translator->trans('admin.quotation.form.isActive.label',
                [], 'admin')),
        ];
    }

}
