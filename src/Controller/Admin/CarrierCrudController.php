<?php

namespace App\Controller\Admin;

use App\Classe\AdminCkEditorField;
use App\Entity\Carrier;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CarrierCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CarrierCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CarrierCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Carrier::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');

        $crud->setPageTitle('index', '<i class="fa fa-truck"></i> ' . $this->translator->trans('admin.carrier.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-truck"></i> ' . $this->translator->trans('admin.carrier.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-truck"></i> ' . $this->translator->trans('admin.carrier.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-truck"></i> ' . $this->translator->trans('admin.carrier.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/carrier/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/carrier/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/carrier/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/carrier/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.carrier.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.carrier.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.carrier.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', $this->translator->trans('admin.carrier.form.name.label',
                [], 'admin'))->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => 'admin.carrier.form.name.placeholder',
                        'autofocus' => true]
                ]
            ),
            AdminCkEditorField::new('description', $this->translator->trans('admin.carrier.form.description.label',
                [], 'admin'))
                ->setCustomOptions(
                    [
                        'config' => ['placeholder' => $this->translator->trans('admin.carrier.form.description.label',
                            [], 'admin')]
                    ]
                ),
            MoneyField::new('price', $this->translator->trans('admin.carrier.form.price.label',
                [], 'admin'))
                ->setCurrency('EUR')
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => '0,00']
                    ]
                ),
        ];
    }

}
