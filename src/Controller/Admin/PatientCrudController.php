<?php

namespace App\Controller\Admin;

use App\Entity\Patient;
use App\Form\CommentType;
use App\Form\DocumentType;
use App\Form\EventPatientType;
use App\Form\EventType;
use App\Form\HistoricalType;
use App\Form\InvoiceType;
use App\Form\PatientInvoiceType;
use App\Form\PaymentType;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use App\Services\CsvService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

/**
 * Class PatientCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PatientCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var CsvService
     */
    private $csvService;

    /**
     * patientCrudController constructor.
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $manager
     * @param AppService $appService
     * @param OrganizationRepository $organizationRepository
     * @param CsvService $csvService
     */
    public function __construct(TranslatorInterface $translator, EntityManagerInterface $manager,
                                AppService $appService, OrganizationRepository $organizationRepository, CsvService $csvService)
    {
        $this->translator = $translator;
        $this->manager = $manager;
        $this->appService = $appService;
        $this->organizationRepository = $organizationRepository;
        $this->csvService = $csvService;
    }

    public static function getEntityFqcn(): string
    {
        return Patient::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.patient.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function (Patient $patient) {
            return (string)'<i class="fa fa-user"></i> '
                . $this->translator->trans('admin.patient.detail.headTitle', [], 'admin') . ': '
                .$patient->getReference() . ' - ' . $patient->getFullName(). ' - ' . $patient->getBirthDate()->format('d/m/Y')
                .' - ' . $patient->getCity();
        });
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.patient.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.patient.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/patient/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/patient/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/patient/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/patient/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.patient.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.patient.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.patient.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {

        $organization = $this->organizationRepository->find(1);
        $nextPatientReference = $this->appService->getNextReference($organization, 'patient');

        return [

            yield IdField::new('id')->hideOnForm(),

            yield AssociationField::new('organization', $this->translator->trans('admin.patient.form.organization.label',
                [], 'admin'))->onlyOnForms(),

            yield DateTimeField::new('createdAt', $this->translator->trans('admin.patient.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
            ])->hideOnForm(),
            yield TextField::new('reference', $this->translator->trans('admin.patient.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true, 'placeholder' => $this->translator->trans('admin.patient.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextPatientReference,
            ])->hideOnForm(),
            yield AssociationField::new('type', $this->translator->trans('admin.patient.form.type.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.type.placeholder',
                    [], 'admin'),],
            ]),
            yield DateField::new('birthDate', $this->translator->trans('admin.patient.form.birthDate.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.birthDate.placeholder',
                    [], 'admin')],
            ]),
            yield IntegerField::new('getPatientAge'.' ans', $this->translator->trans('admin.patient.form.getPatientAge.label',
                [], 'admin'))->hideOnForm(),
            yield ChoiceField::new('civility', $this->translator->trans('admin.patient.form.civility.label',
                [], 'admin'))->setChoices([
                "Monsieur" => Patient::CIVILITY_MISTER,
                "Madame" => Patient::CIVILITY_MISS,
            ])->onlyOnForms(),

            yield TextField::new('firstname', $this->translator->trans('admin.patient.form.firstname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.firstname.placeholder',
                    [], 'admin')],
            ])->onlyOnForms(),
            yield TextField::new('lastname', $this->translator->trans('admin.patient.form.lastname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.lastname.placeholder',
                    [], 'admin')],
            ])->onlyOnForms(),
            yield TextField::new('getFullname', $this->translator->trans('admin.patient.form.getFullname.label',
                [], 'admin'))->hideOnForm(),

            yield TextField::new('user.getRole', $this->translator->trans('admin.patient.form.user.getRole.label',
                [], 'admin'))->onlyOnDetail(),

            yield TextField::new('street', $this->translator->trans('admin.client.form.street.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.street.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield TextField::new('zipcode', $this->translator->trans('admin.client.form.zipcode.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.zipcode.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield TextField::new('city', $this->translator->trans('admin.client.form.city.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.city.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield CountryField::new('country', $this->translator->trans('admin.client.form.country.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.country.placeholder',
                    [], 'admin')
                ]])->hideOnIndex(),
            yield TelephoneField::new('phoneNumber', $this->translator->trans('admin.patient.form.phoneNumber.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.phoneNumber.placeholder',
                    [], 'admin')],
            ]),
            yield EmailField::new('email', $this->translator->trans('admin.patient.form.email.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.email.placeholder',
                    [], 'admin')],
            ]),

            yield AssociationField::new('familySituation', $this->translator->trans('admin.patient.form.familySituation.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.patient.form.familySituation.placeholder',
                    [], 'admin'),],
            ])->hideOnIndex(),

            //documents
            yield CollectionField::new('documents', $this->translator->trans('admin.patient.form.documents.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(DocumentType::class)
                ->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),

            //payments
            yield CollectionField::new('payments', $this->translator->trans('admin.patient.form.payments.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(PaymentType::class)
                ->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),

            //invoices
            yield CollectionField::new('invoices', $this->translator->trans('admin.patient.form.invoices.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(PatientInvoiceType::class)
                ->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),

            //comments
            yield CollectionField::new('comments', $this->translator->trans('admin.patient.form.comments.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(CommentType::class)
                ->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),

            //historicals - A CONSERVER
//            yield CollectionField::new('historicals', $this->translator->trans('admin.patient.form.historicals.label',
//                [], 'admin'))->onlyOnForms()
//                ->setEntryType(HistoricalType::class)
//                ->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),

            //séances de consultation
            yield CollectionField::new('events', $this->translator->trans('admin.patient.form.events.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(EventPatientType::class)
                ->setFormTypeOptions(['by_reference' => false])->onlyOnForms(),

            yield ArrayField::new('getEvents', $this->translator->trans('admin.patient.form.getEvents.label',
                [], 'admin'))->setTemplatePath('admin/patient/detailEvents.html.twig')
                ->onlyOnDetail(),

            yield ArrayField::new('getDocuments', $this->translator->trans('admin.patient.form.getDocuments.label',
                [], 'admin'))->setTemplatePath('admin/patient/detailDocuments.html.twig')
                ->onlyOnDetail(),

            yield ArrayField::new('getPayments', $this->translator->trans('admin.patient.form.getPayments.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/patient/detailPayments.html.twig'),

            yield ArrayField::new('getInvoices', $this->translator->trans('admin.patient.form.getInvoices.label',
                [], 'admin'))->setTemplatePath('admin/patient/detailInvoices.html.twig')
                ->onlyOnDetail(),

        ];
    }
}
