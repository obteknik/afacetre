<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class UserCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->overrideTemplate('crud/index', 'admin/user/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/user/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/user/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/user/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.user.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.user.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.user.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            ImageField::new('personIllustration', $this->translator->trans('admin.user.form.person.illustration.label',
                [], 'admin'))->setTemplatePath('admin/user/personIllustration.html.twig')->hideOnForm(),
            TextField::new('firstname', $this->translator->trans('admin.user.form.firstname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.user.form.firstname.placeholder',
                    [], 'admin'),'autofocus' =>true]
            ])->hideOnIndex(),
            TextField::new('lastname', $this->translator->trans('admin.user.form.lastname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.user.form.lastname.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            TextField::new('getFullname', $this->translator->trans('admin.user.form.getFullname.label',
                [], 'admin'))->onlyOnIndex(),
            EmailField::new('email', $this->translator->trans('admin.user.form.email.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.user.form.email.placeholder',
                    [], 'admin')]
            ]),
            TextField::new('password', $this->translator->trans('admin.user.form.password.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.user.form.password.placeholder',
                    [], 'admin')]
            ])->hideOnIndex(),
            ChoiceField::new('hasRoleAdmin', $this->translator->trans('admin.user.form.role.label',
                [], 'admin'))
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => $this->translator->trans('admin.user.form.role.placeholder',
                            [], 'admin')]
                    ]
                )->setChoices([
                    User::USER_ROLE_USER => 0,
                    User::USER_ROLE_ADMIN => 1,
                ]),
            BooleanField::new('isActive', $this->translator->trans('admin.user.form.isActive.label',
                [], 'admin')),
            IntegerField::new('getCountConnexions', $this->translator->trans('admin.user.form.connexions.label',
                [], 'admin'))->setTextAlign('right')->hideOnForm()
        ];
    }

}
