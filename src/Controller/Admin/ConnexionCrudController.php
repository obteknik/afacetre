<?php

namespace App\Controller\Admin;

use App\Entity\Connexion;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConnexionCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Connexion::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-link"></i> ' . $this->translator->trans('admin.connexion.index.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['createdAt' => 'DESC']);
        $crud->overrideTemplate('crud/index', 'admin/connexion/index.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('new','edit', 'delete')
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            });
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id')->hideOnForm();
        $createdAt = DateTimeField::new('createdAt', $this->translator->trans('admin.connexion.form.createdAt.label',
            [], 'admin'));
        $user = AssociationField::new('user', $this->translator->trans('admin.connexion.form.user.label',
            [], 'admin'));
        $userFullname = TextField::new('user.getFullname', $this->translator->trans('admin.connexion.form.user.getFullname.label',
            [], 'admin'));
        $personIllustration = ImageField::new('user.getPersonIllustration', $this->translator->trans('admin.user.form.person.illustration.label',
                [], 'admin'))->setTemplatePath('admin/user/personIllustration.html.twig')->hideOnForm();

        $userRole = TextField::new('user.getRole', $this->translator->trans('admin.connexion.form.user.getRole.label',
            [], 'admin'));

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $personIllustration, $userFullname, $userRole, $createdAt];
        }

        return [  $userFullname, $createdAt, ];
    }
}