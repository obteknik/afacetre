<?php

namespace App\Controller\Admin;

use App\Entity\Charge;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ChargeCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ChargeCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     * @param AppService $appService
     * @param OrganizationRepository $organizationRepository
     */
    public function __construct(TranslatorInterface $translator, AppService $appService,
                                OrganizationRepository $organizationRepository)
    {
        $this->translator = $translator;
        $this->appService = $appService;
        $this->organizationRepository = $organizationRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Charge::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.charge.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.charge.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.charge.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-euro"></i> ' . $this->translator->trans('admin.charge.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/charge/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/charge/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/charge/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/charge/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.charge.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.charge.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.charge.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }
    
    public function configureFields(string $pageName): iterable
    {
        $organization = $this->organizationRepository->find(1);
        $nextChargeReference = $this->appService->getNextReference($organization, 'charge');

        return [
            yield IdField::new('id')->hideOnForm(),
            yield AssociationField::new('organization', $this->translator->trans('admin.charge.form.organization.label',
                [], 'admin'))->setFormTypeOptions([
                'data' => $this->organizationRepository->find(1),
            ])->hideOnForm(),

            Yield AssociationField::new('fiscalYear', $this->translator->trans('admin.charge.form.fiscalYear.label',
                [], 'admin')),

            yield DateTimeField::new('createdAt', $this->translator->trans('admin.charge.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
            ])->hideOnForm(),

            Yield TextField::new('reference', $this->translator->trans('admin.charge.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true, 'placeholder' => $this->translator->trans('admin.charge.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextChargeReference,
            ])->hideOnForm(),

            yield AssociationField::new('type', $this->translator->trans('admin.charge.form.type.label',
                [], 'admin')),

            Yield TextField::new('label', $this->translator->trans('admin.charge.form.label.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.charge.form.label.placeholder',
                    [], 'admin')],
            ]),
            Yield MoneyField::new('amount', $this->translator->trans('admin.charge.form.amount.label',
                [], 'admin'))->setCurrency('EUR')->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.charge.form.amount.placeholder',
                    [], 'admin')],
            ]),

        ];

    }
}
