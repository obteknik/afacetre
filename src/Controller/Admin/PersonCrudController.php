<?php

namespace App\Controller\Admin;

use App\Classe\AdminCkEditorField;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class PersonCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PersonCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $manager
     * @param CrudUrlGenerator $crudUrlGenerator
     */
    public function __construct(TranslatorInterface $translator, EntityManagerInterface $manager,
                                CrudUrlGenerator $crudUrlGenerator)
    {
        $this->translator = $translator;
        $this->manager = $manager;
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Person::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');

        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->overrideTemplate('crud/index', 'admin/person/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/person/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/person/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/person/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        $activeUserAccount = Action::new('activeUserAccount', 'Activation du compte utilisateur', 'fas fa-user-lock')
            ->linkToCrudAction('activeUserAccount');

        return $actions
            ->add('detail', $activeUserAccount)
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.person.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.person.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.person.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function activeUserAccount(AdminContext $context)
    {
        /** @var Person $person */
        $person = $context->getEntity()->getInstance();

//        dump($person);

        if ($person->getIsActive()) {
            //si intervenant ACTIF alors si je clique sur cette action, je souhaite activer son compte utiisateur
            //donc je passe isActive à true dans User
            $person->getUser()->setIsActive(true);
            $person->getUser()->setActivatedAt(new \DateTime());
            $this->manager->persist($person);
            $this->manager->flush();

            $this->addFlash('notice', "<span style='color:green;'><strong>
            Activation du compte utilisateur affectuée pour <u>".$person->getFirstname(). ' '.
                $person->getLastname()."</u></strong></span>");
        }

        $url = $this->crudUrlGenerator->build()
            ->setController(PersonCrudController::class)
            ->setAction(ACTION::DETAIL)
            ->generateUrl();

        return $this->redirect($url);

    }

    public function configureFields(string $pageName): iterable
    {

        $id = IdField::new('id')->hideOnForm();
        $createdAt = DateTimeField::new('createdAt', $this->translator->trans('admin.invoice.form.createdAt.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['disabled' => true,],
        ])->hideOnForm();
        $isActive = BooleanField::new('isActive', "Actif");

        $firstname = TextField::new('firstname', $this->translator->trans('admin.person.form.firstname.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['placeholder' => $this->translator->trans('admin.person.form.firstname.placeholder',
                [], 'admin'), 'autofocus' => true]
        ]);
        $lastname = TextField::new('lastname', $this->translator->trans('admin.person.form.lastname.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['placeholder' => $this->translator->trans('admin.person.form.lastname.placeholder',
                [], 'admin')]
        ]);
        $address = TextField::new('address', $this->translator->trans('admin.person.form.address.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['placeholder' => $this->translator->trans('admin.person.form.address.placeholder',
                [], 'admin')]
        ]);
        $phoneNumber = TextField::new('phoneNumber', $this->translator->trans('admin.person.form.phoneNumber.label',
            [], 'admin'))->setFormTypeOptions([
            'attr' => ['placeholder' => $this->translator->trans('admin.person.form.phoneNumber.placeholder',
                [], 'admin')]
        ]);
        $email = EmailField::new('email', $this->translator->trans('admin.person.form.email.label',
            [], 'admin'));

        $illustration = ImageField::new('illustration', $this->translator->trans('admin.person.form.illustration.label',
            [], 'admin'))
            ->setBasePath('uploads/')
            ->setUploadDir('public/uploads')
            ->setUploadedFileNamePattern('[randomhash].[extension]')
            ->setRequired(false);

        $description = AdminCkEditorField::new('description', $this->translator->trans('admin.person.form.description.label',
            [], 'admin'))->setCustomOptions(['config' => ['placeholder' => 'Description home...']]);

        $showDescription = AdminCkEditorField::new('showDescription', $this->translator->trans('admin.person.form.showDescription.label',
            [], 'admin'))->setCustomOptions(['config' => ['placeholder' => 'Description show...']]);

        $disciplines = AssociationField::new('disciplines', $this->translator->trans('admin.person.form.disciplines.label',
            [], 'admin'));

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $isActive, $createdAt, $illustration, $firstname, $lastname, $phoneNumber, $email, $description, $showDescription, $disciplines];
        }

        return [
            $createdAt, $isActive, $firstname, $lastname, $address, $phoneNumber, $email,$illustration, $description,
            $disciplines, $showDescription
        ];
    }
}
