<?php

namespace App\Controller\Admin;

use App\Entity\FiscalYear;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class FiscalYearCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class FiscalYearCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return FiscalYear::class;
    }

    public function configureCrud(Crud $crud): Crud
    {

        $crud->setPageTitle('index', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.fiscalYear.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.fiscalYear.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.fiscalYear.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.fiscalYear.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/fiscalYear/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/fiscalYear/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/fiscalYear/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/fiscalYear/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.fiscalYear.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.fiscalYear.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.fiscalYear.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            yield IdField::new('id')->hideOnForm(),
            yield DateTimeField::new('createdAt', $this->translator->trans('admin.fiscalYear.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,],
            ])->onlyWhenCreating(),
            yield TextField::new('name', $this->translator->trans('admin.fiscalYear.form.name.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.fiscalYear.form.name.placeholder',
                    [], 'admin')]
            ]),
            yield DateField::new('startAt', $this->translator->trans('admin.fiscalYear.form.startAt.label',
                [], 'admin')),

            yield DateField::new('endAt', $this->translator->trans('admin.fiscalYear.form.endAt.label',
                [], 'admin')),

            //stats
            yield ArrayField::new('getStats', $this->translator->trans('admin.fiscalYear.form.getStats.label',
                [], 'admin'))->setTemplatePath('admin/fiscalYear/detailStats.html.twig')->onlyOnDetail()

        ];

    }
}
