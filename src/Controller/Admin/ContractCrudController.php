<?php

namespace App\Controller\Admin;

use App\Entity\Contract;
use App\Form\DocumentType;
use App\Form\EventContractType;
use App\Form\ParticipantType;
use App\Repository\ContractRepository;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ContractCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ContractCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var ContractRepository
     */
    private $contractRepository;

    /**
     * ContractCrudController constructor.
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $manager
     * @param OrganizationRepository $organizationRepository
     * @param AppService $appService
     * @param ContractRepository $contractRepository
     */
    public function __construct(TranslatorInterface $translator, EntityManagerInterface $manager,
                                OrganizationRepository $organizationRepository, AppService $appService,
                                ContractRepository $contractRepository)
    {
        $this->translator = $translator;
        $this->manager = $manager;
        $this->organizationRepository = $organizationRepository;
        $this->appService = $appService;
        $this->contractRepository = $contractRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Contract::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');

        $crud->setPageTitle('index', '<i class="fa fa-pencil"></i> ' . $this->translator->trans('admin.contract.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', function (Contract $contract) {
            return (string)'<i class="fa fa-edit"></i> '
                . $this->translator->trans('admin.contract.detail.headTitle', [], 'admin')
                . ' ' . $contract->getReference() . ' - ' . $contract->getClient()->getCompanyName() . ' - '
                . $contract->getClient()->getZipcode()
                . ' ' . $contract->getClient()->getCity();
        });
        $crud->setPageTitle('new', '<i class="fa fa-pencil"></i> ' . $this->translator->trans('admin.contract.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-pencil"></i> ' . $this->translator->trans('admin.contract.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        $crud->setSearchFields(['name', 'description']);
        $crud->overrideTemplate('crud/index', 'admin/contract/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/contract/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/contract/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/contract/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {

        $viewPdf = Action::new('viewPdf', '', 'fas fa-file-pdf')
            ->linkToCrudAction('viewPdf')->displayIf(static function ($entity) {
                /** @var Contract $entity */
                return count($entity->getDocuments()) > 0;
            });

        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->add(Crud::PAGE_INDEX, $viewPdf)
            ->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE, 'viewPdf'])
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {

                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.contract.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.contract.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.contract.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    /**
     * Ouvrir le pdf contract
     * @param AdminContext $context
     * @param KernelInterface $kernel
     * @return string
     */
    public function viewPDF(AdminContext $context, KernelInterface $kernel)
    {
        /** @var Contract $contract */
        $contract = $context->getEntity()->getInstance();
        if (null !== $contract->getDocuments()) {
            //TODO: Récupérer pour le contrat le document type "contrat" [DocumentType] selon l'id type
            $urlContract = $kernel->getProjectDir() . '/public/uploads/documents/' . $contract->getDocuments()[0];
            return new BinaryFileResponse($urlContract);
        }
    }

    public function configureFields(string $pageName): iterable
    {
        $organization = $this->organizationRepository->find(1);
        $nextContractReference = $this->appService->getNextReference($organization, 'contract');

        return [
            yield FormField::addPanel('Informations de base')->onlyOnForms(),
            yield IdField::new('id')->onlyOnIndex(),
            yield AssociationField::new('organization', $this->translator->trans('admin.contract.form.organization.label',
                [], 'admin'))->setFormTypeOptions([
                'data' => $this->organizationRepository->find(1),
            ])->onlyOnDetail(),
            yield TextField::new('reference', $this->translator->trans('admin.contract.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.contract.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextContractReference,
            ])->hideOnForm(),
            yield BooleanField::new('isFaceToFace', $this->translator->trans('admin.contract.form.isFaceToFace.label',
                [], 'admin')),
            yield AssociationField::new('client', $this->translator->trans('admin.contract.form.client.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.contract.form.client.placeholder',
                    [], 'admin'),],
            ]),

            yield AssociationField::new('persons', $this->translator->trans('admin.contract.form.persons.label',
                [], 'admin'))->onlyOnForms(),

            yield DateTimeField::new('startAt', $this->translator->trans('admin.contract.form.startAt.label',
                [], 'admin'))->hideOnForm(),

            yield DateTimeField::new('endAt', $this->translator->trans('admin.contract.form.endAt.label',
                [], 'admin'))->hideOnForm(),

            yield AssociationField::new('products', $this->translator->trans('admin.contract.form.products.label',
                [], 'admin'))->onlyOnForms(),

            yield TextField::new('address', $this->translator->trans('admin.contract.form.address.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.contract.form.address.placeholder',
                    [], 'admin')],
            ]),

            yield TextField::new('foadLink', $this->translator->trans('admin.contract.form.foadLink.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.contract.form.foadLink.placeholder',
                    [], 'admin')],
            ])->onlyOnDetail(),

            //Collection ADHERENTS
            yield CollectionField::new('adherents', $this->translator->trans('admin.contract.form.adherents.label',
                [], 'admin'))
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(ParticipantType::class)
                ->setFormTypeOptions([
                    'by_reference' => false
                ])
                ->onlyOnForms(),
//                ->addCssClass('addInlineBlock'),
//                ->setTemplatePath('form_themes/contractParticipant-formTheme.html.twig'),
//                ->setTemplatePath('bootstrap_4_horizontal_layout.html.twig'),

            //Collection EVENTS
            yield CollectionField::new('events', $this->translator->trans('admin.contract.form.events.label',
                [], 'admin'))
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(EventContractType::class)
                ->setFormTypeOptions([
                    'by_reference' => false
                ])->onlyOnForms(),

            //Collection DOCUMENTS
            yield CollectionField::new('documents', $this->translator->trans('admin.contract.form.documents.label',
                [], 'admin'))->onlyOnForms()
                ->setEntryType(DocumentType::class)
                ->setFormTypeOptions(['by_reference' => false]),

            yield ArrayField::new('getPersons', $this->translator->trans('admin.contract.form.getPersons.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailPersons.html.twig'),

            //1 ou +s devis
            yield AssociationField::new('quotes', $this->translator->trans('admin.contract.form.quotes.label',
                [], 'admin'))->onlyOnForms(),

            //1 ou +s factures
            yield AssociationField::new('invoices', $this->translator->trans('admin.contract.form.invoices.label',
                [], 'admin'))->onlyOnForms(),
            //COMPTEURS & LISTS
            yield TextField::new('getPeriod', $this->translator->trans('admin.contract.form.getPeriod.label',
                [], 'admin'))->onlyOnDetail(),

            yield ArrayField::new('getQuotes', $this->translator->trans('admin.contract.form.getQuotes.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailQuotes.html.twig'),

            yield ArrayField::new('getInvoices', $this->translator->trans('admin.contract.form.getInvoices.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailInvoices.html.twig'),

            yield ArrayField::new('getPayments', $this->translator->trans('admin.contract.form.getPayments.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailPayments.html.twig'),

            yield ArrayField::new('getProducts', $this->translator->trans('admin.contract.form.getProducts.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailProducts.html.twig'),

            yield ArrayField::new('getDocuments', $this->translator->trans('admin.contract.form.getDocuments.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailDocuments.html.twig'),

            yield ArrayField::new('getAdherents', $this->translator->trans('admin.contract.form.getAdherents.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailAdherents.html.twig'),

            yield ArrayField::new('getEvents', $this->translator->trans('admin.contract.form.getEvents.label',
                [], 'admin'))->onlyOnDetail()->setTemplatePath('admin/contract/detailEvents.html.twig'),

            yield IntegerField::new('getCountAdherents', $this->translator->trans('admin.contract.form.getCountAdherents.label',
                [], 'admin'))->onlyOnIndex()->setTextAlign('right'),

            yield IntegerField::new('getCountEvents', $this->translator->trans('admin.contract.form.getCountEvents.label',
                [], 'admin'))->onlyOnIndex()->setTextAlign('right'),

        ];
    }

}