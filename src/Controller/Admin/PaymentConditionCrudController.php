<?php

namespace App\Controller\Admin;

use App\Entity\PaymentCondition;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class PaymentConditionCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return PaymentCondition::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setPageTitle('index', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.paymentCondition.index.headTitle',
                    [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.paymentCondition.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new','<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.paymentCondition.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-cogs"></i> ' . $this->translator->trans('admin.paymentCondition.edit.headTitle',
                [], 'admin'));
        $crud->overrideTemplate('crud/index', 'admin/paymentCondition/index.html.twig');
        $crud->overrideTemplate('crud/new', 'admin/paymentCondition/new.html.twig');
        $crud->overrideTemplate('crud/edit', 'admin/paymentCondition/edit.html.twig');
        $crud->overrideTemplate('crud/detail', 'admin/paymentCondition/detail.html.twig');

        return $crud->showEntityActionsAsDropdown();
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.paymentCondition.new.headTitle',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel($this->translator->trans('admin.action.view.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel($this->translator->trans('admin.action.edit.label', [], 'admin'));
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel($this->translator->trans('admin.action.delete.label', [], 'admin'));
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name', $this->translator->trans('admin.paymentCondition.form.name.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['autofocus' => true, 'placeholder' => $this->translator->trans('admin.paymentCondition.form.name.placeholder',
                    [], 'admin')],
            ]),
        ];
    }

}
