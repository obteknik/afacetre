<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Slider;
use App\Entity\Post;
use App\Entity\Product;
use App\Repository\DisciplineRepository;
use App\Repository\OrganizationActivityRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PersonRepository;
use App\Repository\PostRepository;
use App\Repository\QuotationRepository;
use App\Repository\QuoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class HomeController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class HomeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * HomeController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Aller vers la HOMEPAGE
     * @Route("/", name="home")
     * @param OrganizationRepository $organizationRepository
     * @param QuotationRepository $quotationRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function index(OrganizationRepository $organizationRepository, QuotationRepository $quotationRepository,
                          SerializerInterface $serializer): Response
    {
        $organization = $organizationRepository->find(1);
        $homeActivateTeamSection = $organization->getIsHomeTeam();
        $homeActivateBestSection = $organization->getIsHomeBest();
        $homeActivatePostSection = $organization->getIsHomePost();

        if ($homeActivateTeamSection) {
            $persons = $this->entityManager->getRepository(Person::class)->findAll();
        }

        if ($homeActivateBestSection) {
            $products = $this->entityManager->getRepository(Product::class)->findByIsBest(1);
        }

        if ($homeActivatePostSection) {
            $posts = $this->entityManager->getRepository(Post::class)->findBy(['isEnabled' => true]);
        }

        //citation
        $quotations = $quotationRepository->findBy(['isActive' => true]);
        $jsonQuotations = $serializer->serialize($quotations, 'json', ['groups' => 'group1']);

        return $this->render('home/index.html.twig', [
            'home' => true,
            'organization' => $organization,
            'persons' => isset($persons) && $persons != null ? $persons : [],
            'products' => isset($products) && $products != null ? $products : [],
            'posts' => isset($posts) && $posts != null ? $posts : [],
            'firstQuotation' => $quotations[0],
            'quotations' => $jsonQuotations,
        ]);
    }

    /**
     * Aller vers une fiche ACTIVITE
     * @Route("/activite/{name}", name="app_activity_name")
     * @param $name
     * @param OrganizationActivityRepository $organizationActivityRepository
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function goToActivity($name, OrganizationActivityRepository $organizationActivityRepository,
                                 OrganizationRepository $organizationRepository): Response
    {
//        dd($name);

        return $this->render('activity/index.html.twig', [
            'organization' => $organizationRepository->find(1),
            'activity' => $organizationActivityRepository->findOneByName($name)
        ]);
    }

    /**
     * Aller vers la politique de confidentialité
     * @Route("/politique-confidentialite", name="privacy")
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function goToPrivacy(OrganizationRepository $organizationRepository): Response
    {
        return $this->render('home/privacy.html.twig', [
            'organization' => $organizationRepository->find(1)
        ]);
    }

    /**
     * * Aller vers les mentions légales
     * @Route("/mentions-legales", name="legalterms")
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function goToLegalTerms(OrganizationRepository $organizationRepository): Response
    {
        return $this->render('home/legalTerms.html.twig', [
            'organization' => $organizationRepository->find(1)
        ]);
    }

    /**
     * Aller vers la fiche de l'article
     * @Route("/post/{slug}", name="post_show")
     * @param $slug
     * @param PostRepository $repository
     * @return Response
     */
    public function postShow($slug, PostRepository $repository): Response
    {
        return $this->render('home/post.html.twig', [
            'post' => $repository->findOneBySlug($slug),
            'posts' => $repository->findAll()]);
    }

    /**
     * Aller vers la fiche de l'intervenant
     * @Route("/person/{slug}", name="person_show")
     * @param $slug
     * @param PersonRepository $repository
     * @return Response
     */
    public function personShow($slug, PersonRepository $repository): Response
    {
        return $this->render('home/person.html.twig', [
            'persons' => $repository->findAll(),
            'person' => $repository->findOneBySlug($slug)
        ]);
    }

    /**
     * Aller vers la fiche de la discipline
     * @Route("/discipline/{slug}", name="discipline_show")
     * @param $slug
     * @param DisciplineRepository $repository
     * @return Response
     */
    public function disciplineShow($slug, DisciplineRepository $repository): Response
    {
        return $this->render('home/discipline.html.twig', [
            'disciplines' => $repository->findAll(),
            'discipline' => $repository->findOneBySlug($slug)
        ]);
    }
}
