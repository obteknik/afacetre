<?php

namespace App\Controller;

use App\Repository\OrganizationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class SecurityController extends AbstractController
{
    /**
     * @Route({ "en": "/login", "fr": "/connexion"}, name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils, OrganizationRepository $organizationRepository): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('account');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'organization' =>$organizationRepository->find(1),
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route({ "en": "/logout", "fr": "/deconnexion"}, name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
