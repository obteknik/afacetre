<?php

namespace App\Controller;

use App\Entity\CalendarParameter;
use App\Form\CalendarParameterType;
use App\Repository\CalendarParameterRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalendarParameterController extends AbstractController
{
    /**
     * @Route("/calendar/parameter/edit/{id}", name="calendar_parameter_edit")
     * @param Request $request
     * @param CalendarParameter $calendarParameter
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, CalendarParameter $calendarParameter, EntityManagerInterface $manager): Response
    {

        $form = $this->createForm(CalendarParameterType::class, $calendarParameter);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->flush();

            $this->addFlash('notice', "Paramétrage agenda bien mis à jour.");
            return $this->redirectToRoute('events', ['token' => $this->getUser()->getAgendaToken()]);
        }
        return $this->render('calendar_parameter/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
