<?php

namespace App\Controller;

use App\Entity\AdherentEvents;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdherentEventController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * AdherentEventController constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    /**
     * SWITCH PARTICIPANT PRESENT/ABSENT A UNE SEANCE DE FORMATION
     * @Route("/adherentEvent/{id}/switch/", name="app_adherentevent_switch",
     *     methods={"GET"}, options={"expose":true})
     * @param Request $request
     * @param AdherentEvents $adherentEvent
     * @return JsonResponse
     */
    public function switchOnOff(Request $request, AdherentEvents $adherentEvent)
    {

        $action = $request->query->get('action');
        if($action ==="false"){
            $adherentEvent->setIsHere(false);
        } else {
            $adherentEvent->setIsHere(true);
        }

        $this->manager->persist($adherentEvent);
        $this->manager->flush();

        return new JsonResponse(['message' => 'ok', Response::HTTP_OK]);
    }
}
