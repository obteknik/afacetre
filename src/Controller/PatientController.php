<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\Patient;
use App\Form\EventPatientType;
use App\Manager\EventManager;
use App\Manager\InvoiceManager;
use App\Repository\EventRepository;
use App\Repository\EventTypeRepository;
use App\Repository\FiscalYearRepository;
use App\Repository\PaymentConditionRepository;
use App\Repository\PersonRepository;
use App\Repository\UserRepository;
use App\Services\DompdfService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class PatientController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PatientController extends AbstractController
{
    /**
     * @var DompdfService
     */
    private $dompdfService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PatientController constructor.
     * @param DompdfService $dompdfService
     * @param LoggerInterface $logger
     */
    public function __construct(DompdfService $dompdfService, LoggerInterface $logger)
    {
        $this->dompdfService = $dompdfService;
        $this->logger = $logger;
    }

    /**
     * Créer un rendez-vous pour le patient
     * @Route("/patient/{patient}/addEvent", name="app_patient_add_event", methods={"GET","POST"}, options={"expose":true})
     * @param Request $request
     * @param Patient $patient
     * @param EventManager $eventManager
     * @param FiscalYearRepository $fiscalYearRepository
     * @param PersonRepository $personRepository
     * @param EventTypeRepository $eventTypeRepository
     * @param EventRepository $eventRepository
     * @return JsonResponse
     */
    public function addEvent(Request $request, Patient $patient, EventManager $eventManager,
                             FiscalYearRepository $fiscalYearRepository, PersonRepository $personRepository,
                             EventTypeRepository $eventTypeRepository, EventRepository $eventRepository)
    {

        $event = new Event();
        $form = $this->createForm(EventPatientType::class, $event, ['mode' => 'add']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();

            //check if first event for patient
            $isFirst = count($patient->getEvents()) == 0 ? true : false;

            //si person non présent (1 seul intervenant en base)
            if (!isset($data["event_patient"]["person"])) {
                $person = $personRepository->find(1);
                if (!$person) {
                    return new JsonResponse([
                        'errorPerson' => "Veuillez créer un intervenant avant de créer une séance."
                    ], Response::HTTP_OK);
                }
            }

            //TODO: revoir set fiscalyear
            $fiscalYear = $fiscalYearRepository->find(3);
            $eventType = $eventTypeRepository->find($data["event_patient"]["type"]);

            $resultManager = $eventManager->create(
                $patient->getOrganization(), $fiscalYear,
                $person,
                $eventType != null ? $eventType : null,
                new \DateTime($data["event_patient"]["startAt"]),
                new \DateTime($data["event_patient"]["endAt"]),
                trim($data["event_patient"]["title"]), null,
                $patient, null,$isFirst);

            if (isset($resultManager["errorMessage"]) && $resultManager["errorMessage"] instanceof \Exception) {
                return new JsonResponse([
                    'errorSys' => $resultManager["errorMessage"]],
                    Response::HTTP_OK);

            } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof Event) {

                $tableHtml = $this->render('admin/patient/detail/tabs/event/_table.html.twig', [
                    'patient' => $patient,
                    'events' => $eventRepository->findByPatient($patient)
                ]);

                return new JsonResponse(['tableHtml' => $tableHtml->getContent()], Response::HTTP_OK);
            }
        }

        $formHtml = $this->render('admin/patient/_modalForm.html.twig', [
            'form' => $form->createView(),
            'mode' => 'add'
        ]);
        return new JsonResponse(['formHtml' => $formHtml->getContent()], Response::HTTP_OK);
    }

    /**
     * GENERER UNE FACTURE POUR UNE OU PLUSIEURS SEANCES
     * @Route("/ma-facture-cabinet/{id}", name="patient_events_invoice_add", options={"expose":true}, methods={"GET"})
     * @param Request $request
     * @param Patient $patient
     * @param EventRepository $eventRepository
     * @param PaymentConditionRepository $paymentConditionRepository
     * @param InvoiceManager $invoiceManager
     * @param FiscalYearRepository $fiscalYearRepository
     * @return Response
     */
    public function addInvoiceForEvents(Request $request, Patient $patient, EventRepository $eventRepository,
                                        PaymentConditionRepository $paymentConditionRepository, InvoiceManager $invoiceManager,
                                        FiscalYearRepository $fiscalYearRepository): Response
    {
        //récupération des id events/séances
        $strEventIds = $request->query->get('eventIds');
        $eventIds = explode('_', $strEventIds);

        //pour chaque id event, je remplis un tabl'eau d'events
        $events = [];
        foreach($eventIds as $id){
            $events[] = $eventRepository->find($id);
        }

        /*
         * Pas de facture pdf si pas d'enregistrement Invoice/invoiceDetail pour l'event
         * je passe à InvoiceManager un tableau d'events => pour chaque ligne détail, nous
         * aurons qty = 1 et price = $e->getPrice()
         */

        $organization = $this->getUser()->getOrganization();
        $fiscalYear = $fiscalYearRepository->find(3);
        $paymentCondition = $paymentConditionRepository->find(1);
        $resultManagerInvoice = $invoiceManager->create($organization, $fiscalYear, $patient,
            null, null, $paymentCondition, null, null, null, $events);

        return new JsonResponse([
            'invoiceId' => $resultManagerInvoice['item']->getId(),
            'filename' => $resultManagerInvoice['item']->getFilename(), Response::HTTP_OK]);
    }
}
