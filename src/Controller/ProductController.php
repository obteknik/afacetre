<?php

namespace App\Controller;

use App\Classe\Search;
use App\Entity\Product;
use App\Form\SearchType;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use App\Services\DompdfService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ProductController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ProductController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DompdfService
     */
    private $dompdfService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ProductController constructor.
     * @param EntityManagerInterface $entityManager
     * @param DompdfService $dompdfService
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, DompdfService $dompdfService,
                                LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->dompdfService = $dompdfService;
        $this->logger = $logger;
    }

    /**
     * @Route({ "en": "/our-products", "fr": "/nos-produits"}, name="products")
     * @param ProductRepository $productRepository
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function index(ProductRepository $productRepository, OrganizationRepository $organizationRepository): Response
    {

        $availableProducts = $productRepository->findBy(['isAvailable' => true]);
        $noAvailableProducts = $productRepository->findBy(['isAvailable' => false]);

        return $this->render('product/index.html.twig', [
            'organization' => $organizationRepository->find(1),
            'products' => $productRepository->findAll(),
            'availableProducts' => $availableProducts,
            'noAvailableProducts' => $noAvailableProducts,
        ]);
    }

    /**
     * @Route({ "en": "/product/{slug}", "fr": "/produit/{slug}"}, name="product")
     * @param SessionInterface $session
     * @param $slug
     * @param OrganizationRepository $organizationRepository
     * @return Response
     */
    public function show(SessionInterface $session, $slug, OrganizationRepository $organizationRepository): Response
    {

        $product = $this->entityManager->getRepository(Product::class)->findOneBySlug($slug);
        $products = $this->entityManager->getRepository(Product::class)->findByIsBest(1);

        if (!$product) {
            return $this->redirectToRoute('products');
        }

        $cart = $session->get('cart', []);

        return $this->render('product/show.html.twig', [
            'organization' => $organizationRepository->find(1),
            'product' => $product,
            'products' => $products,
            'cartQuantity' => $cartProductQuantity = isset($cart[$product->getId()]) ? $cart[$product->getId()] : 0
        ]);
    }

    /**
     * @Route("/formation/{id}/fiche-pedagogique", name="product_pedagogic_sheet_pdf")
     * @param Product $product
     * @return Response
     */
    public function getPedagogicSheet(Product $product): Response
    {
        try {
            $this->dompdfService->getPedagogicSheetPdf($product);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }
}
