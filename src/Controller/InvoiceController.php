<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Repository\OrganizationRepository;
use App\Services\MailerClassicService;
use App\Services\MailerMailjetService;
use App\Services\DompdfService;
use App\Services\SwiftMailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class InvoiceController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var DompdfService
     */
    private $dompdfService;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * InvoiceController constructor.
     * @param LoggerInterface $logger
     * @param DompdfService $dompdfService
     * @param EntityManagerInterface $manager
     */
    public function __construct(LoggerInterface $logger, DompdfService $dompdfService, EntityManagerInterface $manager)
    {
        $this->logger = $logger;
        $this->dompdfService = $dompdfService;
        $this->manager = $manager;
    }

    /**
     * GENERER UNE FACTURE POUR UNE OU PLUSIEURS SEANCES
     * @Route("/ma-facture-cabinet/pdf/{id}", name="patient_events_invoice_generatepdf", options={"expose":true}, methods={"GET"})
     * @param Invoice $invoice
     * @return Response
     */
    public function downloadInvoice(Invoice $invoice)
    {
        $this->getPdfInvoice($invoice);

        return new Response();
    }

    /**
     * ENVOYER UNE FACTURE PAR EMAIL AU PATIENT SELON MODE DE TRANSPORT ORGANIZATION
     * @Route("/ma-facture-cabinet/send/{id}", name="patient_events_invoice_send", options={"expose":true}, methods={"GET"})
     * @param Request $request
     * @param SwiftMailerService $swiftMailerService
     * @param Invoice $invoice
     * @param OrganizationRepository $organizationRepository
     * @return JsonResponse
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function send(Request $request, SwiftMailerService $swiftMailerService, Invoice $invoice, OrganizationRepository $organizationRepository)
    {

        $organization = $organizationRepository->find(1);
        $emailTransportMode = $organization->getEmailTransportMode();
        $message = '';

        $bcc = [];
        if ($request->query->get('bcc') == 1) {
            $bcc = [$_ENV["COPY_TECHNIC_EMAIL"] => $_ENV["COPY_TECHNIC_NAME"]];
        }

        if ($emailTransportMode == 'Mode classique') {
            $patient = $invoice->getPatient();
            $to_email = $patient->getEmail();
            $to_name = $patient->getFirstname() . ' ' . $patient->getLastname();
            $subject = "L'AfAC de l'Être - Votre facture réf. " . $invoice->getReference();
            $template = 'email/_sendPatientInvoice.html.twig';
            $templateOptions = ['invoice' => $invoice];

            $path = "output/pdf/invoice/";
            $filename = "Sophrologie-" . $invoice->getPatient()->getFirstname() . " " .
                $invoice->getPatient()->getLastname() . "-Facture réf. " . $invoice->getReference() . ".pdf";

            $result = $swiftMailerService->send([$to_email => $to_name], $subject, $template, $templateOptions,
                $path . $filename, [], $bcc);

            $message = $result == 1 ?
                "Facture envoyée à " . $to_name .
                " sur son courriel <span style='font-weight: bold;'>" . $to_email . "</span>" :
                "Anomalie dans l'envoi du message";

        } else { //VIA Third-party
            //TODO:Envoi par Symfony Mailer/MAILjet ou mailgun
        }

        return new JsonResponse(['message' => $message], Response::HTTP_OK);
    }

    /**
     * @param Invoice $invoice
     */
    public function getPdfInvoice(Invoice $invoice)
    {
        //Génération de la facture en pdf
        try {
            $path = "output/pdf/invoice/";
            $filename = "Sophrologie-" . $invoice->getPatient()->getFirstname() . " " .
                $invoice->getPatient()->getLastname() . "-Facture réf. " . $invoice->getReference() . ".pdf";

            $this->dompdfService->getPdf(
                $invoice,
                '_pdfModels/patient/01-invoice.html.twig',
                null,
                'patientInvoice',
                'portrait',
                $path . $filename,
                'Open Sans Condensed'
            );
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
    }

}