<?php

namespace App\Controller;

use App\Entity\Search\TimeslotSearch;
use App\Entity\Timeslot;
use App\Form\Search\TimeslotSearchType;
use App\Form\TimeslotType;
use App\Repository\PersonRepository;
use App\Repository\TimeslotRepository;
use App\Repository\UserRepository;
use App\Services\DompdfService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class TimeslotController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class TimeslotController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var DompdfService
     */
    private $dompdfService;

    /**
     * EventController constructor.
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $manager
     * @param DompdfService $dompdfService
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $manager, DompdfService $dompdfService)
    {
        $this->logger = $logger;
        $this->manager = $manager;
        $this->dompdfService = $dompdfService;
    }

    /**
     * @Route("/timeslots/{token}", name="timeslots", methods={"GET"}, options={"expose":true})
     * @param $token
     * @param Request $request
     * @param TimeslotRepository $timeslotRepository
     * @param PersonRepository $personRepository
     * @param UserRepository $userRepository
     * @return Response
     */
    public function index($token, Request $request, TimeslotRepository $timeslotRepository,
                          PersonRepository $personRepository, UserRepository $userRepository): Response
    {
        $search = new TimeslotSearch();

        /*
         * Si token récupéré en GET correspond à un user ROLE_ADMIN, alors tous events
         * Si token récupéré en GET correspond à un user ROLE_PERSON, alors perso events
         */

        $userAskedToken = $userRepository->findOneByAgendaToken($token); //user concerné
        $roleUserAskedToken = $userAskedToken->getRoles()[0];

        if ($roleUserAskedToken == 'ROLE_ADMIN') {
            //toutes les plages d'activité
            $timeslots = $timeslotRepository->findAll();
        } else {
            //Rendez-vous de l'intervenant
            $search->setConnectedUser($userAskedToken);
            $timeslots = $timeslotRepository->findAllQuery($search);
        }

        $persons = null;
        $persons = $personRepository->findAll();

        $formSearch = $this->createForm(TimeslotSearchType::class, $search);
        $formSearch->handleRequest($request);

        $dataTimeslots = [];
        //Parcours des plages
        foreach ($timeslots as $t) {
            try {
                /** @var Timeslot $t */
                $dataTimeslot['resourceId'] = $t->getId();
                $dataTimeslot['id'] = $t->getId();
//                $dataTimeslot['title'] = $e->getTitle();
                $dataTimeslot['start'] = $t->getstartAt()->format('Y-m-d H:i:s');
                $dataTimeslot['end'] = $t->getEndAt()->format('Y-m-d H:i:s');

//                $settings['agendaMinDate'] = $agendaMinDate->getOptionValue();
//                $settings['agendaMaxDate'] = $agendaMaxDate->getOptionValue();
//                $settings['firstDayWeek'] = $firstDayWeek->getOptionValue();
//                $settings['minTimeDay'] = $minTimeDay->getOptionValue();
//                $settings['maxTimeDay'] = $maxTimeDay->getOptionValue();
//                $settings['weekNumbers'] = $weekNumbers->getOptionValue();

                //objectParameter
//                if ($e->getEventObjectParameter() !== null) {
//                    $dataEvent['object'] = $e->getEventObjectParameter()->getValue();
//                    $dataEvent['backgroundColor'] = $e->getEventObjectParameter()->getOptionBackgroundColor();
//                    $dataEvent['borderColor'] = $e->getEventObjectParameter()->getOptionBorderColor();
//                    $dataEvent['textColor'] = $e->getEventObjectParameter()->getOptionTextColor();
//
//                } else {
//                    $dataEvent['object'] = null;
//
//                    $dataEvent['type'] = $e->getEventTypeParameter()->getValue();
//                    $dataEvent['backgroundColor'] = $e->getEventTypeParameter()->getOptionBackgroundColor();
//                    $dataEvent['borderColor'] = $e->getEventTypeParameter()->getOptionBorderColor();
//                    $dataEvent['textColor'] = $e->getEventTypeParameter()->getOptionTextColor();
//                }


                //typeParameter & locationParameter
////                $dataEvent['type'] = $e->getEventTypeParameter()->getValue();
//                $dataEvent['location'] = $e->getEventLocationParameter()->getValue();
//
//                $dataEvent['status'] = $e->getStatus();
//                $dataEvent['description'] = $e->getComment() !== null ? $e->getComment()->getNote():'';
//                $dataEvent['isAllDay'] = $e->getIsAllDay();
//                $dataEvent['isPeriodic'] = $e->getIsPeriodic();
//                $dataEvent['isConfirmedByEmail'] = $e->getIsConfirmedByEmail();

                //participants (persons)
//                $dataEvent['persons'] = $e->getPersons();

                $dataTimeslot['htmlTitle'] = "
                    <span style='color:#FFF; font-weight: bold;'>" . $e->getTitle() . "</span>
                    ";
                $dataTimeslot['htmlTitleList'] = "<img style='width: 30px; border-radius: 4px' src='' alt=''> <!-- /images/about/1.jpg -->
                    <span style='color:#000;margin-left: 2px;'>Patient/Client: " . $e->getTitle() . "</span>" .
                    " - Type de rendez-vous: <b>" . "</b>" .
                    " - <a target='_blank' href='/fr/secure/charge' style='text-decoration: underline'>Lien vers la fiche du participant</a>";

                $dataTimeslot['htmlTitleList2'] = "<img style='width: 30px; border-radius: 4px' src='/images/about/1.jpg' alt=''>
                    <span style='color:#000'>" . $e->getTitle() . "</span>" .
                    " - <a href='/fr/secure/charge'><i class='icon-link' href=''></i></a>";

                //Ajout de $data au tableau consolidé final passé à la vue
                array_push($dataTimeslots, $dataTimeslot);

            } catch (\Exception $e) {
                $this->logger->critical('[TIMESLOTS] : ' . $e->getMessage());
            }
        }

        return $this->render('admin/event/agenda.html.twig', [
            'formSearch' => $formSearch->createView(),
            'listing' => $dataTimeslots,
            'persons' => $persons
        ]);
    }

    /**
     * RECUPERATION AJAX
     * Liste des PLAGES D'ACTIVITE par organisation
     *
     * @Route("/timeslot/all/{token}", name="app_timeslot_all", options={"expose":true}, methods={"GET"})
     * @param $token
     * @param Request $request
     * @param TimeslotRepository $timeslotRepository
     * @param UserRepository $userRepository
     * @param RouterInterface $router
     * @return JsonResponse
     */
    public function getAll($token, Request $request, TimeslotRepository $timeslotRepository,
                           UserRepository $userRepository, RouterInterface $router
    ): JsonResponse
    {
//        dd($token);

        //Tableau de données & calendar settings
        $dataTimeslots = [];
        $settings = [];

        // ***  des rendez-vous PAR DEFAUT SUR L APERIODE EN COURS ***
        $search = new TimeslotSearch();

        $userAskedToken = $userRepository->findOneByAgendaToken($token); //user concerné
        $roleUserAskedToken = $userAskedToken->getRoles()[0];

        if ($roleUserAskedToken == 'ROLE_ADMIN') {
            //toutes les plages d'activité
            $timeslots = $timeslotRepository->findAll();
        } else {
            //Rendez-vous de l'intervenant
            $search->setConnectedUser($userAskedToken);
            $timeslots = $timeslotRepository->findAllQuery($search);
        }

        /** @var CalendarParameter $calendarParameter */
        $calendarParameter = $userAskedToken->getCalendarParameter();

        if (!is_null($calendarParameter)) {
            $settings['agendaMinDate'] = $calendarParameter->getMinDate()->format('Y-m-d');
            $settings['agendaMaxDate'] = $calendarParameter->getMaxDate()->format('Y-m-d');
            $settings['firstDayWeek'] = $calendarParameter->getFirstDayWeek();
            $settings['minTimeDay'] = $calendarParameter->getMinTimeDay()->format('H:i');
            $settings['maxTimeDay'] = $calendarParameter->getMaxTimeDay()->format('H:i');
            $settings['weekNumbers'] = $calendarParameter->getWeekNumbers();
        }

        //Parcours des rendez-vous
        foreach ($timeslots as $t) {
            try {
                /** @var Timeslot $t */
                $dataTimeslot['resourceId'] = $t->getId();
                $dataTimeslot['id'] = $t->getId();
                $dataTimeslot['title'] = $t->getTitle();
                $dataTimeslot['start'] = $t->getstartAt()->format('Y-m-d H:i:s');
                $dataTimeslot['end'] = $t->getEndAt()->format('Y-m-d H:i:s');

                //design
                $dataTimeslot['backgroundColor'] = '#A29B9B';
                $dataTimeslot['borderColor'] = '#FF0000';
                $dataTimeslot['textColor'] = '#FFF';

                $dataTimeslot['status'] = $t->getStatus();
                $dataTimeslot['isAllDay'] = $t->getIsAllDay();
                $dataTimeslot['isPeriodic'] = $t->getIsPeriodic();
                $dataTimeslot['isConfirmedByEmail'] = $t->getIsConfirmedByEmail();

                $urlPerson = $this->generateUrl('person_show',['slug' => $t->getPerson()->getSlug()]);
                $dataTimeslot['htmlTitle'] = "<a href='".$urlPerson."'><span style='color:#FFF'>" .
                    $t->getPerson()->getFirstname() . " " . $t->getPerson()->getLastname() . "</span></a>
                    <span style='color:yellow;font-weight:bold'><br>" . $t->getTitle() ."</span>";

                //Ajout de $data au tableau consolidé final passé à la vue
                array_push($dataTimeslots, $dataTimeslot);

            } catch (\Exception $e) {
                $this->logger->critical('[TIMESLOTS] : ' . $e->getMessage());
            }
        }

        //objets de RDV prédéfinis
//        $predefinedObjets = $eventObjectParameterRepository->findBy(['organization' => $this->getUser()->getOrganization()]);
//        $predefinedEventTypes = $this->render('admin/event/_predefinedEventTypes.html.twig', ['predefinedObjets' => $predefinedObjets]);

        return new JsonResponse([
            'listing' => $dataTimeslots,
            'settings' => $settings
        ], Response::HTTP_OK); //formSearchView' => $formSearchView->getContent()]
    }

    /**
     * @Route("/timeslot/new", name="app_timeslot_new", options={"expose":true}, methods={"GET", "POST"})
     * @param Request $request
     * @param EventManager $eventManager
     * @return Response
     */
    public function new(Request $request, EventManager $eventManager): Response
    {
        $event = new Event();

        $form = $this->createForm(EventType::class, $event, [ 'action' => 'create']);
        $form->handleRequest($request);

        //traitement POST => Rendez-vous créé par D&D selon type prédéfni
        if ($request->getMethod() === 'POST' && $request->query->get('mode') === 'direct') {

            $title = $request->query->get('title');
            $startDate = $request->query->get('startDate');
            $endDate = $request->query->get('endDate');
            $startTime = $request->query->get('startTime');
            $endTime = $request->query->get('endTime');

            $startDateTime = \DateTime::createFromFormat('Y-m-d H:i', $startDate . ' ' . $startTime);
            $endDateTime = \DateTime::createFromFormat('Y-m-d H:i', $endDate . ' ' . $endTime);

            $event->setTitle($title);
            $event->setStartAt($startDateTime);
            $event->setEndAt($endDateTime);

            //TODO: appeler le manager !
            $this->manager->persist($event);
            $this->manager->flush();

//            //historical
//            try {
//                $historicalService->generate([$event], 'edit');
//            } catch (\Exception $e) {
//                $resultManager["errorHistoricalMessage"] = "Anomalie rencontrée lors de l'historisation de l'édition (redimensionnement/déplacement) du rendez-vous : "
//                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
//                $this->logger->error("[HistoricalService/generate] - " . $resultManager["errorHistoricalMessage"]);
//            }

            return new JsonResponse(['message' => "Rendez-vous modifié"], Response::HTTP_OK);
        }

        // *** soumission & validation du formulaire ***
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();

//            dd($data);
//            $isAllDay = isset($data["event"]["isAllDay"]) ? $data["event"]["isAllDay"] : false;
//            $isPeriodic = isset($data["event"]["isPeriodic"]) ? $data["event"]["isPeriodic"] : false;
//            $isConfirmedByEmail = isset($data["event"]["isConfirmedByEmail"]) ? $data["event"]["isConfirmedByEmail"] : false;

            $isAllDay = $data["event"]["isAllDay"] ?? false;
            $isPeriodic = $data["event"]["isPeriodic"] ?? false;
            $isConfirmedByEmail = $data["event"]["isConfirmedByEmail"] ?? false;

            $strDateStart = $data["event"]["startAt"].' '.$data["event"]["startAt_time"]['hour'].':'.$data["event"]["startAt_time"]['minute'];
            $strDateEnd = $data["event"]["endAt"].' '.$data["event"]["endAt_time"]['hour'].':'.$data["event"]["endAt_time"]['minute'];

            $resultManager = $eventManager->create(
                new \DateTime($strDateStart),
                new \DateTime($strDateEnd),
                $data["event"]["title"], $data["event"]["comment"],
//                $data["event"]["persons"],
                $isAllDay, $isPeriodic, $isConfirmedByEmail);

            if (isset($resultManager["errorMessage"]) && $resultManager["errorMessage"] instanceof \Exception) {
                return new JsonResponse([
                    'errorSys' => $resultManager["errorMessage"]],
                    Response::HTTP_OK);

            } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof Event) {
                return new JsonResponse([
                    'message' => "Rendez-vous enregistré.",
                    'token' => $this->getUser()->getAgendaToken()
                ], Response::HTTP_OK);
            }
        }

        $formHtml = $this->render('admin/event/_form2.html.twig', [
            'action' => 'create',
            'event' => $event,
            'form' => $form->createView(),
        ]);

        return new JsonResponse(['formHtml' => $formHtml->getContent()], Response::HTTP_OK);
    }

    /**
     * @Route("/timeslot/edit/{id}/", name="app_timeslot_edit", requirements={"id"="\d+"}, options={"expose":true}, methods={"GET", "POST"})
     * @param Request $request
     * @param Timeslot $timeslot
     * @return Response
     */
    public function edit(Request $request, Timeslot $timeslot): Response
    {

        $form = $this->createForm(TimeslotType::class, $timeslot, [
            'organization' => $this->getUser()->getOrganization(),
            'action' => 'edit',
            'startAt' => $timeslot->getStartAt(),
            'endAt' => $timeslot->getEndAt(),
        ]);

        $form->handleRequest($request);

        //traitement POST => redimensionnement & déplacement du rendez-vous
        if ($request->getMethod() === 'POST' && $request->query->get('mode') === 'direct') {

            $startDate = $request->query->get('startDate');
            $endDate = $request->query->get('endDate');
            $startTime = $request->query->get('startTime');
            $endTime = $request->query->get('endTime');

            $startDateTime = \DateTime::createFromFormat('Y-m-d H:i', $startDate . ' ' . $startTime);
            $endDateTime = \DateTime::createFromFormat('Y-m-d H:i', $endDate . ' ' . $endTime);

            $timeslot->setStartAt($startDateTime);
            $timeslot->setEndAt($endDateTime);

            //TODO: appeler le manager !
            $this->manager->persist($timeslot);
            $this->manager->flush();

            //historical
//            try {
//                $historicalService->generate([$event], 'edit');
//            } catch (\Exception $e) {
//                $resultManager["errorHistoricalMessage"] = "Anomalie rencontrée lors de l'historisation de l'édition (redimensionnement/déplacement) du rendez-vous : "
//                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
//                $this->logger->error("[HistoricalService/generate] - " . $resultManager["errorHistoricalMessage"]);
//            }

            return new JsonResponse(['message' => "Rendez-vous modifié"], Response::HTTP_OK);
        }

        //traitement POST du formulaire de modification
        if ($form->isSubmitted() && $form->isValid()) {

            //TODO: appeler le manager !
            $this->manager->persist($timeslot);
            $this->manager->flush();

            //historical
//            try {
//                $historicalService->generate([$event], 'edit');
//            } catch (\Exception $e) {
//                $resultManager["errorHistoricalMessage"] = "Anomalie rencontrée lors de l'historisation de l'édition du rendez-vous : "
//                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
//                $this->logger->error("[HistoricalService/generate] - " . $resultManager["errorHistoricalMessage"]);
//            }

            return new JsonResponse(['message' => "Rendez-vous modifié"], Response::HTTP_OK);
        }

        $formHtml = $this->render('admin/timeslot/_form2.html.twig', [
            'action' => 'edit',
            'timeslot' => $timeslot,
            'form' => $form->createView(),
        ]);

        return new JsonResponse(['formHtml' => $formHtml->getContent()], Response::HTTP_OK);
    }

    /**
     * @Route("/timeslot/pdf", name="app_timeslot_pdf", methods={"GET"})
     * @param TimeslotRepository $timeslotRepository
     * @return Response
     */
    public function getPdf(TimeslotRepository $timeslotRepository){

        $agendaTimeslots = $timeslotRepository->findAll();

        try {
            $this->dompdfService->getAgendaEventPdf($agendaTimeslots);
        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        }
        return new Response();
    }
}
