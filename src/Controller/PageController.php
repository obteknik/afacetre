<?php

namespace App\Controller;

use App\Repository\PageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route({ "en": "/legalNotice", "fr": "/mentions-legales"}, name="legalnotice")
     * @param PageRepository $pageRepository
     * @return Response
     */
    public function getPageLegalNotice(PageRepository $pageRepository): Response
    {

        $page = $pageRepository->find(1);

        if (!$page) {
            return $this->redirectToRoute('home');
        }

        return $this->render('page/model.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @Route({ "en": "/privacy", "fr": "/politique-confidentialite"}, name="privacy")
     * @param PageRepository $pageRepository
     * @return Response
     */
    public function getPagePrivacy(PageRepository $pageRepository): Response
    {

        $page = $pageRepository->find(2);

        if (!$page) {
            return $this->redirectToRoute('home');
        }

        return $this->render('page/model.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @Route({ "en": "/accompaniment", "fr": "/accompagnement"}, name="accompaniment")
     * @param PageRepository $pageRepository
     * @return Response
     */
    public function getPageAccompaniment(PageRepository $pageRepository): Response
    {

        $page = $pageRepository->find(3);

        if (!$page) {
            return $this->redirectToRoute('home');
        }

        return $this->render('page/model.html.twig', [
            'page' => $page,
        ]);
    }
}
