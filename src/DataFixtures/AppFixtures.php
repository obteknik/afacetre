<?php

namespace App\DataFixtures;

use App\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $pages = [

            //MENTIONS LEGALES
            ['title' => 'Mentions légales', 'content' => '
<p>Conform&eacute;ment aux dispositions des articles 6-III et 19 de la Loi n&deg; 2004-575 du 21 juin 2004 pour la Confiance dans l&rsquo;&eacute;conomie num&eacute;rique, dite L.C.E.N., nous portons &agrave; la connaissance des utilisateurs et visiteurs du site : <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> les informations suivantes :</p>

<p style="text-align:justify"><strong>Informations l&eacute;gales : </strong></p>

<p style="text-align:justify">Statut du propri&eacute;taire : <strong>soci&eacute;t&eacute;</strong></p>

<p>Pr&eacute;fixe : <strong>SARL</strong></p>

<p>Nom de la Soci&eacute;t&eacute; : <strong>L&#39;AFAC de l&#39;ETRE</strong></p>

<p>Adresse : <strong>adresse</strong></p>

<p>T&eacute;l : <strong>0199 99 99 99</strong></p>

<p>Au Capital de : <strong> 3 000 &euro;</strong></p>

<p>SIRET :NNNNNNNN<strong>&nbsp;</strong>R.C.S. : <strong> XXXXXX</strong></p>

<p>Num&eacute;ro TVA intracommunautaire : <strong>FRXXXXXXX</strong></p>

<p>Adresse de courrier &eacute;lectronique : <strong><a href="mailto:aaaa@gmail.com">aaaa@gmail.com</a></strong></p>

<p>Le Cr&eacute;ateur du site est :&nbsp;<strong><a href="mailto:aaaa@gmail.com">aaaa@gmail.com</a></strong></p>

<p>Le Responsable de la publication est : <strong>Olivier BRASSART</strong></p>

<p>Contactez le responsable de la publication : <strong><a href="mailto:aaaa@gmail.com">aaaa@gmail.com</a></strong></p>

<p>Le responsable de la publication est une <strong>personne physique</strong></p>

<p>Le Webmaster est :&nbsp;<strong>Olivier BRASSART</strong></p>

<p>Contactez le Webmaster : <strong><a href="mailto:d.renault@karudev-informatique.fr?subject=Contact%20%C3%AF%C2%BF%C2%BD%20partir%20des%20mentions%20l%C3%AF%C2%BF%C2%BDgales%20via%20le%20site%20www.karudev-informatique.fr">dddd@gmail.com</a></strong></p>

<p>L&rsquo;hebergeur du site est : <strong>VVVVVVVV</strong></p>

<p style="text-align:justify"><strong>2. Description des services fournis :</strong></p>

<p>Le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> a pour objet de fournir une information concernant l&rsquo;ensemble des activit&eacute;s de la soci&eacute;t&eacute;.</p>

<p>Le proprietaire du site s&rsquo;efforce de fournir sur le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> des informations aussi pr&eacute;cises que possible. Toutefois, il ne pourra &ecirc;tre tenue responsable des omissions, des inexactitudes et des carences dans la mise &agrave; jour, qu&rsquo;elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.</p>

<p>Tous les informations propos&eacute;es sur le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> sont donn&eacute;es &agrave; titre indicatif, sont non exhaustives, et sont susceptibles d&rsquo;&eacute;voluer. Elles sont donn&eacute;es sous r&eacute;serve de modifications ayant &eacute;t&eacute; apport&eacute;es depuis leur mise en ligne.</p>

<p style="text-align:justify"><strong>3. Propri&eacute;t&eacute; intellectuelle et contrefa&ccedil;ons :</strong></p>

<p style="text-align:justify">Le proprietaire du site est propri&eacute;taire des droits de propri&eacute;t&eacute; intellectuelle ou d&eacute;tient les droits d&rsquo;usage sur tous les &eacute;l&eacute;ments accessibles sur le site, notamment les textes, images, graphismes, logo, ic&ocirc;nes, sons, logiciels&hellip;</p>

<p>Toute reproduction, repr&eacute;sentation, modification, publication, adaptation totale ou partielle des &eacute;l&eacute;ments du site, quel que soit le moyen ou le proc&eacute;d&eacute; utilis&eacute;, est interdite, sauf autorisation &eacute;crite pr&eacute;alable &agrave; l&rsquo;email : <a href="mailto:d.renault@karudev-informatique.fr?subject=Contact ï¿½ partir des mentions lï¿½gales via le site www.karudev-informatique.fr"><strong>d.renault@karudev-informatique.fr</strong></a> .</p>

<p>Toute exploitation non autoris&eacute;e du site ou de l&rsquo;un quelconque de ces &eacute;l&eacute;ments qu&rsquo;il contient sera consid&eacute;r&eacute;e comme constitutive d&rsquo;une contrefa&ccedil;on et poursuivie conform&eacute;ment aux dispositions des articles L.335-2 et suivants du Code de Propri&eacute;t&eacute; Intellectuelle.</p>

<p style="text-align:justify"><strong>4. Liens hypertextes et cookies :</strong></p>

<p>Le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> contient un certain nombre de liens hypertextes vers d&rsquo;autres sites (partenaires, informations &hellip;) mis en place avec l&rsquo;autorisation de le proprietaire du site . Cependant, le proprietaire du site n&rsquo;a pas la possibilit&eacute; de v&eacute;rifier le contenu des sites ainsi visit&eacute;s et d&eacute;cline donc toute responsabilit&eacute; de ce fait quand aux risques &eacute;ventuels de contenus illicites.</p>

<p>L&rsquo;utilisateur est inform&eacute; que lors de ses visites sur le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a>, un ou des cookies sont susceptible de s&rsquo;installer automatiquement sur son ordinateur. Un cookie est un fichier de petite taille, qui ne permet pas l&rsquo;identification de l&rsquo;utilisateur, mais qui enregistre des informations relatives &agrave; la navigation d&rsquo;un ordinateur sur un site. Les donn&eacute;es ainsi obtenues visent &agrave; faciliter la navigation ult&eacute;rieure sur le site, et ont &eacute;galement vocation &agrave; permettre diverses mesures de fr&eacute;quentation.</p>

<p>Le param&eacute;trage du logiciel de navigation permet d&rsquo;informer de la pr&eacute;sence de cookie et &eacute;ventuellement, de refuser de la mani&egrave;re d&eacute;crite &agrave; l&rsquo;adresse suivante : www.cnil.fr</p>

<p>Le refus d&rsquo;installation d&rsquo;un cookie peut entra&icirc;ner l&rsquo;impossibilit&eacute; d&rsquo;acc&eacute;der &agrave; certains services. L&rsquo;utilisateur peut toutefois configurer son ordinateur de la mani&egrave;re suivante, pour refuser l&rsquo;installation des cookies :</p>

<p>Sous Internet Explorer : onglet outil / options internet. Cliquez sur Confidentialit&eacute; et choisissez Bloquer tous les cookies. Validez sur Ok.</p>

<p>Sous Netscape : onglet &eacute;dition / pr&eacute;f&eacute;rences. Cliquez sur Avanc&eacute;es et choisissez D&eacute;sactiver les cookies. Validez sur Ok.</p>

<p style="text-align:justify"><strong>5. Protection des biens et des personnes &ndash; gestion des donn&eacute;es personnelles :</strong></p>

<p>Utilisateur : Internaute se connectant, utilisant le site susnomm&eacute; : <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a></p>

<p>En France, les donn&eacute;es personnelles sont notamment prot&eacute;g&eacute;es par la loi n&deg; 78-87 du 6 janvier 1978, la loi n&deg; 2004-801 du 6 ao&ucirc;t 2004, l&rsquo;article L. 226-13 du Code p&eacute;nal et la Directive Europ&eacute;enne du 24 octobre 1995.</p>

<p style="text-align:justify">Sur le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a>, le proprietaire du site ne collecte des informations personnelles relatives &agrave; l&rsquo;utilisateur que pour le besoin de certains services propos&eacute;s par le site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a>. L&rsquo;utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu&rsquo;il proc&egrave;de par lui-m&ecirc;me &agrave; leur saisie. Il est alors pr&eacute;cis&eacute; &agrave; l&rsquo;utilisateur du site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> l&rsquo;obligation ou non de fournir ces informations.</p>

<p>Conform&eacute;ment aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative &agrave; l&rsquo;informatique, aux fichiers et aux libert&eacute;s, tout utilisateur dispose d&rsquo;un droit d&rsquo;acc&egrave;s, de rectification, de suppression et d&rsquo;opposition aux donn&eacute;es personnelles le concernant. Pour l&rsquo;exercer, adressez votre demande &agrave; <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> par email : email du webmaster ou en effectuant sa demande &eacute;crite et sign&eacute;e, accompagn&eacute;e d&rsquo;une copie du titre d&rsquo;identit&eacute; avec signature du titulaire de la pi&egrave;ce, en pr&eacute;cisant l&rsquo;adresse &agrave; laquelle la r&eacute;ponse doit &ecirc;tre envoy&eacute;e.</p>

<p style="text-align:justify">Aucune information personnelle de l&rsquo;utilisateur du site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> n&rsquo;est publi&eacute;e &agrave; l&rsquo;insu de l&rsquo;utilisateur, &eacute;chang&eacute;e, transf&eacute;r&eacute;e, c&eacute;d&eacute;e ou vendue sur un support quelconque &agrave; des tiers. Seule l&rsquo;hypoth&egrave;se du rachat du site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a> &agrave; le proprietaire du site et de ses droits permettrait la transmission des dites informations &agrave; l&rsquo;&eacute;ventuel acqu&eacute;reur qui serait &agrave; son tour tenu de la m&ecirc;me obligation de conservation et de modification des donn&eacute;es vis &agrave; vis de l&rsquo;utilisateur du site <a href="{{ path(\'home\') }}" rel="noopener" target="_blank">www.afac-etre.fr</a>.</p>
            
            '],
            //POLITIQUE DE CONFIDENTIALITE
            ['title' => 'Politique de confidentialité', 'content' => '
            <p><strong>L&#39;&Eacute;diteur :</strong> La personne, physique ou morale, qui &eacute;dite les services de communication au public en ligne.<br />
<strong>Le Site :</strong> L&#39;ensemble des sites, pages Internet et services en ligne propos&eacute;s par l&#39;&Eacute;diteur.<br />
<strong>L&#39;Utilisateur :</strong> La personne utilisant le Site et les services.</p>

<h2><br />
<br />
Nature des donn&eacute;es collect&eacute;es</h2>

<p><br />
<strong>Dans le cadre de l&#39;utilisation des Sites, l&#39;&Eacute;diteur est susceptible de collecter les cat&eacute;gories de donn&eacute;es suivantes concernant ses Utilisateurs : </strong><br />
Donn&eacute;es d&#39;&eacute;tat-civil, d&#39;identit&eacute;, d&#39;identification...<br />
Donn&eacute;es de connexion (adresses IP, journaux d&#39;&eacute;v&eacute;nements...)<br />
Donn&eacute;es de localisation (d&eacute;placements, donn&eacute;es GPS, GSM...)</p>

<h2><br />
<br />
Communication des donn&eacute;es personnelles &agrave; des tiers</h2>

<p><br />
<strong>Pas de communication &agrave; des tiers </strong><br />
Vos donn&eacute;es ne font l&#39;objet d&#39;aucune communication &agrave; des tiers. Vous &ecirc;tes toutefois inform&eacute;s qu&#39;elles pourront &ecirc;tre divulgu&eacute;es en application d&#39;une loi, d&#39;un r&egrave;glement ou en vertu d&#39;une d&eacute;cision d&#39;une autorit&eacute; r&eacute;glementaire ou judiciaire comp&eacute;tente.<br />
&nbsp;</p>

<h2><br />
Information pr&eacute;alable pour la communication des donn&eacute;es personnelles &agrave; des tiers en cas de fusion / absorption</h2>

<p><br />
<strong>Information pr&eacute;alable et possibilit&eacute; d&rsquo;opt-out avant et apr&egrave;s la fusion / acquisition</strong><br />
Dans le cas o&ugrave; nous prendrions part &agrave; une op&eacute;ration de fusion, d&rsquo;acquisition ou &agrave; toute autre forme de cession d&rsquo;actifs, nous nous engageons &agrave; garantir la confidentialit&eacute; de vos donn&eacute;es personnelles et &agrave; vous informer avant que celles-ci ne soient transf&eacute;r&eacute;es ou soumises &agrave; de nouvelles r&egrave;gles de confidentialit&eacute;.<br />
&nbsp;</p>

<h2><br />
Finalit&eacute; de la r&eacute;utilisation des donn&eacute;es personnelles collect&eacute;es</h2>

<p><br />
<strong>Effectuer les op&eacute;rations relatives &agrave; la gestion des clients concernant</strong><br />
- les contrats ; les commandes ; les livraisons ; les factures ; la comptabilit&eacute; et en particulier la gestion des comptes clients<br />
- un programme de fid&eacute;lit&eacute; au sein d&#39;une entit&eacute; ou plusieurs entit&eacute;s juridiques ;<br />
- le suivi de la relation client tel que la r&eacute;alisation d&#39;enqu&ecirc;tes de satisfaction, la gestion des r&eacute;clamations et du service apr&egrave;s-vente<br />
-&nbsp;la s&eacute;lection de clients pour r&eacute;aliser des &eacute;tudes, sondages et tests produits (sauf consentement des personnes concern&eacute;es recueilli dans les conditions pr&eacute;vues &agrave; l&rsquo;article 6, ces op&eacute;rations ne doivent pas conduire &agrave; l&#39;&eacute;tablissement de profils susceptibles de faire appara&icirc;tre des donn&eacute;es sensibles - origines raciales ou ethniques, opinions philosophiques, politiques, syndicales, religieuses, vie sexuelle ou sant&eacute; des personnes)<br />
<br />
<strong>Effectuer des op&eacute;rations relatives &agrave; la prospection</strong><br />
-&nbsp;la gestion d&#39;op&eacute;rations techniques de prospection (ce qui inclut notamment les op&eacute;rations techniques comme la normalisation, l&#39;enrichissement et la d&eacute;duplication)<br />
-&nbsp;la s&eacute;lection de personnes pour r&eacute;aliser des actions de fid&eacute;lisation, de prospection, de sondage, de test produit et de promotion. Sauf consentement des personnes concern&eacute;es recueilli dans les conditions pr&eacute;vues &agrave; l&#39;article 6, ces op&eacute;rations ne doivent pas conduire &agrave; l&#39;&eacute;tablissement de profils susceptibles de faire appara&icirc;tre des donn&eacute;es sensibles (origines raciales ou ethniques, opinions philosophiques, politiques, syndicales, religieuses, vie sexuelle ou sant&eacute; des personnes)<br />
-&nbsp;la r&eacute;alisation d&#39;op&eacute;rations de sollicitations<br />
<br />
&nbsp;</p>

<h2>Agr&eacute;gation des donn&eacute;es</h2>

<p><br />
<strong>Agr&eacute;gation avec des donn&eacute;es non personnelles</strong><br />
Nous pouvons publier, divulguer et utiliser les informations agr&eacute;g&eacute;es (informations relatives &agrave; tous nos Utilisateurs ou &agrave; des groupes ou cat&eacute;gories sp&eacute;cifiques d&#39;Utilisateurs que nous combinons de mani&egrave;re &agrave; ce qu&#39;un Utilisateur individuel ne puisse plus &ecirc;tre identifi&eacute; ou mentionn&eacute;) et les informations non personnelles &agrave; des fins d&#39;analyse du secteur et du march&eacute;, de profilage d&eacute;mographique, &agrave; des fins promotionnelles et publicitaires et &agrave; d&#39;autres fins commerciales.<br />
<br />
<strong>Agr&eacute;gation avec des donn&eacute;es personnelles disponibles sur les comptes sociaux de l&#39;Utilisateur</strong><br />
Si vous connectez votre compte &agrave; un compte d&rsquo;un autre service afin de faire des envois crois&eacute;s, ledit service pourra nous communiquer vos informations de profil, de connexion, ainsi que toute autre information dont vous avez autoris&eacute; la divulgation. Nous pouvons agr&eacute;ger les informations relatives &agrave; tous nos autres Utilisateurs, groupes, comptes, aux donn&eacute;es personnelles disponibles sur l&rsquo;Utilisateur.<br />
&nbsp;</p>

<h2><br />
Collecte des donn&eacute;es d&#39;identit&eacute;</h2>

<p><br />
<strong>Consultation libre</strong><br />
La consultation du Site ne n&eacute;cessite pas d&#39;inscription ni d&#39;identification pr&eacute;alable. Elle peut s&#39;effectuer sans que vous ne communiquiez de donn&eacute;es nominatives vous concernant (nom, pr&eacute;nom, adresse, etc). Nous ne proc&eacute;dons &agrave; aucun enregistrement de donn&eacute;es nominatives pour la simple consultation du Site.<br />
<br />
&nbsp;</p>

<h2>Collecte des donn&eacute;es d&#39;identification</h2>

<p><br />
<strong>Utilisation de l&#39;identifiant de l&rsquo;utilisateur uniquement pour l&rsquo;acc&egrave;s aux services</strong><br />
Nous utilisons vos identifiants &eacute;lectroniques seulement pour et pendant l&#39;ex&eacute;cution du contrat.<br />
<br />
&nbsp;</p>

<h2>Collecte des donn&eacute;es du terminal</h2>

<p><br />
<strong>Collecte des donn&eacute;es de profilage et des donn&eacute;es techniques &agrave; des fins de fourniture du service</strong><br />
Certaines des donn&eacute;es techniques de votre appareil sont collect&eacute;es automatiquement par le Site. Ces informations incluent notamment votre adresse IP, fournisseur d&#39;acc&egrave;s &agrave; Internet, configuration mat&eacute;rielle, configuration logicielle, type et langue du navigateur... La collecte de ces donn&eacute;es est n&eacute;cessaire &agrave; la fourniture des services.<br />
<br />
<br />
<strong>Collecte des donn&eacute;es techniques &agrave; des fins publicitaires, commerciales et statistiques</strong><br />
Les donn&eacute;es techniques de votre appareil sont automatiquement collect&eacute;es et enregistr&eacute;es par le Site, &agrave; des fins publicitaires, commerciales et statistiques. Ces informations nous aident &agrave; personnaliser et &agrave; am&eacute;liorer continuellement votre exp&eacute;rience sur notre Site. Nous ne collectons ni ne conservons aucune donn&eacute;e nominative (nom, pr&eacute;nom, adresse...) &eacute;ventuellement attach&eacute;e &agrave; une donn&eacute;e technique. Les donn&eacute;es collect&eacute;es sont susceptibles d&rsquo;&ecirc;tre revendues &agrave; des tiers.</p>

<h2><br />
<br />
Cookies</h2>

<p><br />
<strong>Dur&eacute;e de conservation des cookies</strong><br />
Conform&eacute;ment aux recommandations de la CNIL, la dur&eacute;e maximale de conservation des cookies est de 13 mois au maximum apr&egrave;s leur premier d&eacute;p&ocirc;t dans le terminal de l&#39;Utilisateur, tout comme la dur&eacute;e de la validit&eacute; du consentement de l&rsquo;Utilisateur &agrave; l&rsquo;utilisation de ces cookies. La dur&eacute;e de vie des cookies n&rsquo;est pas prolong&eacute;e &agrave; chaque visite. Le consentement de l&rsquo;Utilisateur devra donc &ecirc;tre renouvel&eacute; &agrave; l&#39;issue de ce d&eacute;lai.<br />
<br />
<strong>Finalit&eacute; cookies</strong><br />
Les cookies peuvent &ecirc;tre utilis&eacute;s pour des fins statistiques notamment pour optimiser les services rendus &agrave; l&#39;Utilisateur, &agrave; partir du traitement des informations concernant la fr&eacute;quence d&#39;acc&egrave;s, la personnalisation des pages ainsi que les op&eacute;rations r&eacute;alis&eacute;es et les informations consult&eacute;es. Vous &ecirc;tes inform&eacute; que l&#39;&Eacute;diteur est susceptible de d&eacute;poser des cookies sur votre terminal. Le cookie enregistre des informations relatives &agrave; la navigation sur le service (les pages que vous avez consult&eacute;es, la date et l&#39;heure de la consultation...) que nous pourrons lire lors de vos visites ult&eacute;rieures.<br />
<br />
<strong>Droit de l&#39;Utilisateur de refuser les cookies</strong><br />
Vous reconnaissez avoir &eacute;t&eacute; inform&eacute; que l&#39;&Eacute;diteur peut avoir recours &agrave; des cookies. Si vous ne souhaitez pas que des cookies soient utilis&eacute;s sur votre terminal, la plupart des navigateurs vous permettent de d&eacute;sactiver les cookies en passant par les options de r&eacute;glage.<br />
&nbsp;</p>

<h2><br />
Conservation des donn&eacute;es techniques</h2>

<p><br />
<strong>Dur&eacute;e de conservation des donn&eacute;es techniques</strong><br />
Les donn&eacute;es techniques sont conserv&eacute;es pour la dur&eacute;e strictement n&eacute;cessaire &agrave; la r&eacute;alisation des finalit&eacute;s vis&eacute;es ci-avant.<br />
<br />
&nbsp;</p>

<h2>D&eacute;lai de conservation des donn&eacute;es personnelles et d&#39;anonymisation</h2>

<p><br />
<strong>Conservation des donn&eacute;es pendant la dur&eacute;e de la relation contractuelle</strong><br />
Conform&eacute;ment &agrave; l&#39;article 6-5&deg; de la loi n&deg;78-17 du 6 janvier 1978 relative &agrave; l&#39;informatique, aux fichiers et aux libert&eacute;s, les donn&eacute;es &agrave; caract&egrave;re personnel faisant l&#39;objet d&#39;un traitement ne sont pas conserv&eacute;es au-del&agrave; du temps n&eacute;cessaire &agrave; l&#39;ex&eacute;cution des obligations d&eacute;finies lors de la conclusion du contrat ou de la dur&eacute;e pr&eacute;d&eacute;finie de la relation contractuelle.<br />
<br />
<strong>Conservation des donn&eacute;es anonymis&eacute;es au del&agrave; de la relation contractuelle / apr&egrave;s la suppression du compte</strong><br />
Nous conservons les donn&eacute;es personnelles pour la dur&eacute;e strictement n&eacute;cessaire &agrave; la r&eacute;alisation des finalit&eacute;s d&eacute;crites dans les pr&eacute;sentes CGU. Au-del&agrave; de cette dur&eacute;e, elles seront anonymis&eacute;es et conserv&eacute;es &agrave; des fins exclusivement statistiques et ne donneront lieu &agrave; aucune exploitation, de quelque nature que ce soit.</p>

<h2><br />
<br />
Indications en cas de faille de s&eacute;curit&eacute; d&eacute;cel&eacute;e par l&#39;&Eacute;diteur</h2>

<p><br />
<strong>Information de l&#39;Utilisateur en cas de faille de s&eacute;curit&eacute;</strong><br />
Nous nous engageons &agrave; mettre en oeuvre toutes les mesures techniques et organisationnelles appropri&eacute;es afin de garantir un niveau de s&eacute;curit&eacute; adapt&eacute; au regard des risques d&#39;acc&egrave;s accidentels, non autoris&eacute;s ou ill&eacute;gaux, de divulgation, d&#39;alt&eacute;ration, de perte ou encore de destruction des donn&eacute;es personnelles vous concernant.<br />
Dans l&#39;&eacute;ventualit&eacute; o&ugrave; nous prendrions connaissance d&#39;un acc&egrave;s ill&eacute;gal aux donn&eacute;es personnelles vous concernant stock&eacute;es sur nos serveurs ou ceux de nos prestataires, ou d&#39;un acc&egrave;s non autoris&eacute; ayant pour cons&eacute;quence la r&eacute;alisation des risques identifi&eacute;s ci-dessus, nous nous engageons &agrave; :<br />
- Vous notifier l&#39;incident dans les plus brefs d&eacute;lais<br />
-&nbsp;Examiner les causes de l&#39;incident et vous en informer<br />
-&nbsp;Prendre les mesures n&eacute;cessaires dans la limite du raisonnable afin d&#39;amoindrir les effets n&eacute;gatifs et pr&eacute;judices pouvant r&eacute;sulter dudit incident<br />
<br />
<strong>Limitation de la responsabilit&eacute;</strong><br />
En aucun cas les engagements d&eacute;finis au point ci-dessus relatifs &agrave; la notification en cas de faille de s&eacute;curit&eacute; ne peuvent &ecirc;tre assimil&eacute;s &agrave; une quelconque reconnaissance de faute ou de responsabilit&eacute; quant &agrave; la survenance de l&#39;incident en question.<br />
&nbsp;</p>

<h2><br />
Transfert des donn&eacute;es personnelles &agrave; l&#39;&eacute;tranger</h2>

<p><br />
<strong>Pas de transfert en dehors de l&#39;Union europ&eacute;enne</strong><br />
L&#39;&Eacute;diteur s&#39;engage &agrave; ne pas transf&eacute;rer les donn&eacute;es personnelles de ses Utilisateurs en dehors de l&#39;Union europ&eacute;enne.<br />
&nbsp;</p>

<h2><br />
Modification des CGU et de la politique de confidentialit&eacute;</h2>

<p><br />
<strong>En cas de modification des pr&eacute;sentes CGU, engagement de ne pas baisser le niveau de confidentialit&eacute; de mani&egrave;re substantielle sans l&#39;information pr&eacute;alable des personnes concern&eacute;es</strong><br />
Nous nous engageons &agrave; vous informer en cas de modification substantielle des pr&eacute;sentes CGU, et &agrave; ne pas baisser le niveau de confidentialit&eacute; de vos donn&eacute;es de mani&egrave;re substantielle sans vous en informer et obtenir votre consentement.<br />
&nbsp;</p>

<h2><br />
Droit applicable et modalit&eacute;s de recours</h2>

<p><br />
<strong>Clause d&#39;arbitrage</strong><br />
Vous acceptez express&eacute;ment que tout litige susceptible de na&icirc;tre du fait des pr&eacute;sentes CGU, notamment de son interpr&eacute;tation ou de son ex&eacute;cution, rel&egrave;vera d&#39;une proc&eacute;dure d&#39;arbitrage soumise au r&egrave;glement de la plateforme d&#39;arbitrage choisie d&#39;un commun accord, auquel vous adh&eacute;rerez sans r&eacute;serve<br />
&nbsp;</p>

<h2><br />
Portabilit&eacute; des donn&eacute;es</h2>

<p><br />
<strong>Portabilit&eacute; des donn&eacute;es</strong><br />
L&#39;&Eacute;diteur s&#39;engage &agrave; vous offrir la possibilit&eacute; de vous faire restituer l&#39;ensemble des donn&eacute;es vous concernant sur simple demande. L&#39;Utilisateur se voit ainsi garantir une meilleure ma&icirc;trise de ses donn&eacute;es, et garde la possibilit&eacute; de les r&eacute;utiliser. Ces donn&eacute;es devront &ecirc;tre fournies dans un format ouvert et ais&eacute;ment r&eacute;utilisable.</p>

            ']
        ];

        foreach ($pages as $p) {
            $page = new Page();
            $page->setTitle($p['title']);
            $page->setContent($p['content']);

            $manager->persist($page);
        }

        $manager->flush();
    }
}
