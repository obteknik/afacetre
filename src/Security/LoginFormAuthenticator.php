<?php

namespace App\Security;

use App\Controller\SecurityController;
use App\Entity\Connexion;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Class LoginFormAuthenticator
 * @package App\Security
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * LoginFormAuthenticator constructor.
     * @param EntityManagerInterface $entityManager
     * @param UrlGeneratorInterface $urlGenerator
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param RouterInterface $router
     * @param KernelInterface $kernel
     */
    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator,
                                CsrfTokenManagerInterface $csrfTokenManager,
                                UserPasswordEncoderInterface $passwordEncoder, RouterInterface $router,
                                KernelInterface $kernel)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->router = $router;
        $this->kernel = $kernel;
    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

//    /**
//     * Overrided to avoid redirection loop
//     * @param Request $request
//     * @param AuthenticationException|null $authException
//     *
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @throws \Exception
//     */
//    public function start(Request $request, AuthenticationException $authException = null)
//    {
//        $url = $this->getLoginUrl();
//
//        // If URL different than login one, redirect to login one
//        if ($url !== $this->urlGenerator->generate($request->attributes->get('_route'))) {
//            return new RedirectResponse($url);
//        } // Render login page as a subrequest for this call only
//        else {
//            $path['_controller'] = SecurityController::class . '::adminLogin';
//            $subRequest = $request->duplicate([], null, $path);
//
//            return $this->kernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
//        }
//    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Email could not be found.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        /** @var User $user */
        $user = $token->getUser();

        //Historisation de la connexion
        $connexion = new Connexion();
        $connexion->setCreatedAt(new \DateTime());
        $connexion->setUser($user);
        $this->entityManager->persist($connexion);
        $this->entityManager->flush();

        /* Possibilité d'accéder à une page dédiée selon profil utilisateur */
        if (in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {

            //vers le DASHBOARD SUPER ADMIN
            return new RedirectResponse($this->router->generate('my_admin'));

        } else if (in_array('ROLE_ADMIN', $user->getRoles())) {

            //vers le DASHBOARD ADMIN
            return new RedirectResponse($this->router->generate('my_access'));

        } else if (in_array('ROLE_PERSON', $user->getRoles())) {

            //vers l'agenda
            return new RedirectResponse($this->router->generate('events',
                ['token' => $user->getAgendaToken()]));

        } else if (in_array('ROLE_PATIENT', $user->getRoles())) {

            //vers la vue des séances cabinet/factures pour mutuelle
            return new RedirectResponse($this->router->generate('home',
                ['token' => $user->getAgendaToken()]));
        }

        //par défault ROLE_USER => liste des campagnes
        return new RedirectResponse($this->urlGenerator->generate('admin'));

    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
