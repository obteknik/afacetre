<?php

namespace App\Entity;

use App\Repository\PatientTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PatientTypeRepository::class)
 */
class PatientType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="type")
     */
    private $patients;

    /**
     * @ORM\OneToMany(targetEntity=EventScale::class, mappedBy="patientType")
     */
    private $eventScales;

    public function __construct()
    {
        $this->patients = new ArrayCollection();
        $this->eventScales = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->type;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->setType($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->removeElement($patient)) {
            // set the owning side to null (unless already changed)
            if ($patient->getType() === $this) {
                $patient->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventScale[]
     */
    public function getEventScales(): Collection
    {
        return $this->eventScales;
    }

    public function addEventScale(EventScale $eventScale): self
    {
        if (!$this->eventScales->contains($eventScale)) {
            $this->eventScales[] = $eventScale;
            $eventScale->setPatientType($this);
        }

        return $this;
    }

    public function removeEventScale(EventScale $eventScale): self
    {
        if ($this->eventScales->removeElement($eventScale)) {
            // set the owning side to null (unless already changed)
            if ($eventScale->getPatientType() === $this) {
                $eventScale->setPatientType(null);
            }
        }

        return $this;
    }

}
