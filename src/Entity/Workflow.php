<?php

namespace App\Entity;

use App\Repository\WorkflowRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WorkflowRepository::class)
 */
class Workflow
{
    //couleurs par défaut des events
    public const WORKFLOW_EVENT_BG_COLOR = '000000';
    public const WORKFLOW_EVENT_FT_COLOR = 'FFFFFF';

    //font par défaut pour la génération PDF
    public const WORKFLOW_PDF_FONT = 'Arial black';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Organization::class, mappedBy="workflows")
     */
    private $organizations;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $eventBgColor;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $eventFtColor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pdfFont;

    /**
     * @ORM\OneToMany(targetEntity=EventType::class, mappedBy="workflow")
     */
    private $eventTypes;

    public function __construct()
    {
        $this->organizations = new ArrayCollection();
        $this->eventBgColor = self::WORKFLOW_EVENT_BG_COLOR;
        $this->eventFtColor= self::WORKFLOW_EVENT_FT_COLOR;
        $this->pdfFont= self::WORKFLOW_PDF_FONT;
        $this->eventTypes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->addWorkflow($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->removeElement($organization)) {
            $organization->removeWorkflow($this);
        }

        return $this;
    }

    public function getEventBgColor(): ?string
    {
        return $this->eventBgColor;
    }

    public function setEventBgColor(string $eventBgColor): self
    {
        $this->eventBgColor = $eventBgColor;

        return $this;
    }

    public function getEventFtColor(): ?string
    {
        return $this->eventFtColor;
    }

    public function setEventFtColor(string $eventFtColor): self
    {
        $this->eventFtColor = $eventFtColor;

        return $this;
    }

    public function getPdfFont(): ?string
    {
        return $this->pdfFont;
    }

    public function setPdfFont(string $pdfFont): self
    {
        $this->pdfFont = $pdfFont;

        return $this;
    }

    /**
     * @return Collection|EventType[]
     */
    public function getEventTypes(): Collection
    {
        return $this->eventTypes;
    }

    public function addEventType(EventType $eventType): self
    {
        if (!$this->eventTypes->contains($eventType)) {
            $this->eventTypes[] = $eventType;
            $eventType->setWorkflow($this);
        }

        return $this;
    }

    public function removeEventType(EventType $eventType): self
    {
        if ($this->eventTypes->removeElement($eventType)) {
            // set the owning side to null (unless already changed)
            if ($eventType->getWorkflow() === $this) {
                $eventType->setWorkflow(null);
            }
        }

        return $this;
    }
    
}
