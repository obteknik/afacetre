<?php

namespace App\Entity;

use App\Repository\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class Patient
 * @package App\Entity
 * @author Olivier Brassart <obteknik2007@gmail.com>
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 */
class Patient
{
    const CIVILITY_MISTER = 'Monsieur';
    const CIVILITY_MISS = 'Madame';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=PatientSheet::class, mappedBy="patient")
     */
    private $patientSheets;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @ORM\ManyToOne(targetEntity=PatientType::class, inversedBy="patients")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=FamilySituation::class, inversedBy="patients")
     */
    private $familySituation;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="patient", cascade={"persist", "remove"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="patient",cascade={"persist"})
     */
    private $invoices;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="patient",cascade={"persist"})
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="patient",cascade={"persist"})
     */
    private $payments;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="patients")
     */
    private $organization;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="patient",cascade={"persist"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Historical::class, mappedBy="patient",cascade={"persist"})
     */
    private $historicals;

    public function __construct()
    {
        $this->patientSheets = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setCountry('FR');
        $this->setCivility(self::CIVILITY_MISTER);
        $this->documents = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->historicals = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->civility. ' '. $this->firstname. ' '.$this->lastname;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = ucfirst(strtolower($firstname));;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = strtoupper($lastname);

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|PatientSheet[]
     */
    public function getPatientSheets(): Collection
    {
        return $this->patientSheets;
    }

    public function addPatientSheet(PatientSheet $patientSheet): self
    {
        if (!$this->patientSheets->contains($patientSheet)) {
            $this->patientSheets[] = $patientSheet;
            $patientSheet->setPatient($this);
        }

        return $this;
    }

    public function removePatientSheet(PatientSheet $patientSheet): self
    {
        if ($this->patientSheets->removeElement($patientSheet)) {
            // set the owning side to null (unless already changed)
            if ($patientSheet->getPatient() === $this) {
                $patientSheet->setPatient(null);
            }
        }

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = strtoupper($city);;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getType(): ?PatientType
    {
        return $this->type;
    }

    public function setType(?PatientType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFamilySituation(): ?FamilySituation
    {
        return $this->familySituation;
    }

    public function setFamilySituation(?FamilySituation $familySituation): self
    {
        $this->familySituation = $familySituation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->civility. ' ' .$this->firstname . ' ' . $this->lastname;
    }

    public function getRole(): string
    {
        return isset($this->getUser()->getRoles()[0]) ? $this->getUser()->getRoles()[0] : null;
    }

    public function getSheetPdf(): ?string
    {
        return $this->sheetPdf;
    }

    public function setSheetPdf(?string $sheetPdf): self
    {
        $this->sheetPdf = $sheetPdf;

        return $this;
    }

    /**
     * @param mixed $updatedAt
     * @return Patient
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param File $sheetPdfFile
     * @return Patient
     * @throws \Exception
     */
    public function setSheetPdfFile(File $sheetPdfFile): Patient
    {
        $this->sheetPdfFile = $sheetPdfFile;
        if($sheetPdfFile){
            $this->updatedAt = new \DateTime();
        }
    }

    /**
     * @return File
     */
    public function getSheetPdfFile(): File
    {
        return $this->sheetPdfFile;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setPatient($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getPatient() === $this) {
                $document->setPatient(null);
            }
        }

        return $this;
    }

    public function getCountUploadedFiles(){
        return count($this->getDocuments());
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setPatient($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getPatient() === $this) {
                $invoice->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setPatient($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getPatient() === $this) {
                $event->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setPatient($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getPatient() === $this) {
                $payment->setPatient(null);
            }
        }

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPatient($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPatient() === $this) {
                $comment->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Historical[]
     */
    public function getHistoricals(): Collection
    {
        return $this->historicals;
    }

    public function addHistorical(Historical $historical): self
    {
        if (!$this->historicals->contains($historical)) {
            $this->historicals[] = $historical;
            $historical->setPatient($this);
        }

        return $this;
    }

    public function removeHistorical(Historical $historical): self
    {
        if ($this->historicals->removeElement($historical)) {
            // set the owning side to null (unless already changed)
            if ($historical->getPatient() === $this) {
                $historical->setPatient(null);
            }
        }

        return $this;
    }

    public function getPatientAge():int
    {
        return (int) $this->getAge($this->birthDate);
    }

    public function getAge(\DateTime $birthdate)
    {
        $today = date("Y-m-d");
        $datetime1 = new \DateTime($today);
        $datetime2 = $birthdate;
        $age = $datetime1->diff($datetime2);
        return $age->format('%y');
    }
}
