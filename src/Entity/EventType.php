<?php

namespace App\Entity;

use App\Repository\EventTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventTypeRepository::class)
 */
class EventType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="type")
     */
    private $events;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shortType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eventBgColor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eventFtColor;

    /**
     * @ORM\ManyToOne(targetEntity=Workflow::class, inversedBy="eventTypes")
     */
    private $workflow;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->type;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setEventType($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getType() === $this) {
                $event->setType(null);
            }
        }

        return $this;
    }

    public function getShortType(): ?string
    {
        return $this->shortType;
    }

    public function setShortType(string $shortType): self
    {
        $this->shortType = $shortType;

        return $this;
    }

    public function getEventBgColor(): ?string
    {
        return $this->eventBgColor;
    }

    public function setEventBgColor(?string $eventBgColor): self
    {
        $this->eventBgColor = $eventBgColor;

        return $this;
    }

    public function getEventFtColor(): ?string
    {
        return $this->eventFtColor;
    }

    public function setEventFtColor(?string $eventFtColor): self
    {
        $this->eventFtColor = $eventFtColor;

        return $this;
    }

    public function getBbgColorRender(): ?string
    {
        return '';
    }

    public function getWorkflow(): ?Workflow
    {
        return $this->workflow;
    }

    public function setWorkflow(?Workflow $workflow): self
    {
        $this->workflow = $workflow;

        return $this;
    }

    public function getProcessusWithEventColors(): array
    {
        $colors = [];
        $colors['workflow']= $this->getWorkflow();
        $colors['bgColor']= $this->getWorkflow()->getEventBgColor();
        $colors['ftColor']= $this->getWorkflow()->getEventFtColor();
        return $colors;
    }

    public function getEventColors(): array
    {
        $colors = [];
        $colors['bgColor']= $this->getEventBgColor();
        $colors['ftColor']= $this->getEventFtColor();
        return $colors;
    }
}
