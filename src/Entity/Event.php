<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Event
 * @package App\Entity
 * @author Olivier Brassart <obteknik2007@gmail.com>
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    const EVENT_STATUS_NEW          = "Nouveau";
    const EVENT_STATUS_HONORED      = "Honoré";
    const EVENT_STATUS_POSTPONED    = "Reporté";
    const EVENT_STATUS_CANCELED     = "Annulé";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="events")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAllDay = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPeriodic = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConfirmedByEmail = false;

    //TYPES/OBJECTS/LOCATIONS
    /**
     * @ORM\ManyToOne(targetEntity=EventType::class, inversedBy="events")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="event",cascade={"persist", "remove"})
     */
    private $comments;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="events")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity=EventLocation::class, inversedBy="events")
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Timeslot::class, inversedBy="events")
     */
    private $timeslot;

    /**
     * @ORM\ManyToOne(targetEntity=Discipline::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=true)
     */
    private $object;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity=Contract::class, inversedBy="events",cascade={"persist", "remove"})
     */
    private $contract;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="event")
     */
    private $documents;

    /**
     * @ORM\ManyToOne(targetEntity=FiscalYear::class, inversedBy="events")
     */
    private $fiscalYear;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="events")
     */
    private $patient;

    /**
     * @ORM\OneToMany(targetEntity=AdherentEvents::class, mappedBy="event",cascade={"persist", "remove"})
     */
    private $adherentEvents;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFirst = false;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="events")
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity=Invoice::class, inversedBy="events")
     */
    private $invoice;

//    /**
//     * @ORM\OneToOne(targetEntity=Invoice::class, cascade={"persist", "remove"})
//     */
//    private $invoice;


    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->setStatus(self::EVENT_STATUS_NEW);
        $this->createdAt = new \DateTime();
        $this->documents = new ArrayCollection();
        $this->adherentEvents = new ArrayCollection();
    }
    public function __toString()
    {
        return 'Séance du '
            . $this->getStartAt()->format('d/m/Y')
            . ' de ' . $this->getStartAt()->format('H:i')
            . ' à ' . $this->getEndAt()->format('H:i')
            ;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsAllDay(): ?bool
    {
        return $this->isAllDay;
    }

    public function setIsAllDay(bool $isAllDay): self
    {
        $this->isAllDay = $isAllDay;

        return $this;
    }

    public function getIsPeriodic(): ?bool
    {
        return $this->isPeriodic;
    }

    public function setIsPeriodic(bool $isPeriodic): self
    {
        $this->isPeriodic = $isPeriodic;

        return $this;
    }

    public function getIsConfirmedByEmail(): ?bool
    {
        return $this->isConfirmedByEmail;
    }

    public function setIsConfirmedByEmail(bool $isConfirmedByEmail): self
    {
        $this->isConfirmedByEmail = $isConfirmedByEmail;

        return $this;
    }

    public function getType(): ?EventType
    {
        return $this->type;
    }

    public function setType(?EventType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setEvent($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getEvent() === $this) {
                $comment->setEvent(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getLocation(): ?EventLocation
    {
        return $this->location;
    }

    public function setLocation(?EventLocation $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getTimeslot(): ?Timeslot
    {
        return $this->timeslot;
    }

    public function setTimeslot(?Timeslot $timeslot): self
    {
        $this->timeslot = $timeslot;

        return $this;
    }

    public function getObject(): ?Discipline
    {
        return $this->object;
    }

    public function setObject(?Discipline $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setEvent($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getEvent() === $this) {
                $document->setEvent(null);
            }
        }

        return $this;
    }

    public function getFiscalYear(): ?FiscalYear
    {
        return $this->fiscalYear;
    }

    public function setFiscalYear(?FiscalYear $fiscalYear): self
    {
        $this->fiscalYear = $fiscalYear;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * @return Collection|AdherentEvents[]
     */
    public function getAdherentEvents(): Collection
    {
        return $this->adherentEvents;
    }

    public function addAdherentEvent(AdherentEvents $adherentEvent): self
    {
        if (!$this->adherentEvents->contains($adherentEvent)) {
            $this->adherentEvents[] = $adherentEvent;
            $adherentEvent->setEvent($this);
        }

        return $this;
    }

    public function removeAdherentEvent(AdherentEvents $adherentEvent): self
    {
        if ($this->adherentEvents->removeElement($adherentEvent)) {
            // set the owning side to null (unless already changed)
            if ($adherentEvent->getEvent() === $this) {
                $adherentEvent->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * Retourne les adhérents
     * @return array
     */
    public function getAdherents(): array
    {
        $adherentsEvents = $this->getAdherentEvents();
        $array = [];
        foreach ($adherentsEvents as $ae){
            $array[] = $ae->getAdherent();
        }
        return $array;
    }

    public function getIsFirst(): ?bool
    {
        return $this->isFirst;
    }

    public function setIsFirst(bool $isFirst): self
    {
        $this->isFirst = $isFirst;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

//    public function getInvoice(): ?Invoice
//    {
//        return $this->invoice;
//    }
//
//    public function setInvoice(?Invoice $invoice): self
//    {
//        $this->invoice = $invoice;
//
//        return $this;
//    }

public function getInvoice(): ?Invoice
{
    return $this->invoice;
}

public function setInvoice(?Invoice $invoice): self
{
    $this->invoice = $invoice;

    return $this;
}

}
