<?php

namespace App\Entity\Search;
use App\Entity\User;

/**
 * Class EventSearch
 * @package App\Entity\Search
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventSearch
{

    /**
     * @var \DateTime|null
     */
    private $minCreatedAt;

    /**
     * @var \DateTime|null
     */
    private $maxCreatedAt;

    /**
     * @var User|null
     */
    private $connectedUser;

    /**
     * @param \DateTime|null $minCreatedAt
     * @return EventSearch
     */
    public function setMinCreatedAt(?\DateTime $minCreatedAt): EventSearch
    {
        $this->minCreatedAt = $minCreatedAt;
        return $this;
    }

    /**
     * @param \DateTime|null $maxCreatedAt
     * @return EventSearch
     */
    public function setMaxCreatedAt(?\DateTime $maxCreatedAt): EventSearch
    {
        $this->maxCreatedAt = $maxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMinCreatedAt(): ?\DateTime
    {
        return $this->minCreatedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getMaxCreatedAt(): ?\DateTime
    {
        return $this->maxCreatedAt;
    }

    /**
     * @param User|null $connectedUser
     * @return EventSearch
     */
    public function setConnectedUser(?User $connectedUser): EventSearch
    {
        $this->connectedUser = $connectedUser;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getConnectedUser(): ?User
    {
        return $this->connectedUser;
    }

}