<?php

namespace App\Entity\Search;
use App\Entity\User;

/**
 * Class TimeslotSearch
 * @package App\Entity\Search
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class TimeslotSearch
{

    /**
     * @var \DateTime|null
     */
    private $minCreatedAt;

    /**
     * @var \DateTime|null
     */
    private $maxCreatedAt;

    /**
     * @var User|null
     */
    private $connectedUser;

    /**
     * @param \DateTime|null $minCreatedAt
     * @return TimeslotSearch
     */
    public function setMinCreatedAt(?\DateTime $minCreatedAt): TimeslotSearch
    {
        $this->minCreatedAt = $minCreatedAt;
        return $this;
    }

    /**
     * @param \DateTime|null $maxCreatedAt
     * @return TimeslotSearch
     */
    public function setMaxCreatedAt(?\DateTime $maxCreatedAt): TimeslotSearch
    {
        $this->maxCreatedAt = $maxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMinCreatedAt(): ?\DateTime
    {
        return $this->minCreatedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getMaxCreatedAt(): ?\DateTime
    {
        return $this->maxCreatedAt;
    }

    /**
     * @param User|null $connectedUser
     * @return TimeslotSearch
     */
    public function setConnectedUser(?User $connectedUser): TimeslotSearch
    {
        $this->connectedUser = $connectedUser;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getConnectedUser(): ?User
    {
        return $this->connectedUser;
    }

}