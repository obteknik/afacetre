<?php

namespace App\Entity;

use App\Repository\CalendarParameterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CalendarParameterRepository::class)
 */
class CalendarParameter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $minDate;

    /**
     * @ORM\Column(type="date")
     */
    private $maxDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $firstDayWeek;

    /**
     * @ORM\Column(type="time")
     */
    private $minTimeDay;

    /**
     * @ORM\Column(type="time")
     */
    private $maxTimeDay;

    /**
     * @ORM\Column(type="boolean")
     */
    private $weekNumbers;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="calendarParameter", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMinDate(): ?\DateTimeInterface
    {
        return $this->minDate;
    }

    public function setMinDate(\DateTimeInterface $minDate): self
    {
        $this->minDate = $minDate;

        return $this;
    }

    public function getMaxDate(): ?\DateTimeInterface
    {
        return $this->maxDate;
    }

    public function setMaxDate(\DateTimeInterface $maxDate): self
    {
        $this->maxDate = $maxDate;

        return $this;
    }

    public function getFirstDayWeek(): ?int
    {
        return $this->firstDayWeek;
    }

    public function setFirstDayWeek(int $firstDayWeek): self
    {
        $this->firstDayWeek = $firstDayWeek;

        return $this;
    }

    public function getMinTimeDay(): ?\DateTimeInterface
    {
        return $this->minTimeDay;
    }

    public function setMinTimeDay(\DateTimeInterface $minTimeDay): self
    {
        $this->minTimeDay = $minTimeDay;

        return $this;
    }

    public function getMaxTimeDay(): ?\DateTimeInterface
    {
        return $this->maxTimeDay;
    }

    public function setMaxTimeDay(\DateTimeInterface $maxTimeDay): self
    {
        $this->maxTimeDay = $maxTimeDay;

        return $this;
    }

    public function getWeekNumbers(): ?bool
    {
        return $this->weekNumbers;
    }

    public function setWeekNumbers(bool $weekNumbers): self
    {
        $this->weekNumbers = $weekNumbers;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
