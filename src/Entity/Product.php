<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product
 * @package App\Entity
 * @author Olivier Brassart <obteknik2007@gmail.com>
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=255, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hourDuration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxAdherents;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $illustration;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=100, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $subtitle;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @Assert\NotBlank()
     */
    private $category;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBest;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="product")
     */
    private $stocks;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = true;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\OneToMany(targetEntity=InvoiceDetail::class, mappedBy="product",cascade={"persist","remove"})
     */
    private $invoiceDetails;

    /**
     * @ORM\ManyToMany(targetEntity=Invoice::class, mappedBy="products")
     */
    private $invoices;

    /**
     * @ORM\OneToMany(targetEntity=ProductDetail::class, mappedBy="product",cascade={"persist","remove"})
     */
    private $productDetails;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=Contract::class, mappedBy="products")
     */
    private $contracts;

    /**
     * @ORM\ManyToMany(targetEntity=Quote::class, mappedBy="products")
     */
    private $quotes;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="products")
     */
    private $organization;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPriceShow;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAvailable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $audience;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $effective;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $goal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $program;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $method;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $means;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $evaluation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $validation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $planification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\OneToMany(targetEntity=ProductDay::class, mappedBy="product",cascade={"persist","remove"})
     */
    private $productDays;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="product",cascade={"persist"})
     */
    private $documents;


    public function __construct()
    {
        $this->stocks = new ArrayCollection();
        $this->invoiceDetails = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->productDetails = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->contracts = new ArrayCollection();
        $this->quotes = new ArrayCollection();
        $this->productDays = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function __toString()
    {
//        return (string) $this->getId();
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getIsBest(): ?bool
    {
        return $this->isBest;
    }

    public function setIsBest(bool $isBest): self
    {
        $this->isBest = $isBest;

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setProduct($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getProduct() === $this) {
                $stock->setProduct(null);
            }
        }

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection|InvoiceDetail[]
     */
    public function getInvoiceDetails(): Collection
    {
        return $this->invoiceDetails;
    }

    public function addInvoiceDetail(InvoiceDetail $invoiceDetail): self
    {
        if (!$this->invoiceDetails->contains($invoiceDetail)) {
            $this->invoiceDetails[] = $invoiceDetail;
            $invoiceDetail->setProduct($this);
        }

        return $this;
    }

    public function removeInvoiceDetail(InvoiceDetail $invoiceDetail): self
    {
        if ($this->invoiceDetails->removeElement($invoiceDetail)) {
            // set the owning side to null (unless already changed)
            if ($invoiceDetail->getProduct() === $this) {
                $invoiceDetail->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->addProduct($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            $invoice->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|ProductDetail[]
     */
    public function getProductDetails(): Collection
    {
        return $this->productDetails;
    }

    public function addProductDetail(ProductDetail $productDetail): self
    {
        if (!$this->productDetails->contains($productDetail)) {
            $this->productDetails[] = $productDetail;
            $productDetail->setProduct($this);
        }

        return $this;
    }

    public function removeProductDetail(ProductDetail $productDetail): self
    {
        if ($this->productDetails->removeElement($productDetail)) {
            // set the owning side to null (unless already changed)
            if ($productDetail->getProduct() === $this) {
                $productDetail->setProduct(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

//    /**
//     * @return Collection|Contract[]
//     */
//    public function getContracts(): Collection
//    {
//        return $this->contracts;
//    }
//
//    public function addContract(Contract $contract): self
//    {
//        if (!$this->contracts->contains($contract)) {
//            $this->contracts[] = $contract;
//            $contract->addProduct($this);
//        }
//
//        return $this;
//    }
//
//    public function removeContract(Contract $contract): self
//    {
//        if ($this->contracts->removeElement($contract)) {
//            $contract->removeProduct($this);
//        }
//
//        return $this;
//    }

/**
 * @return Collection|Quote[]
 */
public function getQuotes(): Collection
{
    return $this->quotes;
}

public function addQuote(Quote $quote): self
{
    if (!$this->quotes->contains($quote)) {
        $this->quotes[] = $quote;
        $quote->addProduct($this);
    }

    return $this;
}

public function removeQuote(Quote $quote): self
{
    if ($this->quotes->removeElement($quote)) {
        $quote->removeProduct($this);
    }

    return $this;
}

    /**
     * @param mixed $hourDuration
     * @return Product
     */
    public function setHourDuration($hourDuration)
    {
        $this->hourDuration = $hourDuration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHourDuration()
    {
        return $this->hourDuration;
    }

    /**
     * @param mixed $maxAdherents
     * @return Product
     */
    public function setMaxAdherents($maxAdherents)
    {
        $this->maxAdherents = $maxAdherents;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxAdherents()
    {
        return $this->maxAdherents;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getIsPriceShow(): ?bool
    {
        return $this->isPriceShow;
    }

    public function setIsPriceShow(bool $isPriceShow): self
    {
        $this->isPriceShow = $isPriceShow;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->isAvailable;
    }

    public function setIsAvailable(bool $isAvailable): self
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setAudience(?string $audience): self
    {
        $this->audience = $audience;

        return $this;
    }

    public function getEffective(): ?string
    {
        return $this->effective;
    }

    public function setEffective(?string $effective): self
    {
        $this->effective = $effective;

        return $this;
    }

    public function getGoal(): ?string
    {
        return $this->goal;
    }

    public function setGoal(?string $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getProgram(): ?string
    {
        return $this->program;
    }

    public function setProgram(?string $program): self
    {
        $this->program = $program;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(?string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getMeans(): ?string
    {
        return $this->means;
    }

    public function setMeans(?string $means): self
    {
        $this->means = $means;

        return $this;
    }

    public function getEvaluation(): ?string
    {
        return $this->evaluation;
    }

    public function setEvaluation(?string $evaluation): self
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getValidation(): ?string
    {
        return $this->validation;
    }

    public function setValidation(?string $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPlanification(): ?string
    {
        return $this->planification;
    }

    public function setPlanification(?string $planification): self
    {
        $this->planification = $planification;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection|ProductDay[]
     */
    public function getProductDays(): Collection
    {
        return $this->productDays;
    }

    public function addProductDay(ProductDay $productDay): self
    {
        if (!$this->productDays->contains($productDay)) {
            $this->productDays[] = $productDay;
            $productDay->setProduct($this);
        }

        return $this;
    }

    public function removeProductDay(ProductDay $productDay): self
    {
        if ($this->productDays->removeElement($productDay)) {
            // set the owning side to null (unless already changed)
            if ($productDay->getProduct() === $this) {
                $productDay->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setProduct($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getProduct() === $this) {
                $document->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * Méthode qui retourne l'url du document selon son type
     * @param string $type
     * @return string
     */
    public function getDocumentUrlByType(string $type) : string
    {
        $documents = $this->getDocuments();
        foreach ($documents as $d){
            if($d->getType()->getType() == $type){
                return $d->getDoctPdf();
            }
        }
        return '';
    }
}
