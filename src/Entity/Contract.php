<?php

namespace App\Entity;

use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="contracts")
     */
    private $client;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endAt;

    /**
     * @ORM\ManyToMany(targetEntity=Adherent::class, inversedBy="contracts",cascade={"persist"})
     */
    private $adherents;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="contract",cascade={"persist","remove"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="contract",cascade={"persist","remove"})
     */
    private $payments;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="contracts")
     */
    private $organization;

    /**
     * @var Product[]
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="contracts")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="contract",cascade={"persist","remove"})
     */
    private $events;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pdf;

    /**
     * @ORM\OneToMany(targetEntity=Quote::class, mappedBy="contract")
     */
    private $quotes;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="contract")
     */
    private $invoices;

    /**
     * @ORM\ManyToMany(targetEntity=Person::class, inversedBy="contracts")
     */
    private $persons;

    /**
     * @ORM\ManyToOne(targetEntity=FiscalYear::class, inversedBy="contracts")
     */
    private $fiscalYear;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $foadLink;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFaceToFace;

    public function __construct()
    {
        $this->adherents = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->setStartAt(new \DateTime());
        $this->setEndAt(new \DateTime());
        $this->products = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->quotes = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->persons = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getClient()->getCompanyName().' - '.$this->getReference();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * @return Collection|Adherent[]
     */
    public function getAdherents(): Collection
    {
        return $this->adherents;
    }

    public function addAdherent(Adherent $adherent): self
    {
        if (!$this->adherents->contains($adherent)) {
            $this->adherents[] = $adherent;
        }

        return $this;
    }

    public function removeAdherent(Adherent $adherent): self
    {
        $this->adherents->removeElement($adherent);

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setContract($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getContract() === $this) {
                $document->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setContract($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getContract() === $this) {
                $payment->setContract(null);
            }
        }

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

//    /**
//     * @param Product[] $products
//     */
//    public function setPurchasedItems($products)
//    {
//        $this->products = $products;
//    }
//
//    /**
//     * @return Product[]
//     */
//    public function getProducts()
//    {
//        return $this->products;
//    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setContract($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getContract() === $this) {
                $event->setContract(null);
            }
        }

        return $this;
    }

    public function getPdf(): ?string
    {
        return $this->pdf;
    }

    public function setPdf(?string $pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }

    //Comptage/list PARTICIPANTS
    public function getCountAdherents(){
        return count($this->getAdherents());
    }

    //Comptage/list DOCUMENTS
    public function getCountDocuments(){
        return count($this->getDocuments());
    }

    //Comptage/list SEANCES
    public function getCountEvents(){
        return count($this->getEvents());
    }

    public function getPeriod(){
        return 'Du '. $this->getStartAt()->format('d/m/Y') . ' au ' . $this->getEndAt()->format('d/m/Y');
    }

    /**
     * @return Collection|Quote[]
     */
    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    public function addQuote(Quote $quote): self
    {
        if (!$this->quotes->contains($quote)) {
            $this->quotes[] = $quote;
            $quote->setContract($this);
        }

        return $this;
    }

    public function removeQuote(Quote $quote): self
    {
        if ($this->quotes->removeElement($quote)) {
            // set the owning side to null (unless already changed)
            if ($quote->getContract() === $this) {
                $quote->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setContract($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getContract() === $this) {
                $invoice->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        $this->persons->removeElement($person);

        return $this;
    }

    public function getFiscalYear(): ?FiscalYear
    {
        return $this->fiscalYear;
    }

    public function setFiscalYear(?FiscalYear $fiscalYear): self
    {
        $this->fiscalYear = $fiscalYear;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getFoadLink(): ?string
    {
        return $this->foadLink;
    }

    public function setFoadLink(?string $foadLink): self
    {
        $this->foadLink = $foadLink;

        return $this;
    }

    public function getIsFaceToFace(): ?bool
    {
        return $this->isFaceToFace;
    }

    public function setIsFaceToFace(bool $isFaceToFace): self
    {
        $this->isFaceToFace = $isFaceToFace;

        return $this;
    }

}
