<?php

namespace App\Entity;

use App\Repository\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrganizationRepository::class)
 */
class Organization
{

    public const EMAIL_TRANSPORT_MODE_CLASSIC = 'Mode classique';
    public const EMAIL_TRANSPORT_MODE_MAILJET = 'MailJet';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=20, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=20, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=100, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=10, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=2, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=50, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=14)
     * @Assert\Length( min=14, exactMessage="Le nom doit faire {{ limit }} caractères.")
     */
    private $siretNumber;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $taxRate;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="organization")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="organization")
     */
    private $invoices;

    /**
     * @ORM\OneToMany(targetEntity=Quote::class, mappedBy="organization")
     */
    private $quotes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $illustration;

    /**
     * @ORM\OneToMany(targetEntity=Client::class, mappedBy="organization")
     */
    private $clients;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rcs;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTaxSubject = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bankName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $iban;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHomeBest;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHomePost;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHomeTeam;

    /**
     * @ORM\OneToMany(targetEntity=Contract::class, mappedBy="organization")
     */
    private $contracts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $signature;

    /**
     * @ORM\OneToMany(targetEntity=Charge::class, mappedBy="organization")
     */
    private $charges;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="organization")
     */
    private $payments;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsActiveEcommerce;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="organization")
     */
    private $products;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActiveProductShowBest;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPriceShow;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="organization",cascade={"persist", "remove"})
     */
    private $documents;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHomeCitation;

    /**
     * @ORM\Column(type="integer")
     */
    private $homeCitationTimer;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isMultilingual;

    /**
     * @ORM\OneToMany(targetEntity=OrganizationActivity::class, mappedBy="organization")
     */
    private $organizationActivities;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="organization")
     */
    private $patients;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="organization")
     */
    private $events;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=Historical::class, mappedBy="organization",cascade={"persist"})
     */
    private $historicals;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="organization",cascade={"persist"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Workflow::class, inversedBy="organizations")
     */
    private $workflows;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWorkflowEventColor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pdfFont;

    /**
     * @ORM\ManyToOne(targetEntity=BusinessType::class, inversedBy="organizations")
     */
    private $businessType;

    /**
     * @ORM\ManyToOne(targetEntity=LegalForm::class, inversedBy="organizations")
     */
    private $legalForm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailCc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailBcc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emailTransportMode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $representedBy;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->quotes = new ArrayCollection();
        $this->clients = new ArrayCollection();
        $this->contracts = new ArrayCollection();
        $this->charges = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->organizationActivities = new ArrayCollection();
        $this->patients = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->historicals = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->workflows = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->setCountry('FR');
        $this->setEmailTransportMode(self::EMAIL_TRANSPORT_MODE_CLASSIC);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getSiretNumber(): ?string
    {
        return $this->siretNumber;
    }

    public function setSiretNumber(string $siretNumber): self
    {
        $this->siretNumber = $siretNumber;

        return $this;
    }

    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    public function setTaxRate(float $taxRate): self
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setOrganization($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getOrganization() === $this) {
                $user->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setOrganization($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getOrganization() === $this) {
                $invoice->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quote[]
     */
    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    public function addQuote(Quote $quote): self
    {
        if (!$this->quotes->contains($quote)) {
            $this->quotes[] = $quote;
            $quote->setOrganization($this);
        }

        return $this;
    }

    public function removeQuote(Quote $quote): self
    {
        if ($this->quotes->removeElement($quote)) {
            // set the owning side to null (unless already changed)
            if ($quote->getOrganization() === $this) {
                $quote->setOrganization(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(?string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setOrganization($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->removeElement($client)) {
            // set the owning side to null (unless already changed)
            if ($client->getOrganization() === $this) {
                $client->setOrganization(null);
            }
        }

        return $this;
    }

    public function getRcs(): ?string
    {
        return $this->rcs;
    }

    public function setRcs(?string $rcs): self
    {
        $this->rcs = $rcs;

        return $this;
    }

    public function getIsTaxSubject(): ?bool
    {
        return $this->isTaxSubject;
    }

    public function setIsTaxSubject(bool $isTaxSubject): self
    {
        $this->isTaxSubject = $isTaxSubject;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(?string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getIsHomeBest(): ?bool
    {
        return $this->isHomeBest;
    }

    public function setIsHomeBest(bool $isHomeBest): self
    {
        $this->isHomeBest = $isHomeBest;

        return $this;
    }

    public function getIsHomePost(): ?bool
    {
        return $this->isHomePost;
    }

    public function setIsHomePost(bool $isHomePost): self
    {
        $this->isHomePost = $isHomePost;

        return $this;
    }

    public function getIsHomeTeam(): ?bool
    {
        return $this->isHomeTeam;
    }

    public function setIsHomeTeam(bool $isHomeTeam): self
    {
        $this->isHomeTeam = $isHomeTeam;

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setOrganization($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            // set the owning side to null (unless already changed)
            if ($contract->getOrganization() === $this) {
                $contract->setOrganization(null);
            }
        }

        return $this;
    }

    public function getSignature(): ?string
    {
        return $this->signature;
    }

    public function setSignature(?string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @return Collection|Charge[]
     */
    public function getCharges(): Collection
    {
        return $this->charges;
    }

    public function addCharge(Charge $charge): self
    {
        if (!$this->charges->contains($charge)) {
            $this->charges[] = $charge;
            $charge->setOrganization($this);
        }

        return $this;
    }

    public function removeCharge(Charge $charge): self
    {
        if ($this->charges->removeElement($charge)) {
            // set the owning side to null (unless already changed)
            if ($charge->getOrganization() === $this) {
                $charge->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setOrganization($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getOrganization() === $this) {
                $payment->setOrganization(null);
            }
        }

        return $this;
    }

    public function getIsActiveEcommerce(): ?bool
    {
        return $this->IsActiveEcommerce;
    }

    public function setIsActiveEcommerce(bool $IsActiveEcommerce): self
    {
        $this->IsActiveEcommerce = $IsActiveEcommerce;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setOrganization($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getOrganization() === $this) {
                $product->setOrganization(null);
            }
        }

        return $this;
    }

    public function getIsActiveProductShowBest(): ?bool
    {
        return $this->isActiveProductShowBest;
    }

    public function setIsActiveProductShowBest(bool $isActiveProductShowBest): self
    {
        $this->isActiveProductShowBest = $isActiveProductShowBest;

        return $this;
    }

    public function getIsPriceShow(): ?bool
    {
        return $this->isPriceShow;
    }

    public function setIsPriceShow(bool $isPriceShow): self
    {
        $this->isPriceShow = $isPriceShow;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setOrganization($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getOrganization() === $this) {
                $document->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * Méthode qui retourne l'url du document selon son type
     * @param string $type
     * @return bool
     */
    public function getDocumentUrlByType(string $type) : bool
    {
        $documents = $this->getDocuments();
        foreach ($documents as $d){
            if($d->getType()->getType() == $type){
                return 'uploads/documents/'.$d->getDoctPdf();
            }
        }
        return false;
    }

    public function getIsHomeCitation(): ?bool
    {
        return $this->isHomeCitation;
    }

    public function setIsHomeCitation(bool $isHomeCitation): self
    {
        $this->isHomeCitation = $isHomeCitation;

        return $this;
    }

    public function getHomeCitationTimer(): ?int
    {
        return $this->homeCitationTimer;
    }

    public function setHomeCitationTimer(int $homeCitationTimer): self
    {
        $this->homeCitationTimer = $homeCitationTimer;

        return $this;
    }

    public function getIsMultilingual(): ?bool
    {
        return $this->isMultilingual;
    }

    public function setIsMultilingual(bool $isMultilingual): self
    {
        $this->isMultilingual = $isMultilingual;

        return $this;
    }

    /**
     * @return Collection|OrganizationActivity[]
     */
    public function getOrganizationActivities(): Collection
    {
        return $this->organizationActivities;
    }

    public function addOrganizationActivity(OrganizationActivity $organizationActivity): self
    {
        if (!$this->organizationActivities->contains($organizationActivity)) {
            $this->organizationActivities[] = $organizationActivity;
            $organizationActivity->setOrganization($this);
        }

        return $this;
    }

    public function removeOrganizationActivity(OrganizationActivity $organizationActivity): self
    {
        if ($this->organizationActivities->removeElement($organizationActivity)) {
            // set the owning side to null (unless already changed)
            if ($organizationActivity->getOrganization() === $this) {
                $organizationActivity->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->setOrganization($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->removeElement($patient)) {
            // set the owning side to null (unless already changed)
            if ($patient->getOrganization() === $this) {
                $patient->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setOrganization($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getOrganization() === $this) {
                $event->setOrganization(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Historical[]
     */
    public function getHistoricals(): Collection
    {
        return $this->historicals;
    }

    public function addHistorical(Historical $historical): self
    {
        if (!$this->historicals->contains($historical)) {
            $this->historicals[] = $historical;
            $historical->setOrganization($this);
        }

        return $this;
    }

    public function removeHistorical(Historical $historical): self
    {
        if ($this->historicals->removeElement($historical)) {
            // set the owning side to null (unless already changed)
            if ($historical->getOrganization() === $this) {
                $historical->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setOrganization($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getOrganization() === $this) {
                $comment->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Workflow[]
     */
    public function getWorkflows(): Collection
    {
        return $this->workflows;
    }

    public function addWorkflow(Workflow $workflow): self
    {
        if (!$this->workflows->contains($workflow)) {
            $this->workflows[] = $workflow;
        }

        return $this;
    }

    public function removeWorkflow(Workflow $workflow): self
    {
        $this->workflows->removeElement($workflow);

        return $this;
    }

    public function getIsWorkflowEventColor(): ?bool
    {
        return $this->isWorkflowEventColor;
    }

    public function setIsWorkflowEventColor(bool $isWorkflowEventColor): self
    {
        $this->isWorkflowEventColor = $isWorkflowEventColor;

        return $this;
    }

    public function getPdfFont(): ?string
    {
        return $this->pdfFont;
    }

    public function setPdfFont(string $pdfFont): self
    {
        $this->pdfFont = $pdfFont;

        return $this;
    }

    public function getBusinessType(): ?BusinessType
    {
        return $this->businessType;
    }

    public function setBusinessType(?BusinessType $businessType): self
    {
        $this->businessType = $businessType;

        return $this;
    }

    public function getLegalForm(): ?LegalForm
    {
        return $this->legalForm;
    }

    public function setLegalForm(?LegalForm $legalForm): self
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    public function getEmailCc(): ?string
    {
        return $this->emailCc;
    }

    public function setEmailCc(?string $emailCc): self
    {
        $this->emailCc = $emailCc;

        return $this;
    }

    public function getEmailBcc(): ?string
    {
        return $this->emailBcc;
    }

    public function setEmailBcc(?string $emailBcc): self
    {
        $this->emailBcc = $emailBcc;

        return $this;
    }

    public function getEmailTransportMode(): ?string
    {
        return $this->emailTransportMode;
    }

    public function setEmailTransportMode(string $emailTransportMode): self
    {
        $this->emailTransportMode = $emailTransportMode;

        return $this;
    }

    public function getRepresentedBy(): ?string
    {
        return $this->representedBy;
    }

    public function setRepresentedBy(string $representedBy): self
    {
        $this->representedBy = $representedBy;

        return $this;
    }

}
