<?php

namespace App\Entity;

use App\Repository\TimeslotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Timeslot
 * @package App\Entity
 * @author Olivier Brassart <obteknik2007@gmail.com>
 * @ORM\Entity(repositoryClass=TimeslotRepository::class)
 */
class Timeslot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="timeslots")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPeriodic;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAllDay;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConfirmedByEmail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="timeslots")
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="timeslot")
     */
    private $events;

    /**
     * @ORM\ManyToOne(targetEntity=Discipline::class, inversedBy="timeslots")
     */
    private $discipline;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=EventObject::class, inversedBy="timeslots")
     */
    private $object;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsPeriodic(): ?bool
    {
        return $this->isPeriodic;
    }

    public function setIsPeriodic(bool $isPeriodic): self
    {
        $this->isPeriodic = $isPeriodic;

        return $this;
    }

    public function getIsAllDay(): ?bool
    {
        return $this->isAllDay;
    }

    public function setIsAllDay(bool $isAllDay): self
    {
        $this->isAllDay = $isAllDay;

        return $this;
    }

    public function getIsConfirmedByEmail(): ?bool
    {
        return $this->isConfirmedByEmail;
    }

    public function setIsConfirmedByEmail(bool $isConfirmedByEmail): self
    {
        $this->isConfirmedByEmail = $isConfirmedByEmail;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setTimeslot($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getTimeslot() === $this) {
                $event->setTimeslot(null);
            }
        }

        return $this;
    }

    public function getDiscipline(): ?Discipline
    {
        return $this->discipline;
    }

    public function setDiscipline(?Discipline $discipline): self
    {
        $this->discipline = $discipline;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getObject(): ?EventObject
    {
        return $this->object;
    }

    public function setObject(?EventObject $object): self
    {
        $this->object = $object;

        return $this;
    }
}
