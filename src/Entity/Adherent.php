<?php

namespace App\Entity;

use App\Repository\AdherentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Adherent
 * @package App\Entity
 * @author Olivier Brassart <obteknik2007@gmail.com>
 * @ORM\Entity(repositoryClass=AdherentRepository::class)
 * @ORM\Table(name="Adherent")
 * @UniqueEntity(fields={"lastname", "firstname"}, errorPath="lastname",message="Ce contact a déjà été créé.")
 */
class Adherent
{
    const CIVILITY_MISTER = 'Monsieur';
    const CIVILITY_MISS = 'Madame';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=3, minMessage="Le prénom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=30, maxMessage="Le prénom doit faire au plus {{ limit }} caractères.")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=50, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $lastname;

    public function __toString()
    {
        return $this->civility. ' '. $this->firstname. ' '.$this->lastname;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=100, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(),
     * @Assert\Length( min=5, minMessage="Le nom doit faire au moins {{ limit }} caractères.")
     * @Assert\Length( max=14, maxMessage="Le nom doit faire au plus {{ limit }} caractères.")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $illustration;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=Contract::class, mappedBy="adherents")
     */
    private $contracts;

    /**
     * @ORM\ManyToMany(targetEntity=Discipline::class, inversedBy="adherents")
     */
    private $disciplines;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civility;

    /**
     * @ORM\OneToMany(targetEntity=AdherentEvents::class, mappedBy="adherent")
     */
    private $adherentEvents;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setCivility(self::CIVILITY_MISTER);
        $this->disciplines = new ArrayCollection();
        $this->contracts = new ArrayCollection();
        $this->adherentEvents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = ucfirst(strtolower($firstname));;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = strtoupper($lastname);;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->civility. ' ' .$this->firstname . ' ' . $this->lastname;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->addAdherent($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            $contract->removeAdherent($this);
        }

        return $this;
    }

    /**
     * @return Collection|Discipline[]
     */
    public function getDisciplines(): Collection
    {
        return $this->disciplines;
    }

    public function addDiscipline(Discipline $discipline): self
    {
        if (!$this->disciplines->contains($discipline)) {
            $this->disciplines[] = $discipline;
        }

        return $this;
    }

    public function removeDiscipline(Discipline $discipline): self
    {
        $this->disciplines->removeElement($discipline);

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    /**
     * @return Collection|AdherentEvents[]
     */
    public function getAdherentEvents(): Collection
    {
        return $this->adherentEvents;
    }

    public function addAdherentEvent(AdherentEvents $adherentEvent): self
    {
        if (!$this->adherentEvents->contains($adherentEvent)) {
            $this->adherentEvents[] = $adherentEvent;
            $adherentEvent->setAdherent($this);
        }

        return $this;
    }

    public function removeAdherentEvent(AdherentEvents $adherentEvent): self
    {
        if ($this->adherentEvents->removeElement($adherentEvent)) {
            // set the owning side to null (unless already changed)
            if ($adherentEvent->getAdherent() === $this) {
                $adherentEvent->setAdherent(null);
            }
        }

        return $this;
    }
}
