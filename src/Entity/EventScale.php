<?php

namespace App\Entity;

use App\Repository\EventScaleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventScaleRepository::class)
 */
class EventScale
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=PatientType::class, inversedBy="eventScales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patientType;

    /**
     * @ORM\Column(type="float")
     */
    private $firstEventPrice;

    /**
     * @ORM\Column(type="float")
     */
    private $otherEventPrice;

    /**
     * EventScale constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPatientType(): ?PatientType
    {
        return $this->patientType;
    }

    public function setPatientType(?PatientType $patientType): self
    {
        $this->patientType = $patientType;

        return $this;
    }

    public function getFirstEventPrice(): ?float
    {
        return $this->firstEventPrice;
    }

    public function setFirstEventPrice(float $firstEventPrice): self
    {
        $this->firstEventPrice = $firstEventPrice;

        return $this;
    }

    public function getOtherEventPrice(): ?float
    {
        return $this->otherEventPrice;
    }

    public function setOtherEventPrice(float $otherEventPrice): self
    {
        $this->otherEventPrice = $otherEventPrice;

        return $this;
    }
}
