<?php

namespace App\Entity;

use App\Repository\ClientContactFunctionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientContactFunctionRepository::class)
 */
class ClientContactFunction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

//    /**
//     * @ORM\OneToMany(targetEntity=ClientContact::class, mappedBy="function")
//     */
//    private $clientContacts;
//
//    public function __construct()
//    {
//        $this->clientContacts = new ArrayCollection();
//    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

//    /**
//     * @return Collection|ClientContact[]
//     */
//    public function getClientContacts(): Collection
//    {
//        return $this->clientContacts;
//    }
//
//    public function addClientContact(ClientContact $clientContact): self
//    {
//        if (!$this->clientContacts->contains($clientContact)) {
//            $this->clientContacts[] = $clientContact;
//            $clientContact->setFunction($this);
//        }
//
//        return $this;
//    }
//
//    public function removeClientContact(ClientContact $clientContact): self
//    {
//        if ($this->clientContacts->removeElement($clientContact)) {
//            // set the owning side to null (unless already changed)
//            if ($clientContact->getFunction() === $this) {
//                $clientContact->setFunction(null);
//            }
//        }
//
//        return $this;
//    }
}
