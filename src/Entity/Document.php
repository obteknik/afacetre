<?php

namespace App\Entity;

use App\Repository\DocumentRepository;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 * @Vich\Uploadable
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $doctPdf;

    /**
     * @Vich\UploadableField(mapping="documents", fileNameProperty="doctPdf")
     */
    private $doctPdfFile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToOne(targetEntity=Contract::class, inversedBy="documents")
     */
    private $contract;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="documents")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=DocumentType::class, inversedBy="documents")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="documents")
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="documents")
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="documents")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="documents")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="documents")
     */
    private $person;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setReference('toto');
        $this->setName('titi');
        $this->setIsActive(true);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @param mixed $updatedAt
     * @return Document
     */
    public function
    setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $doctPdf
     * @return Document
     */
    public function setDoctPdf($doctPdf)
    {
        $this->doctPdf = $doctPdf;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctPdf()
    {
        return $this->doctPdf;
    }

    /**
     * @param mixed $doctPdfFile
     * @return void
     */
    public function setDoctPdfFile($doctPdfFile)
    {
        $this->doctPdfFile = $doctPdfFile;
        if($doctPdfFile){
            $this->updatedAt = new \DateTime();
        }
    }

    /**
     * @return mixed
     */
    public function getDoctPdfFile()
    {
        return $this->doctPdfFile;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getType(): ?DocumentType
    {
        return $this->type;
    }

    public function setType(?DocumentType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

}
