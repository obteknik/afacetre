<?php

namespace App\Entity;

use App\Repository\QuoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuoteRepository::class)
 */
class Quote
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="float")
     */
    private $amountHT;

    /**
     * @ORM\Column(type="float")
     */
    private $amountTTC;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=QuoteDetail::class, mappedBy="quote", cascade={"persist", "remove"})
     */
    private $quoteDetails;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="quotes")
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="quotes")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=PaymentCondition::class, inversedBy="quotes")
     */
    private $paymentCondition;

    /**
     * @ORM\ManyToOne(targetEntity=Contract::class, inversedBy="quotes")
     */
    private $contract;

    /**
     * @ORM\ManyToOne(targetEntity=FiscalYear::class, inversedBy="quotes")
     */
    private $fiscalYear;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="quotes")
     */
    private $products;

    public function __construct()
    {
        $this->quoteDetails = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->products = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getReference();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = strtoupper($reference);

        return $this;
    }

    public function getAmountHT(): ?float
    {
        return $this->amountHT;
    }

    public function setAmountHT(?float $amountHT): self
    {
        $this->amountHT = $amountHT;

        return $this;
    }

    public function getAmountTTC(): ?float
    {
        return $this->amountTTC;
    }

    public function setAmountTTC(?float $amountTTC): self
    {
        $this->amountTTC = $amountTTC;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|QuoteDetail[]
     */
    public function getQuoteDetails(): Collection
    {
        return $this->quoteDetails;
    }

    public function addQuoteDetail(QuoteDetail $quoteDetail): self
    {
        if (!$this->quoteDetails->contains($quoteDetail)) {
            $this->quoteDetails[] = $quoteDetail;
            $quoteDetail->setQuote($this);
        }

        return $this;
    }

    public function removeQuoteDetail(QuoteDetail $quoteDetail): self
    {
        if ($this->quoteDetails->removeElement($quoteDetail)) {
            // set the owning side to null (unless already changed)
            if ($quoteDetail->getQuote() === $this) {
                $quoteDetail->setQuote(null);
            }
        }

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getPaymentCondition(): ?PaymentCondition
    {
        return $this->paymentCondition;
    }

    public function setPaymentCondition(?PaymentCondition $paymentCondition): self
    {
        $this->paymentCondition = $paymentCondition;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    public function getFiscalYear(): ?FiscalYear
    {
        return $this->fiscalYear;
    }

    public function setFiscalYear(?FiscalYear $fiscalYear): self
    {
        $this->fiscalYear = $fiscalYear;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }
}
