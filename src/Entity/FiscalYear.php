<?php

namespace App\Entity;

use App\Repository\EventRepository;
use App\Repository\FiscalYearRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FiscalYearRepository::class)
 */
class FiscalYear
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="fiscalYear")
     */
    private $payments;

    /**
     * @ORM\OneToMany(targetEntity=Charge::class, mappedBy="fiscalYear")
     */
    private $charges;

    /**
     * @ORM\OneToMany(targetEntity=Quote::class, mappedBy="fiscalYear")
     */
    private $quotes;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="fiscalYear")
     */
    private $invoices;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="fiscalYear")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=Contract::class, mappedBy="fiscalYear")
     */
    private $contracts;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
        $this->charges = new ArrayCollection();
        $this->quotes = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->contracts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setFiscalYear($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getFiscalYear() === $this) {
                $payment->setFiscalYear(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Charge[]
     */
    public function getCharges(): Collection
    {
        return $this->charges;
    }

    public function addCharge(Charge $charge): self
    {
        if (!$this->charges->contains($charge)) {
            $this->charges[] = $charge;
            $charge->setFiscalYear($this);
        }

        return $this;
    }

    public function removeCharge(Charge $charge): self
    {
        if ($this->charges->removeElement($charge)) {
            // set the owning side to null (unless already changed)
            if ($charge->getFiscalYear() === $this) {
                $charge->setFiscalYear(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quote[]
     */
    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    public function addQuote(Quote $quote): self
    {
        if (!$this->quotes->contains($quote)) {
            $this->quotes[] = $quote;
            $quote->setFiscalYear($this);
        }

        return $this;
    }

    public function removeQuote(Quote $quote): self
    {
        if ($this->quotes->removeElement($quote)) {
            // set the owning side to null (unless already changed)
            if ($quote->getFiscalYear() === $this) {
                $quote->setFiscalYear(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setFiscalYear($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getFiscalYear() === $this) {
                $invoice->setFiscalYear(null);
            }
        }

        return $this;
    }

    public function getStats(): array
    {
        $totalCharges = 0;
        foreach ($this->getCharges() as $c) {
            $totalCharges += $c->getAmount();
        }
        $stats['charges'] = $totalCharges;

        $totalPayments = 0;
        foreach ($this->getPayments() as $p) {
            $totalPayments += $p->getAmount();
        }
        $stats['payments'] = $totalPayments;

        $totalQuotes = 0;
        foreach ($this->getQuotes() as $c) {
            $totalQuotes += $c->getAmountHT();
        }
        $stats['quotes'] = $totalQuotes;

        $totalInvoices= 0;
        foreach ($this->getInvoices() as $c) {
            $totalInvoices += $c->getAmountHT();
        }
        $stats['invoices'] = $totalInvoices;

        return $stats;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setFiscalYear($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getFiscalYear() === $this) {
                $event->setFiscalYear(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setFiscalYear($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            // set the owning side to null (unless already changed)
            if ($contract->getFiscalYear() === $this) {
                $contract->setFiscalYear(null);
            }
        }

        return $this;
    }
}
