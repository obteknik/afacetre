<?php

namespace App\Manager;

use App\Entity\Client;
use App\Entity\Comment;
use App\Entity\Entity;
use App\Entity\Event;
use App\Entity\FamilySituation;
use App\Entity\Organization;
use App\Entity\Patient;
use App\Entity\PatientType;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * @author   Olivier Brassart <brassart_olivier@yahoo.fr>
 * Class PatientManager
 * @package App\Manager
 */
class PatientManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * PatientManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param AppService $appService
     * @param UserManager $userManager
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger, AppService $appService,
                                UserManager $userManager)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->appService = $appService;
        $this->userManager = $userManager;
    }

    /**
     * @param Organization $organization
     * @param PatientType $patientType
     * @param string $civility
     * @param string $firstname
     * @param string $lastname
     * @param string $street
     * @param string $zipcode
     * @param string $city
     * @param string $country
     * @param string $phoneNumber
     * @param string $email
     * @param \DateTime $birthdate
     * @param FamilySituation $familySituation
     * @return Comment
     */
    public function create(Organization $organization, PatientType $patientType, string $civility = Patient::CIVILITY_MISTER,
                           string $firstname, string $lastname, string $street, string $zipcode, string $city, string $country, string $phoneNumber,
                           ?string $email, \DateTime $birthdate, ?FamilySituation $familySituation)
    {

        try {
            $patient = new Patient();
            $patient->setCreatedAt(new \DateTime())
                ->setOrganization($organization)
                ->setType($patientType)
                ->setCivility($civility)
                ->setFirstname($firstname)
                ->setLastname($lastname)
                ->setBirthDate($birthdate)
                ->setFamilySituation($familySituation);

            //adresse
            $patient->setStreet($street)
                ->setZipcode($zipcode)
                ->setCity($city)
                ->setCountry($country);

            //coords
            $patient->setPhoneNumber($phoneNumber);
            if($email){
                $patient->setEmail($email);
            }

            //reference
            $nextPatientReference = $this->appService->getNextReference($organization, 'patient');
            $patient->setReference($nextPatientReference);

            $this->manager->persist($patient);
            $this->manager->flush();

            // **** Création du compte USER ****
            //UTILISER USERMANAGER
            /*
                 public function create(?Organization $organization, ?User $user, string $firstname, string $lastname, string $email,
                           string $plainPassword, $roles = [], bool $agreedTerms = true, bool $isActive=false,
                           \DateTime $activatedAt)
             */
            //création compte use si email
            if($patient->getEmail()){
                $userAccount = $this->userManager->create($organization, null, $patient->getFirstname(), $patient->getLastname(),
                    $patient->getEmail(), 'test', ['ROLE_PATIENT'], true, true, new \DateTime());

                if(isset($userAccount['item'])){
                    $patient->setUser($userAccount['item']);
                }
            }

            $this->manager->persist($patient);
            $this->manager->flush();

            $resultManager["item"] = $patient;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création du patient : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[PatientManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}