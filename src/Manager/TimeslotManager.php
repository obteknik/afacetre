<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Entity\Event;
use App\Entity\Organization;
use App\Entity\Timeslot;
use Doctrine\ORM\EntityManagerInterface;
use \DateTime;
use Psr\Log\LoggerInterface;

/**
 * Class TimeslotManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class TimeslotManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CommentManager
     */
    private $commentManager;

    /**
     * TimeslotManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $manager,
                                LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    /**
     * @param DateTime $startAt
     * @param DateTime $endAt
     * @param string $title
     * @param bool $isAllDay
     * @param bool $isPeriodic
     * @param bool $isConfirmedByEmail
     * @return array
     */
    public function create(
        DateTime $startAt, DateTime $endAt,
        string $title,
        bool $isAllDay = false,
        bool $isPeriodic = false,
        bool $isConfirmedByEmail = false)
    {

        try {
            $timeslot = new Timeslot();
            $timeslot->setCreatedAt(new \DateTime());

            $timeslot->setStartAt($startAt);
            $timeslot->setEndAt($endAt);
            $timeslot->setTitle($title);
            $timeslot->setStatus(Event::EVENT_STATUS_NEW);

            $timeslot->setIsAllDay($isAllDay);
            $timeslot->setIsPeriodic($isPeriodic);
            $timeslot->setIsConfirmedByEmail($isConfirmedByEmail);

            $this->manager->persist($timeslot);
            $this->manager->flush();

            $resultManager["item"] = $timeslot;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de la plage d'activité : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[TimeslotManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}