<?php

namespace App\Manager;

use App\Entity\AdherentEvents;
use App\Entity\Comment;
use App\Entity\Contract;
use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\FiscalYear;
use App\Entity\Invoice;
use App\Entity\InvoiceDetail;
use App\Entity\Order;
use App\Entity\Organization;
use App\Entity\Patient;
use App\Entity\Payment;
use App\Entity\PaymentCondition;
use App\Entity\Person;
use App\Entity\Product;
use App\Entity\Quote;
use App\Repository\EventScaleRepository;
use App\Repository\PaymentConditionRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use \DateTime;
use Psr\Log\LoggerInterface;

/**
 * Class InvoiceManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class InvoiceManager
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var PaymentConditionRepository
     */
    private $paymentConditionRepository;

    /**
     * EventManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param AppService $appService
     * @param PaymentConditionRepository $paymentConditionRepository
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger, AppService $appService,
                                PaymentConditionRepository $paymentConditionRepository)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->appService = $appService;
        $this->paymentConditionRepository = $paymentConditionRepository;
    }

    /**
     * @param Organization $organization
     * @param FiscalYear $fiscalYear
     * @param Patient|null $patient
     * @param Contract|null $contract
     * @param Event[]|null[] $events
     * @param Order|null $order
     * @param PaymentCondition $paymentCondition
     * @param Quote|null $quote
     * @param Product|null $product
     * @param array $payments
     * @return mixed
     */
    public function create(Organization $organization, FiscalYear $fiscalYear, ?Patient $patient, ?Contract $contract,
                           ?Order $order, ?PaymentCondition $paymentCondition, ?Quote $quote, ?Product $product,
                           ?array $payments = [], ?array $events = [])
    {

        try {
            $invoice = new Invoice();

            $taxRate = $organization->getTaxRate();
            $invoice->setCreatedAt(new \DateTime())
                ->setOrganization($organization)
                ->setFiscalYear($fiscalYear)
                ->setPatient($patient)
                ->setContract($contract)
                ->setPaymentCondition($paymentCondition)
                ->setQuote($quote);

            //payment condition
            if (!$paymentCondition) {
                $paymentCondition = $this->paymentConditionRepository->find(1);
                $invoice->setPaymentCondition($paymentCondition);
            } else {
                $invoice->setPaymentCondition($paymentCondition);
            }

            //FACTURE PATIENT
            if (null !== $patient) {

                //pour chaque séance, je constitue une ligne de détail => je parcours un tableau d'events
                //rattachés au patient (tous ou sélection)
                $amountInvoiceHT = 0;
//                dd($events);
                if (!empty($events)) {
                    foreach ($events as $e) {
                        //association events/invoice
                        $invoice->addEvent($e);

                        //association events/invoiceDetail
                        $detail = new InvoiceDetail();
                        $detail->setCreatedAt(new \DateTime())
                            ->setInvoice($invoice)
                            ->setQuantity(1)
                            ->setProduct(null)
                            ->setAmountHT($e->getPrice())
                            ->setDesignation("Séance de sophrologie du " .
                                $e->getStartAt()->format('d/m/Y') . " de " . $e->getStartAt()->format('H:i') .
                                " à " . $e->getEndAt()->format('H:i'));
                        $this->manager->persist($detail);

                        $amountInvoiceHT += $detail->getAmountHT();
                    }
                    $invoice->setAmountHT($amountInvoiceHT);
                    $invoice->setAmountTTC(($amountInvoiceHT * $taxRate) / 100);

                    $filename = uniqid().'.pdf';
                    $invoice->setFilename($filename);
                    $this->manager->persist($invoice);
                }
            }

            //FACTURE CONTRAT DE FORMATION MONO PRODUIT FORMATION
            if (null !== $contract) {
                $detail = new InvoiceDetail();
                $detail->setCreatedAt(new \DateTime())
                    ->setInvoice($invoice)
                    ->setQuantity(1)
                    ->setProduct($product)
                    ->setAmountHT($amountHT);
            }

            //FACTURE DE COMMANDE MULTI PRODUITS
            if (null !== $order) {
                //TODO:...
            }

            //reference
            $nextInvoiceReference = $this->appService->getNextReference($organization, 'invoice');
            $invoice->setReference($nextInvoiceReference);

            //payments
            if (!empty($payments)) {
                foreach ($payments as $p) {
                    //TODO:...
                }
            }

            $this->manager->persist($invoice);
            $this->manager->flush();

            $resultManager["item"] = $invoice;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de la facture : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[InvoiceManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}