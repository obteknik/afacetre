<?php

namespace App\Manager;

use App\Entity\BusinessType;
use App\Entity\LegalForm;
use App\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class OrganizationManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class OrganizationManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    /**
     * @param string $name
     * @param string $representedBy
     * @param string $phoneNumber
     * @param string $email
     * @param string $street
     * @param string $zipcode
     * @param string $city
     * @param string $siretNumber
     * @param BusinessType $businessType
     * @param LegalForm $legalForm
     * @param bool $taxSubject
     * @param float $taxRate
     * @param array $workflows
     * @param string $emailCc
     * @param string $emailBcc
     * @param bool $isHomeTeam
     * @param bool $isHomeBest
     * @param bool $isHomePost
     * @param bool $isActiveEcommerce
     * @param bool $isActiveProductShowBest
     * @param bool $isPriceShow
     * @param bool $isHomeCitation
     * @param bool $isMultilingual
     * @param int $homeCitationTimer
     * @param bool $isWorkflowEventColor
     * @param string $pdfFont
     * @return array
     */
    public function create(string $name, string $representedBy, string $phoneNumber, string $email, string $street, string $zipcode,
                           string $city, string $siretNumber, BusinessType $businessType,
                           ?LegalForm $legalForm, bool $taxSubject, float $taxRate, array $workflows = [],
                           string $emailCc, string $emailBcc,
                           bool $isHomeTeam = false, bool $isHomeBest = false, bool $isHomePost = false,
                           bool $isActiveEcommerce = false, bool $isActiveProductShowBest = false, bool $isPriceShow = false,
                           bool $isHomeCitation = true, bool $isMultilingual = false, int $homeCitationTimer = 4,
                           bool $isWorkflowEventColor = true, string $pdfFont = 'Open Sans Condensed')
    {

        try {
            $organization = new Organization();
            $organization->setCreatedAt(new \DateTime())
                ->setName($name)
                ->setRepresentedBy($representedBy)
                ->setPhoneNumber($phoneNumber)
                ->setEmail($email)
                ->setSiretNumber($siretNumber)
                ->setStreet($street)
                ->setZipcode($zipcode)
                ->setCity($city)
                ->setBusinessType($businessType)
                ->setIsTaxSubject($taxSubject)
                ->setTaxRate($taxRate)
                ->setIsHomeTeam($isHomeTeam)
                ->setIsHomeBest($isHomeBest)
                ->setIsHomePost($isHomePost)
                ->setIsActiveEcommerce($isActiveEcommerce)
                ->setIsActiveProductShowBest($isActiveProductShowBest)
                ->setIsPriceShow($isPriceShow)
                ->setIsHomeCitation($isHomeCitation)
                ->setHomeCitationTimer($homeCitationTimer)
                ->setIsMultilingual($isMultilingual)
                ->setIsWorkflowEventColor($isWorkflowEventColor)
                ->setPdfFont($pdfFont)
                ->setLegalForm($legalForm)
                ->setEmailCc($emailCc)
                ->setEmailBcc($emailBcc);

            //workflows
            if (!empty($workflows)) {
                foreach ($workflows as $w) {
                    $organization->addWorkflow($w);
                }
            }

            $this->manager->persist($organization);
            $this->manager->flush();

            $resultManager["item"] = $organization;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de l'utilisateur : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[UserManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}