<?php

namespace App\Manager;

use App\Entity\AdherentEvents;
use App\Entity\Comment;
use App\Entity\Contract;
use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\FiscalYear;
use App\Entity\Organization;
use App\Entity\Patient;
use App\Entity\Person;
use App\Repository\EventScaleRepository;
use App\Repository\PaymentConditionRepository;
use Doctrine\ORM\EntityManagerInterface;
use \DateTime;
use Psr\Log\LoggerInterface;

/**
 * Class EventManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CommentManager
     */
    private $commentManager;
    /**
     * @var EventScaleRepository
     */
    private $eventScaleRepository;
    /**
     * @var InvoiceManager
     */
    private $invoiceManager;
    /**
     * @var PaymentConditionRepository
     */
    private $paymentConditionRepository;

    /**
     * EventManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param CommentManager $commentManager
     * @param EventScaleRepository $eventScaleRepository
     * @param InvoiceManager $invoiceManager
     * @param PaymentConditionRepository $paymentConditionRepository
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger, CommentManager $commentManager,
                                EventScaleRepository $eventScaleRepository, InvoiceManager $invoiceManager,
                                PaymentConditionRepository $paymentConditionRepository)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->commentManager = $commentManager;
        $this->eventScaleRepository = $eventScaleRepository;
        $this->invoiceManager = $invoiceManager;
        $this->paymentConditionRepository = $paymentConditionRepository;
    }

    /**
     * @param Organization $organization
     * @param FiscalYear $fiscalYear
     * @param Person $person
     * @param EventType $eventType
     * @param DateTime $startAt
     * @param DateTime $endAt
     * @param string $title
     * @param null|string $comment
     * @param Patient|null $patient
     * @param Contract|null $contract
     * @param bool $isFirst
     * @param array|null $adherents
     * @param bool $isAllDay
     * @param bool $isPeriodic
     * @param bool $isConfirmedByEmail
     * @return array
     */
    public function create(Organization $organization, FiscalYear $fiscalYear,
                           Person $person,
                           EventType $eventType,
                           DateTime $startAt, DateTime $endAt,
                           string $title, ?string $comment,
                           ?Patient $patient,
                           ?Contract $contract,
                           bool $isFirst = false,
                           ?array $adherents = [],
                           bool $isAllDay = false,
                           bool $isPeriodic = false,
                           bool $isConfirmedByEmail = false)
    {

        try {
            $event = new Event();
            $event->setOrganization($organization);
            $event->setFiscalYear($fiscalYear);
            $event->setCreatedAt(new \DateTime());
            $event->setStartAt($startAt);
            $event->setEndAt($endAt);
            $event->setTitle($title);

            //type de rendez-vous
            $event->setType($eventType);

            //statut du rendez-vous
            $event->setStatus(Event::EVENT_STATUS_NEW);

            $event->setIsFirst($isFirst);
            $event->setIsAllDay($isAllDay);
            $event->setIsPeriodic($isPeriodic);
            $event->setIsConfirmedByEmail($isConfirmedByEmail);

            //INTERVENANT(S)
            //TODO: addPersons (exemple: atelier animé par 2 intervenants)
//            if (!empty($persons)) {
//                foreach ($persons as $p) {
//                   $event->setPerson()
//                }
//            }
            $event->setPerson($person);

            //ADHERENTS (contrat formation//ateliers)
            if (!empty($adherents)) {
                foreach ($adherents as $adh) {
                    $adherentEvent = new AdherentEvents();
                    $adherentEvent->setCreatedAt(new \DateTime());
                    $adherentEvent->setIsHere(true);
                    $adherentEvent->setAdherent($adh);
                    $adherentEvent->setEvent($event);
                    $this->manager->persist($adherentEvent);
//                    $this->manager->flush();
                }
            }

            //commentaire associé
            if (null !== $comment) {
                $resultManagerComment = $this->commentManager->create($event, $comment);

                if (isset($resultManagerComment["errorMessage"]) && $resultManagerComment["errorMessage"] instanceof \Exception) {
                    $this->logger->error("[CommentManager/create] - " . $resultManagerComment["errorMessage"]);

                } elseif (isset($resultManagerComment["item"]) && $resultManagerComment["item"] instanceof Comment) {
                    $event->addComment($resultManagerComment["item"]);
                    $this->manager->persist($event);
                    $this->manager->flush();
                }
            }

            $event->setPatient($patient);



            //PATIENT
            if ($patient) {
                //Détermination du barème applicable à la séance selon $isFirst ET selon patientType (adulte/ado/enfant)
                $scale = $this->eventScaleRepository->findOneBy(['patientType' => $patient->getType()]);
                if ($scale) {
                    $price = $isFirst ? $scale->getFirstEventPrice() : $scale->getOtherEventPrice();
                    $event->setPrice($price);
                }

//                //Création de la facture de la séance pour la mutuelle pour génération PDF
//                /*
//                     public function create(Organization $organization, FiscalYear $fiscalYear, ?Patient $patient, ?Contract $contract,
//                           ?Event $event, ?Order $order, float $amountHT, ?PaymentCondition $paymentCondition,
//                           ?Quote $quote, ?Product $product, ?array $payments = [])
//                 */
//                $paymentCondition = $this->paymentConditionRepository->find(1);
//                $invoice = $this->invoiceManager->create($organization, $fiscalYear, $patient,
//                    $contract, $event, null, $event->getPrice(), $paymentCondition, null, null, null);
//                $event->setInvoice($invoice['item']);
            }

            $this->manager->persist($event);
            $this->manager->flush();
            $resultManager["item"] = $event;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création du rendez-vous : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[EventManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}