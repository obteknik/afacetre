<?php

namespace App\Manager;

use App\Entity\CalendarParameter;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class CalendarParameterManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CalendarParameterManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CalendarParameterManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    /**
     * @param User $user
     * @param \DateTime $minDate
     * @param \DateTime $maxDate
     * @param int $firstDayWeek
     * @param \DateTime $minTimeDay
     * @param \DateTime $maxTimeDay
     * @param bool $weekNumbers
     * @return array
     */
    public function create(User $user, \DateTime $minDate, \DateTime $maxDate, int $firstDayWeek,
                           \DateTime $minTimeDay, \DateTime $maxTimeDay, bool $weekNumbers=true)
    {

        try {
            $calendarParameter = new CalendarParameter();
            $calendarParameter->setUser($user);
            $calendarParameter->setMinDate($minDate);
            $calendarParameter->setMaxDate($maxDate);
            $calendarParameter->setFirstDayWeek($firstDayWeek);
            $calendarParameter->setMinTimeDay($minTimeDay);
            $calendarParameter->setMaxTimeDay($maxTimeDay);
            $calendarParameter->setWeekNumbers($weekNumbers);

            $this->manager->persist($calendarParameter);
            $this->manager->flush();

            $resultManager["item"] = $calendarParameter;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création du paramétrage agenda : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[CalendarParameterManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}