<?php

namespace App\Manager;

use App\Entity\Client;
use App\Entity\Comment;
use App\Entity\Entity;
use App\Entity\Event;
use App\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * @author   Olivier Brassart <brassart_olivier@yahoo.fr>
 * Class CommentManager
 * @package App\Manager
 */
class CommentManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CommentManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    /**
     * @param Event $event
     * @param string $note
     * @return Comment
     */
    public function create(Event $event, string $note){

        try {
            $comment = new Comment();
            $comment->setCreatedAt(new \DateTime());
            $comment->setEvent($event);
            $comment->setNote($note);

            $this->manager->persist($comment);
            $this->manager->flush();

            $resultManager["item"] = $comment;

        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de la note de commentaire : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[CommentManager/create] - ".$resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}