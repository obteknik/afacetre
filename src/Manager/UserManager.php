<?php

namespace App\Manager;

use App\Entity\Organization;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * Class UserManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class UserManager
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /*
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserRepository $userRepository
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger,
                                UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository,
                                TokenGeneratorInterface $tokenGenerator)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param Organization $organization
     * @param User $user
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $plainPassword
     * @param array $roles
     * @param bool $agreedTerms
     * @param bool $isActive
     * @param \DateTime $activatedAt
     * @return array
     */
    public function create(?Organization $organization, ?User $user, string $firstname, string $lastname, string $email,
                           string $plainPassword, $roles = [], bool $agreedTerms = true, bool $isActive=false,
                           \DateTime $activatedAt)
    {

        if(!$user){ $user = new User();}

        //contrôle de l'unicité de l'email - si connu, je crée le compte
        $userEmail = $this->userRepository->findOneByEmail($email);
        if(!$userEmail){
            try {

                $user->setOrganization($organization);
                $user->setCreatedAt(new \DateTime());
                $user->setFirstname($firstname);
                $user->setLastname($lastname);
                $user->setEmail(strtolower($email));
                $user->setIsActive($isActive);
                $user->setActivatedAt($activatedAt);

                //password
                $user->setPassword($this->passwordEncoder->encodePassword($user, $plainPassword));

                //agenda token
                $user->setAgendaToken($this->tokenGenerator->generateToken());

                //agree terms
                if (true === $agreedTerms) {
                    $user->agreeTerms();
                }

                //roles
                $user->setRoles($roles);

                $this->manager->persist($user);
                $this->manager->flush();

                $resultManager["item"] = $user;


            } catch (\Exception $e) {
                $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de l'utilisateur : "
                    . $e . "\n" . "Veuillez contacter l'administrateur système.";
                $this->logger->error("[UserManager/create] - " . $resultManager["errorMessage"]);
            }
        } else {
            $resultManager["errorMessage"]  = "Email déjà utilisé pour la création d'un compte utilisateur.";
        }

        return $resultManager;
    }

}