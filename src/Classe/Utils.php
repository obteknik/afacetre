<?php

namespace App\Classe;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;

class Utils
{

    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var Action
     */
    private $action;

    public function __construct(CrudUrlGenerator $crudUrlGenerator, Action $action)
    {
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->action = $action;
    }

    public function getUrl(string $crud, string $actionName, string $crudId, int $menuIndex, ?int $entityId)
    {

        $url = $this->crudUrlGenerator->build();
        $crudStr = "App\\Controller\\Admin" . "\\". $crud . "CrudController";
        $url
            ->setDashboard('App\\Controller\\Admin\\DashboardController')
            ->setController($crudStr)
            ->setAction($actionName)
            ->setCrudId($crudId)
            ->set('menuIndex', $menuIndex)
            ->unset('referrer')->generateUrl();

        //edit
        if($actionName == $this->action::EDIT){
            $url->setEntityId($entityId);
        }

        return $url;
    }
}