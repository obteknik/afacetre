<?php

namespace App\Classe;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class Cart
 * @package App\Classe
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class Cart
{
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Cart constructor.
     * @param SessionInterface $session
     * @param ProductRepository $productRepository
     */
    public function __construct(SessionInterface $session, ProductRepository $productRepository)
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    /**
     * Fonction d'ajout d'un article au panier
     * @param $id
     */
    public function add($id){

        $cart = $this->session->get('cart', []);

        if(!empty($cart[$id])){
            $cart[$id]++;
        } else {
            $cart[$id] = 1;
        }
        $this->session->set('cart', $cart);
    }

    /**
     * Fonction de retrait d'un article du panier
     * @param $id
     * @return mixed
     */
    public function delete($id){

        $cart = $this->session->get('cart', []);

        unset($cart[$id]);
        return $this->session->set('cart', $cart);
    }

    /**
     * Fonction d'obtention du contenu du panier
     * @return mixed
     */
    public function get(){

        return $this->session->get('cart');
    }

    /**
     * Fonction de mise à zéro/vidage du panier
     * @return mixed
     */
    public function remove(){

        return $this->session->remove('cart');
    }

    /**
     * Fonction de retrait d'une occurrence de produit du panier
     * @param Cart $cart
     * @param $id
     * @return mixed
     */
    public function decrease(Cart $cart, $id){

        $cart = $this->session->get('cart', []);

        //si la quantité du produit est > 1, je diminue la valeur de l'index de 1
        //si = à 1, je retire le produit
        if(isset($cart[$id])) {
            if($cart[$id] > 1){
                $cart[$id]--;
            } else {
                $this->delete($id);
            }
        }

        return $this->session->set('cart', $cart);
    }

    /**
     * Fonction d'obtention du contenu du panier enrichi des informations par produit
     * @return array
     */
    public function getFull() : array
    {

        $cartComplete = [];
        if ($this->get()) {
            foreach ($this->get() as $id => $quantity) {
                $product_object = $this->productRepository->findOneById($id);

                /* SECURITE: si on tente de passer un id inconnu du style cart/add/456456464,
                 * le pseudo produit est ajouté directement au panier, il faut alors pouvoir
                 * le supprimer à la conception de $cartComplete car Symfony ne trouvera pas
                 * le produit correspondant.
                */
                if(!$product_object){
                    $this->delete($id);
                    continue;
                }
                $cartComplete[] = [
                    'product' => $product_object,
                    'quantity' => $quantity
                ];
            }
        }

        return $cartComplete;
    }

}