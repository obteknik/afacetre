<?php

namespace App\Classe;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

//https://medium.com/suleyman-aydoslu/implementing-ckeditor-and-ckfinder-on-easyadmin-a269888771ce
class AdminCkEditorField implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null, ?string $configName = null): self
    {

        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            // this template is used in 'index' and 'detail' pages
            ->setTemplatePath('@EasyAdmin/crud/field/text_editor.html.twig')
            // this is used in 'edit' and 'new' pages to edit the field contents
            // you can use your own form types too
            ->setFormType(CKEditorType::class)
            ->setFormTypeOptions(
                [
                    'config' => [
                        'toolbar' => [ //'full'
                            ['Styles','Format','Font','FontSize'],
                            '/',
                            ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
                            '/',
                            ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                            ['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
//                            [
//                                'name' => 'basicstyles', 'items' => ['Source', '-', 'Bold', 'Italic']
//                            ],
//                            [
//                                'name' => 'paragraph', 'items' =>  ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
//                            ],
//                            [
//                                'name' => 'clipboard', 'items' => ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
//                                'Undo', 'Redo']
//                            ]
                        ],
//                        'filebrowserUploadRoute' => 'post_ckeditor_image',
//                        'filebrowserUploadRouteParameters' => ['slug' => 'image'],
                        'extraPlugins' => 'templates',
                        'width' => '850',
                        'height' => '200',

                    ],
                ])
            ->addCssClass('field-ck-editor');
    }
}