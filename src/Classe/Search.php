<?php

namespace App\Classe;

use App\Entity\Category;


/**
 * Class Search
 * @package App\Classe
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class Search
{
    /**
     * @var string
     */
    public $string = '';

    /**
     * @var Category[]
     */
    public $categories = [];

}