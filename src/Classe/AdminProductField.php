<?php

namespace App\Classe;

use App\Form\ProductReferenceType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

class AdminProductField implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null, ?string $configName = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTemplatePath('form/jquery-collection.html.twig')
            ->setFormType(ProductReferenceType::class)
            ->setFormTypeOptions(['mapped' => false])
            ->addJsFiles('field-products')
            ->addCssClass('field-products'); //pour jquery collection
    }
}