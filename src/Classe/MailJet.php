<?php

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

/**
 * Class MailJet
 * @package App\Classe
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class MailJet
{
    private $api_key = 'f314d2b549be104435563614ccdac73b';
    private $api_key_secret = '907ebdd0a7e404124e3488542c45c3d5';

    /**
     * @param $to_email
     * @param $to_name
     * @param $subject
     * @param $content
     */
    public function send($to_email, $to_name, $subject, $content){

        $mj = new Client($this->api_key, $this->api_key_secret,true,['version' => 'v3.1']);
        $mj->setTimeout(10);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "obteknik2007@gmail.com",
                        'Name' => "L'A.F.AC de l'Être"
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name
                        ]
                    ],
                    'TemplateID' => 1842172,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content]
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
//        $response->success() && dd($response->getData());
        $response->success();
    }
}
