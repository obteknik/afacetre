<?php

namespace App\Form;

use App\Entity\CalendarParameter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CalendarParameterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minDate', DateType::class, [
                    'required' => true,
                    'label' => 'Date de début',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('maxDate', DateType::class, [
                    'required' => true,
                    'label' => 'Date de fin',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('firstDayWeek')
            ->add('minTimeDay')
            ->add('maxTimeDay')
            ->add('weekNumbers')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CalendarParameter::class,
        ]);
    }
}
