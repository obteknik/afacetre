<?php

namespace App\Form;

use App\Entity\OrganizationActivity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class OrganizationActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.address_name.placeholder',
                    'maxLength' => 30,
                    'class' => 'form-group'
                ],
//                'help' => 'account.manageAddress.form.address_name.help',
//                'constraints' => [
//                    new NotBlank(['message' => 'account.manageAddress.form.address_name.not_blank']),
//                    new Length([
//                        'min' => 5, 'minMessage' => 'account.manageAddress.form.address_name.min_length',
//                        'max' => 30, 'maxMessage' => 'account.manageAddress.form.address_name.max_length',
//                    ])
//                ]
            ])
            ->add('iconFile', VichFileType::class, [
                'label' => 'Icône'
            ])
            ->add('illustrationFile', VichFileType::class, [
                'label' => 'Image'
            ])
            ->add('shortDescription', TextType::class, [
                'label' => 'Description courte',
                'attr' => [
                    'placeholder' => 'Description courte...'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'placeholder' => 'Description...',
                    'class' => 'summernote'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrganizationActivity::class,
        ]);
    }
}
