<?php

namespace App\Form;

use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\Person;
use App\Repository\EventRepository;
use App\Repository\EventScaleRepository;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class EventPatientType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventPatientType extends AbstractType
{
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var EventScaleRepository
     */
    private $eventScaleRepository;
    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * EventContractType constructor.
     * @param EventRepository $eventRepository
     * @param EntityManagerInterface $manager
     * @param EventScaleRepository $eventScaleRepository
     * @param PersonRepository $personRepository
     */
    public function __construct(EventRepository $eventRepository, EntityManagerInterface $manager,
                                EventScaleRepository $eventScaleRepository, PersonRepository $personRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->manager = $manager;
        $this->eventScaleRepository = $eventScaleRepository;
        $this->personRepository = $personRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mode = $options['mode'];
//        dd($mode);


        $builder
//            ->add('person', EntityType::class, [
//                'label' => 'Intervenant',
//                'class' => Person::class,
//                'choice_label' => function ($person) {
//                    /** @var Person $person */
//                    return $person;
//                },
//                'multiple' => false,
//                'expanded' => true,
////                'empty_data' => 1
//            ])
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => 'Titre...'
                ]
            ])
            ->add('type', EntityType::class, [
                'label' => 'Type de rendez-vous',
                'class' => EventType::class,
                'choice_label' => 'type',
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('startAt', DateTimeType::class, [
                    'required' => true,
                    'label' => 'Date de début',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('endAt', DateTimeType::class, [
                    'required' => true,
                    'label' => 'Date de fin',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $form = $event->getForm();

                //Affichage du champ "choix Intervenant" uniquement si plus d'1 intervenant en base
                //TODO: en mode multi organization, il faudrait le faire selon le nb d'intervenant de l'organisation
                if (count($this->personRepository->findAll()) > 1) {
                    $form->add('person', EntityType::class, [
                        'label' => 'Intervenant',
                        'class' => Person::class,
                        'choice_label' => function ($person) {
                            /** @var Person $person */
                            return $person;
                        },
                        'multiple' => false,
                        'expanded' => true,
                    ]);
                }
            });;

        //only in edit form
        if ($mode == 'edit') {
            $builder->add('isFirst', CheckboxType::class, [
                'label' => '1ère séance'
            ]);
        } else {
            $builder->remove('isFirst');
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'mode' => ''
        ]);
    }
}
