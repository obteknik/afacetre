<?php

namespace App\Form;

use App\Entity\Adherent;
use App\Entity\AdherentEvents;
use App\Entity\Contract;
use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipantType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * ParticipantType constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civility', ChoiceType::class, [
                'required' => true,
                'label' => 'Civilité',
                'placeholder' => 'Choisissez la civilité',
                'choices'  => [
                    'Monsieur' => Adherent::CIVILITY_MISTER,
                    'Madame' => Adherent::CIVILITY_MISS,
                ],
                'expanded' => true,'multiple'=>false
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom'
            ])
            ->add('lastname', TextType::class, [
//                'attr' => [
//                    'class' => 'participant-lastname'
//                ]
            ])
            ->add('phoneNumber', TextType::class)
            ->add('email', EmailType::class)

            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                $entity = $event->getData();
                $formData = $event->getForm();
                $contract = $formData->getParent()->getParent()->getData();

                $entity->addContract($contract);
//                dd($entity);

//                // ***** ASSOCIATION DES RENDEZ-VOUS AU NOUVEL ADHERENT AU CONTRAT *****
//                /** @var Contract $contract */
//                $contractEvents = $contract->getEvents();
//
////                dd(count($contractEvents));
//                //on ajoute au nouvel adherent les différents rdv du contrat
//                if(null === $entity->getId()){
//                    /** @var Adherent $adherent */
//                    foreach ($contractEvents as $e) {
//                        $adherentEvent = new AdherentEvents();
//                        $adherentEvent->setCreatedAt(new \DateTime());
//                        $adherentEvent->setAdherent($entity);
//                        $adherentEvent->setEvent($e);
//                        $adherentEvent->setIsHere(true);
//                        $this->manager->persist($adherentEvent);
//                    }
                    $this->manager->flush();

            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adherent::class,
        ]);
    }

}
