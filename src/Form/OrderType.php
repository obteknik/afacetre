<?php

namespace App\Form;

use App\Entity\Address;
use App\Entity\Carrier;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class OrderType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var User $user */
        $user = $options['user'];

        $builder
            ->add('addresses', EntityType::class, [
                'label' => false,
                'required' => true,
                'class' => Address::class,
                'choices' => $user->getAddresses(),
                'multiple' => false,
                'expanded' => true
            ])
            ->remove('carriers')
//            ->add('carriers', EntityType::class, [
//                'label' => 'order.form.carriers.choice_label',
//                'required' => true,
//                'class' => Carrier::class,
//                'multiple' => false,
//                'expanded' => true
//            ])
//            ->add('submit', SubmitType::class, [
//                'label' => 'order.form.submitButton.label',
//                'attr' => [
//                    'class' =>'btn btn-success btn-sm btn-block'
//                ]
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'messages',
            'user' => array()
        ]);
    }
}
