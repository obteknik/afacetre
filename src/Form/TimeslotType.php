<?php

namespace App\Form;

use App\Entity\Timeslot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TimeslotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $action = $options["action"];
        if($action === 'edit'){
            $startAt = $options['startAt'];
            $endAt = $options['endAt'];

            $startDataTime = new  \DateTime($startAt->format('H:i'));
            $endDataTime = new  \DateTime($endAt->format('H:i'));
        } else {

            $startDataTime = null;
            $endDataTime = null;
        }
        $builder
            ->add('title')
            ->add('startAt', DateType::class, [
                    'required' => true,
                    'label' => 'Date de début',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('endAt', DateType::class, [
                    'required' => true,
                    'label' => 'Date de fin',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('startAt_time', TimeType::class, [
                    'required' => true,
                    'label' => 'Heure de début',
                    'input' => 'datetime',
                    'widget' => 'choice',
                    'mapped' => false,
                    'constraints' => [
                        new NotBlank()
                    ],
                    'data' => $startDataTime
                ]
            )
            ->add('endAt_time', TimeType::class, [
                    'required' => true,
                    'label' => 'Heure de fin',
                    'input' => 'datetime',
                    'widget' => 'choice',
                    'mapped' => false,
                    'constraints' => [
                        new NotBlank()
                    ],
                    'data' => $endDataTime
                ]
            )
            ->add('isAllDay')
            ->add('isPeriodic')
            ->add('isConfirmedByEmail')
//            ->add('object')
//            ->add('location')
//            ->add('type')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Timeslot::class,
            'organization' => '',
            'action' => '',
            'startAt' => '',
            'endAt' => '',
        ]);
    }
}
