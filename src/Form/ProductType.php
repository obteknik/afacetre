<?php

namespace App\Form;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * @var ProductRepository
     */
    private $productRepository;


    /**
     * ProductType constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $products = $this->productRepository->findAll();
        $choices = [];
        foreach($products as $p){
            $choices[$p->getName()] = $p->getId();
        }

        $builder->add('product', ChoiceType::class, [
            'choices' => $choices,
        ]);
//        $builder->add('product', EntityType::class, [
//            'class' => Product::class,
//            'query_builder' => function (EntityRepository $er) {
//                return $er->createQueryBuilder('u')
//                    ->orderBy('u.username', 'ASC');
//            },
//            'choice_label' => 'name',
//        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
