<?php

namespace App\Form;

use App\Entity\Adherent;
use App\Entity\AdherentEvents;
use App\Entity\Contract;
use App\Entity\Event;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class EventContractType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventContractType extends AbstractType
{
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * EventContractType constructor.
     * @param EventRepository $eventRepository
     * @param EntityManagerInterface $manager
     */
    public function __construct(EventRepository $eventRepository, EntityManagerInterface $manager)
    {
        $this->eventRepository = $eventRepository;
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('startAt', DateTimeType::class, [
                    'required' => true,
                    'label' => 'Date de début',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('endAt', DateTimeType::class, [
                    'required' => true,
                    'label' => 'Date de fin',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
//            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
//                $entity = $event->getData();
//
//                //ATTENTION LORS DE LA CREATION DU CONTRAT, CELUI-CI NON ENCORE PERSISTE !!!
//                if($entity->getContract()){
//                    $contract = $entity->getContract();
//
//                    // ***** MISE A JOUR DATE DE DEBIT & DATE DE FIN DU CONTRAT SUITE CREATION/EDITION EVENT *****
//                    /** @var Contract $contract */
//                    $contractEvents = $contract->getEvents();
//
//                    //récupération de tous les $startAt (nouveau rdv & anciens)
//                    $dates = [];
//                    $dates[] = $entity->getStartAt();
//                    /** @var Event $event */
//                    foreach ($contractEvents as $event) {
//                        $dates[] = $event->getStartAt();
//                    }
//
//                    //récupération de la date la + proche & mise à jour contract
//                    $contract->setStartAt(min($dates));
//                }
//
//                // ***** ASSOCIATION DES RENDEZ-VOUS AUX ADHERENTS AU CONTRAT *****
//                if($entity->getContract()){
//                    $contractAdherents = $entity->getContract()->getAdherents();
//
//                    dd(null === $entity->getId());
//                    if(null === $entity->getId()){
//                        /** @var Adherent $adherent */
//                        //on ajoute tous les adhérents aux différents rdv du contrat
//                        foreach ($contractAdherents as $adherent) {
//                            $adherentEvent = new AdherentEvents();
//                            $adherentEvent->setCreatedAt(new \DateTime());
//                            $adherentEvent->setAdherent($adherent);
//                            $adherentEvent->setEvent($entity);
//                            $adherentEvent->setIsHere(true);
//                            $this->manager->persist($adherentEvent);
//                        }
//                        $this->manager->flush();
//                    }
//                }
//
//            })
;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
