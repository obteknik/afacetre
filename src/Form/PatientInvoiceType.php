<?php

namespace App\Form;

use App\Entity\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientInvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference')
            ->add('amountHT')
            ->add('amountTTC')
            ->add('createdAt')
            ->add('dueDate')
//            ->add('quote')
//            ->add('organization')
//            ->add('client')
            ->add('paymentCondition')
//            ->add('products')
//            ->add('contract')
            ->add('fiscalYear')
//            ->add('patient')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
