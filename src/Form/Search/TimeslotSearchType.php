<?php

namespace App\Form\Search;

use App\Entity\Search\EventSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TimeslotSearchType
 * @package App\Form\Search
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class TimeslotSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $organization = $options['organization'];

        $builder
            //Période de création
            ->add('minCreatedAt', DateType::class, [
                'required' => false,
                'label' => 'Période de création',
                'widget' => 'single_text',
                'constraints' => [
//                    new GreaterThanOrEqual(array('value' => 2, 'message' => "Le montant minimum de recherche est d'au-moins {{ compared_value }}")),
                ],
            ])
            ->add('maxCreatedAt', DateType::class, [
                'required' => false,
                'label' => false,
                'widget' => 'single_text',
                'constraints' => [
//                    new GreaterThanOrEqual(array('value' => 2, 'message' => "Le montant minimum de recherche est d'au-moins {{ compared_value }}")),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EventSearch::class,
            'csrf_protection' => false,
            'organization' => null,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
