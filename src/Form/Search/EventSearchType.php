<?php

namespace App\Form\Search;

use App\Entity\Search\EventSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EventSearchType
 * @package App\Form\Search
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EventSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $organization = $options['organization'];

        $builder
            //Période de création
            ->add('minCreatedAt', DateType::class, [
                'required' => false,
                'label' => 'Période de création',
                'widget' => 'single_text',
                'constraints' => [
//                    new GreaterThanOrEqual(array('value' => 2, 'message' => "Le montant minimum de recherche est d'au-moins {{ compared_value }}")),
                ],
            ])
            ->add('maxCreatedAt', DateType::class, [
                'required' => false,
                'label' => false,
                'widget' => 'single_text',
                'constraints' => [
//                    new GreaterThanOrEqual(array('value' => 2, 'message' => "Le montant minimum de recherche est d'au-moins {{ compared_value }}")),
                ],
            ])
//            //comment
//            ->add('comment', TextareaType::class, [
//                'required' => false,
//                'label' => 'Commentaire',
//                'attr' => [
//                    'placeholder' => 'Commentaire...'
//                ],
//                'constraints' => [
//                    new Length(['min' => 3, 'minMessage' => 'Name is too short','max' => 25, 'maxMessage' => 'Name is too long'])
//                ],
//            ])
//            ->add('eventTypeParameter', EntityType::class, [
//                'required' => false,
//                'placeholder' => 'Type de rendez-vous...',
//                'class' => EventTypeParameter::class,
////                'query_builder' => function (ChargeTypeParameterRepository $er) use ($organization) {
////                    return $er->createQueryBuilder('ct')
////                        ->andWhere('ct.organization = :organization')->setParameter('organization', $organization);
////                },
//                'choice_label' => 'value',
//                'label' => "Type de rendez-vous",
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EventSearch::class,
            'csrf_protection' => false,
            'organization' => null,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
