<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * Class ContactType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'attr' => [
                    'placeholder' => 'contact.form.firstname_placeholder',
                    'class' => 'form-group'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'contact.form.firstname.not_blank']),
                    new Length([
                        'min' => 3, 'minMessage' => 'contact.form.firstname.min_length',
                        'max' => 20, 'maxMessage' => 'contact.form.firstname.max_length',
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'placeholder' => 'contact.form.lastname_placeholder',
                    'class' => 'form-group'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'contact.form.lastname.not_blank']),
                    new Length([
                        'min' => 3, 'minMessage' => "contact.form.lastname.min_length",
                        'max' => 30, 'maxMessage' => "contact.form.lastname.min_length",
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'contact.form.email_placeholder',
                    'class' => 'form-group'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'contact.form.email.not_blank']),
                    new Email()
                ]
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'contact.form.message_placeholder',
                    'class' => 'form-group',
                    'rows' => 7
                ],
                'constraints' => [
                    new NotBlank(['message' => 'contact.form.message.not_blank']),
                    new Length([
                        'min' => 10, 'minMessage' => "contact.form.message.min_length",
                        'max' => 255, 'maxMessage' => "contact.form.message.min_length",
                    ])
                ]
            ])
            //https://qferrer.medium.com/securing-your-symfony-forms-with-google-recaptcha-v2-dbfc902b0c50
            ->add('captcha', ReCaptchaType::class, [
                'type' => 'checkbox', // (invisible, checkbox)
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'contact.form.submitButton_label',
                'attr' => [
                    'class' => "btn-block btn-success",
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'messages',
        ]);
    }
}
