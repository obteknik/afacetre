<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class RegisterType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'contact.form.email.not_blank']),
                    new Length([
                    'min' => 2, 'minMessage' => "Au-moins 2 caractères",
                    'max' => 30, 'maxMessage' => "Au-plus 30 caractères"
                ])],
                'attr' => [
                    'placeholder' => 'Exemple: Isabelle',
                    'class' => 'form-group',
                    'autofocus' => true
                ]
            ])
            ->add('lastname', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'contact.form.email.not_blank']),
                    new Length([
                    'min' => 2, 'minMessage' => "Au-moins 2 caractères",
                    'max' => 30, 'maxMessage' => "Au-plus 30 caractères"
                ])],
                'attr' => [
                    'placeholder' => ' Exemple: Martin',
                    'class' => 'form-group',
                ]
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new Email(),
                    new NotBlank(['message' => 'contact.form.email.not_blank']),
                    new Length([
                    'min' => 2, 'minMessage' => "Au-moins 2 caractères",
                    'max' => 60, 'maxMessage' => "Au-plus 60 caractères"
                ])],
                'attr' => [
                    'placeholder' => 'Exemple: isabelle.martin@domain.com',
                    'class' => 'form-group',
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'security.register.form.password.same',
                'required' => true,
                'first_options' => ['label' => false, 'attr' => ['placeholder' => 'security.register.form.password.first.placeholder']],
                'second_options' => ['label' => false, 'attr' => ['placeholder' => 'security.register.form.password.second.placeholder']],
                'constraints' => [
                    new NotBlank(['message' => 'security.register.form.password.not_blank']),
                    /*
                     Le mot de passe valide aura
                    - de 8 à 15 caractères
                    - au moins une lettre minuscule
                    - au moins une lettre majuscule
                    - au moins un chiffre
                    - au moins un de ces caractères spéciaux: $ @ % * + - _ !
                    - aucun autre caractère possible: pas de & ni de { par exemple)
                    => ^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$
                     */
                    new Regex([
                        'pattern' => '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$/',
                        'htmlPattern' => '^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$',
                        'message' => 'security.register.form.password.regex']),
                    new Length([
                        'max' => 4096, 'maxMessage' => 'security.register.form.password.max_length',
                    ])
                ]
            ])
            //https://qferrer.medium.com/securing-your-symfony-forms-with-google-recaptcha-v2-dbfc902b0c50
//            ->add('captcha', ReCaptchaType::class, [
//                'type' => 'checkbox', // (invisible, checkbox)
//                'mapped' => false
//            ])
            ->add('submit', SubmitType::class, [
                'label' => 'security.register.form.submitButton.label',
                'attr' => [
                    'class' => 'btn btn-block btn-sm btn-info'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'messages',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
