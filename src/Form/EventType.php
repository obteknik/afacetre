<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('person')
            ->add('startAt', DateTimeType::class, [
                    'required' => true,
                    'label' => 'Date de début',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('endAt', DateTimeType::class, [
                    'required' => true,
                    'label' => 'Date de fin',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('isAllDay')
            ->add('isPeriodic')
            ->add('isConfirmedByEmail')
            ->add('object')
            ->add('location')
            ->add('type')
            ->add('comment', TextareaType::class, [
                'label' => "Commentaire",
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Commentaire...',
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
