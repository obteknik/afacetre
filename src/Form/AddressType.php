<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AddressType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.address_name.placeholder',
                    'maxLength' => 30,
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.address_name.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.address_name.not_blank']),
                    new Length([
                        'min' => 5, 'minMessage' => 'account.manageAddress.form.address_name.min_length',
                        'max' => 30, 'maxMessage' => 'account.manageAddress.form.address_name.max_length',
                    ])
                ]
            ])
            ->add('firstname', TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.firstname.placeholder',
                    'maxLength' => 30,
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.firstname.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.firstname.not_blank']),
                    new Length([
                        'min' => 5, 'minMessage' => 'account.manageAddress.form.firstname.min_length',
                        'max' => 30, 'maxMessage' => 'account.manageAddress.form.firstname.max_length',
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.lastname.placeholder',
                    'maxLength' => 30,
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.lastname.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.lastname.not_blank']),
                    new Length([
                        'min' => 5, 'minMessage' => 'account.manageAddress.form.lastname.min_length',
                        'max' => 30, 'maxMessage' => 'account.manageAddress.form.lastname.max_length',
                    ])
                ]
            ])
            ->add('company', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.company.placeholder',
                    'maxLength' => 30,
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.company.help',
                'constraints' => [
                    new Length([
                        'min' => 5, 'minMessage' => 'account.manageAddress.form.company.min_length',
                        'max' => 30, 'maxMessage' => 'account.manageAddress.form.company.max_length',
                    ])
                ]
            ])
            ->add('address', TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.address.placeholder',
                    'maxLength' => 150,
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.address.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.address.not_blank']),
                    new Length([
                        'min' => 6, 'minMessage' => 'account.manageAddress.form.address.min_length',
                        'max' => 150, 'maxMessage' => 'account.manageAddress.form.address.max_length',
                    ])
                ]
            ])
            ->add('zipcode', TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.zipcode.placeholder',
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.zipcode.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.zipcode.not_blank']),
                    new Length([
                        'min' => 5, 'minMessage' => 'account.manageAddress.form.zipcode.min_length',
                        'max' => 10, 'maxMessage' => 'account.manageAddress.form.zipcode.max_length',
                    ])
                ]
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.city.placeholder',
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.city.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.city.not_blank']),
                    new Length([
                        'min' => 3, 'minMessage' => 'account.manageAddress.form.city.min_length',
                        'max' => 150, 'maxMessage' => 'account.manageAddress.form.city.max_length',
                    ])
                ]
            ])
            ->add('country', CountryType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.country.placeholer',
                    'class' => 'form-group'
                ],
                'preferred_choices' => ['FR'],
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.country.not_blank']),
                ]
            ])
            ->add('phone', TelType::class, [
                'attr' => [
                    'placeholder' => 'account.manageAddress.form.phone.placeholder',
                    'class' => 'form-group'
                ],
                'help' => 'account.manageAddress.form.phone.help',
                'constraints' => [
                    new NotBlank(['message' => 'account.manageAddress.form.phone.not_blank']),
                    new Length([
                        'min' => 10, 'minMessage' => 'account.manageAddress.form.phone.min_length',
                        'max' => 14, 'maxMessage' => 'account.manageAddress.form.phone.max_length',
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'account.manageAddress.form.buttonSubmit.label',
                'attr' => [
                    'class' => 'btn btn-info btn-block'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
            'translation_domain' => 'messages',
        ]);
    }
}
