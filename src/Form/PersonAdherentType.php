<?php

namespace App\Form;

use App\Entity\Adherent;
use App\Entity\AdherentEvents;
use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonAdherentType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * ParticipantType constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,[
                'label' => 'Prénom',
                'attr' => [
                    'placeholder' => 'Prénom...'
                ]
            ])
            ->add('lastname', TextType::class,[
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Nom...'
                ]
            ])
            ->add('address', TextType::class,[
                'label' => 'Adresse',
                'attr' => [
                    'placeholder' => 'Adresse...'
                ]
            ])
            ->add('phoneNumber', TextType::class,[
                'label' => 'N° de téléphone',
                'attr' => [
                    'placeholder' => 'N° de téléphone...'
                ]
            ])
//            ->add('illustration')
//            ->add('description')
//            ->add('isActive')
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

                /** @var Adherent $adherent */
                $adherent = $event->getData();
                $formData = $event->getForm();
                $eventRdv = $formData->getParent()->getParent()->getData();

//                dump($adherent);
//                dump($event);
//                die();

                $adherentEvent = new AdherentEvents();
                $adherentEvent->setCreatedAt(new \DateTime());
                $adherentEvent->setEvent($eventRdv);
                $adherentEvent->setAdherent($adherent);
                $adherentEvent->setIsHere(false);
                $this->manager->persist($adherentEvent);

                /** @var Event $eventRdv */
                $eventRdv->addAdherentEvent($adherentEvent);
//                dd($formData->getParent()->getParent()->getData()->getStartAt());
                $adherent->addAdherentEvent($adherentEvent);
                $this->manager->flush();

            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adherent::class,
        ]);
    }
}
