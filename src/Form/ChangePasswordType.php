<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class ChangePasswordType
 * @package App\Form
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'disabled' => true,
                'label' => 'account.editPassword.form.firstname_label',
            ])
            ->add('lastname', TextType::class, [
                'disabled' => true,
                'label' => 'account.editPassword.form.lastname_label'
            ])
            ->add('email', EmailType::class, [
                'disabled' => true,
                'label' => 'account.editPassword.form.email.label'
            ])
            ->add('old_password', PasswordType::class, [
                'mapped' => false,
                'label' => 'account.editPassword.form.old_password.label',
                'attr' => [
                    'autofocus' => true,
                    'placeholder' => 'account.editPassword.form.old_password.placeholder',
                    'class' => 'form-group'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'security.changePassword.form.old_password.not_blank']),
                ]
            ])
            ->add('new_password', RepeatedType::class, [
                    'mapped' => false,
                    'type' => PasswordType::class,
                    'invalid_message' => 'account.editPassword.form.new_password.invalid_message',
                    'label' => 'account.editPassword.form.password.placeholder',
                    'required' => true,
                    'first_options' => ['label' => false, 'attr' => ['placeholder' => 'account.editPassword.form.new_password.first.placeholder']],
                    'second_options' => ['label' => false, 'attr' => ['placeholder' => 'account.editPassword.form.new_password.second.placeholder']],
                    'constraints' => [
                        new NotBlank(['message' => 'account.editPassword.form.new_password.not_blank']),
                        /*
                         Le mot de passe valide aura
                        - de 8 à 15 caractères
                        - au moins une lettre minuscule
                        - au moins une lettre majuscule
                        - au moins un chiffre
                        - au moins un de ces caractères spéciaux: $ @ % * + - _ !
                        - aucun autre caractère possible: pas de & ni de { par exemple)
                        => ^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$
                         */
                        new Regex(['pattern' => '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$',
                            'message' => 'user.form.new_password.regex']),
                        new Length([
                            'max' => 4096, 'maxMessage' => 'account.editPassword.form.new_password.max_length',
                        ])
                    ]
                ]
            )
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'messages',
        ]);
    }
}
