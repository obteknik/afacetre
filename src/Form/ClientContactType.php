<?php

namespace App\Form;

use App\Entity\ClientContact;
use App\Entity\ClientContactFunction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ClientContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'attr' => [
                    'placeholder' => 'admin.clientContact.form.firstname_placeholder',
                ],
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'placeholder' => 'admin.clientContact.form.lastname.placeholder',
                ],
            ])
            ->add('phoneNumber', TextType::class, [
                'attr' => [
                    'placeholder' => 'admin.clientContact.form.phoneNumber.placeholder',
                ],
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'admin.clientContact.form.email.placeholder',
                ],
            ])
            ->add('function', EntityType::class, [
                'class' => ClientContactFunction::class,
                'choice_label' => 'name',
            ])
;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClientContact::class,
        ]);
    }
}
