<?php

namespace App\Form;

use App\Entity\ProductDay;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductDayType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ProductDayType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference', TextType::class, [
                'label' => $this->translator->trans('admin.productDays.form.reference.label',
                    [], 'admin'),
                'attr' => [
                    'placeholder' => $this->translator->trans('admin.productDays.form.reference.placeholder',
                        [], 'admin'),
                ]
            ])
            ->add('goal', TextareaType::class, [
                'label' => $this->translator->trans('admin.productDays.form.goal.label',
                    [], 'admin'),
                'attr' => [
                    'placeholder' => $this->translator->trans('admin.productDays.form.goal.placeholder',
                        [], 'admin'),
                    'class' => 'summernote'
                ]
            ])
            ->add('content', TextareaType::class, [
                'label' => $this->translator->trans('admin.productDays.form.content.label',
                    [], 'admin'),
                'attr' => [
                    'placeholder' => $this->translator->trans('admin.productDays.form.content.placeholder',
                        [], 'admin'),
                    'class' => 'summernote'
                ]
            ])
            ->add('methodMeans', TextareaType::class, [
                'label' => $this->translator->trans('admin.productDays.form.methodMeans.label',
                    [], 'admin'),
                'attr' => [
                    'placeholder' => $this->translator->trans('admin.productDays.form.methodMeans.placeholder',
                        [], 'admin'),
                    'class' => 'summernote'
                ]
            ])
            ->add('program', TextareaType::class, [
                'label' => $this->translator->trans('admin.productDays.form.program.label',
                    [], 'admin'),
                'attr' => [
                    'placeholder' => $this->translator->trans('admin.productDays.form.program.placeholder',
                        [], 'admin'),
                    'class' => 'summernote'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductDay::class,
            'nextReference' =>null
        ]);
    }
}
