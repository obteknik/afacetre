<?php

namespace App\Form;

use App\Repository\AdherentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExistingAdherentType extends AbstractType
{
    /**
     * @var AdherentRepository
     */
    private $adherentRepository;

    /**
     * ExistingPersonAdherentType constructor.
     * @param AdherentRepository $adherentRepository
     */
    public function __construct(AdherentRepository $adherentRepository)
    {
        $this->adherentRepository = $adherentRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $adherents = $this->adherentRepository->findAll();
        $choices = [];
        foreach($adherents as $a){
            $fullname = $a->getFirstname().' '.$a->getLastname();
            $choices[$fullname] = $a->getId();
        }

        $builder->add('adherent', ChoiceType::class, [
            'choices' => $choices,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
