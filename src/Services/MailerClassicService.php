<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

/**
 * Class MailerClassicService
 * @package App\Service
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class MailerClassicService
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Symfony\Component\Mailer\MailerInterface $mailer
     */
    private $mailer;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * MailerClassicService constructor.
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param MailerInterface $mailer
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, MailerInterface $mailer)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param $data => from/to/subject/template/context
     * @param bool $forceEmail
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function toSend($data, $forceEmail = false)
    {

        //mode dev/prod
        if ($forceEmail == false) {
            if ($_ENV['APP_ENV'] == 'prod') {
                $emailTo = $data['to'];
            } else {
                $emailTo = $_ENV['DELIVERY_ADDRESSES'];
            }
        } else {
            $emailTo = $data['to'];
        }

        //objet email
        $email = (new TemplatedEmail())
            ->from($data['from'])
            ->to($emailTo)
            ->subject($data['subject'])
            ->htmlTemplate($data['template'])
            ->context($data['context']);

        try {
            $this->mailer->send($email);
            $this->logger->info('MailerService - Email bien envoyé.');
        } catch (\Exception $e) {
            $this->logger->error('MailerService - Anomalie dans l\'envoi du mail: ' . '\n' . $e->getMessage());
        }
    }
}