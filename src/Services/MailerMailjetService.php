<?php

namespace App\Services;

use App\Repository\OrganizationRepository;
use Mailjet\Client;
use Mailjet\Resources;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class MailerMailjetService
 * @package App\Service
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class MailerMailjetService
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * MailerMailjetService constructor.
     * @param LoggerInterface $logger
     * @param OrganizationRepository $organizationRepository
     * @param KernelInterface $kernel
     */
    public function __construct(LoggerInterface $logger, OrganizationRepository $organizationRepository,
                                KernelInterface $kernel)
    {
        $this->logger = $logger;
        $this->organizationRepository = $organizationRepository;
        $this->kernel = $kernel;
    }

    /**
     * @param $to_email
     * @param $to_name
     * @param $subject
     * @param $content
     * @param $mailJetTemplateId
     * @param string $filePdf
     * @param string $from_name
     * @param string $from_email
     */
    public function send($to_email, $to_name, $subject, $content, $mailJetTemplateId, $filePdf = '', $cc = false, $bcc = false,
                         $from_name = "L'A.F.AC de l'Être", $from_email = "contact@afac-etre.fr")
    {

        $organization = $this->organizationRepository->find(1);

        //mode dev/prod
//        $to_finalEmail = $_ENV['APP_ENV'] == 'prod' ? $to_email : $_ENV['DELIVERY_ADDRESSES'];
        $to_finalEmail = $to_email;

        $mj = new Client($_ENV['MJ_APIKEY_PUBLIC'], $_ENV['MJ_APIKEY_PRIVATE'], true, ['version' => 'v3.1']);
        $mj->setTimeout(30);

        $toCC = $cc ? [
            ['Email' => $organization->getEmailCc(), 'Name' => 'Copie envoi']
        ] : [];
        $toBCC = $bcc ? [
            ['Email' => $organization->getEmailBcc(), 'Name' => 'Copie cachée envoi']
        ] : [];

//        $pdfBase64 = $filePdf != '' ? base64_encode(file_get_contents($filePdf)) : null;
//        $base64 = $filePdf != '' ? base64_encode(file_get_contents($filePdf)) : null;

        $file = $this->kernel->getProjectDir() . 'test.txt';
        dd($file);
        $body = [
            'Messages' => [
                [
                    'From' => ['Email' => $from_email, 'Name' => $from_name],
                    'To' => [['Email' => $to_finalEmail, 'Name' => $to_name]],
                    'CC' => $toCC,
                    'BCC' => $toBCC,
                    'TemplateID' => $mailJetTemplateId,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => ['content' => $content],
                    'Attachments' => [
                        [
                            'Content-type' => "text/plain",
                            'Filename' => "test.txt",
//                            'Filename' => "C:\Users\Olivier BRASSART\Documents\symfony projects\afac\public\test.txt",
//                            'Base64Content' => "VGhpcyBpcyB5b3VyIGF0dGFjaGVkIGZpbGUhISEK"
                        ]
                    ],
                ]
            ]
        ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success() && dd($response->getData());
//        $response->success();
    }
}