<?php

namespace App\Services;

use Gedmo\Sluggable\Util\Urlizer;
use Knp\Snappy\Pdf;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PdfSnappyService
 * @package App\Service
 * @author Olivier Brassart <obteknik2007@gmail.com>
 * WKHTMLtoPDF est un outil en ligne de commande qui permet de générer un document PDF
 * à partir d'une page HTML en utilisant le moteur de rendu QT Webkit.
 *
 * Vous pouvez l'essayer en générant un PDF de la page d'acceuil de google par exemple :
 * wkhtmltopdf http://google.com google.pdf
 *
 * https://github.com/worlduniting/bookshop/wiki/wkhtmltopdf-options
 * https://medium.com/@greggailly78/symfony-4-export-pdfs-caacfd89ff03
 */
class PdfSnappyService
{

    /**
     * @var ContainerInterface
     */
    private $container;
    private $templating;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Pdf
     */
    private $pdf;

    /**
     * PdfSnappy constructor.
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     * @param Pdf $pdf
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger, Pdf $pdf)
    {
        $this->container = $container;
        $this->templating = $container->get('twig');
        $this->logger = $logger;
        $this->pdf = $pdf;
    }

    /**
     * Fonction de génération PDF
     * @param string $filename
     * @param string $publicDirectory
     * @param string $formatPage => Letter/A4/...
     * @param string $orientation => Landscape/Portrait
     * @param string $twigUrl
     * @param array $twigOptions
     * @param int $outputType => //1: save // 2:attachment // 3:inline
     * @return Response
     */
    public function getPdf(string  $filename, string $publicDirectory, string $formatPage, string $orientation,
                           string $twigUrl, array $twigOptions, int $outputType = 1)
    {

        try {
//            $header = "<div style=\"text-align:center;padding-top:20px;font-size:14px;color:#333;border-top:1px solid #EEE;\">HEADER CONTENT HERE";
//            $header = "<div style='height: 100px'>HEADER CONTENT HERE</div>";


            /*
             * Snappy options
             * https://wkhtmltopdf.org/usage/wkhtmltopdf.txt or in terminal => wkhtmltopdf -H
             */
            $this->pdf->setOption('no-outline', true);
            $this->pdf->setOption('encoding', 'UTF-8');
            $this->pdf->setOption('page-size',$formatPage);
            $this->pdf->setOption('orientation', $orientation);

//            $snappy->setOption('disable-javascript', true);
//            $snappy->setOption('disable-local-file-access', true);

            //HEADER
//            $this->pdf->setOption('header-html', $header);
//            $this->pdf->setOption('header-line', true);

            //FOOTER
//            $this->snappy->setOption('footer-font-name', 'Arial');
//            $this->pdf->setOption('footer-right', 'Page [page] sur [toPage]');
            $footer = "
                <div style='text-align:center;padding-top:20px;font-size:14px;color:#000;border-top:1px solid #EEE;'>
                    <p>L'A.F.A.C de l'Être ©2020 - Tous droits réservés</p>
                    <p>
                        <small>109 rue de la Louvetière 77570 CHATEAU-LANDON - Tel : 07 81 34 06 44 – contact : contact@afac-etre.fr www.afac-etre.fr</small>
                    </p>
                </div>";

            $this->pdf->setOption('footer-html', $footer);
//            $this->snappy->setOption('margin-top', '27');
            $this->pdf->setOption('margin-bottom', '27');

            $filename = ucfirst(Urlizer::urlize($filename)).'-'.uniqid();
            $html = $this->templating->render($twigUrl, $twigOptions);
            $publicDirectory = $this->container->getParameter($publicDirectory);

            //https://github.com/KnpLabs/KnpSnappyBundle/issues/195 => FOR HEADER, FOOTER, ....
            $mode='';
            switch ($outputType){
                case 1: //save
                    //$this->get('knp_snappy.pdf')->generateFromHtml($content, $pdfFolder . time() . '.pdf';
                    //So you could probably just use file_put_contents to write the result of getOutputFromHtml($content)
                    // to the file system.
                    $this->pdf->generateFromHtml($html, $publicDirectory.'/'.$filename);
                    break;
                case 2: $mode = 'attachment';
                    break;
                case 3: $mode = 'inline';
                    break;
                default: $this->pdf->generateFromHtml($html, $publicDirectory.'/'.$filename);
                    break;
            }

            $resultService['filename'] = $filename;
            $resultService['outputType'] = $mode;
            $resultService['pdf'] = $this->pdf->getOutputFromHtml($html);

        } catch (\Exception $e) {
            $resultService["errorMessage"] = "Anomalie rencontrée lors de la génération du fichier PDF : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[PdfSnappyService/getPdf] - " . $resultService["errorMessage"]);
        }

        return $resultService;
    }
}