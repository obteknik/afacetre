<?php

namespace App\Services;

use Dompdf\Adapter\CPDF;
use Dompdf\Canvas;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DompdfService
 * @package App\Service
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class DompdfService
{

    /**
     * @var ContainerInterface
     */
    private $container;
    private $templating;

    /**
     * PdfDomPdf constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->templating = $container->get('twig');
    }

    /**
     * Fonction GENERIQUE génération PDF
     * ATTESTATION FIN DE FORMATION : getPdf($contract, '_pdfModels/contract/04-certificate.html.twig', 'certificate',
     *            'Contrat-' . $contract->getReference() . '-ATTESTATIONS-FIN-DE-FORMATION')
     * @param $object
     * @param string $model
     * @param array $options
     * @param string $typeDocument
     * @param string $orientation
     * @param string $filename
     * @param string $font
     * @param bool $save
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getPdf($object, string $model, ?array $options, string $typeDocument, string $orientation, string $filename,
                           string $font = 'Open Sans Condensed', $save = true)
    {

        //Définition des options du pdf
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', $font)
            ->setIsRemoteEnabled(true)
            ->setIsHtml5ParserEnabled(true)
            ->setIsPhpEnabled( false);

        //Instanciation dompdf
        $dompdf = new Dompdf($pdfOptions);
        $context = stream_context_create([
            'ssl' => [ 'verify_peer' => false,'verify_peer_name' => false, 'allow_self_signed' => true]]);
        $dompdf->setHttpContext($context);

        //genération html
        $params = ['typeDocument' => $typeDocument, 'object' => $object, 'dompdf' => $dompdf];
        $html = $this->templating->render($model, $params);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', $orientation);
        $dompdf->render();

        if($save){
            $publicDirectory = $this->container->getParameter('app.outputpdf.invoice.dir');

            $this->injectPageCount($dompdf);

            $output = $dompdf->output();
            file_put_contents($filename, $publicDirectory.'/'.$output);
        }

        //envoi du pdf au navigateur
        $dompdf->stream($filename, ['Attachment' => true]);
    }

    /**
     * Replace a predefined placeholder with the total page count in the whole PDF document
     *
     * @param Dompdf $dompdf
     */
    function injectPageCount(Dompdf $dompdf): void
    {
        $canvas = $dompdf->getCanvas();
        assert($canvas instanceof CPDF);
//        $search = self::insertNullByteBeforeEachCharacter('DOMPDF_PAGE_COUNT_PLACEHOLDER');
        $search = 'DOMPDF_PAGE_COUNT_PLACEHOLDER';
        $replace = (string) $canvas->get_page_count();

        foreach ($canvas->get_cpdf()->objects as &$object) {
            if ($object['t'] === 'contents') {
                $object['c'] = str_replace($search, $replace, $object['c']);
            }
        }
    }

    private static function insertNullByteBeforeEachCharacter(string $string): string
    {
        return "\u{0000}" . substr(chunk_split($string, 1, "\u{0000}"), 0, -1);
    }
}