<?php

namespace App\Services;

use App\Entity\Organization;
use App\Repository\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class AppService
 * @package App\Services
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AppService
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * AppService constructor.
     * @param OrganizationRepository $organizationRepository
     * @param CrudUrlGenerator $crudUrlGenerator
     * @param RouterInterface $router
     * @param SessionInterface $session
     * @param EntityManagerInterface $manager
     */
    public function __construct(OrganizationRepository $organizationRepository,
                                CrudUrlGenerator $crudUrlGenerator, RouterInterface $router,
                                SessionInterface $session, EntityManagerInterface $manager)
    {
        $this->organizationRepository = $organizationRepository;
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->router = $router;
        $this->session = $session;
        $this->manager = $manager;
    }

    /**
     * @param Organization|null $organization
     * @param string $typeEntity
     * @param null $object
     * @return string
     */
    public function getNextReference(Organization $organization, string $typeEntity, $object = null): string
    {
        $letter = '';
        switch ($typeEntity) {
            case 'patient':
                $letter = 'P';
                break;

            case 'client':
                $letter = 'C';
                break;

            case 'product':
                $letter = 'CT';
                break;

            case 'contract':
                $letter = 'CT';
                break;

            case 'quote':
                $letter = 'D';
                break;

            case 'invoice':
                $letter = 'F';
                break;

            case 'charge':
                $letter = 'CHG';
                break;

            case 'payment':
                $letter = 'PMT';
                break;

            case 'productDay':
                $letter = 'Jour ';
                break;
        }

        //recherche de la dernière occurrence de l'entité en bdd
        if (!$organization) {
            $this->session->getFlashBag()->add('notice', "Aucune organization enregistrée permettant la saisie d'une nouvelle facture");

            $routeBuilder = $this->crudUrlGenerator->build();
            $crudStr = "App\\Controller\\Admin" . "\\" . ucfirst($typeEntity) . "CrudController";

            switch ($typeEntity) {
                case 'patient':
                    $crudId = '060e445';
                    break;
                case 'client':
                    $crudId = '9f67e5e';
                    break;
                case 'contract':
                    $crudId = 'ec35513';
                    break;
                case 'product':
                    $crudId = 'c6b0339';
                    break;
                case 'quote':
                    $crudId = 'df66dc2';
                    break;
                case 'invoice':
                    $crudId = '9d746ac';
                    break;
                case 'charge' :
                    $crudId = 'cc0669d';
                    break;
                case 'payment' :
                    $crudId = '1770c59';
                    break;
            }
            $routeBuilder
                ->setDashboard('App\\Controller\\Admin\\DashboardController')
                ->setController($crudStr)
                ->setAction('index')
                ->setCrudId($crudId);
            return $this->router($routeBuilder->generateUrl());
        }

        //CAS productDay
        if($typeEntity == 'productDay'){

            //recherche du dernier n° de jour de la formation
            $lastItem = $this->manager->getRepository('App\\Entity\\ProductDay')->findOneBy([
                'product' => $object
            ], ['id' => 'DESC']);

            if($lastItem === null){
                $nextItemReference = 'Jour 1';
            } else {
                $str = explode(" ", $lastItem->getReference());
                $lastReference = (int) $str[1];
                $lastReference++;
                $nextItemReference = 'Jour '.$lastReference;
            }

            return $nextItemReference;
        }

        //recherche de la dernière occurrence créée pour l'organization (actt 1 seule organisation => dernier ID)
        $lastItem = $this->manager->getRepository('App\\Entity\\' . ucfirst($typeEntity))->findOneBy([], ['id' => 'DESC']);

        if (!$lastItem) {
            $nextItemReference = $letter . '000001';
        } else {
            $lastReference = $lastItem->getReference();
            $lastChrono = substr($lastReference, -6);

            $nextItemReference = $letter . sprintf("%06d", (int)$lastChrono + 1);
        }

        return $nextItemReference;
    }

}