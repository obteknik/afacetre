<?php

namespace App\Services;

use App\Repository\OrganizationRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SwiftMailerService
 * @package App\Service
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class SwiftMailerService
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    private $templating;

    /**
     * SwiftMailerService constructor.
     * @param LoggerInterface $logger
     * @param OrganizationRepository $organizationRepository
     * @param ContainerInterface $container
     */
    public function __construct(LoggerInterface $logger, OrganizationRepository $organizationRepository,
                                ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->organizationRepository = $organizationRepository;
        $this->templating = $container->get('twig');
    }

    /**
     * Envoi par mail
     * @param array $to
     * @param $subject
     * @param $template
     * @param string $filename
     * @param array $cc
     * @param array $bcc
     * @param array $from
     * @param array $templateOptions
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function send(array $to, $subject, $template, array $templateOptions = [], $filename = '', array $cc = [],
                         array $bcc = [], array $from = ["contact@afac-etre.fr" => "L'A.F.AC de l'Être"])
    {


        $organization = $this->organizationRepository->find(1);

        // Create the Transport
        $transport = (new \Swift_SmtpTransport($_ENV['SWIFTMAILER_HOST'], $_ENV['SWIFTMAILER_PORT']))
            ->setUsername($_ENV['SWIFTMAILER_USERNAME'])
            ->setPassword($_ENV['SWIFTMAILER_PSWD']);

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        // Create a message
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);

        //Body
        $message->setContentType("text/html");;
        $message->setBody($this->templating->render($template, $templateOptions));

        //CC && BCC
        if(!empty($cc)){
            $message->setCc($cc);
        }
        if(!empty($bcc)){
            $message->setBcc($cc);
        }
        // Optionally add any attachments
        if($filename !=''){
            $message->attach(\Swift_Attachment::fromPath($filename, 'application/pdf'));
        }

        // Send the message
        $result = $mailer->send($message);

        return $result;
    }
}