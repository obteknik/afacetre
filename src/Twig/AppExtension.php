<?php

namespace App\Twig;

use App\Entity\Adherent;
use App\Entity\Event;
use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class AppExtension
 * @package App\Twig
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AppExtension extends AbstractExtension
{

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * AppExtension constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('substr', array($this, 'substrFunction')),
            new TwigFilter('age', array($this, 'getAgeFunction')),
        );
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('lettrine', array($this, 'lettrineFunction')),
            new TwigFunction('assetExists', array($this, 'assetExistsFunction')),
            new TwigFunction('isInstanceOfAdherent', array($this, 'isInstanceOfAdherentFunction')),
            new TwigFunction('isInstanceOfEvent', array($this, 'isInstanceOfEventFunction'))
        );
    }

    //FILTERS
    public function getAgeFunction($date)
    {
        if (!$date instanceof \DateTime) {
            // turn $date into a valid \DateTime object or let return
            return null;
        }

        $referenceDate = date('01-01-Y');
        $referenceDateTimeObject = new \DateTime($referenceDate);
        $diff = $referenceDateTimeObject->diff($date);
        return $diff->y;
    }

    /**
     * Réduit la taille d'un texte de la longueur indiquée et ajoute '...' à la fin
     * @param string $text
     * @param $length
     * @return string
     */
    public function substrFunction(string $text, $length): string
    {
        $str = '';
        if (null !== $text) {
            $str = substr($text, 0, $length) . '...';
        }
        return $str;
    }

    //FUNCTIONS
    /**
     * Contrôle l'existence physique d'un fichier
     *
     * @param string $fileRelativePath - chemin relatif de notre fichier en paramètre
     * @param string $dir
     * @return bool
     */
    public function assetExistsFunction(?string $fileRelativePath, string $dir='')
    {
        // si le fichier passé en paramètre de la fonction existe, on retourne true,
        // sinon on retourne false.

        if($fileRelativePath != null){
            return false;
        }
        dd(file_exists($this->kernel->getProjectDir() . "/public/uploads/" . $dir .'/'. $fileRelativePath));
        return file_exists($this->kernel->getProjectDir() . "/public/uploads/" . $dir .'/'. $fileRelativePath) ? true : false;
    }

    /**
     * Application d'une lettrine
     * @param string $text
     * @return string
     */
    public function lettrineFunction(string $text, $fontsize): string
    {
        //récupération 1ère lettre
        if (!$text) {
            return false;
        }

        $firstLetter = substr($text,0,1);
        $length = strlen($text);
        $rest = substr($text,1,$length);
        return '<span style="font-size: '.$fontsize.';color:#000">'.ucfirst($firstLetter).'</span>'.$rest;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function isInstanceOfAdherentFunction($entity): bool
    {
        return ($entity instanceof Adherent);
    }

    /**
     * @param $entity
     * @return bool
     */
    public function isInstanceOfEventFunction($entity): bool
    {
        return ($entity instanceof Event);
    }


}