<?php

namespace App\EventSubscriber;

use App\Entity\Charge;
use App\Entity\Client;
use App\Entity\Invoice;
use App\Entity\InvoiceDetail;
use App\Repository\ClientRepository;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;


/**
 * Class EasyAdminChargeSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminChargeSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var AppService
     */
    private $appService;


    /**
     * EasyAdminClientSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param AppService $appService
     */
    public function __construct(OrganizationRepository $organizationRepository, AppService $appService)
    {
        $this->organizationRepository = $organizationRepository;
        $this->appService = $appService;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [
                ['setOrganization'], ['setCreatedAt'], ['setReference'],
            ],
        ];
    }

    public function setOrganization(BeforeEntityPersistedEvent $event)
    {
        $organization = $this->organizationRepository->find(1);
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Charge)) {
            return;
        }

        $entity->setOrganization($organization);
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Charge)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Charge)) {
            return;
        }
        $organization = $this->organizationRepository->find(1);
        $nextItemReference = $this->appService->getNextReference($organization, 'charge');

        $entity->setReference($nextItemReference);
    }
}