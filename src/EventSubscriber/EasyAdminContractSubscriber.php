<?php

namespace App\EventSubscriber;

use App\Entity\Adherent;
use App\Entity\Contract;
use App\Entity\Event;
use App\Repository\AdherentRepository;
use App\Repository\ContractRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EasyAdminContractSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminContractSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var AdminContextProvider
     */
    private $context;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var ContractRepository
     */
    private $contractRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * EasyAdminInvoiceSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param ContractRepository $contractRepository
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @param AdminContextProvider $context
     * @param AppService $appService
     * @param EntityManagerInterface $manager
     */
    public function __construct(OrganizationRepository $organizationRepository,
                                ContractRepository $contractRepository,
                                ProductRepository $productRepository,
                                EntityManagerInterface $entityManager,
                                AdminContextProvider $context, AppService $appService, EntityManagerInterface $manager)
    {
        $this->organizationRepository = $organizationRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->context = $context;
        $this->appService = $appService;
        $this->contractRepository = $contractRepository;
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setOrganization'],
                ['setCreatedAt'], ['setReference'],
            ],
//            BeforeEntityUpdatedEvent::class => [['updateEventAdherents']]
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setOrganization(BeforeEntityPersistedEvent $event)
    {

        $organization = $this->organizationRepository->find(1);
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Contract)) {
            return;
        }

        $entity->setOrganization($organization);
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Contract)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Contract)) {
            return;
        }

        //recherche de la dernière facture créée pour l'organization (actt 1 seule organisation => dernier ID)
        $organization = $this->organizationRepository->find(1);
        $nextItemReference = $this->appService->getNextReference($organization, 'contract');

        $entity->setReference($nextItemReference);
    }


//    public function updateEventAdherents(BeforeEntityUpdatedEvent $event)
//    {
//        $entity = $event->getEntityInstance();
//        if (($entity instanceof Event)) {
//            dd($event->getEntityInstance());
//
//        }
////
////        if (!($entity instanceof Contract)) {
////            return;
////        }
////
////        // ***** MISE A JOUR DES ADHERENTS CONTRACTUELS AU RENDEZ-VOUS *****
////        //Qd je modifie un RDV contractuel, je lui associe les adherents déjà associés au contrat
////        $contractAdherents = $entity->getAdherents();
////
//////        dd($contractAdherents);
////
////        /** @var Adherent $adherent */
////        //on ajoute tous les adhérents aux différents rdv du contrat
////        foreach($contractAdherents as $adherent){
////            $adherent->addEvent($entity);
////            $entity->addAdherent($adherent);
////            $this->manager->persist($adherent);
////        }
////        $this->manager->flush();
//
//    }
}