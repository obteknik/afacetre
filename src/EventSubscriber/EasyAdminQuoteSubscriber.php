<?php

namespace App\EventSubscriber;

use App\Entity\InvoiceDetail;
use App\Entity\Quote;
use App\Entity\QuoteDetail;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use App\Repository\QuoteRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;

/**
 * Class EasyAdminQuoteSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminQuoteSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var AdminContext|AdminContextProvider
     */
    private $context;
    /**
     * @var QuoteRepository
     */
    private $quoteRepository;
    /**
     * @var AppService
     */
    private $appService;

    /**
     * EasyAdminInvoiceSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param QuoteRepository $quoteRepository
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @param AdminContextProvider $context
     * @param AppService $appService
     */
    public function __construct(OrganizationRepository $organizationRepository,
                                QuoteRepository $quoteRepository,
                                ProductRepository $productRepository,
                                EntityManagerInterface $entityManager,
                                AdminContextProvider $context, AppService $appService)
    {
        $this->organizationRepository = $organizationRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->context = $context;
        $this->quoteRepository = $quoteRepository;
        $this->appService = $appService;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [
                ['setOrganization'], ['setCreatedAt'], ['setReference'], ['setAmountHT'], ['setAmountTTC']
            ],
            AfterEntityPersistedEvent::class => [['addProduct']]
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setOrganization(BeforeEntityPersistedEvent $event)
    {

        $organization = $this->organizationRepository->find(1);
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Quote)) {
            return;
        }

        $entity->setOrganization($organization);
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Quote)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Quote)) {
            return;
        }

        //recherche de la dernière facture créée pour l'organization (actt 1 seule organisation => dernier ID)
        $organization = $this->organizationRepository->find(1);
        $nextItemReference = $this->appService->getNextReference($organization, 'quote');

        $entity->setReference($nextItemReference);
    }

    public function setAmountHT(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Quote)) {
            return;
        }

        $entity->setAmountHT(0);
    }

    public function setAmountTTC(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Quote)) {
            return;
        }

        $entity->setAmountTTC(0);
    }

    // --- AfterEntityPersistedEvent ---
    public function addProduct(AfterEntityPersistedEvent $event)
    {
        /** @var Quote $entity */
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Quote)) {
            return;
        }
        $products = $this->context->getContext()->getRequest()->request->all()['Quote']['products'];

        $products = array_values($products);
        $arrIdProduct = [];
        foreach ($products as $product) {
            foreach ($product as $p) {
                $arrIdProduct[] = $p;
            }
        }

        //Pour chaque id Product je crée les enregistrements de lignes de détails devis
        $totalAmountHT = 0;
        $organization = $this->organizationRepository->find(1);
        $taxRate = $organization->getTaxRate();
        foreach ($arrIdProduct as $idProduct) {

            //recherche du prix unitaire du produit
            $quote = $this->quoteRepository->find($entity->getId());
            $product = $this->productRepository->find($idProduct);
            $price = $product->getPrice();

            $quoteDetail = new QuoteDetail();
            $quoteDetail->setCreatedAt(new \DateTime());
            $quoteDetail->setQuote($quote);
            $quoteDetail->setProduct($product);
            $quoteDetail->setQuantity(1);
            $quoteDetail->setAmountHT($price);

            $this->entityManager->persist($quoteDetail);

            //calcul total HT
            $totalAmountHT += $quoteDetail->getAmountHT();
        }

        $entity->setAmountHT($totalAmountHT);
        $entity->setAmountTTC(1+($totalAmountHT * ($taxRate/100)));
        $this->entityManager->persist($entity);

        $this->entityManager->flush();
    }

}