<?php

namespace App\EventSubscriber;

use App\Entity\Patient;
use App\Entity\User;
use App\Manager\UserManager;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Gedmo\Sluggable\Util\Urlizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class EasyAdminPatientSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminPatientSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * EasyAdminPatientSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param AppService $appService
     * @param UserManager $userManager
     * @param LoggerInterface $logger
     * @param SessionInterface $session
     * @param CrudUrlGenerator $crudUrlGenerator
     * @param EntityManagerInterface $manager
     */
    public function __construct(OrganizationRepository $organizationRepository, AppService $appService,
                                UserManager $userManager, LoggerInterface $logger, SessionInterface $session,
                                CrudUrlGenerator $crudUrlGenerator, EntityManagerInterface $manager)
    {
        $this->organizationRepository = $organizationRepository;
        $this->appService = $appService;
        $this->userManager = $userManager;
        $this->logger = $logger;
        $this->session = $session;
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setCreatedAt'], ['setReference']],
            AfterEntityPersistedEvent::class => [['createUserAccount']]
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Patient)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Patient)) {
            return;
        }

        //recherche de la dernière facture créée pour l'organization (actt 1 seule organisation => dernier ID)
        $organization = $this->organizationRepository->find(1);
        $nextItemReference = $this->appService->getNextReference($organization, 'patient');

        $entity->setReference($nextItemReference);
    }

    // --- AfterEntityPersistedEvent ---
    public function createUserAccount(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Patient)) {
            return false;
        }

        //Création du compte utilisateur
        $organization = $this->organizationRepository->find(1);
        $email = $entity->getFirstname() . '.' . $entity->getLastname() . uniqid() . '@yopmail.com';

        $resultManager = $this->userManager->create(
            $organization, null, $entity->getFirstname(), $entity->getLastname(), $email, 'test',
            ['ROLE_PATIENT']);

        if (isset($resultManager["errorMessage"])) {
            $this->logger->error("[EasyAdminPatientSubscriber/createUserAccount] - " . $resultManager["errorMessage"]);

            //message flash
            $this->session->getFlashBag()->add('notice', "Anomalie dans l'enregistrement du compte utilisateur - veuillez contacter l'adminsitrateur système");           //$this->container->get('session')->getFlashBag()->add($type, $message);

        } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof User) {
            $entity->setUser($resultManager["item"]);
            $this->manager->flush();
            $this->logger->info("[EasyAdminPersonSubscriber/createUserAccount] - Compte user créé pour la personne" .
                $entity->getFirstname() . ' ' . $entity->getLastname());
        }


        //redirection sur liste patients avec addFlash si anomalie
        $routeBuilder = $this->crudUrlGenerator->build();

        $crudId = '060e445';
        $routeBuilder
            ->setDashboard('App\\Controller\\Admin\\DashboardController')
            ->setController('App\\Controller\\Admin\PatientCrudController')
            ->setAction('index')
            ->setCrudId($crudId);
        return new RedirectResponse($routeBuilder->generateUrl(), 302);

    }
}