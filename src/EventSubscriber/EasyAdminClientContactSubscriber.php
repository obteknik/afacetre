<?php

namespace App\EventSubscriber;

use App\Entity\ClientContact;
use App\Entity\Discipline;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EasyAdminClientContactSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminClientContactSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [ ['setCreatedAt'],],

        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof ClientContact)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

}
