<?php

namespace App\EventSubscriber;

use App\Entity\Adherent;
use App\Entity\AdherentEvents;
use App\Entity\Event;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class ContractEventSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
//        return array('postPersist', 'postUpdate', 'preRemove');
        return array('postPersist'); //, 'prePersist',
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->indexEvent($args, 'postPersist');
        $this->indexAdherent($args, 'postPersist');
    }

    /**
     * Nouveau participant => lui associer les éventuels rdv du contrat
     * @param LifecycleEventArgs $args
     * @param $event
     */
    public function indexAdherent(LifecycleEventArgs $args, $event)
    {
        $entity = $args->getObject();
        $manager = $args->getObjectManager();

        if ($entity instanceof Adherent) {
            if ($entity->getContracts() !== null) {
                //TODO: Tester si adhérent dispose déjà d'un autre contrat - si pb, il faudra récupérer le denier
                $contract = $entity->getContracts()[0];

                if($contract){
                    $contractEvents = $contract->getEvents();
                    if ($contract->getEvents()) {
                        //on ajoute au nouvel adherent les différents rdv du contrat
                        /** @var Adherent $adherent */
                        foreach ($contractEvents as $e) {
                            $adherentEvent = new AdherentEvents();
                            $adherentEvent->setCreatedAt(new \DateTime());
                            $adherentEvent->setAdherent($entity);
                            $adherentEvent->setEvent($e);
                            $adherentEvent->setIsHere(true);
                            $manager->persist($adherentEvent);
                        }
                        $manager->flush();
                    }
                }

            }
        }
    }

    /**
     * Nouvelle séance => je l'ajoute aux différents participants
     * @param LifecycleEventArgs $args
     * @param $event
     */
    public function indexEvent(LifecycleEventArgs $args, $event)
    {
        $entity = $args->getObject();
        $manager = $args->getObjectManager();

        if ($entity instanceof Event) {
            $contract = $entity->getContract();

            if ($contract) {
                //s'il ya des participants
                if ($contract->getAdherents()) {
                    foreach ($contract->getAdherents() as $a) {
                        $adherentEvent = new AdherentEvents();
                        $adherentEvent->setCreatedAt(new \DateTime());
                        $adherentEvent->setAdherent($a);
                        $adherentEvent->setEvent($entity);
                        $adherentEvent->setIsHere(true);
                        $manager->persist($adherentEvent);
                    }
                    $manager->flush();
                }
            }

        }
    }
}