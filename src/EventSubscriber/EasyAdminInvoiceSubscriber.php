<?php

namespace App\EventSubscriber;

use App\Entity\Invoice;
use App\Entity\InvoiceDetail;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;


/**
 * Class EasyAdminInvoiceSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminInvoiceSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var AdminContext|AdminContextProvider
     */
    private $context;
    /**
     * @var AppService
     */
    private $appService;

    /**
     * EasyAdminInvoiceSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param InvoiceRepository $invoiceRepository
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @param AdminContextProvider $context
     * @param AppService $appService
     */
    public function __construct(OrganizationRepository $organizationRepository,
                                InvoiceRepository $invoiceRepository,
                                ProductRepository $productRepository,
                                EntityManagerInterface $entityManager,
                                AdminContextProvider $context, AppService $appService)
    {
        $this->organizationRepository = $organizationRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->context = $context;
        $this->appService = $appService;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [
                ['setOrganization'], ['setCreatedAt'], ['setReference'], ['setAmountHT'], ['setAmountTTC']
            ],
            AfterEntityPersistedEvent::class => [['addProduct']]
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setOrganization(BeforeEntityPersistedEvent $event)
    {

        $organization = $this->organizationRepository->find(1);
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Invoice)) {
            return;
        }

        $entity->setOrganization($organization);
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        //recherche de la dernière facture créée pour l'organization (actt 1 seule organisation => dernier ID)
        $organization = $this->organizationRepository->find(1);
        $nextItemReference = $this->appService->getNextReference($organization, 'invoice');

        $entity->setReference($nextItemReference);
    }

    public function setAmountHT(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        $entity->setAmountHT(0);
    }

    public function setAmountTTC(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        $entity->setAmountTTC(0);
    }

    // --- AfterEntityPersistedEvent ---
    public function addProduct(AfterEntityPersistedEvent $event)
    {
        /** @var Invoice $event */
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        //Pour chaque id Product je crée les enregistrements de lignes de détails facture
        $organization = $this->organizationRepository->find(1);
        $taxRate = $organization->getTaxRate();
        $totalAmountHT = 0;

        $products = $entity->getProducts();

        // Pour chaque produit, on crée une ligne de détail facture
        foreach ($products as $p) {
            $invoiceDetail = new InvoiceDetail();
            $invoiceDetail->setCreatedAt(new \DateTime());
            $invoiceDetail->setInvoice($entity);
            $invoiceDetail->setProduct($p);
            $invoiceDetail->setQuantity(1);
            $invoiceDetail->setAmountHT($p->getPrice());
            $this->entityManager->persist($invoiceDetail);

            //calcul total HT
            $totalAmountHT += $invoiceDetail->getAmountHT();
        }
        $entity->setAmountHT($totalAmountHT);
        $entity->setAmountTTC(1 + ($totalAmountHT * ($taxRate / 100)));
        $this->entityManager->persist($entity);

        $this->entityManager->flush();
    }

}