<?php

namespace App\EventSubscriber;

use App\Entity\Client;
use App\Entity\Invoice;
use App\Entity\InvoiceDetail;
use App\Entity\Person;
use App\Entity\User;
use App\Manager\CalendarParameterManager;
use App\Manager\UserManager;
use App\Repository\ClientRepository;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PersonRepository;
use App\Repository\ProductRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Gedmo\Sluggable\Util\Urlizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class EasyAdminPersonSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminPersonSubscriber implements EventSubscriberInterface
{
    /**
     * @var PersonRepository
     */
    private $personRepository;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var CalendarParameterManager
     */
    private $calendarParameterManager;

    /**
     * EasyAdminClientSubscriber constructor.
     * @param PersonRepository $personRepository
     * @param UserManager $userManager
     * @param LoggerInterface $logger
     * @param CrudUrlGenerator $crudUrlGenerator
     * @param SessionInterface $session
     * @param OrganizationRepository $organizationRepository
     * @param EntityManagerInterface $manager
     * @param CalendarParameterManager $calendarParameterManager
     */
    public function __construct(PersonRepository $personRepository, UserManager $userManager,
                                LoggerInterface $logger, CrudUrlGenerator $crudUrlGenerator,
                                SessionInterface $session, OrganizationRepository $organizationRepository,
                                EntityManagerInterface $manager, CalendarParameterManager $calendarParameterManager)
    {
        $this->personRepository = $personRepository;
        $this->userManager = $userManager;
        $this->logger = $logger;
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->session = $session;
        $this->organizationRepository = $organizationRepository;
        $this->manager = $manager;
        $this->calendarParameterManager = $calendarParameterManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setCreatedAt'], ['setIsActive'], ['setSlug']],
            AfterEntityPersistedEvent::class => [['createUserAccount']]
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Person)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setSlug(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();

        if (!($entity instanceof Person)) {
            return;
        }

        $slug =  Urlizer::urlize($entity->getFirstname().' '.$entity->getLastname());
        $entity->setSlug($slug);
    }

    public function setIsActive(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Person)) {
            return;
        }
        $entity->setCreatedAt(new \DateTime());
    }

    // --- AfterEntityPersistedEvent ---
    public function createUserAccount(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Person)) {
            return false;
        }

        //Création du compte utilisateur
        $organization = $this->organizationRepository->find(1);
        $email = $entity->getFirstname() . '.' . $entity->getLastname() .uniqid(). '@yopmail.com';

        $resultManager = $this->userManager->create(
            $organization, null, $entity->getFirstname(), $entity->getLastname(), $email, 'test', ['ROLE_PERSON']);

        if (isset($resultManager["errorMessage"])) {
            $this->logger->error("[EasyAdminPersonSubscriber/createUserAccount] - " . $resultManager["errorMessage"]);

            //message flash
            $this->session->getFlashBag()->add('notice', "Anomalie dans l'enregistrement du compte utilisateur - veuillez contacter l'adminsitrateur système");           //$this->container->get('session')->getFlashBag()->add($type, $message);

        } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof User) {
            $entity->setUser($resultManager["item"]);
            $this->manager->flush();
            $this->logger->info("[EasyAdminPersonSubscriber/createUserAccount] - Compte user créé pour la personne" .
                $entity->getFirstname() . ' ' . $entity->getLastname());
        }

        //Création du paramétrage par défaut calendar
        $resultManagerCalParameter = $this->calendarParameterManager->create($resultManager["item"],
            new \DateTime('2020-01-01'),
            new \DateTime('2020-12-31'),
            1,
            new \DateTime('0000-00-00 08:00:00'),
            new \DateTime('0000-00-00 17:00:00'),
            true
        );

        if (isset($resultManagerCalParameter["errorMessage"]) && $resultManagerCalParameter["errorMessage"] instanceof Exception) {
            $this->logger->warning($resultManagerCalParameter["errorMessage"]);

        } elseif (isset($resultManagerCalParameter["item"]) && $resultManagerCalParameter["item"] instanceof CalendarParameter) {
            $this->logger->info('Great ! Calendar parameter created');
        }

        //redirection sur liste persons avec addFlash si anomalie
        $routeBuilder = $this->crudUrlGenerator->build();

        $crudId = '0f6c80b';
        $routeBuilder
            ->setDashboard('App\\Controller\\Admin\\DashboardController')
            ->setController('App\\Controller\\Admin\PersonCrudController')
            ->setAction('index')
            ->setCrudId($crudId);
        return new RedirectResponse($routeBuilder->generateUrl(), 302);

    }
}