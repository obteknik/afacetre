<?php

namespace App\EventSubscriber;

use App\Entity\Adherent;
use App\Entity\Event;
use App\Manager\CalendarParameterManager;
use App\Manager\UserManager;
use App\Repository\AdherentRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class EasyAdminAdherentSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminAdherentSubscriber implements EventSubscriberInterface
{
    /**
     * @var PersonRepository
     */
    private $personRepository;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CrudUrlGenerator
     */
    private $crudUrlGenerator;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var CalendarParameterManager
     */
    private $calendarParameterManager;
    /**
     * @var AdminContextProvider
     */
    private $context;
    /**
     * @var AdherentRepository
     */
    private $adherentRepository;

    /**
     * EasyAdminClientSubscriber constructor.
     * @param PersonRepository $personRepository
     * @param UserManager $userManager
     * @param LoggerInterface $logger
     * @param CrudUrlGenerator $crudUrlGenerator
     * @param SessionInterface $session
     * @param OrganizationRepository $organizationRepository
     * @param EntityManagerInterface $manager
     * @param CalendarParameterManager $calendarParameterManager
     * @param AdminContextProvider $context
     * @param AdherentRepository $adherentRepository
     */
    public function __construct(PersonRepository $personRepository, UserManager $userManager,
                                LoggerInterface $logger, CrudUrlGenerator $crudUrlGenerator,
                                SessionInterface $session, OrganizationRepository $organizationRepository,
                                EntityManagerInterface $manager, CalendarParameterManager $calendarParameterManager,
                                AdminContextProvider $context, AdherentRepository $adherentRepository)
    {
        $this->personRepository = $personRepository;
        $this->userManager = $userManager;
        $this->logger = $logger;
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->session = $session;
        $this->organizationRepository = $organizationRepository;
        $this->manager = $manager;
        $this->calendarParameterManager = $calendarParameterManager;
        $this->context = $context;
        $this->adherentRepository = $adherentRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setCreatedAt'],],
            AfterEntityPersistedEvent::class => [['addAdherent'],],
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Adherent)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    // --- AfterEntityPersistedEvent ---
    public function addAdherent(AfterEntityPersistedEvent $event)
    {
        /** @var Event $event */
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Event)) {
            return;
        }

        if (isset($this->context->getContext()->getRequest()->request->all()['Event']['existingAdherents'])) {
            $existingAdherents = $this->context->getContext()->getRequest()->request->all()['Event']['existingAdherents'];

            $existingAdherents = array_values($existingAdherents);
            $arrIdAdherent = [];
            foreach ($existingAdherents as $adher) {
                foreach ($adher as $a) {
                    $arrIdAdherent[] = $a;
                }
            }


            //Pour chaque id Adherent je crée les enregistrements dans la table de jointure adherent_Event
            foreach ($arrIdAdherent as $idAdherent) {

                $adherent = $this->adherentRepository->find($idAdherent);
                $entity->addAdherent($adherent);

                $this->manager->persist($entity);
            }

            $this->manager->flush();
        }
    }

}