<?php

namespace App\EventSubscriber;

use App\Entity\Discipline;
use App\Entity\Patient;
use App\Entity\PatientSheet;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EasyAdminPatientSheetSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminPatientSheetSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [ ['setCreatedAt'],],
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof PatientSheet)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

}
