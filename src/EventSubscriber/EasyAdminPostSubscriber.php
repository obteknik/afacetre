<?php

namespace App\EventSubscriber;

use App\Entity\Post;
use App\Entity\Product;
use App\Repository\ProductRepository;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminPostSubscriber implements EventSubscriberInterface
{

//    /**
//     * @var ProductRepository
//     */
//    private $productRepository;
//
//    /**
//     * EasyAdminInvoiceSubscriber constructor.
//     * @param ProductRepository $productRepository
//     */
//    public function __construct(ProductRepository $productRepository)
//    {
//        $this->productRepository = $productRepository;
//    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [ ['setCreatedAt'], ['setSlug'],],

        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Post)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setSlug(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();

        if (!($entity instanceof Post)) {
            return;
        }

        $slug =  Urlizer::urlize($entity->getTitle());
        $entity->setSlug($slug);
    }

}
