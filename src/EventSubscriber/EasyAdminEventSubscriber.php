<?php

namespace App\EventSubscriber;

use App\Entity\Event;
use App\Entity\EventScale;
use App\Entity\Patient;
use App\Entity\User;
use App\Repository\EventRepository;
use App\Repository\EventScaleRepository;
use App\Repository\EventTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class EasyAdminEventSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminEventSubscriber implements EventSubscriberInterface
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var AdminContextProvider
     */
    private $adminContext;
    /**
     * @var EventTypeRepository
     */
    private $eventTypeRepository;
    /**
     * @var EventScaleRepository
     */
    private $eventScaleRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * EasyAdminClientSubscriber constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EventRepository $eventRepository
     * @param AdminContextProvider $adminContext
     * @param EventTypeRepository $eventTypeRepository
     * @param EventScaleRepository $eventScaleRepository
     * @param EntityManagerInterface $manager
     */
    public function __construct(TokenStorageInterface $tokenStorage, EventRepository $eventRepository,
                                AdminContextProvider $adminContext, EventTypeRepository $eventTypeRepository,
                                EventScaleRepository $eventScaleRepository, EntityManagerInterface $manager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->eventRepository = $eventRepository;
        $this->adminContext = $adminContext;
        $this->eventTypeRepository = $eventTypeRepository;
        $this->eventScaleRepository = $eventScaleRepository;
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setContract'],
            BeforeEntityUpdatedEvent::class => ['updateContract'],

//            BeforeEntityUpdatedEvent::class => ['updateEventPriceWithScale'],
        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Event)) {
            return;
        }

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $entity->setUser($user);
    }

    public function setContract(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Event)) {
            return;
        }

        if (null !== $entity->getContract()) {
            $contractEvents = $this->eventRepository->findByContract($entity->getContract());

            //récupération de tous les $startAt (nouveau rdv & anciens)
            $dates = [];
            $dates[] = $entity->getStartAt();
            /** @var Event $event */
            foreach ($contractEvents as $event) {
                $dates[] = $event->getStartAt();
            }

            //récupération de la date la + proche & mise à jour contract
            $entity->getContract()->setStartAt(min($dates));
        }
    }

    // --- BeforeEntityUpdatedEvent ---
    public function updateContract(BeforeEntityUpdatedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Event)) {
            return;
        }

        if (null !== $entity->getContract()) {
            $contractEvents = $this->eventRepository->findByContract($entity->getContract());

            //récupération de tous les $startAt (nouveau rdv & anciens)
            $dates = [];
            $dates[] = $entity->getStartAt();
            /** @var Event $event */
            foreach ($contractEvents as $event) {
                $dates[] = $event->getStartAt();
            }

            //récupération de la date la + proche & mise à jour contract
            $entity->getContract()->setStartAt(min($dates));
        }
    }

    // --- AfterEntityPersistedEvent ---

    /**
     * @param BeforeEntityUpdatedEvent $event
     */
//    public function updateEventPriceWithScale(BeforeEntityUpdatedEvent $event)
//    {
//        /** @var Patient $entity */
//        $entity = $event->getEntityInstance();
//        $patientEventType = $this->eventTypeRepository->find(1); //séance de sophrologie/Activité cabinet
//
//        if ($patientEventType) {
//            //uniquement les rdv de séance de sophrologie
////            if (!($entity instanceof Patient && ($entity->getType() !== $patientEventType))) {
//            if (!($entity instanceof Patient)) {
//                return;
//            }
//
//            $patientType = $entity->getType();
//            //
//            $isFirst = $entity->getIsFirst();
//
//            dump($patient);
//            dump($patientType);
//            dump($isFirst);
//            die();
//
//            //recherche du bon barème à appliquer
//            /** @var EventScale $scale */
//            $scale = $this->eventScaleRepository->findBy(['patientType' => $patientType]);
//            $eventPrice = $isFirst ? $scale->getFirstEventPrice() : $scale->getOtherEventPrice();
//
//            //Mise à jour du tarif de la séance
//            $entity->setPrice($eventPrice);
//            $this->manager->persist($entity);
//            $this->manager->flush();
//        }
//    }

}