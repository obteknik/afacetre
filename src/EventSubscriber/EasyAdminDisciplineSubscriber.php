<?php

namespace App\EventSubscriber;

use App\Entity\Discipline;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminDisciplineSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [ ['setCreatedAt'], ['setSlug'],],

        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Discipline)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setSlug(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();

        if (!($entity instanceof Discipline)) {
            return;
        }

        $slug =  Urlizer::urlize($entity->getName());
        $entity->setSlug($slug);
    }

}
