<?php

namespace App\EventSubscriber;

use App\Entity\ProductDetail;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminProductDetailSubscriber implements EventSubscriberInterface
{


    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setCreatedAt'],],

        ];
    }

    // --- BeforeEntityPersistedEvent ---
    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof ProductDetail)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }
}
