<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductDay;
use App\Repository\CategoryRepository;
use App\Repository\OrganizationRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * CREATION DU CATALOGUE DE FORMATION INITIAL
 * Class ProductCommand
 * @package App\Command
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ProductCommand extends Command
{
    /*
     * Création:
     *  - des catégories de produits/formations
     *  - des produits/formations
     */

    protected static $defaultName = 'app:product';
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var AppService
     */
    private $appService;

    /**
     * ProductCommand constructor.
     * @param EntityManagerInterface $manager
     * @param CategoryRepository $categoryRepository
     * @param OrganizationRepository $organizationRepository
     * @param AppService $appService
     */
    public function __construct(EntityManagerInterface $manager, CategoryRepository $categoryRepository,
                                OrganizationRepository $organizationRepository, AppService $appService)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->categoryRepository = $categoryRepository;
        $this->organizationRepository = $organizationRepository;
        $this->appService = $appService;
    }

    protected function configure()
    {
        $this->setDescription('Application products initialization');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $organization = $this->organizationRepository->find(1);

        //CATEGORIES DE FORMATION
        $productCategories = ['Travail', 'Enfance & famille'];
        foreach ($productCategories as $c) {
            $category = new Category();
            $category->setName($c);
            $this->manager->persist($category);
            $io->success('Great ! Category created : ' . $category->getName());
        }
        $this->manager->flush();

        //FORMATIONS
        $nextProductReference = $this->appService->getNextReference($organization, 'product');
        $products = [
            [
                'category' => $this->categoryRepository->find(1),
                'reference' => $nextProductReference,
                'createdAt' => new \DateTime(),
                'name' => 'Renforcer nos relations sociales pour retrouver le PEP\'S au travail',
                'subtitle' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'description' => 'Quisque nisl eros, 
pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. 
Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex, 
in pretium orci vestibulum eget. Class aptent taciti sociosqu ad litora torquent
per conubia nostra, per inceptos himenaeos. Duis pharetra luctus lacus ut 
vestibulum. Maecenas ipsum lacus, lacinia quis posuere ut, pulvinar vitae dolor.',
                'price' => '150000',
                'hourDuration' => 70,
                'maxAdherents' => 12,
                'isBest' => false,
                'isEnabled' => true,
                'isAvailable' => true,

                //FICHE PEDAGOGIQUE
                'audience' => "Professionnels de tous secteurs",
                'effective' => "A définir",
                'goal' => "<span style='color: green;font-weight: bold;'>Identifier ses comportements et utiliser ses ressources, de manière individuelle
                 ou collective pour renforcer l’engagement, la performance,  la satisfaction, liée au travail
                  et le bien être en Entreprise</span>.
                  <br>
                    <ul style='color: #800080;font-weight: bold;'>
                      <li>Apprendre de soi, pour  laisser place à l’autre dans la relation</li>
                      <li>Favoriser l'engagement par le développement des compétences.</li>
                      <li>Reconnaître l'implication de chacun et donner du sens.</li>
                    </ul>                  
                  ",
                'program' => "
                <ul>
                    <li>Définir et comprendre l’importance du travail d’équipe, en s’appuyant sur la force collective issue de personnalités toutes différentes</li>
                    <li>Découvrir de nouvelles ressources pour développer d’autres compétences collectives et individuelles au travail</li>
                    <li>Comprendre et nourrir ses relations sociales au travail</li>
                    <li>Avoir une réflexion sur les bénéfices d’amener les émotions positives au travail</li>
                    <li>Donner Sens, Faire Sens avec son travail. Est-ce la clef d’un épanouissement garanti ?</li>
                    <li>Développer un état d’esprit de changement ? D’adaptation ?</li>
                </ul>
                <br>
                <span style='font-size: 12px;color: dodgerblue'>+ 15 jeux d'intelligence émotionnelle</span>
                ",
                'method' => "
                <ul>
                    <li>Alternance d’apports théoriques et pratiques.</li>
                    <li>Travaux de réflexion en sous-groupes.</li>
                    <li>Expérimenter des jeux d’intelligence émotionnelle. Application de concepts et d’exercices issues de la psychologie positive au travail</li>
                    <li>A partir des observations de tous, élaboration de nouvelles pistes de travail. </li>
                    <br>
                    <li>Mises en situation à travers de cas concrets.</li>
                </ul>
                ",
                'means' => "- Support écrit, diaporama, vidéo, mise en situation",
                'evaluation' => "- Dispositif d’évaluation avec un questionnaire",
                'validation' => "- Attestation de formation",
                'duration' => "- 2 jours (journée de 7hres)",
                'planification' => "A définir sur plusieurs jours non consécutifs",
                'location' => "- Support écrit, diaporama, vidéo, mise en situation",
            ]
        ];

        foreach ($products as $p) {
            $product = new Product();
            $product->setOrganization($this->organizationRepository->find(1));
            $product->setCategory($p['category']);
            $product->setReference($p['reference']);
            $product->setCreatedAt(new \DateTime());
            $product->setName($p['name']);
            $product->setSlug(Urlizer::urlize($product->getName()));
            $product->setSubtitle($p['subtitle']);
            $product->setDescription($p['description']);
            $product->setPrice($p['price']);
            $product->setIsPriceShow(false);
            $product->setIsAvailable($p['isAvailable']);
            $product->setIsBest($p['isBest']);
            $product->setIsEnabled($p['isEnabled']);
            $product->setHourDuration($p['hourDuration']);
            $product->setMaxAdherents($p['maxAdherents']);
            //fiche pédagogique
            $product->setAudience($p['audience'])
                ->setEffective($p['effective'])
                ->setGoal($p['goal'])
                ->setProgram($p['program'])
                ->setMethod($p['method'])
                ->setMeans($p['means'])
                ->setEvaluation($p['evaluation'])
                ->setValidation($p['validation'])
                ->setDuration($p['duration'])
                ->setPlanification($p['planification'])
                ->setLocation($p['location']);

            $this->manager->persist($product);

            //JOURS DE FORMATION
            $productDays = [
                //JOUR 1
                [
                    'goal' => "
                <ul>
                    <li>- Expliquer les bénéfices de la Psychologie positive appliquée au travail. </li>
                    <li>- Renforcer les relations sociales au travail</li>
                    <li>- Utiliser et comprendre les bénéfices, d’appliquer  les émotions positives au travail</li>
                </ul>",
                    'content' => "- Définir la Psychologie positive et le bonheur au travail",
                    'methodMeans' => "
                <ul>
                    <li>- Apports d’informations théoriques et scientifiques</li>
                    <li>- Echanges et croisements de vécus expérientiels</li>
                    <li>- Mise en pratiques dynamiques de nombreux exercices issue de l’intelligence émotionnelle pour ancrer le vécu de la formation</li>
                    <li>- Manipulation de jeux de cartes et d’outils de Psychologie positive</li>
                    <li>-Support du stagiaire remis au participant.</li>
                </ul>",
                    'program' => "
                <ul>
                    <li style='font-weight: bold;'>9h00 :      Accueil</li>
                    <li>
                        9h15 :      Présentation guidée par le jeu (tour de table : identité su stagiaire,  parcours professionnel, attentes quant aux objectifs de la formation)
                        <br>
                        Présentation du formateur
                    </li>
                    <li style='font-weight: bold;'>10h15 :     Pause</li>
                    <li>
                    10h30 :     Définir la Psychologie Positive appliquée au travail <br>
                        Qu’est ce que le bonheur pour nous ? <br>
                            - Travail en sous groupe, réfléchir sur ce qu’est le bonheur ? Y a-t-il des freins au bonheur ? Trouver ensemble une définition commune <br>
                        - Restitution du travail en sous-groupe au tableau <br>
                        Qu’est ce qui contribue à mon épanouissement professionnel ?
                    </li>
                    <li style='font-weight: bold;'>12h30 :     Repas</li>
                    <li>13h30 :     Pourquoi amener les émotions positives au travail ? <br>
	                    Réflexions et mise en applications d’exercices par binome et en groupe</li>
	                <li style='font-weight: bold;'>15h :       Pause</li>
	                <li>15h15 :     Nourrir ses connexions sociales – Pratiques de jeux d’intelligence émotionnelle</li>
	                <li style='font-weight: bold;'>16h30 :     Bilan de la journée</li>
                </ul>",
                ],
                //JOUR 2
                [
                    'goal' => "
                    <ul>
                        <li>Faire et donner Sens à son travail</li>
                        <li>Evaluer les leviers de ma motivation</li>
                        <li>Développer un état d’esprit de changement</li>
                        <li>Dégager de nouveaux axes concrets d’engagement au travail</li>
                    </ul>",
                    'content' => "
                    <ul>
                        <li>Chercher du Sens au travail</li>
                        <li>Evaluer son niveau d’engagement au travail</li>
                        <li>C’est quoi , être épanoui au travail ?</li>
                        <li>Définir concrètement de nouveaux objectifs de travail. Passer de la réflexion à l’action</li>
                    </ul>",
                    'methodMeans' => "
                    <ul>
                        <li>Apports d’informations théoriques et scientifiques</li>
                        <li>Echanges et croisements de vécus expérientiels</li>
                        <li>Mise en pratiques dynamiques de nombreux exercices issue de l’intelligence émotionnelle pour ancrer le vécu de la formation</li>
                        <li>Manipulation de jeux de cartes et d’outils de Psychologie positive</li>
                        <li>Support du stagiaire remis au participant.</li>
                    </ul>",
                    'program' => "
                    <ul>
                        <li style='font-weight: bold;'>9h00 :      Accueil</li>
                        <li>9h15 :      Présentation de la journée</li>
                        <li>9h30 :      Comment  activer les leviers pour à nouveau être dans l’action positive ?</li>
                        <li style='font-weight: bold;'>10h30 :     Pause</li>
                        <li>10h45 :     Donner du Sens au travail pour retrouver le PEP’S (Plaisir, Engagement, Performance et Sens)</li>
                        <li style='font-weight: bold;'>12h30 :     Repas</li>
                        <li>13h30 :     Atelier - Evaluer son niveau d’engagement au travail maintenant</li>
                        <li style='font-weight: bold;'>15h00 :     Pause</li>
                        <li>15h15 :     Se donner de nouveaux objectifs à mettre en place dès demain, en tenant compte de ses valeurs, besoins.</li>
                        <li style='font-weight: bold;'>16h15 :     Bilan de la formation - 		Evaluation de la formation</li>
                    </ul>",
                ],
            ];
            foreach ($productDays as $pd) {
                $productDay = new ProductDay();
                $nextProductDayReference = $this->appService->getNextReference($organization,
                    'productDay',$product);
                $productDay->setCreatedAt(new \DateTime())
                    ->setProduct($product)
                    ->setReference($nextProductDayReference)
                    ->setGoal($pd['goal'])
                    ->setContent($pd['content'])
                    ->setMethodMeans($pd['methodMeans'])
                    ->setProgram($pd['program']);
                $product->addProductDay($productDay);
                $this->manager->persist($productDay);
                $this->manager->flush();
            }

            $io->success('Great ! Product created : ' . $product->getName());
        }

        $this->manager->flush();
        return Command::SUCCESS;
    }
}
