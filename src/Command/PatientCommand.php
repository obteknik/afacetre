<?php

namespace App\Command;

use App\Entity\Invoice;
use App\Entity\Patient;
use App\Manager\EventManager;
use App\Manager\InvoiceManager;
use App\Manager\PatientManager;
use App\Repository\EventTypeRepository;
use App\Repository\FamilySituationRepository;
use App\Repository\FiscalYearRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PatientTypeRepository;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * CREATION DE LA PATIENTELE
 * Class PatientCommand
 * @package App\Command
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PatientCommand extends Command
{
    /*
     * Création:
     *  - de la base des patients
     *  - historique d'activité cabinet
     */

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var PatientManager
     */
    private $patientManager;
    /**
     * @var PatientTypeRepository
     */
    private $patientTypeRepository;
    /**
     * @var FamilySituationRepository
     */
    private $familySituationRepository;
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var FiscalYearRepository
     */
    private $fiscalYearRepository;
    /**
     * @var PersonRepository
     */
    private $personRepository;
    /**
     * @var EventTypeRepository
     */
    private $eventTypeRepository;
    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    /**
     * PatientCommand constructor.
     * @param EntityManagerInterface $manager
     * @param OrganizationRepository $organizationRepository
     * @param PatientManager $patientManager
     * @param PatientTypeRepository $patientTypeRepository
     * @param FamilySituationRepository $familySituationRepository
     * @param EventManager $eventManager
     * @param FiscalYearRepository $fiscalYearRepository
     * @param PersonRepository $personRepository
     * @param EventTypeRepository $eventTypeRepository
     * @param InvoiceManager $invoiceManager
     */
    public function __construct(EntityManagerInterface $manager, OrganizationRepository $organizationRepository,
                                PatientManager $patientManager, PatientTypeRepository $patientTypeRepository,
                                FamilySituationRepository $familySituationRepository, EventManager $eventManager,
                                FiscalYearRepository $fiscalYearRepository, PersonRepository $personRepository,
                                EventTypeRepository $eventTypeRepository, InvoiceManager $invoiceManager)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->organizationRepository = $organizationRepository;
        $this->patientManager = $patientManager;
        $this->patientTypeRepository = $patientTypeRepository;
        $this->familySituationRepository = $familySituationRepository;
        $this->eventManager = $eventManager;
        $this->fiscalYearRepository = $fiscalYearRepository;
        $this->personRepository = $personRepository;
        $this->eventTypeRepository = $eventTypeRepository;
        $this->invoiceManager = $invoiceManager;
    }

    protected static $defaultName = 'app:patient';

    protected function configure()
    {
        $this->setDescription('Application patients initialization');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /**** CREATION DE 5 PATIENTS PILOTE ****/
        $organization = $this->organizationRepository->find(1);
        $patients = [
            [
                'civility' => Patient::CIVILITY_MISTER,
                'firstname' => 'Pierre',
                'lastname' => 'Brunoy',
                'street' => 'Rue Albert Thierry',
                'zipcode' => '45200',
                'city' => 'Montargis',
                'country' => 'FR',
                'phoneNumber' => '0789451245',
                'email' => 'pierre.brunoy@yopmail.com',
                'birthdate' => '19/09/1969',
            ],
            [
                'civility' => Patient::CIVILITY_MISS,
                'firstname' => 'Isabelle',
                'lastname' => 'Andréni',
                'street' => 'Rue des Archers',
                'zipcode' => '45200',
                'city' => 'Montargis',
                'country' => 'FR',
                'phoneNumber' => '0656897845',
                'email' => 'isabelle.andreni@yopmail.com',
                'birthdate' => '10/08/1980',
            ],
            [
                'civility' => Patient::CIVILITY_MISS,
                'firstname' => 'Virginie',
                'lastname' => 'Thauvin',
                'street' => 'Rue des Murs',
                'zipcode' => '45200',
                'city' => 'Montargis',
                'country' => 'FR',
                'phoneNumber' => '0701020445',
                'email' => 'virginie.thauvin@yopmail.com',
                'birthdate' => '02/09/1952',
            ],
            [
                'civility' => Patient::CIVILITY_MISTER,
                'firstname' => 'Thomas',
                'lastname' => 'Serre',
                'street' => 'Rue Marcel Sembat',
                'zipcode' => '45200',
                'city' => 'Montargis',
                'country' => 'FR',
                'phoneNumber' => '0766559988',
                'email' => 'thomas.serre@yopmail.com',
                'birthdate' => '01/01/1990',
            ],
            [
                'civility' => Patient::CIVILITY_MISTER,
                'firstname' => 'Jean',
                'lastname' => 'Diret',
                'street' => 'Rue Molière',
                'zipcode' => '45200',
                'city' => 'Montargis',
                'country' => 'FR',
                'phoneNumber' => '0766557845',
                'email' => 'emma.diret@yopmail.com',
                'birthdate' => '13/12/1996'
            ],
        ];
        $i = 0;

        foreach ($patients as $p) {
            $birthdate = \DateTime::createFromFormat('d/m/Y', $p['birthdate']);

            /** @var Patient|null $patient */
            $patient = $this->patientManager->create(
                $organization, $this->patientTypeRepository->find(mt_rand(1, 3)), $p['civility'],
                $p['firstname'], $p['lastname'], $p['street'], $p['zipcode'], $p['city'], $p['country'],
                $p['phoneNumber'], $p['email'], $birthdate, $this->familySituationRepository->find(mt_rand(1, 3)));

            //Pour chaque patient, création de 5 séances de sophrologie dans les 5 semaines à venir
            //entre le lundi et le samedi de chaque semaine
            $date = date_create('now')->format('Y-m-d');
            $l1 = date_create($date . 'Next Monday');
            $v1 = clone $l1;
            $j = 4;
            for ($i = 0; $i < 5; $i++) {
                $v1->modify("+" . $j . " days");

                //A - 1 date aléatoire entre l1 et v1
                $l1_timestamp = $l1->getTimestamp();
                $v1_timestamp = $v1->getTimestamp();
                $randTimestamp = mt_rand($l1_timestamp, $v1_timestamp);
                $randDate = date('Y-m-d H:i:s', $randTimestamp);
                $randDateTime = new \DateTime($randDate);

                //B - une fois la date récupérée, choix aléatoire d'une heure de début entre 9 et 17h
                $startAtDateTime = $randDateTime;
                $endAtDateTime = $randDateTime;

                $dayHours = [];
                //todo: préremplir $dayHours des redv déjà connus
                $hour = $this->getAvailableHour($dayHours);
                $startAtDateTime->setTime($hour, 0, 0, 0);
                $endAtDateTime->setTime($hour + 1, 0, 0, 0);

                $title = 'Rdv' . $i;

                //CREATION DU RDV
                $event = $this->eventManager->create(
                    $organization, $this->fiscalYearRepository->find(3),
                    $this->personRepository->find(1), $this->eventTypeRepository->find(1),
                    $startAtDateTime, $endAtDateTime,
                    $title, null, $patient['item'], null);

                $io->success('Great ! Patient ' . $patient["item"]->getFirstname() . ' ' . $patient["item"]->getLastname() .
                    ': created : RDV added => Le ' . $event['item']->getStartAt()->format('d/m/Y'));
            }

            $io->success('Great ! Patient created : ' . 'Nom: ' . $patient["item"]->getFirstname() . ' ' . $patient["item"]->getLastname());

            $j = $j + 7;
        } /*fin boucle */

        return Command::SUCCESS;
    }

    public function getAvailableHour(array $dayHours)
    {
        $hour = rand(9, 16);
        if (!in_array($hour, $dayHours)) {
            return $hour;
        }

        $dayHours[] = $hour;
        $this->getAvailableHour($dayHours);
    }
}
