<?php

namespace App\Command;

use App\Entity\Adherent;
use App\Entity\CalendarParameter;
use App\Entity\Category;
use App\Entity\ChargeType;
use App\Entity\Client;
use App\Entity\ClientContact;
use App\Entity\ClientContactFunction;
use App\Entity\Contract;
use App\Entity\Discipline;
use App\Entity\DocumentType;
use App\Entity\EventLocation;
use App\Entity\EventType;
use App\Entity\FamilySituation;
use App\Entity\FiscalYear;
use App\Entity\Organization;
use App\Entity\Page;
use App\Entity\PatientType;
use App\Entity\PaymentCondition;
use App\Entity\PaymentType;
use App\Entity\Product;
use App\Entity\Quotation;
use App\Entity\User;
use App\Manager\CalendarParameterManager;
use App\Manager\OrganizationManager;
use App\Manager\UserManager;
use App\Repository\AdherentRepository;
use App\Repository\ClientContactFunctionRepository;
use App\Repository\DisciplineRepository;
use App\Repository\DocumentRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PageRepository;
use App\Repository\PersonRepository;
use App\Repository\ProductRepository;
use App\Repository\QuotationRepository;
use App\Repository\UserRepository;
use App\Services\AppService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\managerInterface;
use App\Repository\CategoryRepository;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * CREATION DU CONTRAT PILOTE "ADAPAGE"
 * Class ContractCommand
 * @package App\Command
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ContractCommand extends Command
{
    /*
     * Création:
     *  - du client ADAPAGE
     *  - du contrat de formation:
     *      -> les informations de base
     *      -> les adhérents
     *      -> les documents
     *      -> les rendez-vous/séances de formation
     *
     */

    protected static $defaultName = 'app:contract';
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var ClientContactFunctionRepository
     */
    private $clientContactFunctionRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var AdherentRepository
     */
    private $adherentRepository;
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var AppService
     */
    private $appService;
    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * ContractCommand constructor.
     * @param EntityManagerInterface $manager
     * @param OrganizationRepository $organizationRepository
     * @param ClientContactFunctionRepository $clientContactFunctionRepository
     * @param ProductRepository $productRepository
     * @param AdherentRepository $adherentRepository
     * @param DocumentRepository $documentRepository
     * @param AppService $appService
     * @param PersonRepository $personRepository
     */
    public function __construct(EntityManagerInterface $manager, OrganizationRepository $organizationRepository,
                                ClientContactFunctionRepository $clientContactFunctionRepository, ProductRepository $productRepository,
                                AdherentRepository $adherentRepository, DocumentRepository $documentRepository,
                                AppService $appService, PersonRepository $personRepository)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->organizationRepository = $organizationRepository;
        $this->clientContactFunctionRepository = $clientContactFunctionRepository;
        $this->productRepository = $productRepository;
        $this->adherentRepository = $adherentRepository;
        $this->documentRepository = $documentRepository;
        $this->appService = $appService;
        $this->personRepository = $personRepository;
    }

    protected function configure()
    {
        $this->setDescription('Application contract ADAPAGE initialization');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //=>CLIENT PILOTE
        $pilotClient = new Client();
        $pilotClient->setCreatedAt(new \DateTime())
            ->setOrganization($this->organizationRepository->find(1))
            ->setReference('C000001')
            ->setCompanyName('ADAPAGE')
            ->setStreet('12 rue des Tulipes')
            ->setZipcode('45200')
            ->setCity('Montargis')
            ->setCountry('FR')
            ->setPhoneNumber('0781340644')
            ->setEmail('contact@adapage.fr');

        $this->manager->persist($pilotClient);
        $this->manager->flush();
        $io->success('Great ! Pilot Client created : ' . $pilotClient->getReference() . ' - ' . $pilotClient->getCompanyName());

        //=>CONTACT CLIENT PILOTE
        $function = $this->clientContactFunctionRepository->find(1);
        $pilotContactClient = new ClientContact();
        $pilotContactClient->setCreatedAt(new \DateTime())
            ->setClient($pilotClient)
            ->setFirstname('Marc')
            ->setLastname('Poirier')
            ->setPhoneNumber('0652535254')
            ->setEmail('contact1@adapage.fr')
            ->setFunction($function);

        $this->manager->persist($pilotContactClient);
        $this->manager->flush();
        $io->success('Great ! Pilot Contact Client created : ' . $pilotContactClient->getFirstname() . ' ' . $pilotContactClient->getLastname());

        //=>ADHERENTS PILOTE
        $pilotAdherents = [
            ['firstname' => 'Isabelle', 'lastname' => 'Normand', 'phoneNumber' => '0689457812', 'address' => '6 bd Berthelot 45200 Montargis',],
            ['firstname' => 'Pierre', 'lastname' => 'Bertier', 'phoneNumber' => '0678454512', 'address' => '178 bd Pivert 45120 Villemandeur',],
            ['firstname' => 'Virginie', 'lastname' => 'Bléau', 'phoneNumber' => '0789458978', 'address' => 'Route de Dordives 45200 Montargis',],
        ];
        foreach ($pilotAdherents as $pa) {
            $pilotAdherent = new Adherent();
            $pilotAdherent->setCreatedAt(new \DateTime());
            $pilotAdherent->setFirstname($pa['firstname']);
            $pilotAdherent->setLastname($pa['lastname']);
            $pilotAdherent->setPhoneNumber($pa['phoneNumber']);
            $pilotAdherent->setAddress($pa['address']);
            $pilotAdherent->setIsActive(true);

            $this->manager->persist($pilotClient);

            $io->success('Great ! Pilot Adherent created : ' . $pilotAdherent->getFirstname() . ' ' . $pilotAdherent->getLastname());
        }
        $this->manager->flush();

        //=>CONTRAT PILOTE
        $pilotProduct = $this->productRepository->find(1);
        $startAt = new \DateTime();
        $endAt = clone $startAt;
        $endAt->modify('+90days');

        $contract = new Contract();
        //ajout au contrat des différents adhérents pilote
        $organization = $this->organizationRepository->find(1);
        $nextContractReference = $this->appService->getNextReference($organization, 'contract');
        $person = $this->personRepository->find(1);
        $contract->setOrganization($organization)
            ->setCreatedAt(new \DateTime())
            ->setReference($nextContractReference)
            ->setClient($pilotClient)
            ->addProduct($pilotProduct)
            ->setStartAt($startAt)
            ->setEndAt($endAt)
            ->setIsFaceToFace(true)
            ->setAddress("12 rue des Pivoines 45200 MONTARGIS")
            ->addPerson($person);
        $ad = $this->adherentRepository->findAll();
        foreach ($ad as $a) {
            $contract->addAdherent($a);
        }

        // *** DOCUMENTS ****
        $documents = $this->documentRepository->findAll();
        foreach ($documents as $d) {
            $contract->addDocument($d);
        }

        // *** EVENTS ****
        $events = [
            [],
            [],
            [],
        ];

        /*
         create(Organization $organization, FiscalYear $fiscalYear,
                           Person $person,
                           EventType $eventType,
                           DateTime $startAt, DateTime $endAt,
                           string $title, ?string $comment,
                           Patient $patient,
                           bool $isFirst = false,
                           array $adherents = [],
                           bool $isAllDay = false,
                           bool $isPeriodic = false,
                           bool $isConfirmedByEmail = false)
         */

        //TODO:...


        $this->manager->persist($contract);
        $io->success('Great ! Pilot Contract created : ' . $contract->getReference());

        $this->manager->flush();
        return Command::SUCCESS;
    }
}
