<?php

namespace App\Command;

use App\Entity\Adherent;
use App\Entity\BusinessType;
use App\Entity\CalendarParameter;
use App\Entity\Category;
use App\Entity\ChargeType;
use App\Entity\Client;
use App\Entity\ClientContact;
use App\Entity\ClientContactFunction;
use App\Entity\Contract;
use App\Entity\Discipline;
use App\Entity\DocumentType;
use App\Entity\EventLocation;
use App\Entity\EventScale;
use App\Entity\EventType;
use App\Entity\FamilySituation;
use App\Entity\FiscalYear;
use App\Entity\LegalForm;
use App\Entity\Organization;
use App\Entity\OrganizationActivity;
use App\Entity\Page;
use App\Entity\PatientType;
use App\Entity\PaymentCondition;
use App\Entity\PaymentType;
use App\Entity\Person;
use App\Entity\Product;
use App\Entity\Quotation;
use App\Entity\User;
use App\Entity\Workflow;
use App\Manager\CalendarParameterManager;
use App\Manager\OrganizationManager;
use App\Manager\UserManager;
use App\Repository\AdherentRepository;
use App\Repository\BusinessTypeRepository;
use App\Repository\ClientContactFunctionRepository;
use App\Repository\DisciplineRepository;
use App\Repository\DocumentRepository;
use App\Repository\LegalFormRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PageRepository;
use App\Repository\PatientTypeRepository;
use App\Repository\ProductRepository;
use App\Repository\QuotationRepository;
use App\Repository\UserRepository;
use App\Repository\WorkflowRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryRepository;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * CREATION DES DATA D'INITIALISATION DE LA PLATEFORME
 * Class InitCommand
 * @package App\Command
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class InitCommand extends Command
{
    /*
     * Création de :
     * - l'organisation propriétaire de la plateforme
     * - du compte utilisateur ADMIN admin@afac-etre.fr//test
     * - des paramètres :
     *      -> les workflows : cabinet/contrat/atelier
     *      -> les types de business
     *      -> les formes juridiques
     *      -> l'intervenante Gwénaëlle Brassart (avec son compte utilisateur)
     *      -> les disciplines
     *      -> les types de rendez-vous
     *      -> les lieux de rendez-vous
     *      -> les types de patient
     *      -> les fonctions de contact client
     *      -> les situations familiales
     *      -> les exercices ficaux
     *      -> les conditions de paiement
     *      -> les types de documents
     *      -> les types de charge
     *      -> les types de patient
     *      -> les pages
     *      -> les citations
     *      -> le barème de tarfis patient
     */

    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrganizationManager
     */
    private $organizationManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DisciplineRepository
     */
    private $disciplineRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var CalendarParameterManager
     */
    private $calendarParameterManager;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var AdherentRepository
     */
    private $adherentRepository;
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var ClientContactFunctionRepository
     */
    private $clientContactFunctionRepository;
    /**
     * @var QuotationRepository
     */
    private $quotationRepository;
    /**
     * @var PatientTypeRepository
     */
    private $patientTypeRepository;
    /**
     * @var WorkflowRepository
     */
    private $workflowRepository;
    /**
     * @var BusinessTypeRepository
     */
    private $businessTypeRepository;
    /**
     * @var LegalFormRepository
     */
    private $legalFormRepository;

    /**
     * CreateAdminUserCommand constructor.
     * @param UserRepository $userRepository
     * @param UserManager $userManager
     * @param OrganizationManager $organizationManager
     * @param EntityManagerInterface $entityManager
     * @param DisciplineRepository $disciplineRepository
     * @param PageRepository $pageRepository
     * @param CalendarParameterManager $calendarParameterManager
     * @param CategoryRepository $categoryRepository
     * @param OrganizationRepository $organizationRepository
     * @param ProductRepository $productRepository
     * @param AdherentRepository $adherentRepository
     * @param DocumentRepository $documentRepository
     * @param ClientContactFunctionRepository $clientContactFunctionRepository
     * @param QuotationRepository $quotationRepository
     * @param PatientTypeRepository $patientTypeRepository
     * @param WorkflowRepository $workflowRepository
     * @param BusinessTypeRepository $businessTypeRepository
     * @param LegalFormRepository $legalFormRepository
     */
    public function __construct(UserRepository $userRepository, UserManager $userManager,
                                OrganizationManager $organizationManager, EntityManagerInterface $entityManager,
                                DisciplineRepository $disciplineRepository, PageRepository $pageRepository,
                                CalendarParameterManager $calendarParameterManager, CategoryRepository $categoryRepository,
                                OrganizationRepository $organizationRepository, ProductRepository $productRepository,
                                AdherentRepository $adherentRepository, DocumentRepository $documentRepository,
                                ClientContactFunctionRepository $clientContactFunctionRepository,
                                QuotationRepository $quotationRepository, PatientTypeRepository $patientTypeRepository,
                                WorkflowRepository $workflowRepository, BusinessTypeRepository $businessTypeRepository,
                                LegalFormRepository $legalFormRepository)
    {
        parent::__construct();
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
        $this->organizationManager = $organizationManager;
        $this->entityManager = $entityManager;
        $this->disciplineRepository = $disciplineRepository;
        $this->pageRepository = $pageRepository;
        $this->calendarParameterManager = $calendarParameterManager;
        $this->categoryRepository = $categoryRepository;
        $this->organizationRepository = $organizationRepository;
        $this->productRepository = $productRepository;
        $this->adherentRepository = $adherentRepository;
        $this->documentRepository = $documentRepository;
        $this->clientContactFunctionRepository = $clientContactFunctionRepository;
        $this->quotationRepository = $quotationRepository;
        $this->patientTypeRepository = $patientTypeRepository;
        $this->workflowRepository = $workflowRepository;
        $this->businessTypeRepository = $businessTypeRepository;
        $this->legalFormRepository = $legalFormRepository;
    }


    protected static $defaultName = 'app:init';

    protected function configure()
    {
        $this->setDescription('Application initialization');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // ***** WORKFLOWS *****
        $workflows = [
            ['name' => 'Parcours Cabinet', 'eventBgColor' => "#000000", 'eventFtColor' => "#FFFFFF", 'pdfFont' => "Open Sans Condensed"],
            ['name' => 'Parcours Contrat', 'eventBgColor' => "#800080", 'eventFtColor' => "#FFFF00", 'pdfFont' => "Arial"],
            ['name' => 'Parcours Atelier', 'eventBgColor' => "#FFFF00", 'eventFtColor' => "#808000", 'pdfFont' => "Verdana"],
        ];

        foreach ($workflows as $w) {
            $workflow = new Workflow();
            $workflow->setCreatedAt(new \DateTime());
            $workflow->setName($w['name']);
            $workflow->setEventBgColor($w['eventBgColor']);
            $workflow->setEventFtColor($w['eventFtColor']);
            $workflow->setPdfFont($w['pdfFont']);
            $this->entityManager->persist($workflow);
            $this->entityManager->flush();
            $io->success('Great ! Organization workflow created: ' . $workflow->getName());
        }

        // ***** BUSINESS TYPES *****
        $businessTypes = [
            ['type' => 'Vente de biens'],
            ['type' => 'Vente de services'],
            ['type' => 'Vente de biens et services'],
        ];

        foreach ($businessTypes as $bt) {
            $businessType = new BusinessType();
            $businessType->setType($bt['type']);
            $this->entityManager->persist($businessType);
            $this->entityManager->flush();
            $io->success('Great ! Business type created: ' . $businessType->getType());
        }

        //FORMES JURIDIQUES
        $legalForms = ['Auto-entrepreneur', 'Entreprise individuelle', 'EURL'];

        foreach ($legalForms as $lf) {
            $legalForm = new LegalForm();
            $legalForm->setName($lf);

            $this->entityManager->persist($legalForm);
            $this->entityManager->flush();
            $io->success('Great ! Legal form created : ' . 'Nom: ' . $legalForm->getName());
        }

        // ***** SUPER ADMIN USER ACCOUNT *****
        //je vérifie qu'un user ne possède pas déjà l'email 'superadmin@afac-etre.fr'
        $emailSuperAdmin = 'superadmin@afac-etre.fr';
        $superAdminUser = $this->userRepository->findOneBy(['email' => $emailSuperAdmin]);
        if ($superAdminUser) {
            $io->warning("L'email " . $emailSuperAdmin . " existe déjà en base.");
        } else {
            $superAdminUser = new User();
            $resultManagerUser = $this->userManager->create(null,
                $superAdminUser, 'Olivier', 'BRASSART', $emailSuperAdmin,
                'test', ['ROLE_SUPER_ADMIN'], true, true, new \DateTime());

            if (isset($resultManagerUser["errorMessage"]) && $resultManagerUser["errorMessage"] instanceof Exception) {
                $io->warning($resultManagerUser["errorMessage"]);

            } elseif (isset($resultManagerUser["item"]) && $resultManagerUser["item"] instanceof User) {
                $io->success('Great ! SUPER ADMIN USER ACCOUNT created : ' .
                    'Prénom: ' . $superAdminUser->getFirstname() . ' - ' .
                    'Nom :' . $superAdminUser->getLastname() . ' - ' .
                    'Email :' . $superAdminUser->getEmail());

                //Création du paramétrage par défaut calendar
                $resultManagerCalParameter = $this->calendarParameterManager->create($resultManagerUser["item"],
                    new \DateTime('2021-01-01'),
                    new \DateTime('2021-12-31'),
                    1,
                    new \DateTime('0000-00-00 08:00:00'),
                    new \DateTime('0000-00-00 17:00:00'),
                    true
                );

                if (isset($resultManagerCalParameter["errorMessage"]) && $resultManagerCalParameter["errorMessage"] instanceof Exception) {
                    $io->warning($resultManagerCalParameter["errorMessage"]);

                } elseif (isset($resultManagerCalParameter["item"]) && $resultManagerCalParameter["item"] instanceof CalendarParameter) {
                    $io->success('Great ! Calendar parameter created');
                }
            }
        }

        // ***** ORGANIZATION AFAC CREATION *****
        $officeWorkflow = $this->workflowRepository->find(1);
        $businessType = $this->businessTypeRepository->find(2);
        $legalForm = $this->legalFormRepository->find(1);

        $resultManagerOrganization = $this->organizationManager->create(
            "L'A.F.A.C de l'être",
            "Gwéanëlle BRASSART",
            "07.81.34.06.44",
            "contact@afac-etre.fr",
            "109 rue de la Louvetière",
            "77570", "CHATEAU-LANDON",
            "99999999900032",
            $businessType, $legalForm,
            false,
            0,
            [$officeWorkflow],
            'obteknik2007@gmail.com',
            'brassart_olivier@yahoo.fr');

        if (isset($resultManagerOrganization["errorMessage"]) && $resultManagerOrganization["errorMessage"] instanceof Exception) {
            $io->warning($resultManagerOrganization["errorMessage"]);

        } elseif (isset($resultManagerOrganization["item"]) && $resultManagerOrganization["item"] instanceof Organization) {

            $io->success('Great ! Organization created : ' . 'Nom: ' . $resultManagerOrganization["item"]->getName());
        }

        // ***** ADMIN USER CREATION *****
        //je vérifie qu'un user ne possède pas déjà l'email 'admin@afac-etre.fr'
        $emailAdmin = 'admin@afac-etre.fr';
        $adminUser = $this->userRepository->findOneBy(['email' => $emailAdmin]);
        if ($adminUser) {
            $io->warning("L'email " . $emailAdmin . " existe déjà en base.");
        } else {
            $adminUser = new User();
            $resultManagerUser = $this->userManager->create($resultManagerOrganization["item"],
                $adminUser, 'Gwénaëlle', 'BRASSART', $emailAdmin,
                'test', ['ROLE_ADMIN'], true, true, new \DateTime());

            if (isset($resultManagerUser["errorMessage"]) && $resultManagerUser["errorMessage"] instanceof Exception) {
                $io->warning($resultManagerUser["errorMessage"]);

            } elseif (isset($resultManagerUser["item"]) && $resultManagerUser["item"] instanceof User) {
                $io->success('Great ! ADMIN USER ACCOUNT created : ' .
                    'Prénom: ' . $adminUser->getFirstname() . ' - ' .
                    'Nom :' . $adminUser->getLastname() . ' - ' .
                    'Email :' . $adminUser->getEmail());

                //Création du paramétrage par défaut calendar
                $resultManagerCalParameter = $this->calendarParameterManager->create($resultManagerUser["item"],
                    new \DateTime('2021-01-01'),
                    new \DateTime('2021-12-31'),
                    1,
                    new \DateTime('0000-00-00 08:00:00'),
                    new \DateTime('0000-00-00 17:00:00'),
                    true
                );

                if (isset($resultManagerCalParameter["errorMessage"]) && $resultManagerCalParameter["errorMessage"] instanceof Exception) {
                    $io->warning($resultManagerCalParameter["errorMessage"]);

                } elseif (isset($resultManagerCalParameter["item"]) && $resultManagerCalParameter["item"] instanceof CalendarParameter) {
                    $io->success('Great ! Calendar parameter created');
                }
            }
        }

        // ***** ORGANIZATION ACTIVITIES *****
        $organizationActivities = [
            [
                'name' => "Accompagnement",
                'shortDescription' => "A définir",
                'description' => "A définir",
            ],
            [
                'name' => "Formation",
                'shortDescription' => "A définir",
                'description' => "A définir",
            ],
            [
                'name' => "Animation",
                'shortDescription' => "A définir",
                'description' => "A définir",
            ],
            [
                'name' => "Coordination",
                'shortDescription' => "A définir",
                'description' => "A définir",
            ],
        ];
        foreach ($organizationActivities as $oa) {
            $organizationActivity = new OrganizationActivity();
            $organizationActivity->setCreatedAt(new \DateTime());
            $organizationActivity->setUpdatedAt(new \DateTime());
            $organizationActivity->setName($oa["name"]);
            $organizationActivity->setShortDescription($oa["shortDescription"]);
            $organizationActivity->setDescription($oa["description"]);
            $organizationActivity->setOrganization($resultManagerOrganization["item"]);
            $this->entityManager->persist($organizationActivity);
            $io->success('Great ! Organization activity created: ' . $organizationActivity->getName());
        }

        // ***** ORGANIZATION PARAMETERS CREATION *****
        //PERSON
        $person = new Person();
        $person->setCreatedAt(new \DateTime())
            ->setIsActive(true)
            ->setFirstname('Gwénaëlle')
            ->setLastname('Brassart')
            ->setEmail('gb.sophrologue@yopmail.com')
            ->setPhoneNumber('07.81.34.06.44')
            ->setSlug('gwenaelle-brassart')
            ->setAddress('109 rue de la Louvetière 77570 Châeau-Landon')
            ->setShowDescription('A définir');

        $this->entityManager->persist($person);

        //je vérifie qu'un user ne possède pas déjà l'email 'admin@afac-etre.fr'
        $personUser = $this->userRepository->findOneBy(['email' => $person->getEmail()]);
        if ($adminUser) {
            $io->warning("L'email " . $person->getEmail() . " existe déjà en base.");
        } else {
            $personUser = new User();
            $resultManagerUser = $this->userManager->create($resultManagerOrganization["item"],
                $personUser, $person->getFirstname(), $person->getLastname(), $person->getEmail(),
                'test', ['ROLE_PERSON'], true, true);

            if (isset($resultManagerUser["errorMessage"]) && $resultManagerUser["errorMessage"] instanceof Exception) {
                $io->warning($resultManagerUser["errorMessage"]);

            } elseif (isset($resultManagerUser["item"]) && $resultManagerUser["item"] instanceof User) {
                $io->success('Great ! Intervenant user created : ' .
                    'Prénom: ' . $personUser->getFirstname() . ' - ' .
                    'Nom :' . $personUser->getLastname() . ' - ' .
                    'Email :' . $personUser->getEmail());
            }
        }

        //DISCIPLINES
        $disciplines = ['Médiation animale', 'Sport handicap', 'Massage bien-être', 'Sophro psycho', 'Qi gong'];

        foreach ($disciplines as $d) {
            $item = $this->disciplineRepository->findOneBy(['name' => $d]);
            if (!$item) {
                $discipline = new Discipline();
                $discipline->setName($d);
                $discipline->setSlug(Urlizer::urlize($d));
                $discipline->setIsActive(true);

                $this->entityManager->persist($discipline);
                $io->success('Great ! Organization parameter/discipline created : ' . 'Nom: ' . $discipline->getName());
            }
        }

        //TYPES DE RDV
        $eventTypes = [
            ['workflow' => $this->workflowRepository->find(1),
                'type' => 'Séance de sophrologie',
                'shortType' => 'Sophrologie',
                'bgColor' => '#000000', 'ftColor' => '#FFFFFF'],

            ['workflow' => $this->workflowRepository->find(1),
                'type' => 'Séance de massage Parents-bébé',
                'shortType' => 'Massage parents-bébé',
                'bgColor' => '#000000', 'ftColor' => '#FFFFFF'],

            ['workflow' => $this->workflowRepository->find(3),
                'type' => 'Séance de massage Amma-assis',
                'shortType' => 'Massage Amma-assis',
                'bgColor' => '#000000', 'ftColor' => '#FFFFFF'],

            ['workflow' => $this->workflowRepository->find(3),
                'type' => 'Séance Sophro-ballade',
                'shortType' => 'Sophro-ballade',
                'bgColor' => '#000000', 'ftColor' => '#FFFFFF'],

            ['workflow' => $this->workflowRepository->find(2),
                'type' => 'Séance de Formation',
                'shortType' => 'Formation',
                'bgColor' => '#000000', 'ftColor' => '#FFFFFF'],

        ];

        foreach ($eventTypes as $e) {
            $type = new EventType();
            $type->setWorkflow($e['workflow']);
            $type->setType($e['type']);
            $type->setShortType($e['shortType']);
            $type->setEventBgColor($e['bgColor']);
            $type->setEventFtColor($e['ftColor']);
            $this->entityManager->persist($type);
            $io->success('Great ! Event type created : ' . $type->getType());
        }

        //EVENT LOCATIONS
        $eventLocations = [
            ['label' => 'Adresse 1', 'address' => '12 rue de Tulipe 45200 Montargis'],
            ['label' => 'Adresse 2', 'address' => '7 bd de l\'Eglise 45100 Orléans'],
        ];

        foreach ($eventLocations as $e) {
            $location = new EventLocation();
            $location->setLabel($e['label']);
            $location->setAddress($e['address']);
            $this->entityManager->persist($location);
            $io->success('Great ! Event location created : ' . $location->getLabel());
        }

        //TYPES DE PATIENT
        $patientTypes = ['Non défini', 'Adulte', 'Adolescent', 'Enfant'];
        foreach ($patientTypes as $pt) {
            $patientType = new PatientType();
            $patientType->setType($pt);
            $this->entityManager->persist($patientType);
            $this->entityManager->flush();
            $io->success('Great ! Patient type created : ' . $patientType->getType());
        }

        //BAREMES DE TARIF PATIENT
        $adultPatientType = $this->patientTypeRepository->findOneByType('Adulte');
        $adopatientType = $this->patientTypeRepository->findOneByType('Adolescent');
        $enfantPatientType = $this->patientTypeRepository->findOneByType('Enfant');
        $eventScales = [
            ['patientType' => $adultPatientType, 'firstEventPrice' => 6000, 'otherEventPrice' => 5500],
            ['patientType' => $adopatientType, 'firstEventPrice' => 6000, 'otherEventPrice' => 5000],
            ['patientType' => $enfantPatientType, 'firstEventPrice' => 5000, 'otherEventPrice' => 4500],
        ];

        if ($adultPatientType && $adopatientType && $enfantPatientType) {
            foreach ($eventScales as $es) {
                $eventScale = new EventScale();
                $eventScale->setCreatedAt(new \DateTime());
                $eventScale->setPatientType($es['patientType']);
                $eventScale->setFirstEventPrice($es['firstEventPrice']);
                $eventScale->setOtherEventPrice($es['otherEventPrice']);
                $this->entityManager->persist($eventScale);
                $io->success('Great ! Event scale created : ' . $eventScale->getPatientType());
            }
        }

        //CONTACT CLIENT FUNCTIONS
        $functions = ['Président', 'Gérant', 'Responsabe de service'];
        foreach ($functions as $fc) {
            $function = new ClientContactFunction();
            $function->setName($fc);
            $this->entityManager->persist($function);
            $io->success('Great ! Client contact function created : ' . $function->getName());
        }

        //SITUATIONS FAMILIALES
        $familySituations = ['Marié', 'Union libre', 'Divorcé'];
        foreach ($familySituations as $fs) {
            $familySituation = new FamilySituation();
            $familySituation->setSituation($fs);
            $this->entityManager->persist($familySituation);
            $io->success('Great ! Family situation created : ' . $familySituation->getSituation());
        }

        //FISCAL YEARS
        $fiscalYears = [
            ['name' => 'Année 2019', 'startAt' => '2019-01-01', 'endAt' => '2019-12-31'],
            ['name' => 'Année 2020', 'startAt' => '2020-01-01', 'endAt' => '2020-12-31'],
            ['name' => 'Année 2021', 'startAt' => '2021-01-01', 'endAt' => '2021-12-31'],
        ];
        foreach ($fiscalYears as $fy) {
            $fiscalYear = new FiscalYear();
            $fiscalYear->setCreatedAt(new \DateTime());
            $fiscalYear->setName($fy['name']);
            $fiscalYear->setStartAt(new \DateTime($fy['startAt']));
            $fiscalYear->setEndAt(new \DateTime($fy['endAt']));
            $this->entityManager->persist($fiscalYear);
            $io->success('Great ! fiscal year created : ' . $fiscalYear->getName());
        }

        //PAYMENT CONDITIONS
        $paymentConditions = [
            'Paiement à immédiat à réception',
            'Paiement 30 jours fin de mois',
        ];
        foreach ($paymentConditions as $pc) {
            $paymentCondition = new PaymentCondition();
            $paymentCondition->setName($pc);
            $this->entityManager->persist($paymentCondition);
            $io->success('Great ! Payment condition created : ' . $paymentCondition->getName());
        }

        //DOCUMENT TYPES
        $documentTypes = [
            ['type' => 'Fiche du patient', 'isActive' => true],
            ['type' => 'Convention de la formation', 'isActive' => true],
            ['type' => 'Devis validé', 'isActive' => true],
            ['type' => 'Facture validée', 'isActive' => true],
            ['type' => 'Feuille d\'émargement', 'isActive' => true],
            ['type' => 'Attestation de fin de formation', 'isActive' => true],
            ['type' => 'Déroulé pédagogique', 'isActive' => true],
            ['type' => 'Fiche pédagogique', 'isActive' => true],
            ['type' => 'Livret de formation du stagiaire', 'isActive' => true],
        ];
        foreach ($documentTypes as $dt) {
            $documentType = new DocumentType();
            $documentType->setType($dt['type']);
            $documentType->setIsActive($dt['isActive']);
            $this->entityManager->persist($documentType);
            $io->success('Great ! Document type created : ' . $documentType->getType());
        }

        //CHARGES TYPES
        $chargeTypes = [
            ['type' => 'Loyer'],
            ['type' => 'Essence'],
            ['type' => 'Divers'],
        ];
        foreach ($chargeTypes as $ct) {
            $chargeType = new ChargeType();
            $chargeType->setType($ct['type']);
            $this->entityManager->persist($chargeType);
            $io->success('Great ! Charge type created : ' . $chargeType->getType());
        }

        //PAYMENT TYPES
        $paymentTypes = [
            ['type' => 'Loyer'],
            ['type' => 'Essence'],
            ['type' => 'Divers'],
        ];
        foreach ($paymentTypes as $pt) {
            $paymentType = new PaymentType();
            $paymentType->setType($pt['type']);
            $this->entityManager->persist($paymentType);
            $io->success('Great ! Payment type created : ' . $paymentType->getType());
        }

        //CITATIONS
        $quotations = [
            ['author' => 'Nelson Mandela, 1918-2013',
                'content' => 'Être libre, ce n\'est pas seulement se débarrasser de ses chaînes ; c\'est vivre d\'une façon qui respecte et renforce la liberté des autres.'],

            ['author' => 'Nelson Mandela, 1918-2013',
                'content' => 'J\'ai appris que le courage n\'est pas l\'absence de peur, mais la capacité de la vaincre.'],

            ['author' => 'Nelson Mandela, 1918-2013',
                'content' => 'Cela semble toujours impossible, jusqu\'à ce qu\'on le fasse.'],

            ['author' => 'Charles Caleb Colton',
                'content' => 'Le moment présent a un avantage sur tous les autres: il nous appartient.'],

            ['author' => 'Confucius',
                'content' => 'Tous les hommes pensent que le bonheur se trouve au sommet de la montagne alors qu’il réside dans la façon de la gravir.'],

            ['author' => 'Gandhi',
                'content' => 'Le secret du bonheur c’est l’alignement entre ce que vous pensez, ce que vous dites et ce que vous faites.'],

            ['author' => 'Jacques Salomé',
                'content' => 'La porte du changement ne peut s’ouvrir que de l’intérieur.'],
        ];

        foreach ($quotations as $q) {
            $quotation = new Quotation();
            $quotation->setAuthor($q['author']);
            $quotation->setContent($q['content']);
            $quotation->setIsActive(true);

            $this->entityManager->persist($quotation);
            $io->success('Great ! Quotation created : ' . 'Text: ' . $quotation->getContent());
        }

        //PAGES
        $pages = [
            ['title' => 'Politique de confidentialité',
                'content' => '<h1 style="color:blue">Politique de confidentialité</h1>'],

            ['title' => 'Mentions légales',
                'content' => '<h1 style="color:blue">Mentions légales</h1>'],
        ];

        foreach ($pages as $p) {
            $item = $this->pageRepository->findOneBy(['title' => $p["title"]]);
            if (!$item) {
                $page = new Page();
                $page->setTitle($p['title']);
                $page->setContent($p['content']);

                $this->entityManager->persist($page);
                $io->success('Great ! Organization parameter/page created : ' . 'Nom: ' . $page->getTitle());
            }
        }

        $this->entityManager->flush();
        return Command::SUCCESS;
    }
}
