<?php

namespace App\Command;

use App\Entity\Adherent;
use App\Entity\CalendarParameter;
use App\Entity\Category;
use App\Entity\ChargeType;
use App\Entity\Client;
use App\Entity\ClientContact;
use App\Entity\ClientContactFunction;
use App\Entity\Contract;
use App\Entity\Discipline;
use App\Entity\DocumentType;
use App\Entity\EventLocation;
use App\Entity\EventType;
use App\Entity\FamilySituation;
use App\Entity\FiscalYear;
use App\Entity\Organization;
use App\Entity\Page;
use App\Entity\PatientType;
use App\Entity\PaymentCondition;
use App\Entity\PaymentType;
use App\Entity\Product;
use App\Entity\Quotation;
use App\Entity\User;
use App\Manager\CalendarParameterManager;
use App\Manager\OrganizationManager;
use App\Manager\UserManager;
use App\Repository\AdherentRepository;
use App\Repository\ClientContactFunctionRepository;
use App\Repository\DisciplineRepository;
use App\Repository\DocumentRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PageRepository;
use App\Repository\ProductRepository;
use App\Repository\QuotationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryRepository;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * CREATION DE LA CLIENTELE
 * Class PatientCommand
 * @package App\Command
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ClientCommand extends Command
{
    /*
     * Création:
     *  - de la base des patients
     *  - historique d'activité cabinet
     */

    protected static $defaultName = 'app:client';
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * ClientCommand constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }

    protected function configure()
    {
        $this->setDescription('Application clients initialization');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        //TODO: ... Reprise des clients institutions médico-sociales

        
        $this->manager->flush();
        return Command::SUCCESS;
    }
}
