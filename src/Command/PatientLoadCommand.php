<?php

namespace App\Command;

use App\Entity\Invoice;
use App\Entity\Patient;
use App\Manager\EventManager;
use App\Manager\InvoiceManager;
use App\Manager\PatientManager;
use App\Repository\EventTypeRepository;
use App\Repository\FamilySituationRepository;
use App\Repository\FiscalYearRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PatientTypeRepository;
use App\Repository\PersonRepository;
use App\Services\CsvService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * UPLOAD PATIENT LIST
 * Class PatientLoadCommand
 * @package App\Command
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PatientLoadCommand extends Command
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var PatientManager
     */
    private $patientManager;
    /**
     * @var PatientTypeRepository
     */
    private $patientTypeRepository;
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    /**
     * PatientCommand constructor.
     * @param EntityManagerInterface $manager
     * @param ContainerInterface $container
     * @param PatientManager $patientManager
     * @param OrganizationRepository $organizationRepository
     * @param PatientTypeRepository $patientTypeRepository
     */
    public function __construct(EntityManagerInterface $manager, ContainerInterface $container, PatientManager $patientManager,
                                OrganizationRepository $organizationRepository, PatientTypeRepository $patientTypeRepository)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->container = $container;
        $this->patientManager = $patientManager;
        $this->patientTypeRepository = $patientTypeRepository;
        $this->organizationRepository = $organizationRepository;
    }

    protected static $defaultName = 'app:importPatient';

    protected function configure()
    {
        $this->setDescription('Application patients load initialization');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('**********************************************************************************');
        $io->writeln('***** Chargement des patients');
        $io->writeln('**********************************************************************************');

        $path = $this->container->getParameter('app.cmd.resource_patient');
        $data = [];
        $csv_path = file($path . '210118_patients.csv');

        $keys = str_getcsv(array_shift($csv_path), ';');
        foreach ($csv_path as $csvRecord) {
            // combine our $csvRecord with $keys array
            $data[] = array_combine($keys, str_getcsv($csvRecord, ';'));
        }

        $organization = $this->organizationRepository->find(1);
        foreach ($data as $enrgt) {

            $patientType = null;
            if ($enrgt['birthdate'] != "") {
                $age = (int) $this->getAge($enrgt['birthdate']);

                switch ($age) {
                    case $age >= 5 and $age <= 11:
                        $patientType = $this->patientTypeRepository->find(4);
                        break;
                    case $age >= 12 and $age <= 19:
                        $patientType = $this->patientTypeRepository->find(3);
                        break;
                    case $age >= 20:
                        $patientType = $this->patientTypeRepository->find(2);
                        break;
                    default:
                        $patientType = $this->patientTypeRepository->find(1);
                }

                /*
                   public function create(Organization $organization, PatientType $patientType, string $civility = Patient::CIVILITY_MISTER,
                           string $firstname, string $lastname, string $street, string $zipcode, string $city, string $country, string $phoneNumber,
                           string $email, \DateTime $birthdate, FamilySituation $familySituation)
                 */

                $patient = $this->patientManager->create($organization, $patientType, $enrgt['civility'], $enrgt['firstname'], $enrgt['lastname'],
                    $enrgt['street'],$enrgt['zipcode'], $enrgt['city'], 'FR', $enrgt['phoneNumber'], $enrgt['email'], new \DateTime($enrgt['birthdate']),null);
            }

            /** @var Patient['item'] $patient */
            $io->success('Great ! Patient loaded: '.$patient['item']->getFirstname().' '.$patient['item']->getLastname());
        }


        return Command::SUCCESS;
    }

    public function getAge(string $birthdate):string
    {
        $today = date("Y-m-d");
        $datetime1 = new \DateTime($today);
        $datetime2 = new \DateTime($birthdate);
        $age = $datetime1->diff($datetime2);
        return $age->format('%y');
    }

}
