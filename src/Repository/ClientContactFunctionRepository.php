<?php

namespace App\Repository;

use App\Entity\ClientContactFunction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientContactFunction|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientContactFunction|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientContactFunction[]    findAll()
 * @method ClientContactFunction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientContactFunctionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientContactFunction::class);
    }

    // /**
    //  * @return ClientContactFunction[] Returns an array of ClientContactFunction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientContactFunction
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
