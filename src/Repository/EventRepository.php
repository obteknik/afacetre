<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\Search\EventSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @return QueryBuilder
     */
    public function findQb(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('e');
        return $qb;
    }

    /**
     * @param EventSearch $search
     * @return array
     */
    public function findAllQuery(EventSearch $search): array
    {
        $query = $this->findQb();

        //*** CONDITIONS ***
        //période de création
        if ($search->getMinCreatedAt() && $search->getMaxCreatedAt()) {
            $minCreatedAt = $search->getMinCreatedAt() ->format('Y-m-d') . " 00:00:00";
            $maxCreatedAt = $search->getMaxCreatedAt()->format('Y-m-d') . " 23:59:59";

            $query = $query->andWhere('e.createdAt BETWEEN :minCreatedAt AND :maxCreatedAt')
                ->setParameter('minCreatedAt', $minCreatedAt)
                ->setParameter('maxCreatedAt', $maxCreatedAt);
        }

        //User
        if ($search->getConnectedUser()){
            $query = $query->andWhere('e.user = :connectedUser')
                ->setParameter('connectedUser', $search->getConnectedUser());
        }

        //retour
        return $query->getQuery()->getResult();
    }

//    /**
//     * @param array $params
//     * @param bool $queryBuilder
//     * @param array $order
//     * @return mixed
//     */
//    public function search(array $params = [], bool $queryBuilder = true, array $order = ['id' => 'asc'])
//    {
//
//        $qb = $this->createQueryBuilder('e');
//
//        //FILTRES
//        if(isset($params['organization']) && $params['organization'] instanceof Organization){
//            $qb->innerJoin('e.organization','organization');
//            $qb->andWhere('organization.id =:organization')->setParameter('organization', $params['organization']);
//        }
//
//        //TRI
//        $qb->orderBy('e.'.array_keys($order)[0], array_values($order)[0]);
//
//        return $queryBuilder ? $qb : $qb->getQuery()->getResult();
//    }

}
