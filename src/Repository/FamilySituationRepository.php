<?php

namespace App\Repository;

use App\Entity\FamilySituation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FamilySituation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilySituation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilySituation[]    findAll()
 * @method FamilySituation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilySituationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FamilySituation::class);
    }

    // /**
    //  * @return FamilySituation[] Returns an array of FamilySituation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FamilySituation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
