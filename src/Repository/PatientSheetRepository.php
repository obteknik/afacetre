<?php

namespace App\Repository;

use App\Entity\PatientSheet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatientSheet|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatientSheet|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatientSheet[]    findAll()
 * @method PatientSheet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientSheetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatientSheet::class);
    }

    // /**
    //  * @return PatientSheet[] Returns an array of PatientSheet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PatientSheet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
