<?php

namespace App\Repository;

use App\Entity\EventObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventObject[]    findAll()
 * @method EventObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventObjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventObject::class);
    }

    // /**
    //  * @return EventObject[] Returns an array of EventObject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventObject
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
