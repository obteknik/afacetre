<?php

namespace App\Repository;

use App\Entity\ProductDay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductDay[]    findAll()
 * @method ProductDay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductDayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductDay::class);
    }

    // /**
    //  * @return ProductDay[] Returns an array of ProductDay objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductDay
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
