<?php

namespace App\Repository;

use App\Entity\EventScale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventScale|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventScale|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventScale[]    findAll()
 * @method EventScale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventScaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventScale::class);
    }

    // /**
    //  * @return EventScale[] Returns an array of EventScale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventScale
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
