<?php

namespace App\Repository;

use App\Entity\AdherentEvents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdherentEvents|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdherentEvents|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdherentEvents[]    findAll()
 * @method AdherentEvents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdherentEventsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdherentEvents::class);
    }

    // /**
    //  * @return AdherentEvents[] Returns an array of AdherentEvents objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdherentEvents
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
