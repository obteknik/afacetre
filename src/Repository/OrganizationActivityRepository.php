<?php

namespace App\Repository;

use App\Entity\OrganizationActivity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationActivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationActivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationActivity[]    findAll()
 * @method OrganizationActivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationActivity::class);
    }

    // /**
    //  * @return OrganizationActivity[] Returns an array of OrganizationActivity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationActivity
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
