<?php

namespace App\Repository;

use App\Entity\CalendarParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CalendarParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method CalendarParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method CalendarParameter[]    findAll()
 * @method CalendarParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalendarParameterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CalendarParameter::class);
    }

    // /**
    //  * @return CalendarParameter[] Returns an array of CalendarParameter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CalendarParameter
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
