<?php

namespace App\Repository;

use App\Entity\QuoteDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuoteDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuoteDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuoteDetail[]    findAll()
 * @method QuoteDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuoteDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuoteDetail::class);
    }

    // /**
    //  * @return QuoteDetail[] Returns an array of QuoteDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuoteDetail
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
