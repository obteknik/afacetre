<?php

namespace App\Repository;

use App\Entity\PaymentCondition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaymentCondition|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentCondition|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentCondition[]    findAll()
 * @method PaymentCondition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentConditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentCondition::class);
    }

    // /**
    //  * @return PaymentCondition[] Returns an array of PaymentCondition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentCondition
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
